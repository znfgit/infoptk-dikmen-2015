<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Errortype;
use angulex\Model\ErrortypePeer;
use angulex\Model\ErrortypeQuery;
use angulex\Model\VldAnak;
use angulex\Model\VldAnakQuery;
use angulex\Model\VldBeaPd;
use angulex\Model\VldBeaPdQuery;
use angulex\Model\VldBeaPtk;
use angulex\Model\VldBeaPtkQuery;
use angulex\Model\VldBukuPtk;
use angulex\Model\VldBukuPtkQuery;
use angulex\Model\VldDemografi;
use angulex\Model\VldDemografiQuery;
use angulex\Model\VldInpassing;
use angulex\Model\VldInpassingQuery;
use angulex\Model\VldJurusanSp;
use angulex\Model\VldJurusanSpQuery;
use angulex\Model\VldKaryaTulis;
use angulex\Model\VldKaryaTulisQuery;
use angulex\Model\VldKesejahteraan;
use angulex\Model\VldKesejahteraanQuery;
use angulex\Model\VldMou;
use angulex\Model\VldMouQuery;
use angulex\Model\VldNilaiRapor;
use angulex\Model\VldNilaiRaporQuery;
use angulex\Model\VldNilaiTest;
use angulex\Model\VldNilaiTestQuery;
use angulex\Model\VldNonsekolah;
use angulex\Model\VldNonsekolahQuery;
use angulex\Model\VldPdLong;
use angulex\Model\VldPdLongQuery;
use angulex\Model\VldPembelajaran;
use angulex\Model\VldPembelajaranQuery;
use angulex\Model\VldPenghargaan;
use angulex\Model\VldPenghargaanQuery;
use angulex\Model\VldPesertaDidik;
use angulex\Model\VldPesertaDidikQuery;
use angulex\Model\VldPrasarana;
use angulex\Model\VldPrasaranaQuery;
use angulex\Model\VldPrestasi;
use angulex\Model\VldPrestasiQuery;
use angulex\Model\VldPtk;
use angulex\Model\VldPtkQuery;
use angulex\Model\VldRombel;
use angulex\Model\VldRombelQuery;
use angulex\Model\VldRwyFungsional;
use angulex\Model\VldRwyFungsionalQuery;
use angulex\Model\VldRwyKepangkatan;
use angulex\Model\VldRwyKepangkatanQuery;
use angulex\Model\VldRwyPendFormal;
use angulex\Model\VldRwyPendFormalQuery;
use angulex\Model\VldRwySertifikasi;
use angulex\Model\VldRwySertifikasiQuery;
use angulex\Model\VldRwyStruktural;
use angulex\Model\VldRwyStrukturalQuery;
use angulex\Model\VldSarana;
use angulex\Model\VldSaranaQuery;
use angulex\Model\VldSekolah;
use angulex\Model\VldSekolahQuery;
use angulex\Model\VldTugasTambahan;
use angulex\Model\VldTugasTambahanQuery;
use angulex\Model\VldTunjangan;
use angulex\Model\VldTunjanganQuery;
use angulex\Model\VldUn;
use angulex\Model\VldUnQuery;
use angulex\Model\VldYayasan;
use angulex\Model\VldYayasanQuery;

/**
 * Base class that represents a row from the 'ref.errortype' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseErrortype extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\ErrortypePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ErrortypePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the idtype field.
     * @var        int
     */
    protected $idtype;

    /**
     * The value for the kategori_error field.
     * @var        int
     */
    protected $kategori_error;

    /**
     * The value for the keterangan field.
     * @var        string
     */
    protected $keterangan;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        PropelObjectCollection|VldRwyKepangkatan[] Collection to store aggregation of VldRwyKepangkatan objects.
     */
    protected $collVldRwyKepangkatansRelatedByIdtype;
    protected $collVldRwyKepangkatansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwyKepangkatan[] Collection to store aggregation of VldRwyKepangkatan objects.
     */
    protected $collVldRwyKepangkatansRelatedByIdtype;
    protected $collVldRwyKepangkatansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwyFungsional[] Collection to store aggregation of VldRwyFungsional objects.
     */
    protected $collVldRwyFungsionalsRelatedByIdtype;
    protected $collVldRwyFungsionalsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwyFungsional[] Collection to store aggregation of VldRwyFungsional objects.
     */
    protected $collVldRwyFungsionalsRelatedByIdtype;
    protected $collVldRwyFungsionalsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRombel[] Collection to store aggregation of VldRombel objects.
     */
    protected $collVldRombelsRelatedByIdtype;
    protected $collVldRombelsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRombel[] Collection to store aggregation of VldRombel objects.
     */
    protected $collVldRombelsRelatedByIdtype;
    protected $collVldRombelsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPtk[] Collection to store aggregation of VldPtk objects.
     */
    protected $collVldPtksRelatedByIdtype;
    protected $collVldPtksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPtk[] Collection to store aggregation of VldPtk objects.
     */
    protected $collVldPtksRelatedByIdtype;
    protected $collVldPtksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPrestasi[] Collection to store aggregation of VldPrestasi objects.
     */
    protected $collVldPrestasisRelatedByIdtype;
    protected $collVldPrestasisRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPrestasi[] Collection to store aggregation of VldPrestasi objects.
     */
    protected $collVldPrestasisRelatedByIdtype;
    protected $collVldPrestasisRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPrasarana[] Collection to store aggregation of VldPrasarana objects.
     */
    protected $collVldPrasaranasRelatedByIdtype;
    protected $collVldPrasaranasRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPrasarana[] Collection to store aggregation of VldPrasarana objects.
     */
    protected $collVldPrasaranasRelatedByIdtype;
    protected $collVldPrasaranasRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPesertaDidik[] Collection to store aggregation of VldPesertaDidik objects.
     */
    protected $collVldPesertaDidiksRelatedByIdtype;
    protected $collVldPesertaDidiksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPesertaDidik[] Collection to store aggregation of VldPesertaDidik objects.
     */
    protected $collVldPesertaDidiksRelatedByIdtype;
    protected $collVldPesertaDidiksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPenghargaan[] Collection to store aggregation of VldPenghargaan objects.
     */
    protected $collVldPenghargaansRelatedByIdtype;
    protected $collVldPenghargaansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPenghargaan[] Collection to store aggregation of VldPenghargaan objects.
     */
    protected $collVldPenghargaansRelatedByIdtype;
    protected $collVldPenghargaansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPembelajaran[] Collection to store aggregation of VldPembelajaran objects.
     */
    protected $collVldPembelajaransRelatedByIdtype;
    protected $collVldPembelajaransRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPembelajaran[] Collection to store aggregation of VldPembelajaran objects.
     */
    protected $collVldPembelajaransRelatedByIdtype;
    protected $collVldPembelajaransRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPdLong[] Collection to store aggregation of VldPdLong objects.
     */
    protected $collVldPdLongsRelatedByIdtype;
    protected $collVldPdLongsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldPdLong[] Collection to store aggregation of VldPdLong objects.
     */
    protected $collVldPdLongsRelatedByIdtype;
    protected $collVldPdLongsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldNonsekolah[] Collection to store aggregation of VldNonsekolah objects.
     */
    protected $collVldNonsekolahsRelatedByIdtype;
    protected $collVldNonsekolahsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldNonsekolah[] Collection to store aggregation of VldNonsekolah objects.
     */
    protected $collVldNonsekolahsRelatedByIdtype;
    protected $collVldNonsekolahsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldNilaiTest[] Collection to store aggregation of VldNilaiTest objects.
     */
    protected $collVldNilaiTestsRelatedByIdtype;
    protected $collVldNilaiTestsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldNilaiTest[] Collection to store aggregation of VldNilaiTest objects.
     */
    protected $collVldNilaiTestsRelatedByIdtype;
    protected $collVldNilaiTestsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldNilaiRapor[] Collection to store aggregation of VldNilaiRapor objects.
     */
    protected $collVldNilaiRaporsRelatedByIdtype;
    protected $collVldNilaiRaporsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldNilaiRapor[] Collection to store aggregation of VldNilaiRapor objects.
     */
    protected $collVldNilaiRaporsRelatedByIdtype;
    protected $collVldNilaiRaporsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldMou[] Collection to store aggregation of VldMou objects.
     */
    protected $collVldMousRelatedByIdtype;
    protected $collVldMousRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldMou[] Collection to store aggregation of VldMou objects.
     */
    protected $collVldMousRelatedByIdtype;
    protected $collVldMousRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldKesejahteraan[] Collection to store aggregation of VldKesejahteraan objects.
     */
    protected $collVldKesejahteraansRelatedByIdtype;
    protected $collVldKesejahteraansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldKesejahteraan[] Collection to store aggregation of VldKesejahteraan objects.
     */
    protected $collVldKesejahteraansRelatedByIdtype;
    protected $collVldKesejahteraansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldKaryaTulis[] Collection to store aggregation of VldKaryaTulis objects.
     */
    protected $collVldKaryaTulissRelatedByIdtype;
    protected $collVldKaryaTulissRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldKaryaTulis[] Collection to store aggregation of VldKaryaTulis objects.
     */
    protected $collVldKaryaTulissRelatedByIdtype;
    protected $collVldKaryaTulissRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldJurusanSp[] Collection to store aggregation of VldJurusanSp objects.
     */
    protected $collVldJurusanSpsRelatedByIdtype;
    protected $collVldJurusanSpsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldJurusanSp[] Collection to store aggregation of VldJurusanSp objects.
     */
    protected $collVldJurusanSpsRelatedByIdtype;
    protected $collVldJurusanSpsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldInpassing[] Collection to store aggregation of VldInpassing objects.
     */
    protected $collVldInpassingsRelatedByIdtype;
    protected $collVldInpassingsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldInpassing[] Collection to store aggregation of VldInpassing objects.
     */
    protected $collVldInpassingsRelatedByIdtype;
    protected $collVldInpassingsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldDemografi[] Collection to store aggregation of VldDemografi objects.
     */
    protected $collVldDemografisRelatedByIdtype;
    protected $collVldDemografisRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldDemografi[] Collection to store aggregation of VldDemografi objects.
     */
    protected $collVldDemografisRelatedByIdtype;
    protected $collVldDemografisRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldBukuPtk[] Collection to store aggregation of VldBukuPtk objects.
     */
    protected $collVldBukuPtksRelatedByIdtype;
    protected $collVldBukuPtksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldBukuPtk[] Collection to store aggregation of VldBukuPtk objects.
     */
    protected $collVldBukuPtksRelatedByIdtype;
    protected $collVldBukuPtksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldBeaPtk[] Collection to store aggregation of VldBeaPtk objects.
     */
    protected $collVldBeaPtksRelatedByIdtype;
    protected $collVldBeaPtksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldBeaPtk[] Collection to store aggregation of VldBeaPtk objects.
     */
    protected $collVldBeaPtksRelatedByIdtype;
    protected $collVldBeaPtksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldBeaPd[] Collection to store aggregation of VldBeaPd objects.
     */
    protected $collVldBeaPdsRelatedByIdtype;
    protected $collVldBeaPdsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldBeaPd[] Collection to store aggregation of VldBeaPd objects.
     */
    protected $collVldBeaPdsRelatedByIdtype;
    protected $collVldBeaPdsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldAnak[] Collection to store aggregation of VldAnak objects.
     */
    protected $collVldAnaksRelatedByIdtype;
    protected $collVldAnaksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldAnak[] Collection to store aggregation of VldAnak objects.
     */
    protected $collVldAnaksRelatedByIdtype;
    protected $collVldAnaksRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldYayasan[] Collection to store aggregation of VldYayasan objects.
     */
    protected $collVldYayasansRelatedByIdtype;
    protected $collVldYayasansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldYayasan[] Collection to store aggregation of VldYayasan objects.
     */
    protected $collVldYayasansRelatedByIdtype;
    protected $collVldYayasansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldUn[] Collection to store aggregation of VldUn objects.
     */
    protected $collVldUnsRelatedByIdtype;
    protected $collVldUnsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldUn[] Collection to store aggregation of VldUn objects.
     */
    protected $collVldUnsRelatedByIdtype;
    protected $collVldUnsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldTunjangan[] Collection to store aggregation of VldTunjangan objects.
     */
    protected $collVldTunjangansRelatedByIdtype;
    protected $collVldTunjangansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldTunjangan[] Collection to store aggregation of VldTunjangan objects.
     */
    protected $collVldTunjangansRelatedByIdtype;
    protected $collVldTunjangansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldTugasTambahan[] Collection to store aggregation of VldTugasTambahan objects.
     */
    protected $collVldTugasTambahansRelatedByIdtype;
    protected $collVldTugasTambahansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldTugasTambahan[] Collection to store aggregation of VldTugasTambahan objects.
     */
    protected $collVldTugasTambahansRelatedByIdtype;
    protected $collVldTugasTambahansRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldSekolah[] Collection to store aggregation of VldSekolah objects.
     */
    protected $collVldSekolahsRelatedByIdtype;
    protected $collVldSekolahsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldSekolah[] Collection to store aggregation of VldSekolah objects.
     */
    protected $collVldSekolahsRelatedByIdtype;
    protected $collVldSekolahsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldSarana[] Collection to store aggregation of VldSarana objects.
     */
    protected $collVldSaranasRelatedByIdtype;
    protected $collVldSaranasRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldSarana[] Collection to store aggregation of VldSarana objects.
     */
    protected $collVldSaranasRelatedByIdtype;
    protected $collVldSaranasRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwyStruktural[] Collection to store aggregation of VldRwyStruktural objects.
     */
    protected $collVldRwyStrukturalsRelatedByIdtype;
    protected $collVldRwyStrukturalsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwyStruktural[] Collection to store aggregation of VldRwyStruktural objects.
     */
    protected $collVldRwyStrukturalsRelatedByIdtype;
    protected $collVldRwyStrukturalsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwySertifikasi[] Collection to store aggregation of VldRwySertifikasi objects.
     */
    protected $collVldRwySertifikasisRelatedByIdtype;
    protected $collVldRwySertifikasisRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwySertifikasi[] Collection to store aggregation of VldRwySertifikasi objects.
     */
    protected $collVldRwySertifikasisRelatedByIdtype;
    protected $collVldRwySertifikasisRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwyPendFormal[] Collection to store aggregation of VldRwyPendFormal objects.
     */
    protected $collVldRwyPendFormalsRelatedByIdtype;
    protected $collVldRwyPendFormalsRelatedByIdtypePartial;

    /**
     * @var        PropelObjectCollection|VldRwyPendFormal[] Collection to store aggregation of VldRwyPendFormal objects.
     */
    protected $collVldRwyPendFormalsRelatedByIdtype;
    protected $collVldRwyPendFormalsRelatedByIdtypePartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRombelsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRombelsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPtksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPtksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPrestasisRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPrestasisRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPrasaranasRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPrasaranasRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPesertaDidiksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPesertaDidiksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPenghargaansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPenghargaansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPembelajaransRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPembelajaransRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPdLongsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPdLongsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldNonsekolahsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldNonsekolahsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldNilaiTestsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldNilaiTestsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldNilaiRaporsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldNilaiRaporsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldMousRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldMousRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldKesejahteraansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldKesejahteraansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldKaryaTulissRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldKaryaTulissRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldJurusanSpsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldJurusanSpsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldInpassingsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldInpassingsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldDemografisRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldDemografisRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldBukuPtksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldBukuPtksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldBeaPtksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldBeaPtksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldBeaPdsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldBeaPdsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldAnaksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldAnaksRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldYayasansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldYayasansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldUnsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldUnsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldTunjangansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldTunjangansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldTugasTambahansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldTugasTambahansRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldSekolahsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldSekolahsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldSaranasRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldSaranasRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwySertifikasisRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwySertifikasisRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion = null;

    /**
     * Get the [idtype] column value.
     * 
     * @return int
     */
    public function getIdtype()
    {
        return $this->idtype;
    }

    /**
     * Get the [kategori_error] column value.
     * 
     * @return int
     */
    public function getKategoriError()
    {
        return $this->kategori_error;
    }

    /**
     * Get the [keterangan] column value.
     * 
     * @return string
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [idtype] column.
     * 
     * @param int $v new value
     * @return Errortype The current object (for fluent API support)
     */
    public function setIdtype($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->idtype !== $v) {
            $this->idtype = $v;
            $this->modifiedColumns[] = ErrortypePeer::IDTYPE;
        }


        return $this;
    } // setIdtype()

    /**
     * Set the value of [kategori_error] column.
     * 
     * @param int $v new value
     * @return Errortype The current object (for fluent API support)
     */
    public function setKategoriError($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kategori_error !== $v) {
            $this->kategori_error = $v;
            $this->modifiedColumns[] = ErrortypePeer::KATEGORI_ERROR;
        }


        return $this;
    } // setKategoriError()

    /**
     * Set the value of [keterangan] column.
     * 
     * @param string $v new value
     * @return Errortype The current object (for fluent API support)
     */
    public function setKeterangan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->keterangan !== $v) {
            $this->keterangan = $v;
            $this->modifiedColumns[] = ErrortypePeer::KETERANGAN;
        }


        return $this;
    } // setKeterangan()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Errortype The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = ErrortypePeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Errortype The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = ErrortypePeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Errortype The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = ErrortypePeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Errortype The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = ErrortypePeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->idtype = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->kategori_error = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->keterangan = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->create_date = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->last_update = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->expired_date = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->last_sync = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 7; // 7 = ErrortypePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Errortype object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ErrortypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ErrortypePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collVldRwyKepangkatansRelatedByIdtype = null;

            $this->collVldRwyKepangkatansRelatedByIdtype = null;

            $this->collVldRwyFungsionalsRelatedByIdtype = null;

            $this->collVldRwyFungsionalsRelatedByIdtype = null;

            $this->collVldRombelsRelatedByIdtype = null;

            $this->collVldRombelsRelatedByIdtype = null;

            $this->collVldPtksRelatedByIdtype = null;

            $this->collVldPtksRelatedByIdtype = null;

            $this->collVldPrestasisRelatedByIdtype = null;

            $this->collVldPrestasisRelatedByIdtype = null;

            $this->collVldPrasaranasRelatedByIdtype = null;

            $this->collVldPrasaranasRelatedByIdtype = null;

            $this->collVldPesertaDidiksRelatedByIdtype = null;

            $this->collVldPesertaDidiksRelatedByIdtype = null;

            $this->collVldPenghargaansRelatedByIdtype = null;

            $this->collVldPenghargaansRelatedByIdtype = null;

            $this->collVldPembelajaransRelatedByIdtype = null;

            $this->collVldPembelajaransRelatedByIdtype = null;

            $this->collVldPdLongsRelatedByIdtype = null;

            $this->collVldPdLongsRelatedByIdtype = null;

            $this->collVldNonsekolahsRelatedByIdtype = null;

            $this->collVldNonsekolahsRelatedByIdtype = null;

            $this->collVldNilaiTestsRelatedByIdtype = null;

            $this->collVldNilaiTestsRelatedByIdtype = null;

            $this->collVldNilaiRaporsRelatedByIdtype = null;

            $this->collVldNilaiRaporsRelatedByIdtype = null;

            $this->collVldMousRelatedByIdtype = null;

            $this->collVldMousRelatedByIdtype = null;

            $this->collVldKesejahteraansRelatedByIdtype = null;

            $this->collVldKesejahteraansRelatedByIdtype = null;

            $this->collVldKaryaTulissRelatedByIdtype = null;

            $this->collVldKaryaTulissRelatedByIdtype = null;

            $this->collVldJurusanSpsRelatedByIdtype = null;

            $this->collVldJurusanSpsRelatedByIdtype = null;

            $this->collVldInpassingsRelatedByIdtype = null;

            $this->collVldInpassingsRelatedByIdtype = null;

            $this->collVldDemografisRelatedByIdtype = null;

            $this->collVldDemografisRelatedByIdtype = null;

            $this->collVldBukuPtksRelatedByIdtype = null;

            $this->collVldBukuPtksRelatedByIdtype = null;

            $this->collVldBeaPtksRelatedByIdtype = null;

            $this->collVldBeaPtksRelatedByIdtype = null;

            $this->collVldBeaPdsRelatedByIdtype = null;

            $this->collVldBeaPdsRelatedByIdtype = null;

            $this->collVldAnaksRelatedByIdtype = null;

            $this->collVldAnaksRelatedByIdtype = null;

            $this->collVldYayasansRelatedByIdtype = null;

            $this->collVldYayasansRelatedByIdtype = null;

            $this->collVldUnsRelatedByIdtype = null;

            $this->collVldUnsRelatedByIdtype = null;

            $this->collVldTunjangansRelatedByIdtype = null;

            $this->collVldTunjangansRelatedByIdtype = null;

            $this->collVldTugasTambahansRelatedByIdtype = null;

            $this->collVldTugasTambahansRelatedByIdtype = null;

            $this->collVldSekolahsRelatedByIdtype = null;

            $this->collVldSekolahsRelatedByIdtype = null;

            $this->collVldSaranasRelatedByIdtype = null;

            $this->collVldSaranasRelatedByIdtype = null;

            $this->collVldRwyStrukturalsRelatedByIdtype = null;

            $this->collVldRwyStrukturalsRelatedByIdtype = null;

            $this->collVldRwySertifikasisRelatedByIdtype = null;

            $this->collVldRwySertifikasisRelatedByIdtype = null;

            $this->collVldRwyPendFormalsRelatedByIdtype = null;

            $this->collVldRwyPendFormalsRelatedByIdtype = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ErrortypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ErrortypeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ErrortypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ErrortypePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwyKepangkatanQuery::create()
                        ->filterByPrimaryKeys($this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwyKepangkatansRelatedByIdtype !== null) {
                foreach ($this->collVldRwyKepangkatansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwyKepangkatanQuery::create()
                        ->filterByPrimaryKeys($this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwyKepangkatansRelatedByIdtype !== null) {
                foreach ($this->collVldRwyKepangkatansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwyFungsionalQuery::create()
                        ->filterByPrimaryKeys($this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwyFungsionalsRelatedByIdtype !== null) {
                foreach ($this->collVldRwyFungsionalsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwyFungsionalQuery::create()
                        ->filterByPrimaryKeys($this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwyFungsionalsRelatedByIdtype !== null) {
                foreach ($this->collVldRwyFungsionalsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRombelsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRombelsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRombelQuery::create()
                        ->filterByPrimaryKeys($this->vldRombelsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRombelsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRombelsRelatedByIdtype !== null) {
                foreach ($this->collVldRombelsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRombelsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRombelsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRombelQuery::create()
                        ->filterByPrimaryKeys($this->vldRombelsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRombelsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRombelsRelatedByIdtype !== null) {
                foreach ($this->collVldRombelsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPtksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPtksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldPtksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPtksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPtksRelatedByIdtype !== null) {
                foreach ($this->collVldPtksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPtksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPtksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldPtksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPtksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPtksRelatedByIdtype !== null) {
                foreach ($this->collVldPtksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPrestasisRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPrestasisRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPrestasiQuery::create()
                        ->filterByPrimaryKeys($this->vldPrestasisRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPrestasisRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPrestasisRelatedByIdtype !== null) {
                foreach ($this->collVldPrestasisRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPrestasisRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPrestasisRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPrestasiQuery::create()
                        ->filterByPrimaryKeys($this->vldPrestasisRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPrestasisRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPrestasisRelatedByIdtype !== null) {
                foreach ($this->collVldPrestasisRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPrasaranasRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPrasaranasRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPrasaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldPrasaranasRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPrasaranasRelatedByIdtype !== null) {
                foreach ($this->collVldPrasaranasRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPrasaranasRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPrasaranasRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPrasaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldPrasaranasRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPrasaranasRelatedByIdtype !== null) {
                foreach ($this->collVldPrasaranasRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPesertaDidiksRelatedByIdtype !== null) {
                foreach ($this->collVldPesertaDidiksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPesertaDidikQuery::create()
                        ->filterByPrimaryKeys($this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPesertaDidiksRelatedByIdtype !== null) {
                foreach ($this->collVldPesertaDidiksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPenghargaansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPenghargaansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPenghargaanQuery::create()
                        ->filterByPrimaryKeys($this->vldPenghargaansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPenghargaansRelatedByIdtype !== null) {
                foreach ($this->collVldPenghargaansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPenghargaansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPenghargaansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPenghargaanQuery::create()
                        ->filterByPrimaryKeys($this->vldPenghargaansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPenghargaansRelatedByIdtype !== null) {
                foreach ($this->collVldPenghargaansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPembelajaransRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPembelajaransRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->vldPembelajaransRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPembelajaransRelatedByIdtype !== null) {
                foreach ($this->collVldPembelajaransRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPembelajaransRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPembelajaransRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->vldPembelajaransRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPembelajaransRelatedByIdtype !== null) {
                foreach ($this->collVldPembelajaransRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPdLongsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPdLongsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPdLongQuery::create()
                        ->filterByPrimaryKeys($this->vldPdLongsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPdLongsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPdLongsRelatedByIdtype !== null) {
                foreach ($this->collVldPdLongsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPdLongsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldPdLongsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldPdLongQuery::create()
                        ->filterByPrimaryKeys($this->vldPdLongsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPdLongsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldPdLongsRelatedByIdtype !== null) {
                foreach ($this->collVldPdLongsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldNonsekolahQuery::create()
                        ->filterByPrimaryKeys($this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldNonsekolahsRelatedByIdtype !== null) {
                foreach ($this->collVldNonsekolahsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldNonsekolahQuery::create()
                        ->filterByPrimaryKeys($this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldNonsekolahsRelatedByIdtype !== null) {
                foreach ($this->collVldNonsekolahsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldNilaiTestQuery::create()
                        ->filterByPrimaryKeys($this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldNilaiTestsRelatedByIdtype !== null) {
                foreach ($this->collVldNilaiTestsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldNilaiTestQuery::create()
                        ->filterByPrimaryKeys($this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldNilaiTestsRelatedByIdtype !== null) {
                foreach ($this->collVldNilaiTestsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldNilaiRaporQuery::create()
                        ->filterByPrimaryKeys($this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldNilaiRaporsRelatedByIdtype !== null) {
                foreach ($this->collVldNilaiRaporsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldNilaiRaporQuery::create()
                        ->filterByPrimaryKeys($this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldNilaiRaporsRelatedByIdtype !== null) {
                foreach ($this->collVldNilaiRaporsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldMousRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldMousRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldMouQuery::create()
                        ->filterByPrimaryKeys($this->vldMousRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldMousRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldMousRelatedByIdtype !== null) {
                foreach ($this->collVldMousRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldMousRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldMousRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldMouQuery::create()
                        ->filterByPrimaryKeys($this->vldMousRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldMousRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldMousRelatedByIdtype !== null) {
                foreach ($this->collVldMousRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldKesejahteraanQuery::create()
                        ->filterByPrimaryKeys($this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldKesejahteraansRelatedByIdtype !== null) {
                foreach ($this->collVldKesejahteraansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldKesejahteraanQuery::create()
                        ->filterByPrimaryKeys($this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldKesejahteraansRelatedByIdtype !== null) {
                foreach ($this->collVldKesejahteraansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldKaryaTulisQuery::create()
                        ->filterByPrimaryKeys($this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldKaryaTulissRelatedByIdtype !== null) {
                foreach ($this->collVldKaryaTulissRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldKaryaTulisQuery::create()
                        ->filterByPrimaryKeys($this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldKaryaTulissRelatedByIdtype !== null) {
                foreach ($this->collVldKaryaTulissRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldJurusanSpQuery::create()
                        ->filterByPrimaryKeys($this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldJurusanSpsRelatedByIdtype !== null) {
                foreach ($this->collVldJurusanSpsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldJurusanSpQuery::create()
                        ->filterByPrimaryKeys($this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldJurusanSpsRelatedByIdtype !== null) {
                foreach ($this->collVldJurusanSpsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldInpassingsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldInpassingsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldInpassingQuery::create()
                        ->filterByPrimaryKeys($this->vldInpassingsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldInpassingsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldInpassingsRelatedByIdtype !== null) {
                foreach ($this->collVldInpassingsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldInpassingsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldInpassingsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldInpassingQuery::create()
                        ->filterByPrimaryKeys($this->vldInpassingsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldInpassingsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldInpassingsRelatedByIdtype !== null) {
                foreach ($this->collVldInpassingsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldDemografisRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldDemografisRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldDemografiQuery::create()
                        ->filterByPrimaryKeys($this->vldDemografisRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldDemografisRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldDemografisRelatedByIdtype !== null) {
                foreach ($this->collVldDemografisRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldDemografisRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldDemografisRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldDemografiQuery::create()
                        ->filterByPrimaryKeys($this->vldDemografisRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldDemografisRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldDemografisRelatedByIdtype !== null) {
                foreach ($this->collVldDemografisRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldBukuPtksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldBukuPtksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldBukuPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldBukuPtksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldBukuPtksRelatedByIdtype !== null) {
                foreach ($this->collVldBukuPtksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldBukuPtksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldBukuPtksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldBukuPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldBukuPtksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldBukuPtksRelatedByIdtype !== null) {
                foreach ($this->collVldBukuPtksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldBeaPtksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldBeaPtksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldBeaPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldBeaPtksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldBeaPtksRelatedByIdtype !== null) {
                foreach ($this->collVldBeaPtksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldBeaPtksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldBeaPtksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldBeaPtkQuery::create()
                        ->filterByPrimaryKeys($this->vldBeaPtksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldBeaPtksRelatedByIdtype !== null) {
                foreach ($this->collVldBeaPtksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldBeaPdsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldBeaPdsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldBeaPdQuery::create()
                        ->filterByPrimaryKeys($this->vldBeaPdsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldBeaPdsRelatedByIdtype !== null) {
                foreach ($this->collVldBeaPdsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldBeaPdsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldBeaPdsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldBeaPdQuery::create()
                        ->filterByPrimaryKeys($this->vldBeaPdsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldBeaPdsRelatedByIdtype !== null) {
                foreach ($this->collVldBeaPdsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldAnaksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldAnaksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldAnakQuery::create()
                        ->filterByPrimaryKeys($this->vldAnaksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldAnaksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldAnaksRelatedByIdtype !== null) {
                foreach ($this->collVldAnaksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldAnaksRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldAnaksRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldAnakQuery::create()
                        ->filterByPrimaryKeys($this->vldAnaksRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldAnaksRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldAnaksRelatedByIdtype !== null) {
                foreach ($this->collVldAnaksRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldYayasansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldYayasansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldYayasanQuery::create()
                        ->filterByPrimaryKeys($this->vldYayasansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldYayasansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldYayasansRelatedByIdtype !== null) {
                foreach ($this->collVldYayasansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldYayasansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldYayasansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldYayasanQuery::create()
                        ->filterByPrimaryKeys($this->vldYayasansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldYayasansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldYayasansRelatedByIdtype !== null) {
                foreach ($this->collVldYayasansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldUnsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldUnsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldUnQuery::create()
                        ->filterByPrimaryKeys($this->vldUnsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldUnsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldUnsRelatedByIdtype !== null) {
                foreach ($this->collVldUnsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldUnsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldUnsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldUnQuery::create()
                        ->filterByPrimaryKeys($this->vldUnsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldUnsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldUnsRelatedByIdtype !== null) {
                foreach ($this->collVldUnsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldTunjangansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldTunjangansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldTunjanganQuery::create()
                        ->filterByPrimaryKeys($this->vldTunjangansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldTunjangansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldTunjangansRelatedByIdtype !== null) {
                foreach ($this->collVldTunjangansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldTunjangansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldTunjangansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldTunjanganQuery::create()
                        ->filterByPrimaryKeys($this->vldTunjangansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldTunjangansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldTunjangansRelatedByIdtype !== null) {
                foreach ($this->collVldTunjangansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldTugasTambahanQuery::create()
                        ->filterByPrimaryKeys($this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldTugasTambahansRelatedByIdtype !== null) {
                foreach ($this->collVldTugasTambahansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldTugasTambahanQuery::create()
                        ->filterByPrimaryKeys($this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldTugasTambahansRelatedByIdtype !== null) {
                foreach ($this->collVldTugasTambahansRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldSekolahsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldSekolahsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldSekolahQuery::create()
                        ->filterByPrimaryKeys($this->vldSekolahsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldSekolahsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldSekolahsRelatedByIdtype !== null) {
                foreach ($this->collVldSekolahsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldSekolahsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldSekolahsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldSekolahQuery::create()
                        ->filterByPrimaryKeys($this->vldSekolahsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldSekolahsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldSekolahsRelatedByIdtype !== null) {
                foreach ($this->collVldSekolahsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldSaranasRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldSaranasRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldSaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldSaranasRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldSaranasRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldSaranasRelatedByIdtype !== null) {
                foreach ($this->collVldSaranasRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldSaranasRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldSaranasRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldSaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldSaranasRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldSaranasRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldSaranasRelatedByIdtype !== null) {
                foreach ($this->collVldSaranasRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwyStrukturalQuery::create()
                        ->filterByPrimaryKeys($this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwyStrukturalsRelatedByIdtype !== null) {
                foreach ($this->collVldRwyStrukturalsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwyStrukturalQuery::create()
                        ->filterByPrimaryKeys($this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwyStrukturalsRelatedByIdtype !== null) {
                foreach ($this->collVldRwyStrukturalsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwySertifikasiQuery::create()
                        ->filterByPrimaryKeys($this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwySertifikasisRelatedByIdtype !== null) {
                foreach ($this->collVldRwySertifikasisRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwySertifikasiQuery::create()
                        ->filterByPrimaryKeys($this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwySertifikasisRelatedByIdtype !== null) {
                foreach ($this->collVldRwySertifikasisRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwyPendFormalQuery::create()
                        ->filterByPrimaryKeys($this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwyPendFormalsRelatedByIdtype !== null) {
                foreach ($this->collVldRwyPendFormalsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion !== null) {
                if (!$this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion->isEmpty()) {
                    VldRwyPendFormalQuery::create()
                        ->filterByPrimaryKeys($this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion = null;
                }
            }

            if ($this->collVldRwyPendFormalsRelatedByIdtype !== null) {
                foreach ($this->collVldRwyPendFormalsRelatedByIdtype as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ErrortypePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldRwyKepangkatansRelatedByIdtype !== null) {
                    foreach ($this->collVldRwyKepangkatansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwyKepangkatansRelatedByIdtype !== null) {
                    foreach ($this->collVldRwyKepangkatansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwyFungsionalsRelatedByIdtype !== null) {
                    foreach ($this->collVldRwyFungsionalsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwyFungsionalsRelatedByIdtype !== null) {
                    foreach ($this->collVldRwyFungsionalsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRombelsRelatedByIdtype !== null) {
                    foreach ($this->collVldRombelsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRombelsRelatedByIdtype !== null) {
                    foreach ($this->collVldRombelsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPtksRelatedByIdtype !== null) {
                    foreach ($this->collVldPtksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPtksRelatedByIdtype !== null) {
                    foreach ($this->collVldPtksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPrestasisRelatedByIdtype !== null) {
                    foreach ($this->collVldPrestasisRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPrestasisRelatedByIdtype !== null) {
                    foreach ($this->collVldPrestasisRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPrasaranasRelatedByIdtype !== null) {
                    foreach ($this->collVldPrasaranasRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPrasaranasRelatedByIdtype !== null) {
                    foreach ($this->collVldPrasaranasRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPesertaDidiksRelatedByIdtype !== null) {
                    foreach ($this->collVldPesertaDidiksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPesertaDidiksRelatedByIdtype !== null) {
                    foreach ($this->collVldPesertaDidiksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPenghargaansRelatedByIdtype !== null) {
                    foreach ($this->collVldPenghargaansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPenghargaansRelatedByIdtype !== null) {
                    foreach ($this->collVldPenghargaansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPembelajaransRelatedByIdtype !== null) {
                    foreach ($this->collVldPembelajaransRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPembelajaransRelatedByIdtype !== null) {
                    foreach ($this->collVldPembelajaransRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPdLongsRelatedByIdtype !== null) {
                    foreach ($this->collVldPdLongsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPdLongsRelatedByIdtype !== null) {
                    foreach ($this->collVldPdLongsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldNonsekolahsRelatedByIdtype !== null) {
                    foreach ($this->collVldNonsekolahsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldNonsekolahsRelatedByIdtype !== null) {
                    foreach ($this->collVldNonsekolahsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldNilaiTestsRelatedByIdtype !== null) {
                    foreach ($this->collVldNilaiTestsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldNilaiTestsRelatedByIdtype !== null) {
                    foreach ($this->collVldNilaiTestsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldNilaiRaporsRelatedByIdtype !== null) {
                    foreach ($this->collVldNilaiRaporsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldNilaiRaporsRelatedByIdtype !== null) {
                    foreach ($this->collVldNilaiRaporsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldMousRelatedByIdtype !== null) {
                    foreach ($this->collVldMousRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldMousRelatedByIdtype !== null) {
                    foreach ($this->collVldMousRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldKesejahteraansRelatedByIdtype !== null) {
                    foreach ($this->collVldKesejahteraansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldKesejahteraansRelatedByIdtype !== null) {
                    foreach ($this->collVldKesejahteraansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldKaryaTulissRelatedByIdtype !== null) {
                    foreach ($this->collVldKaryaTulissRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldKaryaTulissRelatedByIdtype !== null) {
                    foreach ($this->collVldKaryaTulissRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldJurusanSpsRelatedByIdtype !== null) {
                    foreach ($this->collVldJurusanSpsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldJurusanSpsRelatedByIdtype !== null) {
                    foreach ($this->collVldJurusanSpsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldInpassingsRelatedByIdtype !== null) {
                    foreach ($this->collVldInpassingsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldInpassingsRelatedByIdtype !== null) {
                    foreach ($this->collVldInpassingsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldDemografisRelatedByIdtype !== null) {
                    foreach ($this->collVldDemografisRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldDemografisRelatedByIdtype !== null) {
                    foreach ($this->collVldDemografisRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldBukuPtksRelatedByIdtype !== null) {
                    foreach ($this->collVldBukuPtksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldBukuPtksRelatedByIdtype !== null) {
                    foreach ($this->collVldBukuPtksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldBeaPtksRelatedByIdtype !== null) {
                    foreach ($this->collVldBeaPtksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldBeaPtksRelatedByIdtype !== null) {
                    foreach ($this->collVldBeaPtksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldBeaPdsRelatedByIdtype !== null) {
                    foreach ($this->collVldBeaPdsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldBeaPdsRelatedByIdtype !== null) {
                    foreach ($this->collVldBeaPdsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldAnaksRelatedByIdtype !== null) {
                    foreach ($this->collVldAnaksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldAnaksRelatedByIdtype !== null) {
                    foreach ($this->collVldAnaksRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldYayasansRelatedByIdtype !== null) {
                    foreach ($this->collVldYayasansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldYayasansRelatedByIdtype !== null) {
                    foreach ($this->collVldYayasansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldUnsRelatedByIdtype !== null) {
                    foreach ($this->collVldUnsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldUnsRelatedByIdtype !== null) {
                    foreach ($this->collVldUnsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldTunjangansRelatedByIdtype !== null) {
                    foreach ($this->collVldTunjangansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldTunjangansRelatedByIdtype !== null) {
                    foreach ($this->collVldTunjangansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldTugasTambahansRelatedByIdtype !== null) {
                    foreach ($this->collVldTugasTambahansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldTugasTambahansRelatedByIdtype !== null) {
                    foreach ($this->collVldTugasTambahansRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldSekolahsRelatedByIdtype !== null) {
                    foreach ($this->collVldSekolahsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldSekolahsRelatedByIdtype !== null) {
                    foreach ($this->collVldSekolahsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldSaranasRelatedByIdtype !== null) {
                    foreach ($this->collVldSaranasRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldSaranasRelatedByIdtype !== null) {
                    foreach ($this->collVldSaranasRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwyStrukturalsRelatedByIdtype !== null) {
                    foreach ($this->collVldRwyStrukturalsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwyStrukturalsRelatedByIdtype !== null) {
                    foreach ($this->collVldRwyStrukturalsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwySertifikasisRelatedByIdtype !== null) {
                    foreach ($this->collVldRwySertifikasisRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwySertifikasisRelatedByIdtype !== null) {
                    foreach ($this->collVldRwySertifikasisRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwyPendFormalsRelatedByIdtype !== null) {
                    foreach ($this->collVldRwyPendFormalsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldRwyPendFormalsRelatedByIdtype !== null) {
                    foreach ($this->collVldRwyPendFormalsRelatedByIdtype as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ErrortypePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdtype();
                break;
            case 1:
                return $this->getKategoriError();
                break;
            case 2:
                return $this->getKeterangan();
                break;
            case 3:
                return $this->getCreateDate();
                break;
            case 4:
                return $this->getLastUpdate();
                break;
            case 5:
                return $this->getExpiredDate();
                break;
            case 6:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Errortype'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Errortype'][$this->getPrimaryKey()] = true;
        $keys = ErrortypePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdtype(),
            $keys[1] => $this->getKategoriError(),
            $keys[2] => $this->getKeterangan(),
            $keys[3] => $this->getCreateDate(),
            $keys[4] => $this->getLastUpdate(),
            $keys[5] => $this->getExpiredDate(),
            $keys[6] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collVldRwyKepangkatansRelatedByIdtype) {
                $result['VldRwyKepangkatansRelatedByIdtype'] = $this->collVldRwyKepangkatansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwyKepangkatansRelatedByIdtype) {
                $result['VldRwyKepangkatansRelatedByIdtype'] = $this->collVldRwyKepangkatansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwyFungsionalsRelatedByIdtype) {
                $result['VldRwyFungsionalsRelatedByIdtype'] = $this->collVldRwyFungsionalsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwyFungsionalsRelatedByIdtype) {
                $result['VldRwyFungsionalsRelatedByIdtype'] = $this->collVldRwyFungsionalsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRombelsRelatedByIdtype) {
                $result['VldRombelsRelatedByIdtype'] = $this->collVldRombelsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRombelsRelatedByIdtype) {
                $result['VldRombelsRelatedByIdtype'] = $this->collVldRombelsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPtksRelatedByIdtype) {
                $result['VldPtksRelatedByIdtype'] = $this->collVldPtksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPtksRelatedByIdtype) {
                $result['VldPtksRelatedByIdtype'] = $this->collVldPtksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPrestasisRelatedByIdtype) {
                $result['VldPrestasisRelatedByIdtype'] = $this->collVldPrestasisRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPrestasisRelatedByIdtype) {
                $result['VldPrestasisRelatedByIdtype'] = $this->collVldPrestasisRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPrasaranasRelatedByIdtype) {
                $result['VldPrasaranasRelatedByIdtype'] = $this->collVldPrasaranasRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPrasaranasRelatedByIdtype) {
                $result['VldPrasaranasRelatedByIdtype'] = $this->collVldPrasaranasRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPesertaDidiksRelatedByIdtype) {
                $result['VldPesertaDidiksRelatedByIdtype'] = $this->collVldPesertaDidiksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPesertaDidiksRelatedByIdtype) {
                $result['VldPesertaDidiksRelatedByIdtype'] = $this->collVldPesertaDidiksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPenghargaansRelatedByIdtype) {
                $result['VldPenghargaansRelatedByIdtype'] = $this->collVldPenghargaansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPenghargaansRelatedByIdtype) {
                $result['VldPenghargaansRelatedByIdtype'] = $this->collVldPenghargaansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPembelajaransRelatedByIdtype) {
                $result['VldPembelajaransRelatedByIdtype'] = $this->collVldPembelajaransRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPembelajaransRelatedByIdtype) {
                $result['VldPembelajaransRelatedByIdtype'] = $this->collVldPembelajaransRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPdLongsRelatedByIdtype) {
                $result['VldPdLongsRelatedByIdtype'] = $this->collVldPdLongsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPdLongsRelatedByIdtype) {
                $result['VldPdLongsRelatedByIdtype'] = $this->collVldPdLongsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldNonsekolahsRelatedByIdtype) {
                $result['VldNonsekolahsRelatedByIdtype'] = $this->collVldNonsekolahsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldNonsekolahsRelatedByIdtype) {
                $result['VldNonsekolahsRelatedByIdtype'] = $this->collVldNonsekolahsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldNilaiTestsRelatedByIdtype) {
                $result['VldNilaiTestsRelatedByIdtype'] = $this->collVldNilaiTestsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldNilaiTestsRelatedByIdtype) {
                $result['VldNilaiTestsRelatedByIdtype'] = $this->collVldNilaiTestsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldNilaiRaporsRelatedByIdtype) {
                $result['VldNilaiRaporsRelatedByIdtype'] = $this->collVldNilaiRaporsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldNilaiRaporsRelatedByIdtype) {
                $result['VldNilaiRaporsRelatedByIdtype'] = $this->collVldNilaiRaporsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldMousRelatedByIdtype) {
                $result['VldMousRelatedByIdtype'] = $this->collVldMousRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldMousRelatedByIdtype) {
                $result['VldMousRelatedByIdtype'] = $this->collVldMousRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldKesejahteraansRelatedByIdtype) {
                $result['VldKesejahteraansRelatedByIdtype'] = $this->collVldKesejahteraansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldKesejahteraansRelatedByIdtype) {
                $result['VldKesejahteraansRelatedByIdtype'] = $this->collVldKesejahteraansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldKaryaTulissRelatedByIdtype) {
                $result['VldKaryaTulissRelatedByIdtype'] = $this->collVldKaryaTulissRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldKaryaTulissRelatedByIdtype) {
                $result['VldKaryaTulissRelatedByIdtype'] = $this->collVldKaryaTulissRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldJurusanSpsRelatedByIdtype) {
                $result['VldJurusanSpsRelatedByIdtype'] = $this->collVldJurusanSpsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldJurusanSpsRelatedByIdtype) {
                $result['VldJurusanSpsRelatedByIdtype'] = $this->collVldJurusanSpsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldInpassingsRelatedByIdtype) {
                $result['VldInpassingsRelatedByIdtype'] = $this->collVldInpassingsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldInpassingsRelatedByIdtype) {
                $result['VldInpassingsRelatedByIdtype'] = $this->collVldInpassingsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldDemografisRelatedByIdtype) {
                $result['VldDemografisRelatedByIdtype'] = $this->collVldDemografisRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldDemografisRelatedByIdtype) {
                $result['VldDemografisRelatedByIdtype'] = $this->collVldDemografisRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldBukuPtksRelatedByIdtype) {
                $result['VldBukuPtksRelatedByIdtype'] = $this->collVldBukuPtksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldBukuPtksRelatedByIdtype) {
                $result['VldBukuPtksRelatedByIdtype'] = $this->collVldBukuPtksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldBeaPtksRelatedByIdtype) {
                $result['VldBeaPtksRelatedByIdtype'] = $this->collVldBeaPtksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldBeaPtksRelatedByIdtype) {
                $result['VldBeaPtksRelatedByIdtype'] = $this->collVldBeaPtksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldBeaPdsRelatedByIdtype) {
                $result['VldBeaPdsRelatedByIdtype'] = $this->collVldBeaPdsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldBeaPdsRelatedByIdtype) {
                $result['VldBeaPdsRelatedByIdtype'] = $this->collVldBeaPdsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldAnaksRelatedByIdtype) {
                $result['VldAnaksRelatedByIdtype'] = $this->collVldAnaksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldAnaksRelatedByIdtype) {
                $result['VldAnaksRelatedByIdtype'] = $this->collVldAnaksRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldYayasansRelatedByIdtype) {
                $result['VldYayasansRelatedByIdtype'] = $this->collVldYayasansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldYayasansRelatedByIdtype) {
                $result['VldYayasansRelatedByIdtype'] = $this->collVldYayasansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldUnsRelatedByIdtype) {
                $result['VldUnsRelatedByIdtype'] = $this->collVldUnsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldUnsRelatedByIdtype) {
                $result['VldUnsRelatedByIdtype'] = $this->collVldUnsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldTunjangansRelatedByIdtype) {
                $result['VldTunjangansRelatedByIdtype'] = $this->collVldTunjangansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldTunjangansRelatedByIdtype) {
                $result['VldTunjangansRelatedByIdtype'] = $this->collVldTunjangansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldTugasTambahansRelatedByIdtype) {
                $result['VldTugasTambahansRelatedByIdtype'] = $this->collVldTugasTambahansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldTugasTambahansRelatedByIdtype) {
                $result['VldTugasTambahansRelatedByIdtype'] = $this->collVldTugasTambahansRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldSekolahsRelatedByIdtype) {
                $result['VldSekolahsRelatedByIdtype'] = $this->collVldSekolahsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldSekolahsRelatedByIdtype) {
                $result['VldSekolahsRelatedByIdtype'] = $this->collVldSekolahsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldSaranasRelatedByIdtype) {
                $result['VldSaranasRelatedByIdtype'] = $this->collVldSaranasRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldSaranasRelatedByIdtype) {
                $result['VldSaranasRelatedByIdtype'] = $this->collVldSaranasRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwyStrukturalsRelatedByIdtype) {
                $result['VldRwyStrukturalsRelatedByIdtype'] = $this->collVldRwyStrukturalsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwyStrukturalsRelatedByIdtype) {
                $result['VldRwyStrukturalsRelatedByIdtype'] = $this->collVldRwyStrukturalsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwySertifikasisRelatedByIdtype) {
                $result['VldRwySertifikasisRelatedByIdtype'] = $this->collVldRwySertifikasisRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwySertifikasisRelatedByIdtype) {
                $result['VldRwySertifikasisRelatedByIdtype'] = $this->collVldRwySertifikasisRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwyPendFormalsRelatedByIdtype) {
                $result['VldRwyPendFormalsRelatedByIdtype'] = $this->collVldRwyPendFormalsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldRwyPendFormalsRelatedByIdtype) {
                $result['VldRwyPendFormalsRelatedByIdtype'] = $this->collVldRwyPendFormalsRelatedByIdtype->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ErrortypePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdtype($value);
                break;
            case 1:
                $this->setKategoriError($value);
                break;
            case 2:
                $this->setKeterangan($value);
                break;
            case 3:
                $this->setCreateDate($value);
                break;
            case 4:
                $this->setLastUpdate($value);
                break;
            case 5:
                $this->setExpiredDate($value);
                break;
            case 6:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ErrortypePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdtype($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setKategoriError($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setKeterangan($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCreateDate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setLastUpdate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setExpiredDate($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLastSync($arr[$keys[6]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ErrortypePeer::DATABASE_NAME);

        if ($this->isColumnModified(ErrortypePeer::IDTYPE)) $criteria->add(ErrortypePeer::IDTYPE, $this->idtype);
        if ($this->isColumnModified(ErrortypePeer::KATEGORI_ERROR)) $criteria->add(ErrortypePeer::KATEGORI_ERROR, $this->kategori_error);
        if ($this->isColumnModified(ErrortypePeer::KETERANGAN)) $criteria->add(ErrortypePeer::KETERANGAN, $this->keterangan);
        if ($this->isColumnModified(ErrortypePeer::CREATE_DATE)) $criteria->add(ErrortypePeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(ErrortypePeer::LAST_UPDATE)) $criteria->add(ErrortypePeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(ErrortypePeer::EXPIRED_DATE)) $criteria->add(ErrortypePeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(ErrortypePeer::LAST_SYNC)) $criteria->add(ErrortypePeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ErrortypePeer::DATABASE_NAME);
        $criteria->add(ErrortypePeer::IDTYPE, $this->idtype);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdtype();
    }

    /**
     * Generic method to set the primary key (idtype column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdtype($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdtype();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Errortype (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKategoriError($this->getKategoriError());
        $copyObj->setKeterangan($this->getKeterangan());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldRwyKepangkatansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwyKepangkatanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwyKepangkatansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwyKepangkatanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwyFungsionalsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwyFungsionalRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwyFungsionalsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwyFungsionalRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRombelsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRombelRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRombelsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRombelRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPtksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPtkRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPtksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPtkRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPrestasisRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPrestasiRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPrestasisRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPrestasiRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPrasaranasRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPrasaranaRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPrasaranasRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPrasaranaRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPesertaDidiksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPesertaDidikRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPesertaDidiksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPesertaDidikRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPenghargaansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPenghargaanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPenghargaansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPenghargaanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPembelajaransRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPembelajaranRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPembelajaransRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPembelajaranRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPdLongsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPdLongRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPdLongsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPdLongRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldNonsekolahsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldNonsekolahRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldNonsekolahsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldNonsekolahRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldNilaiTestsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldNilaiTestRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldNilaiTestsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldNilaiTestRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldNilaiRaporsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldNilaiRaporRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldNilaiRaporsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldNilaiRaporRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldMousRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldMouRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldMousRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldMouRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldKesejahteraansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldKesejahteraanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldKesejahteraansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldKesejahteraanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldKaryaTulissRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldKaryaTulisRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldKaryaTulissRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldKaryaTulisRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldJurusanSpsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldJurusanSpRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldJurusanSpsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldJurusanSpRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldInpassingsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldInpassingRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldInpassingsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldInpassingRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldDemografisRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldDemografiRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldDemografisRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldDemografiRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldBukuPtksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldBukuPtkRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldBukuPtksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldBukuPtkRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldBeaPtksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldBeaPtkRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldBeaPtksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldBeaPtkRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldBeaPdsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldBeaPdRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldBeaPdsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldBeaPdRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldAnaksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldAnakRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldAnaksRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldAnakRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldYayasansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldYayasanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldYayasansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldYayasanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldUnsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldUnRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldUnsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldUnRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldTunjangansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldTunjanganRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldTunjangansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldTunjanganRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldTugasTambahansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldTugasTambahanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldTugasTambahansRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldTugasTambahanRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldSekolahsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldSekolahRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldSekolahsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldSekolahRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldSaranasRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldSaranaRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldSaranasRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldSaranaRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwyStrukturalsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwyStrukturalRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwyStrukturalsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwyStrukturalRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwySertifikasisRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwySertifikasiRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwySertifikasisRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwySertifikasiRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwyPendFormalsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwyPendFormalRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldRwyPendFormalsRelatedByIdtype() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRwyPendFormalRelatedByIdtype($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdtype(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Errortype Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ErrortypePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ErrortypePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldRwyKepangkatanRelatedByIdtype' == $relationName) {
            $this->initVldRwyKepangkatansRelatedByIdtype();
        }
        if ('VldRwyKepangkatanRelatedByIdtype' == $relationName) {
            $this->initVldRwyKepangkatansRelatedByIdtype();
        }
        if ('VldRwyFungsionalRelatedByIdtype' == $relationName) {
            $this->initVldRwyFungsionalsRelatedByIdtype();
        }
        if ('VldRwyFungsionalRelatedByIdtype' == $relationName) {
            $this->initVldRwyFungsionalsRelatedByIdtype();
        }
        if ('VldRombelRelatedByIdtype' == $relationName) {
            $this->initVldRombelsRelatedByIdtype();
        }
        if ('VldRombelRelatedByIdtype' == $relationName) {
            $this->initVldRombelsRelatedByIdtype();
        }
        if ('VldPtkRelatedByIdtype' == $relationName) {
            $this->initVldPtksRelatedByIdtype();
        }
        if ('VldPtkRelatedByIdtype' == $relationName) {
            $this->initVldPtksRelatedByIdtype();
        }
        if ('VldPrestasiRelatedByIdtype' == $relationName) {
            $this->initVldPrestasisRelatedByIdtype();
        }
        if ('VldPrestasiRelatedByIdtype' == $relationName) {
            $this->initVldPrestasisRelatedByIdtype();
        }
        if ('VldPrasaranaRelatedByIdtype' == $relationName) {
            $this->initVldPrasaranasRelatedByIdtype();
        }
        if ('VldPrasaranaRelatedByIdtype' == $relationName) {
            $this->initVldPrasaranasRelatedByIdtype();
        }
        if ('VldPesertaDidikRelatedByIdtype' == $relationName) {
            $this->initVldPesertaDidiksRelatedByIdtype();
        }
        if ('VldPesertaDidikRelatedByIdtype' == $relationName) {
            $this->initVldPesertaDidiksRelatedByIdtype();
        }
        if ('VldPenghargaanRelatedByIdtype' == $relationName) {
            $this->initVldPenghargaansRelatedByIdtype();
        }
        if ('VldPenghargaanRelatedByIdtype' == $relationName) {
            $this->initVldPenghargaansRelatedByIdtype();
        }
        if ('VldPembelajaranRelatedByIdtype' == $relationName) {
            $this->initVldPembelajaransRelatedByIdtype();
        }
        if ('VldPembelajaranRelatedByIdtype' == $relationName) {
            $this->initVldPembelajaransRelatedByIdtype();
        }
        if ('VldPdLongRelatedByIdtype' == $relationName) {
            $this->initVldPdLongsRelatedByIdtype();
        }
        if ('VldPdLongRelatedByIdtype' == $relationName) {
            $this->initVldPdLongsRelatedByIdtype();
        }
        if ('VldNonsekolahRelatedByIdtype' == $relationName) {
            $this->initVldNonsekolahsRelatedByIdtype();
        }
        if ('VldNonsekolahRelatedByIdtype' == $relationName) {
            $this->initVldNonsekolahsRelatedByIdtype();
        }
        if ('VldNilaiTestRelatedByIdtype' == $relationName) {
            $this->initVldNilaiTestsRelatedByIdtype();
        }
        if ('VldNilaiTestRelatedByIdtype' == $relationName) {
            $this->initVldNilaiTestsRelatedByIdtype();
        }
        if ('VldNilaiRaporRelatedByIdtype' == $relationName) {
            $this->initVldNilaiRaporsRelatedByIdtype();
        }
        if ('VldNilaiRaporRelatedByIdtype' == $relationName) {
            $this->initVldNilaiRaporsRelatedByIdtype();
        }
        if ('VldMouRelatedByIdtype' == $relationName) {
            $this->initVldMousRelatedByIdtype();
        }
        if ('VldMouRelatedByIdtype' == $relationName) {
            $this->initVldMousRelatedByIdtype();
        }
        if ('VldKesejahteraanRelatedByIdtype' == $relationName) {
            $this->initVldKesejahteraansRelatedByIdtype();
        }
        if ('VldKesejahteraanRelatedByIdtype' == $relationName) {
            $this->initVldKesejahteraansRelatedByIdtype();
        }
        if ('VldKaryaTulisRelatedByIdtype' == $relationName) {
            $this->initVldKaryaTulissRelatedByIdtype();
        }
        if ('VldKaryaTulisRelatedByIdtype' == $relationName) {
            $this->initVldKaryaTulissRelatedByIdtype();
        }
        if ('VldJurusanSpRelatedByIdtype' == $relationName) {
            $this->initVldJurusanSpsRelatedByIdtype();
        }
        if ('VldJurusanSpRelatedByIdtype' == $relationName) {
            $this->initVldJurusanSpsRelatedByIdtype();
        }
        if ('VldInpassingRelatedByIdtype' == $relationName) {
            $this->initVldInpassingsRelatedByIdtype();
        }
        if ('VldInpassingRelatedByIdtype' == $relationName) {
            $this->initVldInpassingsRelatedByIdtype();
        }
        if ('VldDemografiRelatedByIdtype' == $relationName) {
            $this->initVldDemografisRelatedByIdtype();
        }
        if ('VldDemografiRelatedByIdtype' == $relationName) {
            $this->initVldDemografisRelatedByIdtype();
        }
        if ('VldBukuPtkRelatedByIdtype' == $relationName) {
            $this->initVldBukuPtksRelatedByIdtype();
        }
        if ('VldBukuPtkRelatedByIdtype' == $relationName) {
            $this->initVldBukuPtksRelatedByIdtype();
        }
        if ('VldBeaPtkRelatedByIdtype' == $relationName) {
            $this->initVldBeaPtksRelatedByIdtype();
        }
        if ('VldBeaPtkRelatedByIdtype' == $relationName) {
            $this->initVldBeaPtksRelatedByIdtype();
        }
        if ('VldBeaPdRelatedByIdtype' == $relationName) {
            $this->initVldBeaPdsRelatedByIdtype();
        }
        if ('VldBeaPdRelatedByIdtype' == $relationName) {
            $this->initVldBeaPdsRelatedByIdtype();
        }
        if ('VldAnakRelatedByIdtype' == $relationName) {
            $this->initVldAnaksRelatedByIdtype();
        }
        if ('VldAnakRelatedByIdtype' == $relationName) {
            $this->initVldAnaksRelatedByIdtype();
        }
        if ('VldYayasanRelatedByIdtype' == $relationName) {
            $this->initVldYayasansRelatedByIdtype();
        }
        if ('VldYayasanRelatedByIdtype' == $relationName) {
            $this->initVldYayasansRelatedByIdtype();
        }
        if ('VldUnRelatedByIdtype' == $relationName) {
            $this->initVldUnsRelatedByIdtype();
        }
        if ('VldUnRelatedByIdtype' == $relationName) {
            $this->initVldUnsRelatedByIdtype();
        }
        if ('VldTunjanganRelatedByIdtype' == $relationName) {
            $this->initVldTunjangansRelatedByIdtype();
        }
        if ('VldTunjanganRelatedByIdtype' == $relationName) {
            $this->initVldTunjangansRelatedByIdtype();
        }
        if ('VldTugasTambahanRelatedByIdtype' == $relationName) {
            $this->initVldTugasTambahansRelatedByIdtype();
        }
        if ('VldTugasTambahanRelatedByIdtype' == $relationName) {
            $this->initVldTugasTambahansRelatedByIdtype();
        }
        if ('VldSekolahRelatedByIdtype' == $relationName) {
            $this->initVldSekolahsRelatedByIdtype();
        }
        if ('VldSekolahRelatedByIdtype' == $relationName) {
            $this->initVldSekolahsRelatedByIdtype();
        }
        if ('VldSaranaRelatedByIdtype' == $relationName) {
            $this->initVldSaranasRelatedByIdtype();
        }
        if ('VldSaranaRelatedByIdtype' == $relationName) {
            $this->initVldSaranasRelatedByIdtype();
        }
        if ('VldRwyStrukturalRelatedByIdtype' == $relationName) {
            $this->initVldRwyStrukturalsRelatedByIdtype();
        }
        if ('VldRwyStrukturalRelatedByIdtype' == $relationName) {
            $this->initVldRwyStrukturalsRelatedByIdtype();
        }
        if ('VldRwySertifikasiRelatedByIdtype' == $relationName) {
            $this->initVldRwySertifikasisRelatedByIdtype();
        }
        if ('VldRwySertifikasiRelatedByIdtype' == $relationName) {
            $this->initVldRwySertifikasisRelatedByIdtype();
        }
        if ('VldRwyPendFormalRelatedByIdtype' == $relationName) {
            $this->initVldRwyPendFormalsRelatedByIdtype();
        }
        if ('VldRwyPendFormalRelatedByIdtype' == $relationName) {
            $this->initVldRwyPendFormalsRelatedByIdtype();
        }
    }

    /**
     * Clears out the collVldRwyKepangkatansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwyKepangkatansRelatedByIdtype()
     */
    public function clearVldRwyKepangkatansRelatedByIdtype()
    {
        $this->collVldRwyKepangkatansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwyKepangkatansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwyKepangkatansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwyKepangkatansRelatedByIdtype($v = true)
    {
        $this->collVldRwyKepangkatansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwyKepangkatansRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwyKepangkatansRelatedByIdtype collection to an empty array (like clearcollVldRwyKepangkatansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwyKepangkatansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwyKepangkatansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwyKepangkatansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwyKepangkatansRelatedByIdtype->setModel('VldRwyKepangkatan');
    }

    /**
     * Gets an array of VldRwyKepangkatan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwyKepangkatan[] List of VldRwyKepangkatan objects
     * @throws PropelException
     */
    public function getVldRwyKepangkatansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyKepangkatansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyKepangkatansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwyKepangkatansRelatedByIdtype) {
                // return empty collection
                $this->initVldRwyKepangkatansRelatedByIdtype();
            } else {
                $collVldRwyKepangkatansRelatedByIdtype = VldRwyKepangkatanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwyKepangkatansRelatedByIdtypePartial && count($collVldRwyKepangkatansRelatedByIdtype)) {
                      $this->initVldRwyKepangkatansRelatedByIdtype(false);

                      foreach($collVldRwyKepangkatansRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwyKepangkatansRelatedByIdtype->contains($obj)) {
                          $this->collVldRwyKepangkatansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwyKepangkatansRelatedByIdtypePartial = true;
                    }

                    $collVldRwyKepangkatansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwyKepangkatansRelatedByIdtype;
                }

                if($partial && $this->collVldRwyKepangkatansRelatedByIdtype) {
                    foreach($this->collVldRwyKepangkatansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwyKepangkatansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwyKepangkatansRelatedByIdtype = $collVldRwyKepangkatansRelatedByIdtype;
                $this->collVldRwyKepangkatansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwyKepangkatansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwyKepangkatanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwyKepangkatansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwyKepangkatansRelatedByIdtype(PropelCollection $vldRwyKepangkatansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwyKepangkatansRelatedByIdtypeToDelete = $this->getVldRwyKepangkatansRelatedByIdtype(new Criteria(), $con)->diff($vldRwyKepangkatansRelatedByIdtype);

        $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwyKepangkatansRelatedByIdtypeToDelete));

        foreach ($vldRwyKepangkatansRelatedByIdtypeToDelete as $vldRwyKepangkatanRelatedByIdtypeRemoved) {
            $vldRwyKepangkatanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwyKepangkatansRelatedByIdtype = null;
        foreach ($vldRwyKepangkatansRelatedByIdtype as $vldRwyKepangkatanRelatedByIdtype) {
            $this->addVldRwyKepangkatanRelatedByIdtype($vldRwyKepangkatanRelatedByIdtype);
        }

        $this->collVldRwyKepangkatansRelatedByIdtype = $vldRwyKepangkatansRelatedByIdtype;
        $this->collVldRwyKepangkatansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwyKepangkatan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwyKepangkatan objects.
     * @throws PropelException
     */
    public function countVldRwyKepangkatansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyKepangkatansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyKepangkatansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwyKepangkatansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwyKepangkatansRelatedByIdtype());
            }
            $query = VldRwyKepangkatanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwyKepangkatansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwyKepangkatan object to this object
     * through the VldRwyKepangkatan foreign key attribute.
     *
     * @param    VldRwyKepangkatan $l VldRwyKepangkatan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwyKepangkatanRelatedByIdtype(VldRwyKepangkatan $l)
    {
        if ($this->collVldRwyKepangkatansRelatedByIdtype === null) {
            $this->initVldRwyKepangkatansRelatedByIdtype();
            $this->collVldRwyKepangkatansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwyKepangkatansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwyKepangkatanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwyKepangkatanRelatedByIdtype $vldRwyKepangkatanRelatedByIdtype The vldRwyKepangkatanRelatedByIdtype object to add.
     */
    protected function doAddVldRwyKepangkatanRelatedByIdtype($vldRwyKepangkatanRelatedByIdtype)
    {
        $this->collVldRwyKepangkatansRelatedByIdtype[]= $vldRwyKepangkatanRelatedByIdtype;
        $vldRwyKepangkatanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwyKepangkatanRelatedByIdtype $vldRwyKepangkatanRelatedByIdtype The vldRwyKepangkatanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwyKepangkatanRelatedByIdtype($vldRwyKepangkatanRelatedByIdtype)
    {
        if ($this->getVldRwyKepangkatansRelatedByIdtype()->contains($vldRwyKepangkatanRelatedByIdtype)) {
            $this->collVldRwyKepangkatansRelatedByIdtype->remove($this->collVldRwyKepangkatansRelatedByIdtype->search($vldRwyKepangkatanRelatedByIdtype));
            if (null === $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwyKepangkatansRelatedByIdtype;
                $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion[]= clone $vldRwyKepangkatanRelatedByIdtype;
            $vldRwyKepangkatanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyKepangkatansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyKepangkatan[] List of VldRwyKepangkatan objects
     */
    public function getVldRwyKepangkatansRelatedByIdtypeJoinRwyKepangkatanRelatedByRiwayatKepangkatanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('RwyKepangkatanRelatedByRiwayatKepangkatanId', $join_behavior);

        return $this->getVldRwyKepangkatansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyKepangkatansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyKepangkatan[] List of VldRwyKepangkatan objects
     */
    public function getVldRwyKepangkatansRelatedByIdtypeJoinRwyKepangkatanRelatedByRiwayatKepangkatanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('RwyKepangkatanRelatedByRiwayatKepangkatanId', $join_behavior);

        return $this->getVldRwyKepangkatansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwyKepangkatansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwyKepangkatansRelatedByIdtype()
     */
    public function clearVldRwyKepangkatansRelatedByIdtype()
    {
        $this->collVldRwyKepangkatansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwyKepangkatansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwyKepangkatansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwyKepangkatansRelatedByIdtype($v = true)
    {
        $this->collVldRwyKepangkatansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwyKepangkatansRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwyKepangkatansRelatedByIdtype collection to an empty array (like clearcollVldRwyKepangkatansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwyKepangkatansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwyKepangkatansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwyKepangkatansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwyKepangkatansRelatedByIdtype->setModel('VldRwyKepangkatan');
    }

    /**
     * Gets an array of VldRwyKepangkatan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwyKepangkatan[] List of VldRwyKepangkatan objects
     * @throws PropelException
     */
    public function getVldRwyKepangkatansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyKepangkatansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyKepangkatansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwyKepangkatansRelatedByIdtype) {
                // return empty collection
                $this->initVldRwyKepangkatansRelatedByIdtype();
            } else {
                $collVldRwyKepangkatansRelatedByIdtype = VldRwyKepangkatanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwyKepangkatansRelatedByIdtypePartial && count($collVldRwyKepangkatansRelatedByIdtype)) {
                      $this->initVldRwyKepangkatansRelatedByIdtype(false);

                      foreach($collVldRwyKepangkatansRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwyKepangkatansRelatedByIdtype->contains($obj)) {
                          $this->collVldRwyKepangkatansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwyKepangkatansRelatedByIdtypePartial = true;
                    }

                    $collVldRwyKepangkatansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwyKepangkatansRelatedByIdtype;
                }

                if($partial && $this->collVldRwyKepangkatansRelatedByIdtype) {
                    foreach($this->collVldRwyKepangkatansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwyKepangkatansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwyKepangkatansRelatedByIdtype = $collVldRwyKepangkatansRelatedByIdtype;
                $this->collVldRwyKepangkatansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwyKepangkatansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwyKepangkatanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwyKepangkatansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwyKepangkatansRelatedByIdtype(PropelCollection $vldRwyKepangkatansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwyKepangkatansRelatedByIdtypeToDelete = $this->getVldRwyKepangkatansRelatedByIdtype(new Criteria(), $con)->diff($vldRwyKepangkatansRelatedByIdtype);

        $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwyKepangkatansRelatedByIdtypeToDelete));

        foreach ($vldRwyKepangkatansRelatedByIdtypeToDelete as $vldRwyKepangkatanRelatedByIdtypeRemoved) {
            $vldRwyKepangkatanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwyKepangkatansRelatedByIdtype = null;
        foreach ($vldRwyKepangkatansRelatedByIdtype as $vldRwyKepangkatanRelatedByIdtype) {
            $this->addVldRwyKepangkatanRelatedByIdtype($vldRwyKepangkatanRelatedByIdtype);
        }

        $this->collVldRwyKepangkatansRelatedByIdtype = $vldRwyKepangkatansRelatedByIdtype;
        $this->collVldRwyKepangkatansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwyKepangkatan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwyKepangkatan objects.
     * @throws PropelException
     */
    public function countVldRwyKepangkatansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyKepangkatansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyKepangkatansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwyKepangkatansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwyKepangkatansRelatedByIdtype());
            }
            $query = VldRwyKepangkatanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwyKepangkatansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwyKepangkatan object to this object
     * through the VldRwyKepangkatan foreign key attribute.
     *
     * @param    VldRwyKepangkatan $l VldRwyKepangkatan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwyKepangkatanRelatedByIdtype(VldRwyKepangkatan $l)
    {
        if ($this->collVldRwyKepangkatansRelatedByIdtype === null) {
            $this->initVldRwyKepangkatansRelatedByIdtype();
            $this->collVldRwyKepangkatansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwyKepangkatansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwyKepangkatanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwyKepangkatanRelatedByIdtype $vldRwyKepangkatanRelatedByIdtype The vldRwyKepangkatanRelatedByIdtype object to add.
     */
    protected function doAddVldRwyKepangkatanRelatedByIdtype($vldRwyKepangkatanRelatedByIdtype)
    {
        $this->collVldRwyKepangkatansRelatedByIdtype[]= $vldRwyKepangkatanRelatedByIdtype;
        $vldRwyKepangkatanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwyKepangkatanRelatedByIdtype $vldRwyKepangkatanRelatedByIdtype The vldRwyKepangkatanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwyKepangkatanRelatedByIdtype($vldRwyKepangkatanRelatedByIdtype)
    {
        if ($this->getVldRwyKepangkatansRelatedByIdtype()->contains($vldRwyKepangkatanRelatedByIdtype)) {
            $this->collVldRwyKepangkatansRelatedByIdtype->remove($this->collVldRwyKepangkatansRelatedByIdtype->search($vldRwyKepangkatanRelatedByIdtype));
            if (null === $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwyKepangkatansRelatedByIdtype;
                $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwyKepangkatansRelatedByIdtypeScheduledForDeletion[]= clone $vldRwyKepangkatanRelatedByIdtype;
            $vldRwyKepangkatanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyKepangkatansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyKepangkatan[] List of VldRwyKepangkatan objects
     */
    public function getVldRwyKepangkatansRelatedByIdtypeJoinRwyKepangkatanRelatedByRiwayatKepangkatanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('RwyKepangkatanRelatedByRiwayatKepangkatanId', $join_behavior);

        return $this->getVldRwyKepangkatansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyKepangkatansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyKepangkatan[] List of VldRwyKepangkatan objects
     */
    public function getVldRwyKepangkatansRelatedByIdtypeJoinRwyKepangkatanRelatedByRiwayatKepangkatanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyKepangkatanQuery::create(null, $criteria);
        $query->joinWith('RwyKepangkatanRelatedByRiwayatKepangkatanId', $join_behavior);

        return $this->getVldRwyKepangkatansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwyFungsionalsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwyFungsionalsRelatedByIdtype()
     */
    public function clearVldRwyFungsionalsRelatedByIdtype()
    {
        $this->collVldRwyFungsionalsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwyFungsionalsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwyFungsionalsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwyFungsionalsRelatedByIdtype($v = true)
    {
        $this->collVldRwyFungsionalsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwyFungsionalsRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwyFungsionalsRelatedByIdtype collection to an empty array (like clearcollVldRwyFungsionalsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwyFungsionalsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwyFungsionalsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwyFungsionalsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwyFungsionalsRelatedByIdtype->setModel('VldRwyFungsional');
    }

    /**
     * Gets an array of VldRwyFungsional objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwyFungsional[] List of VldRwyFungsional objects
     * @throws PropelException
     */
    public function getVldRwyFungsionalsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyFungsionalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyFungsionalsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwyFungsionalsRelatedByIdtype) {
                // return empty collection
                $this->initVldRwyFungsionalsRelatedByIdtype();
            } else {
                $collVldRwyFungsionalsRelatedByIdtype = VldRwyFungsionalQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwyFungsionalsRelatedByIdtypePartial && count($collVldRwyFungsionalsRelatedByIdtype)) {
                      $this->initVldRwyFungsionalsRelatedByIdtype(false);

                      foreach($collVldRwyFungsionalsRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwyFungsionalsRelatedByIdtype->contains($obj)) {
                          $this->collVldRwyFungsionalsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwyFungsionalsRelatedByIdtypePartial = true;
                    }

                    $collVldRwyFungsionalsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwyFungsionalsRelatedByIdtype;
                }

                if($partial && $this->collVldRwyFungsionalsRelatedByIdtype) {
                    foreach($this->collVldRwyFungsionalsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwyFungsionalsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwyFungsionalsRelatedByIdtype = $collVldRwyFungsionalsRelatedByIdtype;
                $this->collVldRwyFungsionalsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwyFungsionalsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwyFungsionalRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwyFungsionalsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwyFungsionalsRelatedByIdtype(PropelCollection $vldRwyFungsionalsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwyFungsionalsRelatedByIdtypeToDelete = $this->getVldRwyFungsionalsRelatedByIdtype(new Criteria(), $con)->diff($vldRwyFungsionalsRelatedByIdtype);

        $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwyFungsionalsRelatedByIdtypeToDelete));

        foreach ($vldRwyFungsionalsRelatedByIdtypeToDelete as $vldRwyFungsionalRelatedByIdtypeRemoved) {
            $vldRwyFungsionalRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwyFungsionalsRelatedByIdtype = null;
        foreach ($vldRwyFungsionalsRelatedByIdtype as $vldRwyFungsionalRelatedByIdtype) {
            $this->addVldRwyFungsionalRelatedByIdtype($vldRwyFungsionalRelatedByIdtype);
        }

        $this->collVldRwyFungsionalsRelatedByIdtype = $vldRwyFungsionalsRelatedByIdtype;
        $this->collVldRwyFungsionalsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwyFungsional objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwyFungsional objects.
     * @throws PropelException
     */
    public function countVldRwyFungsionalsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyFungsionalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyFungsionalsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwyFungsionalsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwyFungsionalsRelatedByIdtype());
            }
            $query = VldRwyFungsionalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwyFungsionalsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwyFungsional object to this object
     * through the VldRwyFungsional foreign key attribute.
     *
     * @param    VldRwyFungsional $l VldRwyFungsional
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwyFungsionalRelatedByIdtype(VldRwyFungsional $l)
    {
        if ($this->collVldRwyFungsionalsRelatedByIdtype === null) {
            $this->initVldRwyFungsionalsRelatedByIdtype();
            $this->collVldRwyFungsionalsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwyFungsionalsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwyFungsionalRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwyFungsionalRelatedByIdtype $vldRwyFungsionalRelatedByIdtype The vldRwyFungsionalRelatedByIdtype object to add.
     */
    protected function doAddVldRwyFungsionalRelatedByIdtype($vldRwyFungsionalRelatedByIdtype)
    {
        $this->collVldRwyFungsionalsRelatedByIdtype[]= $vldRwyFungsionalRelatedByIdtype;
        $vldRwyFungsionalRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwyFungsionalRelatedByIdtype $vldRwyFungsionalRelatedByIdtype The vldRwyFungsionalRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwyFungsionalRelatedByIdtype($vldRwyFungsionalRelatedByIdtype)
    {
        if ($this->getVldRwyFungsionalsRelatedByIdtype()->contains($vldRwyFungsionalRelatedByIdtype)) {
            $this->collVldRwyFungsionalsRelatedByIdtype->remove($this->collVldRwyFungsionalsRelatedByIdtype->search($vldRwyFungsionalRelatedByIdtype));
            if (null === $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwyFungsionalsRelatedByIdtype;
                $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion[]= clone $vldRwyFungsionalRelatedByIdtype;
            $vldRwyFungsionalRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyFungsionalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyFungsional[] List of VldRwyFungsional objects
     */
    public function getVldRwyFungsionalsRelatedByIdtypeJoinRwyFungsionalRelatedByRiwayatFungsionalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyFungsionalQuery::create(null, $criteria);
        $query->joinWith('RwyFungsionalRelatedByRiwayatFungsionalId', $join_behavior);

        return $this->getVldRwyFungsionalsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyFungsionalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyFungsional[] List of VldRwyFungsional objects
     */
    public function getVldRwyFungsionalsRelatedByIdtypeJoinRwyFungsionalRelatedByRiwayatFungsionalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyFungsionalQuery::create(null, $criteria);
        $query->joinWith('RwyFungsionalRelatedByRiwayatFungsionalId', $join_behavior);

        return $this->getVldRwyFungsionalsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwyFungsionalsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwyFungsionalsRelatedByIdtype()
     */
    public function clearVldRwyFungsionalsRelatedByIdtype()
    {
        $this->collVldRwyFungsionalsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwyFungsionalsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwyFungsionalsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwyFungsionalsRelatedByIdtype($v = true)
    {
        $this->collVldRwyFungsionalsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwyFungsionalsRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwyFungsionalsRelatedByIdtype collection to an empty array (like clearcollVldRwyFungsionalsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwyFungsionalsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwyFungsionalsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwyFungsionalsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwyFungsionalsRelatedByIdtype->setModel('VldRwyFungsional');
    }

    /**
     * Gets an array of VldRwyFungsional objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwyFungsional[] List of VldRwyFungsional objects
     * @throws PropelException
     */
    public function getVldRwyFungsionalsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyFungsionalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyFungsionalsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwyFungsionalsRelatedByIdtype) {
                // return empty collection
                $this->initVldRwyFungsionalsRelatedByIdtype();
            } else {
                $collVldRwyFungsionalsRelatedByIdtype = VldRwyFungsionalQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwyFungsionalsRelatedByIdtypePartial && count($collVldRwyFungsionalsRelatedByIdtype)) {
                      $this->initVldRwyFungsionalsRelatedByIdtype(false);

                      foreach($collVldRwyFungsionalsRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwyFungsionalsRelatedByIdtype->contains($obj)) {
                          $this->collVldRwyFungsionalsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwyFungsionalsRelatedByIdtypePartial = true;
                    }

                    $collVldRwyFungsionalsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwyFungsionalsRelatedByIdtype;
                }

                if($partial && $this->collVldRwyFungsionalsRelatedByIdtype) {
                    foreach($this->collVldRwyFungsionalsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwyFungsionalsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwyFungsionalsRelatedByIdtype = $collVldRwyFungsionalsRelatedByIdtype;
                $this->collVldRwyFungsionalsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwyFungsionalsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwyFungsionalRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwyFungsionalsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwyFungsionalsRelatedByIdtype(PropelCollection $vldRwyFungsionalsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwyFungsionalsRelatedByIdtypeToDelete = $this->getVldRwyFungsionalsRelatedByIdtype(new Criteria(), $con)->diff($vldRwyFungsionalsRelatedByIdtype);

        $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwyFungsionalsRelatedByIdtypeToDelete));

        foreach ($vldRwyFungsionalsRelatedByIdtypeToDelete as $vldRwyFungsionalRelatedByIdtypeRemoved) {
            $vldRwyFungsionalRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwyFungsionalsRelatedByIdtype = null;
        foreach ($vldRwyFungsionalsRelatedByIdtype as $vldRwyFungsionalRelatedByIdtype) {
            $this->addVldRwyFungsionalRelatedByIdtype($vldRwyFungsionalRelatedByIdtype);
        }

        $this->collVldRwyFungsionalsRelatedByIdtype = $vldRwyFungsionalsRelatedByIdtype;
        $this->collVldRwyFungsionalsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwyFungsional objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwyFungsional objects.
     * @throws PropelException
     */
    public function countVldRwyFungsionalsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyFungsionalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyFungsionalsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwyFungsionalsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwyFungsionalsRelatedByIdtype());
            }
            $query = VldRwyFungsionalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwyFungsionalsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwyFungsional object to this object
     * through the VldRwyFungsional foreign key attribute.
     *
     * @param    VldRwyFungsional $l VldRwyFungsional
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwyFungsionalRelatedByIdtype(VldRwyFungsional $l)
    {
        if ($this->collVldRwyFungsionalsRelatedByIdtype === null) {
            $this->initVldRwyFungsionalsRelatedByIdtype();
            $this->collVldRwyFungsionalsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwyFungsionalsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwyFungsionalRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwyFungsionalRelatedByIdtype $vldRwyFungsionalRelatedByIdtype The vldRwyFungsionalRelatedByIdtype object to add.
     */
    protected function doAddVldRwyFungsionalRelatedByIdtype($vldRwyFungsionalRelatedByIdtype)
    {
        $this->collVldRwyFungsionalsRelatedByIdtype[]= $vldRwyFungsionalRelatedByIdtype;
        $vldRwyFungsionalRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwyFungsionalRelatedByIdtype $vldRwyFungsionalRelatedByIdtype The vldRwyFungsionalRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwyFungsionalRelatedByIdtype($vldRwyFungsionalRelatedByIdtype)
    {
        if ($this->getVldRwyFungsionalsRelatedByIdtype()->contains($vldRwyFungsionalRelatedByIdtype)) {
            $this->collVldRwyFungsionalsRelatedByIdtype->remove($this->collVldRwyFungsionalsRelatedByIdtype->search($vldRwyFungsionalRelatedByIdtype));
            if (null === $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwyFungsionalsRelatedByIdtype;
                $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwyFungsionalsRelatedByIdtypeScheduledForDeletion[]= clone $vldRwyFungsionalRelatedByIdtype;
            $vldRwyFungsionalRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyFungsionalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyFungsional[] List of VldRwyFungsional objects
     */
    public function getVldRwyFungsionalsRelatedByIdtypeJoinRwyFungsionalRelatedByRiwayatFungsionalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyFungsionalQuery::create(null, $criteria);
        $query->joinWith('RwyFungsionalRelatedByRiwayatFungsionalId', $join_behavior);

        return $this->getVldRwyFungsionalsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyFungsionalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyFungsional[] List of VldRwyFungsional objects
     */
    public function getVldRwyFungsionalsRelatedByIdtypeJoinRwyFungsionalRelatedByRiwayatFungsionalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyFungsionalQuery::create(null, $criteria);
        $query->joinWith('RwyFungsionalRelatedByRiwayatFungsionalId', $join_behavior);

        return $this->getVldRwyFungsionalsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRombelsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRombelsRelatedByIdtype()
     */
    public function clearVldRombelsRelatedByIdtype()
    {
        $this->collVldRombelsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRombelsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRombelsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRombelsRelatedByIdtype($v = true)
    {
        $this->collVldRombelsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRombelsRelatedByIdtype collection.
     *
     * By default this just sets the collVldRombelsRelatedByIdtype collection to an empty array (like clearcollVldRombelsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRombelsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRombelsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRombelsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRombelsRelatedByIdtype->setModel('VldRombel');
    }

    /**
     * Gets an array of VldRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     * @throws PropelException
     */
    public function getVldRombelsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRombelsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRombelsRelatedByIdtype) {
                // return empty collection
                $this->initVldRombelsRelatedByIdtype();
            } else {
                $collVldRombelsRelatedByIdtype = VldRombelQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRombelsRelatedByIdtypePartial && count($collVldRombelsRelatedByIdtype)) {
                      $this->initVldRombelsRelatedByIdtype(false);

                      foreach($collVldRombelsRelatedByIdtype as $obj) {
                        if (false == $this->collVldRombelsRelatedByIdtype->contains($obj)) {
                          $this->collVldRombelsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRombelsRelatedByIdtypePartial = true;
                    }

                    $collVldRombelsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRombelsRelatedByIdtype;
                }

                if($partial && $this->collVldRombelsRelatedByIdtype) {
                    foreach($this->collVldRombelsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRombelsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRombelsRelatedByIdtype = $collVldRombelsRelatedByIdtype;
                $this->collVldRombelsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRombelsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRombelRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRombelsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRombelsRelatedByIdtype(PropelCollection $vldRombelsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRombelsRelatedByIdtypeToDelete = $this->getVldRombelsRelatedByIdtype(new Criteria(), $con)->diff($vldRombelsRelatedByIdtype);

        $this->vldRombelsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRombelsRelatedByIdtypeToDelete));

        foreach ($vldRombelsRelatedByIdtypeToDelete as $vldRombelRelatedByIdtypeRemoved) {
            $vldRombelRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRombelsRelatedByIdtype = null;
        foreach ($vldRombelsRelatedByIdtype as $vldRombelRelatedByIdtype) {
            $this->addVldRombelRelatedByIdtype($vldRombelRelatedByIdtype);
        }

        $this->collVldRombelsRelatedByIdtype = $vldRombelsRelatedByIdtype;
        $this->collVldRombelsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRombel objects.
     * @throws PropelException
     */
    public function countVldRombelsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRombelsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRombelsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRombelsRelatedByIdtype());
            }
            $query = VldRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRombelsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRombel object to this object
     * through the VldRombel foreign key attribute.
     *
     * @param    VldRombel $l VldRombel
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRombelRelatedByIdtype(VldRombel $l)
    {
        if ($this->collVldRombelsRelatedByIdtype === null) {
            $this->initVldRombelsRelatedByIdtype();
            $this->collVldRombelsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRombelsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRombelRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRombelRelatedByIdtype $vldRombelRelatedByIdtype The vldRombelRelatedByIdtype object to add.
     */
    protected function doAddVldRombelRelatedByIdtype($vldRombelRelatedByIdtype)
    {
        $this->collVldRombelsRelatedByIdtype[]= $vldRombelRelatedByIdtype;
        $vldRombelRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRombelRelatedByIdtype $vldRombelRelatedByIdtype The vldRombelRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRombelRelatedByIdtype($vldRombelRelatedByIdtype)
    {
        if ($this->getVldRombelsRelatedByIdtype()->contains($vldRombelRelatedByIdtype)) {
            $this->collVldRombelsRelatedByIdtype->remove($this->collVldRombelsRelatedByIdtype->search($vldRombelRelatedByIdtype));
            if (null === $this->vldRombelsRelatedByIdtypeScheduledForDeletion) {
                $this->vldRombelsRelatedByIdtypeScheduledForDeletion = clone $this->collVldRombelsRelatedByIdtype;
                $this->vldRombelsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRombelsRelatedByIdtypeScheduledForDeletion[]= clone $vldRombelRelatedByIdtype;
            $vldRombelRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRombelsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsRelatedByIdtypeJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getVldRombelsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRombelsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsRelatedByIdtypeJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getVldRombelsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRombelsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRombelsRelatedByIdtype()
     */
    public function clearVldRombelsRelatedByIdtype()
    {
        $this->collVldRombelsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRombelsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRombelsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRombelsRelatedByIdtype($v = true)
    {
        $this->collVldRombelsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRombelsRelatedByIdtype collection.
     *
     * By default this just sets the collVldRombelsRelatedByIdtype collection to an empty array (like clearcollVldRombelsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRombelsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRombelsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRombelsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRombelsRelatedByIdtype->setModel('VldRombel');
    }

    /**
     * Gets an array of VldRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     * @throws PropelException
     */
    public function getVldRombelsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRombelsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRombelsRelatedByIdtype) {
                // return empty collection
                $this->initVldRombelsRelatedByIdtype();
            } else {
                $collVldRombelsRelatedByIdtype = VldRombelQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRombelsRelatedByIdtypePartial && count($collVldRombelsRelatedByIdtype)) {
                      $this->initVldRombelsRelatedByIdtype(false);

                      foreach($collVldRombelsRelatedByIdtype as $obj) {
                        if (false == $this->collVldRombelsRelatedByIdtype->contains($obj)) {
                          $this->collVldRombelsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRombelsRelatedByIdtypePartial = true;
                    }

                    $collVldRombelsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRombelsRelatedByIdtype;
                }

                if($partial && $this->collVldRombelsRelatedByIdtype) {
                    foreach($this->collVldRombelsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRombelsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRombelsRelatedByIdtype = $collVldRombelsRelatedByIdtype;
                $this->collVldRombelsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRombelsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRombelRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRombelsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRombelsRelatedByIdtype(PropelCollection $vldRombelsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRombelsRelatedByIdtypeToDelete = $this->getVldRombelsRelatedByIdtype(new Criteria(), $con)->diff($vldRombelsRelatedByIdtype);

        $this->vldRombelsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRombelsRelatedByIdtypeToDelete));

        foreach ($vldRombelsRelatedByIdtypeToDelete as $vldRombelRelatedByIdtypeRemoved) {
            $vldRombelRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRombelsRelatedByIdtype = null;
        foreach ($vldRombelsRelatedByIdtype as $vldRombelRelatedByIdtype) {
            $this->addVldRombelRelatedByIdtype($vldRombelRelatedByIdtype);
        }

        $this->collVldRombelsRelatedByIdtype = $vldRombelsRelatedByIdtype;
        $this->collVldRombelsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRombel objects.
     * @throws PropelException
     */
    public function countVldRombelsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRombelsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRombelsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRombelsRelatedByIdtype());
            }
            $query = VldRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRombelsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRombel object to this object
     * through the VldRombel foreign key attribute.
     *
     * @param    VldRombel $l VldRombel
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRombelRelatedByIdtype(VldRombel $l)
    {
        if ($this->collVldRombelsRelatedByIdtype === null) {
            $this->initVldRombelsRelatedByIdtype();
            $this->collVldRombelsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRombelsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRombelRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRombelRelatedByIdtype $vldRombelRelatedByIdtype The vldRombelRelatedByIdtype object to add.
     */
    protected function doAddVldRombelRelatedByIdtype($vldRombelRelatedByIdtype)
    {
        $this->collVldRombelsRelatedByIdtype[]= $vldRombelRelatedByIdtype;
        $vldRombelRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRombelRelatedByIdtype $vldRombelRelatedByIdtype The vldRombelRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRombelRelatedByIdtype($vldRombelRelatedByIdtype)
    {
        if ($this->getVldRombelsRelatedByIdtype()->contains($vldRombelRelatedByIdtype)) {
            $this->collVldRombelsRelatedByIdtype->remove($this->collVldRombelsRelatedByIdtype->search($vldRombelRelatedByIdtype));
            if (null === $this->vldRombelsRelatedByIdtypeScheduledForDeletion) {
                $this->vldRombelsRelatedByIdtypeScheduledForDeletion = clone $this->collVldRombelsRelatedByIdtype;
                $this->vldRombelsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRombelsRelatedByIdtypeScheduledForDeletion[]= clone $vldRombelRelatedByIdtype;
            $vldRombelRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRombelsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsRelatedByIdtypeJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getVldRombelsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRombelsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsRelatedByIdtypeJoinRombonganBelajarRelatedByRombonganBelajarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('RombonganBelajarRelatedByRombonganBelajarId', $join_behavior);

        return $this->getVldRombelsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPtksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPtksRelatedByIdtype()
     */
    public function clearVldPtksRelatedByIdtype()
    {
        $this->collVldPtksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPtksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPtksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPtksRelatedByIdtype($v = true)
    {
        $this->collVldPtksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPtksRelatedByIdtype collection.
     *
     * By default this just sets the collVldPtksRelatedByIdtype collection to an empty array (like clearcollVldPtksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPtksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPtksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPtksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPtksRelatedByIdtype->setModel('VldPtk');
    }

    /**
     * Gets an array of VldPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     * @throws PropelException
     */
    public function getVldPtksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPtksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPtksRelatedByIdtype) {
                // return empty collection
                $this->initVldPtksRelatedByIdtype();
            } else {
                $collVldPtksRelatedByIdtype = VldPtkQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPtksRelatedByIdtypePartial && count($collVldPtksRelatedByIdtype)) {
                      $this->initVldPtksRelatedByIdtype(false);

                      foreach($collVldPtksRelatedByIdtype as $obj) {
                        if (false == $this->collVldPtksRelatedByIdtype->contains($obj)) {
                          $this->collVldPtksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPtksRelatedByIdtypePartial = true;
                    }

                    $collVldPtksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPtksRelatedByIdtype;
                }

                if($partial && $this->collVldPtksRelatedByIdtype) {
                    foreach($this->collVldPtksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPtksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPtksRelatedByIdtype = $collVldPtksRelatedByIdtype;
                $this->collVldPtksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPtksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPtkRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPtksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPtksRelatedByIdtype(PropelCollection $vldPtksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPtksRelatedByIdtypeToDelete = $this->getVldPtksRelatedByIdtype(new Criteria(), $con)->diff($vldPtksRelatedByIdtype);

        $this->vldPtksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPtksRelatedByIdtypeToDelete));

        foreach ($vldPtksRelatedByIdtypeToDelete as $vldPtkRelatedByIdtypeRemoved) {
            $vldPtkRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPtksRelatedByIdtype = null;
        foreach ($vldPtksRelatedByIdtype as $vldPtkRelatedByIdtype) {
            $this->addVldPtkRelatedByIdtype($vldPtkRelatedByIdtype);
        }

        $this->collVldPtksRelatedByIdtype = $vldPtksRelatedByIdtype;
        $this->collVldPtksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPtk objects.
     * @throws PropelException
     */
    public function countVldPtksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPtksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPtksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPtksRelatedByIdtype());
            }
            $query = VldPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPtksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPtk object to this object
     * through the VldPtk foreign key attribute.
     *
     * @param    VldPtk $l VldPtk
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPtkRelatedByIdtype(VldPtk $l)
    {
        if ($this->collVldPtksRelatedByIdtype === null) {
            $this->initVldPtksRelatedByIdtype();
            $this->collVldPtksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPtksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPtkRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPtkRelatedByIdtype $vldPtkRelatedByIdtype The vldPtkRelatedByIdtype object to add.
     */
    protected function doAddVldPtkRelatedByIdtype($vldPtkRelatedByIdtype)
    {
        $this->collVldPtksRelatedByIdtype[]= $vldPtkRelatedByIdtype;
        $vldPtkRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPtkRelatedByIdtype $vldPtkRelatedByIdtype The vldPtkRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPtkRelatedByIdtype($vldPtkRelatedByIdtype)
    {
        if ($this->getVldPtksRelatedByIdtype()->contains($vldPtkRelatedByIdtype)) {
            $this->collVldPtksRelatedByIdtype->remove($this->collVldPtksRelatedByIdtype->search($vldPtkRelatedByIdtype));
            if (null === $this->vldPtksRelatedByIdtypeScheduledForDeletion) {
                $this->vldPtksRelatedByIdtypeScheduledForDeletion = clone $this->collVldPtksRelatedByIdtype;
                $this->vldPtksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPtksRelatedByIdtypeScheduledForDeletion[]= clone $vldPtkRelatedByIdtype;
            $vldPtkRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     */
    public function getVldPtksRelatedByIdtypeJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPtkQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getVldPtksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     */
    public function getVldPtksRelatedByIdtypeJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPtkQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getVldPtksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPtksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPtksRelatedByIdtype()
     */
    public function clearVldPtksRelatedByIdtype()
    {
        $this->collVldPtksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPtksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPtksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPtksRelatedByIdtype($v = true)
    {
        $this->collVldPtksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPtksRelatedByIdtype collection.
     *
     * By default this just sets the collVldPtksRelatedByIdtype collection to an empty array (like clearcollVldPtksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPtksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPtksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPtksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPtksRelatedByIdtype->setModel('VldPtk');
    }

    /**
     * Gets an array of VldPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     * @throws PropelException
     */
    public function getVldPtksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPtksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPtksRelatedByIdtype) {
                // return empty collection
                $this->initVldPtksRelatedByIdtype();
            } else {
                $collVldPtksRelatedByIdtype = VldPtkQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPtksRelatedByIdtypePartial && count($collVldPtksRelatedByIdtype)) {
                      $this->initVldPtksRelatedByIdtype(false);

                      foreach($collVldPtksRelatedByIdtype as $obj) {
                        if (false == $this->collVldPtksRelatedByIdtype->contains($obj)) {
                          $this->collVldPtksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPtksRelatedByIdtypePartial = true;
                    }

                    $collVldPtksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPtksRelatedByIdtype;
                }

                if($partial && $this->collVldPtksRelatedByIdtype) {
                    foreach($this->collVldPtksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPtksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPtksRelatedByIdtype = $collVldPtksRelatedByIdtype;
                $this->collVldPtksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPtksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPtkRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPtksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPtksRelatedByIdtype(PropelCollection $vldPtksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPtksRelatedByIdtypeToDelete = $this->getVldPtksRelatedByIdtype(new Criteria(), $con)->diff($vldPtksRelatedByIdtype);

        $this->vldPtksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPtksRelatedByIdtypeToDelete));

        foreach ($vldPtksRelatedByIdtypeToDelete as $vldPtkRelatedByIdtypeRemoved) {
            $vldPtkRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPtksRelatedByIdtype = null;
        foreach ($vldPtksRelatedByIdtype as $vldPtkRelatedByIdtype) {
            $this->addVldPtkRelatedByIdtype($vldPtkRelatedByIdtype);
        }

        $this->collVldPtksRelatedByIdtype = $vldPtksRelatedByIdtype;
        $this->collVldPtksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPtk objects.
     * @throws PropelException
     */
    public function countVldPtksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPtksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPtksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPtksRelatedByIdtype());
            }
            $query = VldPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPtksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPtk object to this object
     * through the VldPtk foreign key attribute.
     *
     * @param    VldPtk $l VldPtk
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPtkRelatedByIdtype(VldPtk $l)
    {
        if ($this->collVldPtksRelatedByIdtype === null) {
            $this->initVldPtksRelatedByIdtype();
            $this->collVldPtksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPtksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPtkRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPtkRelatedByIdtype $vldPtkRelatedByIdtype The vldPtkRelatedByIdtype object to add.
     */
    protected function doAddVldPtkRelatedByIdtype($vldPtkRelatedByIdtype)
    {
        $this->collVldPtksRelatedByIdtype[]= $vldPtkRelatedByIdtype;
        $vldPtkRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPtkRelatedByIdtype $vldPtkRelatedByIdtype The vldPtkRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPtkRelatedByIdtype($vldPtkRelatedByIdtype)
    {
        if ($this->getVldPtksRelatedByIdtype()->contains($vldPtkRelatedByIdtype)) {
            $this->collVldPtksRelatedByIdtype->remove($this->collVldPtksRelatedByIdtype->search($vldPtkRelatedByIdtype));
            if (null === $this->vldPtksRelatedByIdtypeScheduledForDeletion) {
                $this->vldPtksRelatedByIdtypeScheduledForDeletion = clone $this->collVldPtksRelatedByIdtype;
                $this->vldPtksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPtksRelatedByIdtypeScheduledForDeletion[]= clone $vldPtkRelatedByIdtype;
            $vldPtkRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     */
    public function getVldPtksRelatedByIdtypeJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPtkQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getVldPtksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPtk[] List of VldPtk objects
     */
    public function getVldPtksRelatedByIdtypeJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPtkQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getVldPtksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPrestasisRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPrestasisRelatedByIdtype()
     */
    public function clearVldPrestasisRelatedByIdtype()
    {
        $this->collVldPrestasisRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPrestasisRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPrestasisRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPrestasisRelatedByIdtype($v = true)
    {
        $this->collVldPrestasisRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPrestasisRelatedByIdtype collection.
     *
     * By default this just sets the collVldPrestasisRelatedByIdtype collection to an empty array (like clearcollVldPrestasisRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPrestasisRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPrestasisRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPrestasisRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPrestasisRelatedByIdtype->setModel('VldPrestasi');
    }

    /**
     * Gets an array of VldPrestasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPrestasi[] List of VldPrestasi objects
     * @throws PropelException
     */
    public function getVldPrestasisRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPrestasisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPrestasisRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPrestasisRelatedByIdtype) {
                // return empty collection
                $this->initVldPrestasisRelatedByIdtype();
            } else {
                $collVldPrestasisRelatedByIdtype = VldPrestasiQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPrestasisRelatedByIdtypePartial && count($collVldPrestasisRelatedByIdtype)) {
                      $this->initVldPrestasisRelatedByIdtype(false);

                      foreach($collVldPrestasisRelatedByIdtype as $obj) {
                        if (false == $this->collVldPrestasisRelatedByIdtype->contains($obj)) {
                          $this->collVldPrestasisRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPrestasisRelatedByIdtypePartial = true;
                    }

                    $collVldPrestasisRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPrestasisRelatedByIdtype;
                }

                if($partial && $this->collVldPrestasisRelatedByIdtype) {
                    foreach($this->collVldPrestasisRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPrestasisRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPrestasisRelatedByIdtype = $collVldPrestasisRelatedByIdtype;
                $this->collVldPrestasisRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPrestasisRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPrestasiRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPrestasisRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPrestasisRelatedByIdtype(PropelCollection $vldPrestasisRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPrestasisRelatedByIdtypeToDelete = $this->getVldPrestasisRelatedByIdtype(new Criteria(), $con)->diff($vldPrestasisRelatedByIdtype);

        $this->vldPrestasisRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPrestasisRelatedByIdtypeToDelete));

        foreach ($vldPrestasisRelatedByIdtypeToDelete as $vldPrestasiRelatedByIdtypeRemoved) {
            $vldPrestasiRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPrestasisRelatedByIdtype = null;
        foreach ($vldPrestasisRelatedByIdtype as $vldPrestasiRelatedByIdtype) {
            $this->addVldPrestasiRelatedByIdtype($vldPrestasiRelatedByIdtype);
        }

        $this->collVldPrestasisRelatedByIdtype = $vldPrestasisRelatedByIdtype;
        $this->collVldPrestasisRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPrestasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPrestasi objects.
     * @throws PropelException
     */
    public function countVldPrestasisRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPrestasisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPrestasisRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPrestasisRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPrestasisRelatedByIdtype());
            }
            $query = VldPrestasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPrestasisRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPrestasi object to this object
     * through the VldPrestasi foreign key attribute.
     *
     * @param    VldPrestasi $l VldPrestasi
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPrestasiRelatedByIdtype(VldPrestasi $l)
    {
        if ($this->collVldPrestasisRelatedByIdtype === null) {
            $this->initVldPrestasisRelatedByIdtype();
            $this->collVldPrestasisRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPrestasisRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPrestasiRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPrestasiRelatedByIdtype $vldPrestasiRelatedByIdtype The vldPrestasiRelatedByIdtype object to add.
     */
    protected function doAddVldPrestasiRelatedByIdtype($vldPrestasiRelatedByIdtype)
    {
        $this->collVldPrestasisRelatedByIdtype[]= $vldPrestasiRelatedByIdtype;
        $vldPrestasiRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPrestasiRelatedByIdtype $vldPrestasiRelatedByIdtype The vldPrestasiRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPrestasiRelatedByIdtype($vldPrestasiRelatedByIdtype)
    {
        if ($this->getVldPrestasisRelatedByIdtype()->contains($vldPrestasiRelatedByIdtype)) {
            $this->collVldPrestasisRelatedByIdtype->remove($this->collVldPrestasisRelatedByIdtype->search($vldPrestasiRelatedByIdtype));
            if (null === $this->vldPrestasisRelatedByIdtypeScheduledForDeletion) {
                $this->vldPrestasisRelatedByIdtypeScheduledForDeletion = clone $this->collVldPrestasisRelatedByIdtype;
                $this->vldPrestasisRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPrestasisRelatedByIdtypeScheduledForDeletion[]= clone $vldPrestasiRelatedByIdtype;
            $vldPrestasiRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPrestasisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrestasi[] List of VldPrestasi objects
     */
    public function getVldPrestasisRelatedByIdtypeJoinPrestasiRelatedByPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrestasiQuery::create(null, $criteria);
        $query->joinWith('PrestasiRelatedByPrestasiId', $join_behavior);

        return $this->getVldPrestasisRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPrestasisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrestasi[] List of VldPrestasi objects
     */
    public function getVldPrestasisRelatedByIdtypeJoinPrestasiRelatedByPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrestasiQuery::create(null, $criteria);
        $query->joinWith('PrestasiRelatedByPrestasiId', $join_behavior);

        return $this->getVldPrestasisRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPrestasisRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPrestasisRelatedByIdtype()
     */
    public function clearVldPrestasisRelatedByIdtype()
    {
        $this->collVldPrestasisRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPrestasisRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPrestasisRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPrestasisRelatedByIdtype($v = true)
    {
        $this->collVldPrestasisRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPrestasisRelatedByIdtype collection.
     *
     * By default this just sets the collVldPrestasisRelatedByIdtype collection to an empty array (like clearcollVldPrestasisRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPrestasisRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPrestasisRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPrestasisRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPrestasisRelatedByIdtype->setModel('VldPrestasi');
    }

    /**
     * Gets an array of VldPrestasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPrestasi[] List of VldPrestasi objects
     * @throws PropelException
     */
    public function getVldPrestasisRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPrestasisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPrestasisRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPrestasisRelatedByIdtype) {
                // return empty collection
                $this->initVldPrestasisRelatedByIdtype();
            } else {
                $collVldPrestasisRelatedByIdtype = VldPrestasiQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPrestasisRelatedByIdtypePartial && count($collVldPrestasisRelatedByIdtype)) {
                      $this->initVldPrestasisRelatedByIdtype(false);

                      foreach($collVldPrestasisRelatedByIdtype as $obj) {
                        if (false == $this->collVldPrestasisRelatedByIdtype->contains($obj)) {
                          $this->collVldPrestasisRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPrestasisRelatedByIdtypePartial = true;
                    }

                    $collVldPrestasisRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPrestasisRelatedByIdtype;
                }

                if($partial && $this->collVldPrestasisRelatedByIdtype) {
                    foreach($this->collVldPrestasisRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPrestasisRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPrestasisRelatedByIdtype = $collVldPrestasisRelatedByIdtype;
                $this->collVldPrestasisRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPrestasisRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPrestasiRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPrestasisRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPrestasisRelatedByIdtype(PropelCollection $vldPrestasisRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPrestasisRelatedByIdtypeToDelete = $this->getVldPrestasisRelatedByIdtype(new Criteria(), $con)->diff($vldPrestasisRelatedByIdtype);

        $this->vldPrestasisRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPrestasisRelatedByIdtypeToDelete));

        foreach ($vldPrestasisRelatedByIdtypeToDelete as $vldPrestasiRelatedByIdtypeRemoved) {
            $vldPrestasiRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPrestasisRelatedByIdtype = null;
        foreach ($vldPrestasisRelatedByIdtype as $vldPrestasiRelatedByIdtype) {
            $this->addVldPrestasiRelatedByIdtype($vldPrestasiRelatedByIdtype);
        }

        $this->collVldPrestasisRelatedByIdtype = $vldPrestasisRelatedByIdtype;
        $this->collVldPrestasisRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPrestasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPrestasi objects.
     * @throws PropelException
     */
    public function countVldPrestasisRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPrestasisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPrestasisRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPrestasisRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPrestasisRelatedByIdtype());
            }
            $query = VldPrestasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPrestasisRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPrestasi object to this object
     * through the VldPrestasi foreign key attribute.
     *
     * @param    VldPrestasi $l VldPrestasi
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPrestasiRelatedByIdtype(VldPrestasi $l)
    {
        if ($this->collVldPrestasisRelatedByIdtype === null) {
            $this->initVldPrestasisRelatedByIdtype();
            $this->collVldPrestasisRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPrestasisRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPrestasiRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPrestasiRelatedByIdtype $vldPrestasiRelatedByIdtype The vldPrestasiRelatedByIdtype object to add.
     */
    protected function doAddVldPrestasiRelatedByIdtype($vldPrestasiRelatedByIdtype)
    {
        $this->collVldPrestasisRelatedByIdtype[]= $vldPrestasiRelatedByIdtype;
        $vldPrestasiRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPrestasiRelatedByIdtype $vldPrestasiRelatedByIdtype The vldPrestasiRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPrestasiRelatedByIdtype($vldPrestasiRelatedByIdtype)
    {
        if ($this->getVldPrestasisRelatedByIdtype()->contains($vldPrestasiRelatedByIdtype)) {
            $this->collVldPrestasisRelatedByIdtype->remove($this->collVldPrestasisRelatedByIdtype->search($vldPrestasiRelatedByIdtype));
            if (null === $this->vldPrestasisRelatedByIdtypeScheduledForDeletion) {
                $this->vldPrestasisRelatedByIdtypeScheduledForDeletion = clone $this->collVldPrestasisRelatedByIdtype;
                $this->vldPrestasisRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPrestasisRelatedByIdtypeScheduledForDeletion[]= clone $vldPrestasiRelatedByIdtype;
            $vldPrestasiRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPrestasisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrestasi[] List of VldPrestasi objects
     */
    public function getVldPrestasisRelatedByIdtypeJoinPrestasiRelatedByPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrestasiQuery::create(null, $criteria);
        $query->joinWith('PrestasiRelatedByPrestasiId', $join_behavior);

        return $this->getVldPrestasisRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPrestasisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrestasi[] List of VldPrestasi objects
     */
    public function getVldPrestasisRelatedByIdtypeJoinPrestasiRelatedByPrestasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrestasiQuery::create(null, $criteria);
        $query->joinWith('PrestasiRelatedByPrestasiId', $join_behavior);

        return $this->getVldPrestasisRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPrasaranasRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPrasaranasRelatedByIdtype()
     */
    public function clearVldPrasaranasRelatedByIdtype()
    {
        $this->collVldPrasaranasRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPrasaranasRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPrasaranasRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPrasaranasRelatedByIdtype($v = true)
    {
        $this->collVldPrasaranasRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPrasaranasRelatedByIdtype collection.
     *
     * By default this just sets the collVldPrasaranasRelatedByIdtype collection to an empty array (like clearcollVldPrasaranasRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPrasaranasRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPrasaranasRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPrasaranasRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPrasaranasRelatedByIdtype->setModel('VldPrasarana');
    }

    /**
     * Gets an array of VldPrasarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     * @throws PropelException
     */
    public function getVldPrasaranasRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPrasaranasRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranasRelatedByIdtype) {
                // return empty collection
                $this->initVldPrasaranasRelatedByIdtype();
            } else {
                $collVldPrasaranasRelatedByIdtype = VldPrasaranaQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPrasaranasRelatedByIdtypePartial && count($collVldPrasaranasRelatedByIdtype)) {
                      $this->initVldPrasaranasRelatedByIdtype(false);

                      foreach($collVldPrasaranasRelatedByIdtype as $obj) {
                        if (false == $this->collVldPrasaranasRelatedByIdtype->contains($obj)) {
                          $this->collVldPrasaranasRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPrasaranasRelatedByIdtypePartial = true;
                    }

                    $collVldPrasaranasRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPrasaranasRelatedByIdtype;
                }

                if($partial && $this->collVldPrasaranasRelatedByIdtype) {
                    foreach($this->collVldPrasaranasRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPrasaranasRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPrasaranasRelatedByIdtype = $collVldPrasaranasRelatedByIdtype;
                $this->collVldPrasaranasRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPrasaranasRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPrasaranaRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPrasaranasRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPrasaranasRelatedByIdtype(PropelCollection $vldPrasaranasRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPrasaranasRelatedByIdtypeToDelete = $this->getVldPrasaranasRelatedByIdtype(new Criteria(), $con)->diff($vldPrasaranasRelatedByIdtype);

        $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPrasaranasRelatedByIdtypeToDelete));

        foreach ($vldPrasaranasRelatedByIdtypeToDelete as $vldPrasaranaRelatedByIdtypeRemoved) {
            $vldPrasaranaRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPrasaranasRelatedByIdtype = null;
        foreach ($vldPrasaranasRelatedByIdtype as $vldPrasaranaRelatedByIdtype) {
            $this->addVldPrasaranaRelatedByIdtype($vldPrasaranaRelatedByIdtype);
        }

        $this->collVldPrasaranasRelatedByIdtype = $vldPrasaranasRelatedByIdtype;
        $this->collVldPrasaranasRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPrasarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPrasarana objects.
     * @throws PropelException
     */
    public function countVldPrasaranasRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPrasaranasRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranasRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPrasaranasRelatedByIdtype());
            }
            $query = VldPrasaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPrasaranasRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPrasarana object to this object
     * through the VldPrasarana foreign key attribute.
     *
     * @param    VldPrasarana $l VldPrasarana
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPrasaranaRelatedByIdtype(VldPrasarana $l)
    {
        if ($this->collVldPrasaranasRelatedByIdtype === null) {
            $this->initVldPrasaranasRelatedByIdtype();
            $this->collVldPrasaranasRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPrasaranasRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPrasaranaRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPrasaranaRelatedByIdtype $vldPrasaranaRelatedByIdtype The vldPrasaranaRelatedByIdtype object to add.
     */
    protected function doAddVldPrasaranaRelatedByIdtype($vldPrasaranaRelatedByIdtype)
    {
        $this->collVldPrasaranasRelatedByIdtype[]= $vldPrasaranaRelatedByIdtype;
        $vldPrasaranaRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPrasaranaRelatedByIdtype $vldPrasaranaRelatedByIdtype The vldPrasaranaRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPrasaranaRelatedByIdtype($vldPrasaranaRelatedByIdtype)
    {
        if ($this->getVldPrasaranasRelatedByIdtype()->contains($vldPrasaranaRelatedByIdtype)) {
            $this->collVldPrasaranasRelatedByIdtype->remove($this->collVldPrasaranasRelatedByIdtype->search($vldPrasaranaRelatedByIdtype));
            if (null === $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion) {
                $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion = clone $this->collVldPrasaranasRelatedByIdtype;
                $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion[]= clone $vldPrasaranaRelatedByIdtype;
            $vldPrasaranaRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPrasaranasRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasRelatedByIdtypeJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getVldPrasaranasRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPrasaranasRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasRelatedByIdtypeJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getVldPrasaranasRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPrasaranasRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPrasaranasRelatedByIdtype()
     */
    public function clearVldPrasaranasRelatedByIdtype()
    {
        $this->collVldPrasaranasRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPrasaranasRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPrasaranasRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPrasaranasRelatedByIdtype($v = true)
    {
        $this->collVldPrasaranasRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPrasaranasRelatedByIdtype collection.
     *
     * By default this just sets the collVldPrasaranasRelatedByIdtype collection to an empty array (like clearcollVldPrasaranasRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPrasaranasRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPrasaranasRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPrasaranasRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPrasaranasRelatedByIdtype->setModel('VldPrasarana');
    }

    /**
     * Gets an array of VldPrasarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     * @throws PropelException
     */
    public function getVldPrasaranasRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPrasaranasRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranasRelatedByIdtype) {
                // return empty collection
                $this->initVldPrasaranasRelatedByIdtype();
            } else {
                $collVldPrasaranasRelatedByIdtype = VldPrasaranaQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPrasaranasRelatedByIdtypePartial && count($collVldPrasaranasRelatedByIdtype)) {
                      $this->initVldPrasaranasRelatedByIdtype(false);

                      foreach($collVldPrasaranasRelatedByIdtype as $obj) {
                        if (false == $this->collVldPrasaranasRelatedByIdtype->contains($obj)) {
                          $this->collVldPrasaranasRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPrasaranasRelatedByIdtypePartial = true;
                    }

                    $collVldPrasaranasRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPrasaranasRelatedByIdtype;
                }

                if($partial && $this->collVldPrasaranasRelatedByIdtype) {
                    foreach($this->collVldPrasaranasRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPrasaranasRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPrasaranasRelatedByIdtype = $collVldPrasaranasRelatedByIdtype;
                $this->collVldPrasaranasRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPrasaranasRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPrasaranaRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPrasaranasRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPrasaranasRelatedByIdtype(PropelCollection $vldPrasaranasRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPrasaranasRelatedByIdtypeToDelete = $this->getVldPrasaranasRelatedByIdtype(new Criteria(), $con)->diff($vldPrasaranasRelatedByIdtype);

        $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPrasaranasRelatedByIdtypeToDelete));

        foreach ($vldPrasaranasRelatedByIdtypeToDelete as $vldPrasaranaRelatedByIdtypeRemoved) {
            $vldPrasaranaRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPrasaranasRelatedByIdtype = null;
        foreach ($vldPrasaranasRelatedByIdtype as $vldPrasaranaRelatedByIdtype) {
            $this->addVldPrasaranaRelatedByIdtype($vldPrasaranaRelatedByIdtype);
        }

        $this->collVldPrasaranasRelatedByIdtype = $vldPrasaranasRelatedByIdtype;
        $this->collVldPrasaranasRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPrasarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPrasarana objects.
     * @throws PropelException
     */
    public function countVldPrasaranasRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPrasaranasRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranasRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPrasaranasRelatedByIdtype());
            }
            $query = VldPrasaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPrasaranasRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPrasarana object to this object
     * through the VldPrasarana foreign key attribute.
     *
     * @param    VldPrasarana $l VldPrasarana
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPrasaranaRelatedByIdtype(VldPrasarana $l)
    {
        if ($this->collVldPrasaranasRelatedByIdtype === null) {
            $this->initVldPrasaranasRelatedByIdtype();
            $this->collVldPrasaranasRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPrasaranasRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPrasaranaRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPrasaranaRelatedByIdtype $vldPrasaranaRelatedByIdtype The vldPrasaranaRelatedByIdtype object to add.
     */
    protected function doAddVldPrasaranaRelatedByIdtype($vldPrasaranaRelatedByIdtype)
    {
        $this->collVldPrasaranasRelatedByIdtype[]= $vldPrasaranaRelatedByIdtype;
        $vldPrasaranaRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPrasaranaRelatedByIdtype $vldPrasaranaRelatedByIdtype The vldPrasaranaRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPrasaranaRelatedByIdtype($vldPrasaranaRelatedByIdtype)
    {
        if ($this->getVldPrasaranasRelatedByIdtype()->contains($vldPrasaranaRelatedByIdtype)) {
            $this->collVldPrasaranasRelatedByIdtype->remove($this->collVldPrasaranasRelatedByIdtype->search($vldPrasaranaRelatedByIdtype));
            if (null === $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion) {
                $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion = clone $this->collVldPrasaranasRelatedByIdtype;
                $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPrasaranasRelatedByIdtypeScheduledForDeletion[]= clone $vldPrasaranaRelatedByIdtype;
            $vldPrasaranaRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPrasaranasRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasRelatedByIdtypeJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getVldPrasaranasRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPrasaranasRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasRelatedByIdtypeJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getVldPrasaranasRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPesertaDidiksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPesertaDidiksRelatedByIdtype()
     */
    public function clearVldPesertaDidiksRelatedByIdtype()
    {
        $this->collVldPesertaDidiksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPesertaDidiksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPesertaDidiksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPesertaDidiksRelatedByIdtype($v = true)
    {
        $this->collVldPesertaDidiksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPesertaDidiksRelatedByIdtype collection.
     *
     * By default this just sets the collVldPesertaDidiksRelatedByIdtype collection to an empty array (like clearcollVldPesertaDidiksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPesertaDidiksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPesertaDidiksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPesertaDidiksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPesertaDidiksRelatedByIdtype->setModel('VldPesertaDidik');
    }

    /**
     * Gets an array of VldPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     * @throws PropelException
     */
    public function getVldPesertaDidiksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPesertaDidiksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPesertaDidiksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPesertaDidiksRelatedByIdtype) {
                // return empty collection
                $this->initVldPesertaDidiksRelatedByIdtype();
            } else {
                $collVldPesertaDidiksRelatedByIdtype = VldPesertaDidikQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPesertaDidiksRelatedByIdtypePartial && count($collVldPesertaDidiksRelatedByIdtype)) {
                      $this->initVldPesertaDidiksRelatedByIdtype(false);

                      foreach($collVldPesertaDidiksRelatedByIdtype as $obj) {
                        if (false == $this->collVldPesertaDidiksRelatedByIdtype->contains($obj)) {
                          $this->collVldPesertaDidiksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPesertaDidiksRelatedByIdtypePartial = true;
                    }

                    $collVldPesertaDidiksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPesertaDidiksRelatedByIdtype;
                }

                if($partial && $this->collVldPesertaDidiksRelatedByIdtype) {
                    foreach($this->collVldPesertaDidiksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPesertaDidiksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPesertaDidiksRelatedByIdtype = $collVldPesertaDidiksRelatedByIdtype;
                $this->collVldPesertaDidiksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPesertaDidiksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPesertaDidikRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPesertaDidiksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPesertaDidiksRelatedByIdtype(PropelCollection $vldPesertaDidiksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPesertaDidiksRelatedByIdtypeToDelete = $this->getVldPesertaDidiksRelatedByIdtype(new Criteria(), $con)->diff($vldPesertaDidiksRelatedByIdtype);

        $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPesertaDidiksRelatedByIdtypeToDelete));

        foreach ($vldPesertaDidiksRelatedByIdtypeToDelete as $vldPesertaDidikRelatedByIdtypeRemoved) {
            $vldPesertaDidikRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPesertaDidiksRelatedByIdtype = null;
        foreach ($vldPesertaDidiksRelatedByIdtype as $vldPesertaDidikRelatedByIdtype) {
            $this->addVldPesertaDidikRelatedByIdtype($vldPesertaDidikRelatedByIdtype);
        }

        $this->collVldPesertaDidiksRelatedByIdtype = $vldPesertaDidiksRelatedByIdtype;
        $this->collVldPesertaDidiksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPesertaDidik objects.
     * @throws PropelException
     */
    public function countVldPesertaDidiksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPesertaDidiksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPesertaDidiksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPesertaDidiksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPesertaDidiksRelatedByIdtype());
            }
            $query = VldPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPesertaDidiksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPesertaDidik object to this object
     * through the VldPesertaDidik foreign key attribute.
     *
     * @param    VldPesertaDidik $l VldPesertaDidik
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPesertaDidikRelatedByIdtype(VldPesertaDidik $l)
    {
        if ($this->collVldPesertaDidiksRelatedByIdtype === null) {
            $this->initVldPesertaDidiksRelatedByIdtype();
            $this->collVldPesertaDidiksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPesertaDidiksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPesertaDidikRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPesertaDidikRelatedByIdtype $vldPesertaDidikRelatedByIdtype The vldPesertaDidikRelatedByIdtype object to add.
     */
    protected function doAddVldPesertaDidikRelatedByIdtype($vldPesertaDidikRelatedByIdtype)
    {
        $this->collVldPesertaDidiksRelatedByIdtype[]= $vldPesertaDidikRelatedByIdtype;
        $vldPesertaDidikRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPesertaDidikRelatedByIdtype $vldPesertaDidikRelatedByIdtype The vldPesertaDidikRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPesertaDidikRelatedByIdtype($vldPesertaDidikRelatedByIdtype)
    {
        if ($this->getVldPesertaDidiksRelatedByIdtype()->contains($vldPesertaDidikRelatedByIdtype)) {
            $this->collVldPesertaDidiksRelatedByIdtype->remove($this->collVldPesertaDidiksRelatedByIdtype->search($vldPesertaDidikRelatedByIdtype));
            if (null === $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion) {
                $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion = clone $this->collVldPesertaDidiksRelatedByIdtype;
                $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion[]= clone $vldPesertaDidikRelatedByIdtype;
            $vldPesertaDidikRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPesertaDidiksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     */
    public function getVldPesertaDidiksRelatedByIdtypeJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getVldPesertaDidiksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPesertaDidiksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     */
    public function getVldPesertaDidiksRelatedByIdtypeJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getVldPesertaDidiksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPesertaDidiksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPesertaDidiksRelatedByIdtype()
     */
    public function clearVldPesertaDidiksRelatedByIdtype()
    {
        $this->collVldPesertaDidiksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPesertaDidiksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPesertaDidiksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPesertaDidiksRelatedByIdtype($v = true)
    {
        $this->collVldPesertaDidiksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPesertaDidiksRelatedByIdtype collection.
     *
     * By default this just sets the collVldPesertaDidiksRelatedByIdtype collection to an empty array (like clearcollVldPesertaDidiksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPesertaDidiksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPesertaDidiksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPesertaDidiksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPesertaDidiksRelatedByIdtype->setModel('VldPesertaDidik');
    }

    /**
     * Gets an array of VldPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     * @throws PropelException
     */
    public function getVldPesertaDidiksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPesertaDidiksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPesertaDidiksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPesertaDidiksRelatedByIdtype) {
                // return empty collection
                $this->initVldPesertaDidiksRelatedByIdtype();
            } else {
                $collVldPesertaDidiksRelatedByIdtype = VldPesertaDidikQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPesertaDidiksRelatedByIdtypePartial && count($collVldPesertaDidiksRelatedByIdtype)) {
                      $this->initVldPesertaDidiksRelatedByIdtype(false);

                      foreach($collVldPesertaDidiksRelatedByIdtype as $obj) {
                        if (false == $this->collVldPesertaDidiksRelatedByIdtype->contains($obj)) {
                          $this->collVldPesertaDidiksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPesertaDidiksRelatedByIdtypePartial = true;
                    }

                    $collVldPesertaDidiksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPesertaDidiksRelatedByIdtype;
                }

                if($partial && $this->collVldPesertaDidiksRelatedByIdtype) {
                    foreach($this->collVldPesertaDidiksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPesertaDidiksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPesertaDidiksRelatedByIdtype = $collVldPesertaDidiksRelatedByIdtype;
                $this->collVldPesertaDidiksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPesertaDidiksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPesertaDidikRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPesertaDidiksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPesertaDidiksRelatedByIdtype(PropelCollection $vldPesertaDidiksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPesertaDidiksRelatedByIdtypeToDelete = $this->getVldPesertaDidiksRelatedByIdtype(new Criteria(), $con)->diff($vldPesertaDidiksRelatedByIdtype);

        $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPesertaDidiksRelatedByIdtypeToDelete));

        foreach ($vldPesertaDidiksRelatedByIdtypeToDelete as $vldPesertaDidikRelatedByIdtypeRemoved) {
            $vldPesertaDidikRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPesertaDidiksRelatedByIdtype = null;
        foreach ($vldPesertaDidiksRelatedByIdtype as $vldPesertaDidikRelatedByIdtype) {
            $this->addVldPesertaDidikRelatedByIdtype($vldPesertaDidikRelatedByIdtype);
        }

        $this->collVldPesertaDidiksRelatedByIdtype = $vldPesertaDidiksRelatedByIdtype;
        $this->collVldPesertaDidiksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPesertaDidik objects.
     * @throws PropelException
     */
    public function countVldPesertaDidiksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPesertaDidiksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPesertaDidiksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPesertaDidiksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPesertaDidiksRelatedByIdtype());
            }
            $query = VldPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPesertaDidiksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPesertaDidik object to this object
     * through the VldPesertaDidik foreign key attribute.
     *
     * @param    VldPesertaDidik $l VldPesertaDidik
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPesertaDidikRelatedByIdtype(VldPesertaDidik $l)
    {
        if ($this->collVldPesertaDidiksRelatedByIdtype === null) {
            $this->initVldPesertaDidiksRelatedByIdtype();
            $this->collVldPesertaDidiksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPesertaDidiksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPesertaDidikRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPesertaDidikRelatedByIdtype $vldPesertaDidikRelatedByIdtype The vldPesertaDidikRelatedByIdtype object to add.
     */
    protected function doAddVldPesertaDidikRelatedByIdtype($vldPesertaDidikRelatedByIdtype)
    {
        $this->collVldPesertaDidiksRelatedByIdtype[]= $vldPesertaDidikRelatedByIdtype;
        $vldPesertaDidikRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPesertaDidikRelatedByIdtype $vldPesertaDidikRelatedByIdtype The vldPesertaDidikRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPesertaDidikRelatedByIdtype($vldPesertaDidikRelatedByIdtype)
    {
        if ($this->getVldPesertaDidiksRelatedByIdtype()->contains($vldPesertaDidikRelatedByIdtype)) {
            $this->collVldPesertaDidiksRelatedByIdtype->remove($this->collVldPesertaDidiksRelatedByIdtype->search($vldPesertaDidikRelatedByIdtype));
            if (null === $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion) {
                $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion = clone $this->collVldPesertaDidiksRelatedByIdtype;
                $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPesertaDidiksRelatedByIdtypeScheduledForDeletion[]= clone $vldPesertaDidikRelatedByIdtype;
            $vldPesertaDidikRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPesertaDidiksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     */
    public function getVldPesertaDidiksRelatedByIdtypeJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getVldPesertaDidiksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPesertaDidiksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPesertaDidik[] List of VldPesertaDidik objects
     */
    public function getVldPesertaDidiksRelatedByIdtypeJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getVldPesertaDidiksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPenghargaansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPenghargaansRelatedByIdtype()
     */
    public function clearVldPenghargaansRelatedByIdtype()
    {
        $this->collVldPenghargaansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPenghargaansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPenghargaansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPenghargaansRelatedByIdtype($v = true)
    {
        $this->collVldPenghargaansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPenghargaansRelatedByIdtype collection.
     *
     * By default this just sets the collVldPenghargaansRelatedByIdtype collection to an empty array (like clearcollVldPenghargaansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPenghargaansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPenghargaansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPenghargaansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPenghargaansRelatedByIdtype->setModel('VldPenghargaan');
    }

    /**
     * Gets an array of VldPenghargaan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPenghargaan[] List of VldPenghargaan objects
     * @throws PropelException
     */
    public function getVldPenghargaansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPenghargaansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPenghargaansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPenghargaansRelatedByIdtype) {
                // return empty collection
                $this->initVldPenghargaansRelatedByIdtype();
            } else {
                $collVldPenghargaansRelatedByIdtype = VldPenghargaanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPenghargaansRelatedByIdtypePartial && count($collVldPenghargaansRelatedByIdtype)) {
                      $this->initVldPenghargaansRelatedByIdtype(false);

                      foreach($collVldPenghargaansRelatedByIdtype as $obj) {
                        if (false == $this->collVldPenghargaansRelatedByIdtype->contains($obj)) {
                          $this->collVldPenghargaansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPenghargaansRelatedByIdtypePartial = true;
                    }

                    $collVldPenghargaansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPenghargaansRelatedByIdtype;
                }

                if($partial && $this->collVldPenghargaansRelatedByIdtype) {
                    foreach($this->collVldPenghargaansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPenghargaansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPenghargaansRelatedByIdtype = $collVldPenghargaansRelatedByIdtype;
                $this->collVldPenghargaansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPenghargaansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPenghargaanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPenghargaansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPenghargaansRelatedByIdtype(PropelCollection $vldPenghargaansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPenghargaansRelatedByIdtypeToDelete = $this->getVldPenghargaansRelatedByIdtype(new Criteria(), $con)->diff($vldPenghargaansRelatedByIdtype);

        $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPenghargaansRelatedByIdtypeToDelete));

        foreach ($vldPenghargaansRelatedByIdtypeToDelete as $vldPenghargaanRelatedByIdtypeRemoved) {
            $vldPenghargaanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPenghargaansRelatedByIdtype = null;
        foreach ($vldPenghargaansRelatedByIdtype as $vldPenghargaanRelatedByIdtype) {
            $this->addVldPenghargaanRelatedByIdtype($vldPenghargaanRelatedByIdtype);
        }

        $this->collVldPenghargaansRelatedByIdtype = $vldPenghargaansRelatedByIdtype;
        $this->collVldPenghargaansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPenghargaan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPenghargaan objects.
     * @throws PropelException
     */
    public function countVldPenghargaansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPenghargaansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPenghargaansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPenghargaansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPenghargaansRelatedByIdtype());
            }
            $query = VldPenghargaanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPenghargaansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPenghargaan object to this object
     * through the VldPenghargaan foreign key attribute.
     *
     * @param    VldPenghargaan $l VldPenghargaan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPenghargaanRelatedByIdtype(VldPenghargaan $l)
    {
        if ($this->collVldPenghargaansRelatedByIdtype === null) {
            $this->initVldPenghargaansRelatedByIdtype();
            $this->collVldPenghargaansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPenghargaansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPenghargaanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPenghargaanRelatedByIdtype $vldPenghargaanRelatedByIdtype The vldPenghargaanRelatedByIdtype object to add.
     */
    protected function doAddVldPenghargaanRelatedByIdtype($vldPenghargaanRelatedByIdtype)
    {
        $this->collVldPenghargaansRelatedByIdtype[]= $vldPenghargaanRelatedByIdtype;
        $vldPenghargaanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPenghargaanRelatedByIdtype $vldPenghargaanRelatedByIdtype The vldPenghargaanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPenghargaanRelatedByIdtype($vldPenghargaanRelatedByIdtype)
    {
        if ($this->getVldPenghargaansRelatedByIdtype()->contains($vldPenghargaanRelatedByIdtype)) {
            $this->collVldPenghargaansRelatedByIdtype->remove($this->collVldPenghargaansRelatedByIdtype->search($vldPenghargaanRelatedByIdtype));
            if (null === $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion) {
                $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion = clone $this->collVldPenghargaansRelatedByIdtype;
                $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion[]= clone $vldPenghargaanRelatedByIdtype;
            $vldPenghargaanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPenghargaansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPenghargaan[] List of VldPenghargaan objects
     */
    public function getVldPenghargaansRelatedByIdtypeJoinPenghargaanRelatedByPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPenghargaanQuery::create(null, $criteria);
        $query->joinWith('PenghargaanRelatedByPenghargaanId', $join_behavior);

        return $this->getVldPenghargaansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPenghargaansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPenghargaan[] List of VldPenghargaan objects
     */
    public function getVldPenghargaansRelatedByIdtypeJoinPenghargaanRelatedByPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPenghargaanQuery::create(null, $criteria);
        $query->joinWith('PenghargaanRelatedByPenghargaanId', $join_behavior);

        return $this->getVldPenghargaansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPenghargaansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPenghargaansRelatedByIdtype()
     */
    public function clearVldPenghargaansRelatedByIdtype()
    {
        $this->collVldPenghargaansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPenghargaansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPenghargaansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPenghargaansRelatedByIdtype($v = true)
    {
        $this->collVldPenghargaansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPenghargaansRelatedByIdtype collection.
     *
     * By default this just sets the collVldPenghargaansRelatedByIdtype collection to an empty array (like clearcollVldPenghargaansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPenghargaansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPenghargaansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPenghargaansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPenghargaansRelatedByIdtype->setModel('VldPenghargaan');
    }

    /**
     * Gets an array of VldPenghargaan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPenghargaan[] List of VldPenghargaan objects
     * @throws PropelException
     */
    public function getVldPenghargaansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPenghargaansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPenghargaansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPenghargaansRelatedByIdtype) {
                // return empty collection
                $this->initVldPenghargaansRelatedByIdtype();
            } else {
                $collVldPenghargaansRelatedByIdtype = VldPenghargaanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPenghargaansRelatedByIdtypePartial && count($collVldPenghargaansRelatedByIdtype)) {
                      $this->initVldPenghargaansRelatedByIdtype(false);

                      foreach($collVldPenghargaansRelatedByIdtype as $obj) {
                        if (false == $this->collVldPenghargaansRelatedByIdtype->contains($obj)) {
                          $this->collVldPenghargaansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPenghargaansRelatedByIdtypePartial = true;
                    }

                    $collVldPenghargaansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPenghargaansRelatedByIdtype;
                }

                if($partial && $this->collVldPenghargaansRelatedByIdtype) {
                    foreach($this->collVldPenghargaansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPenghargaansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPenghargaansRelatedByIdtype = $collVldPenghargaansRelatedByIdtype;
                $this->collVldPenghargaansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPenghargaansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPenghargaanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPenghargaansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPenghargaansRelatedByIdtype(PropelCollection $vldPenghargaansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPenghargaansRelatedByIdtypeToDelete = $this->getVldPenghargaansRelatedByIdtype(new Criteria(), $con)->diff($vldPenghargaansRelatedByIdtype);

        $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPenghargaansRelatedByIdtypeToDelete));

        foreach ($vldPenghargaansRelatedByIdtypeToDelete as $vldPenghargaanRelatedByIdtypeRemoved) {
            $vldPenghargaanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPenghargaansRelatedByIdtype = null;
        foreach ($vldPenghargaansRelatedByIdtype as $vldPenghargaanRelatedByIdtype) {
            $this->addVldPenghargaanRelatedByIdtype($vldPenghargaanRelatedByIdtype);
        }

        $this->collVldPenghargaansRelatedByIdtype = $vldPenghargaansRelatedByIdtype;
        $this->collVldPenghargaansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPenghargaan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPenghargaan objects.
     * @throws PropelException
     */
    public function countVldPenghargaansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPenghargaansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPenghargaansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPenghargaansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPenghargaansRelatedByIdtype());
            }
            $query = VldPenghargaanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPenghargaansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPenghargaan object to this object
     * through the VldPenghargaan foreign key attribute.
     *
     * @param    VldPenghargaan $l VldPenghargaan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPenghargaanRelatedByIdtype(VldPenghargaan $l)
    {
        if ($this->collVldPenghargaansRelatedByIdtype === null) {
            $this->initVldPenghargaansRelatedByIdtype();
            $this->collVldPenghargaansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPenghargaansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPenghargaanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPenghargaanRelatedByIdtype $vldPenghargaanRelatedByIdtype The vldPenghargaanRelatedByIdtype object to add.
     */
    protected function doAddVldPenghargaanRelatedByIdtype($vldPenghargaanRelatedByIdtype)
    {
        $this->collVldPenghargaansRelatedByIdtype[]= $vldPenghargaanRelatedByIdtype;
        $vldPenghargaanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPenghargaanRelatedByIdtype $vldPenghargaanRelatedByIdtype The vldPenghargaanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPenghargaanRelatedByIdtype($vldPenghargaanRelatedByIdtype)
    {
        if ($this->getVldPenghargaansRelatedByIdtype()->contains($vldPenghargaanRelatedByIdtype)) {
            $this->collVldPenghargaansRelatedByIdtype->remove($this->collVldPenghargaansRelatedByIdtype->search($vldPenghargaanRelatedByIdtype));
            if (null === $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion) {
                $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion = clone $this->collVldPenghargaansRelatedByIdtype;
                $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPenghargaansRelatedByIdtypeScheduledForDeletion[]= clone $vldPenghargaanRelatedByIdtype;
            $vldPenghargaanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPenghargaansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPenghargaan[] List of VldPenghargaan objects
     */
    public function getVldPenghargaansRelatedByIdtypeJoinPenghargaanRelatedByPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPenghargaanQuery::create(null, $criteria);
        $query->joinWith('PenghargaanRelatedByPenghargaanId', $join_behavior);

        return $this->getVldPenghargaansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPenghargaansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPenghargaan[] List of VldPenghargaan objects
     */
    public function getVldPenghargaansRelatedByIdtypeJoinPenghargaanRelatedByPenghargaanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPenghargaanQuery::create(null, $criteria);
        $query->joinWith('PenghargaanRelatedByPenghargaanId', $join_behavior);

        return $this->getVldPenghargaansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPembelajaransRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPembelajaransRelatedByIdtype()
     */
    public function clearVldPembelajaransRelatedByIdtype()
    {
        $this->collVldPembelajaransRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPembelajaransRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPembelajaransRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPembelajaransRelatedByIdtype($v = true)
    {
        $this->collVldPembelajaransRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPembelajaransRelatedByIdtype collection.
     *
     * By default this just sets the collVldPembelajaransRelatedByIdtype collection to an empty array (like clearcollVldPembelajaransRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPembelajaransRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPembelajaransRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPembelajaransRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPembelajaransRelatedByIdtype->setModel('VldPembelajaran');
    }

    /**
     * Gets an array of VldPembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     * @throws PropelException
     */
    public function getVldPembelajaransRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPembelajaransRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPembelajaransRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPembelajaransRelatedByIdtype) {
                // return empty collection
                $this->initVldPembelajaransRelatedByIdtype();
            } else {
                $collVldPembelajaransRelatedByIdtype = VldPembelajaranQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPembelajaransRelatedByIdtypePartial && count($collVldPembelajaransRelatedByIdtype)) {
                      $this->initVldPembelajaransRelatedByIdtype(false);

                      foreach($collVldPembelajaransRelatedByIdtype as $obj) {
                        if (false == $this->collVldPembelajaransRelatedByIdtype->contains($obj)) {
                          $this->collVldPembelajaransRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPembelajaransRelatedByIdtypePartial = true;
                    }

                    $collVldPembelajaransRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPembelajaransRelatedByIdtype;
                }

                if($partial && $this->collVldPembelajaransRelatedByIdtype) {
                    foreach($this->collVldPembelajaransRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPembelajaransRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPembelajaransRelatedByIdtype = $collVldPembelajaransRelatedByIdtype;
                $this->collVldPembelajaransRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPembelajaransRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPembelajaranRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPembelajaransRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPembelajaransRelatedByIdtype(PropelCollection $vldPembelajaransRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPembelajaransRelatedByIdtypeToDelete = $this->getVldPembelajaransRelatedByIdtype(new Criteria(), $con)->diff($vldPembelajaransRelatedByIdtype);

        $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPembelajaransRelatedByIdtypeToDelete));

        foreach ($vldPembelajaransRelatedByIdtypeToDelete as $vldPembelajaranRelatedByIdtypeRemoved) {
            $vldPembelajaranRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPembelajaransRelatedByIdtype = null;
        foreach ($vldPembelajaransRelatedByIdtype as $vldPembelajaranRelatedByIdtype) {
            $this->addVldPembelajaranRelatedByIdtype($vldPembelajaranRelatedByIdtype);
        }

        $this->collVldPembelajaransRelatedByIdtype = $vldPembelajaransRelatedByIdtype;
        $this->collVldPembelajaransRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPembelajaran objects.
     * @throws PropelException
     */
    public function countVldPembelajaransRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPembelajaransRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPembelajaransRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPembelajaransRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPembelajaransRelatedByIdtype());
            }
            $query = VldPembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPembelajaransRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPembelajaran object to this object
     * through the VldPembelajaran foreign key attribute.
     *
     * @param    VldPembelajaran $l VldPembelajaran
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPembelajaranRelatedByIdtype(VldPembelajaran $l)
    {
        if ($this->collVldPembelajaransRelatedByIdtype === null) {
            $this->initVldPembelajaransRelatedByIdtype();
            $this->collVldPembelajaransRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPembelajaransRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPembelajaranRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPembelajaranRelatedByIdtype $vldPembelajaranRelatedByIdtype The vldPembelajaranRelatedByIdtype object to add.
     */
    protected function doAddVldPembelajaranRelatedByIdtype($vldPembelajaranRelatedByIdtype)
    {
        $this->collVldPembelajaransRelatedByIdtype[]= $vldPembelajaranRelatedByIdtype;
        $vldPembelajaranRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPembelajaranRelatedByIdtype $vldPembelajaranRelatedByIdtype The vldPembelajaranRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPembelajaranRelatedByIdtype($vldPembelajaranRelatedByIdtype)
    {
        if ($this->getVldPembelajaransRelatedByIdtype()->contains($vldPembelajaranRelatedByIdtype)) {
            $this->collVldPembelajaransRelatedByIdtype->remove($this->collVldPembelajaransRelatedByIdtype->search($vldPembelajaranRelatedByIdtype));
            if (null === $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion) {
                $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion = clone $this->collVldPembelajaransRelatedByIdtype;
                $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion[]= clone $vldPembelajaranRelatedByIdtype;
            $vldPembelajaranRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPembelajaransRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     */
    public function getVldPembelajaransRelatedByIdtypeJoinPembelajaranRelatedByPembelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPembelajaranQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByPembelajaranId', $join_behavior);

        return $this->getVldPembelajaransRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPembelajaransRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     */
    public function getVldPembelajaransRelatedByIdtypeJoinPembelajaranRelatedByPembelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPembelajaranQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByPembelajaranId', $join_behavior);

        return $this->getVldPembelajaransRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPembelajaransRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPembelajaransRelatedByIdtype()
     */
    public function clearVldPembelajaransRelatedByIdtype()
    {
        $this->collVldPembelajaransRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPembelajaransRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPembelajaransRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPembelajaransRelatedByIdtype($v = true)
    {
        $this->collVldPembelajaransRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPembelajaransRelatedByIdtype collection.
     *
     * By default this just sets the collVldPembelajaransRelatedByIdtype collection to an empty array (like clearcollVldPembelajaransRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPembelajaransRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPembelajaransRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPembelajaransRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPembelajaransRelatedByIdtype->setModel('VldPembelajaran');
    }

    /**
     * Gets an array of VldPembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     * @throws PropelException
     */
    public function getVldPembelajaransRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPembelajaransRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPembelajaransRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPembelajaransRelatedByIdtype) {
                // return empty collection
                $this->initVldPembelajaransRelatedByIdtype();
            } else {
                $collVldPembelajaransRelatedByIdtype = VldPembelajaranQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPembelajaransRelatedByIdtypePartial && count($collVldPembelajaransRelatedByIdtype)) {
                      $this->initVldPembelajaransRelatedByIdtype(false);

                      foreach($collVldPembelajaransRelatedByIdtype as $obj) {
                        if (false == $this->collVldPembelajaransRelatedByIdtype->contains($obj)) {
                          $this->collVldPembelajaransRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPembelajaransRelatedByIdtypePartial = true;
                    }

                    $collVldPembelajaransRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPembelajaransRelatedByIdtype;
                }

                if($partial && $this->collVldPembelajaransRelatedByIdtype) {
                    foreach($this->collVldPembelajaransRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPembelajaransRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPembelajaransRelatedByIdtype = $collVldPembelajaransRelatedByIdtype;
                $this->collVldPembelajaransRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPembelajaransRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPembelajaranRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPembelajaransRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPembelajaransRelatedByIdtype(PropelCollection $vldPembelajaransRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPembelajaransRelatedByIdtypeToDelete = $this->getVldPembelajaransRelatedByIdtype(new Criteria(), $con)->diff($vldPembelajaransRelatedByIdtype);

        $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPembelajaransRelatedByIdtypeToDelete));

        foreach ($vldPembelajaransRelatedByIdtypeToDelete as $vldPembelajaranRelatedByIdtypeRemoved) {
            $vldPembelajaranRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPembelajaransRelatedByIdtype = null;
        foreach ($vldPembelajaransRelatedByIdtype as $vldPembelajaranRelatedByIdtype) {
            $this->addVldPembelajaranRelatedByIdtype($vldPembelajaranRelatedByIdtype);
        }

        $this->collVldPembelajaransRelatedByIdtype = $vldPembelajaransRelatedByIdtype;
        $this->collVldPembelajaransRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPembelajaran objects.
     * @throws PropelException
     */
    public function countVldPembelajaransRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPembelajaransRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPembelajaransRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPembelajaransRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPembelajaransRelatedByIdtype());
            }
            $query = VldPembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPembelajaransRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPembelajaran object to this object
     * through the VldPembelajaran foreign key attribute.
     *
     * @param    VldPembelajaran $l VldPembelajaran
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPembelajaranRelatedByIdtype(VldPembelajaran $l)
    {
        if ($this->collVldPembelajaransRelatedByIdtype === null) {
            $this->initVldPembelajaransRelatedByIdtype();
            $this->collVldPembelajaransRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPembelajaransRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPembelajaranRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPembelajaranRelatedByIdtype $vldPembelajaranRelatedByIdtype The vldPembelajaranRelatedByIdtype object to add.
     */
    protected function doAddVldPembelajaranRelatedByIdtype($vldPembelajaranRelatedByIdtype)
    {
        $this->collVldPembelajaransRelatedByIdtype[]= $vldPembelajaranRelatedByIdtype;
        $vldPembelajaranRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPembelajaranRelatedByIdtype $vldPembelajaranRelatedByIdtype The vldPembelajaranRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPembelajaranRelatedByIdtype($vldPembelajaranRelatedByIdtype)
    {
        if ($this->getVldPembelajaransRelatedByIdtype()->contains($vldPembelajaranRelatedByIdtype)) {
            $this->collVldPembelajaransRelatedByIdtype->remove($this->collVldPembelajaransRelatedByIdtype->search($vldPembelajaranRelatedByIdtype));
            if (null === $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion) {
                $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion = clone $this->collVldPembelajaransRelatedByIdtype;
                $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPembelajaransRelatedByIdtypeScheduledForDeletion[]= clone $vldPembelajaranRelatedByIdtype;
            $vldPembelajaranRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPembelajaransRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     */
    public function getVldPembelajaransRelatedByIdtypeJoinPembelajaranRelatedByPembelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPembelajaranQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByPembelajaranId', $join_behavior);

        return $this->getVldPembelajaransRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPembelajaransRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPembelajaran[] List of VldPembelajaran objects
     */
    public function getVldPembelajaransRelatedByIdtypeJoinPembelajaranRelatedByPembelajaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPembelajaranQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByPembelajaranId', $join_behavior);

        return $this->getVldPembelajaransRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPdLongsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPdLongsRelatedByIdtype()
     */
    public function clearVldPdLongsRelatedByIdtype()
    {
        $this->collVldPdLongsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPdLongsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPdLongsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPdLongsRelatedByIdtype($v = true)
    {
        $this->collVldPdLongsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPdLongsRelatedByIdtype collection.
     *
     * By default this just sets the collVldPdLongsRelatedByIdtype collection to an empty array (like clearcollVldPdLongsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPdLongsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPdLongsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPdLongsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPdLongsRelatedByIdtype->setModel('VldPdLong');
    }

    /**
     * Gets an array of VldPdLong objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPdLong[] List of VldPdLong objects
     * @throws PropelException
     */
    public function getVldPdLongsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPdLongsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPdLongsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPdLongsRelatedByIdtype) {
                // return empty collection
                $this->initVldPdLongsRelatedByIdtype();
            } else {
                $collVldPdLongsRelatedByIdtype = VldPdLongQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPdLongsRelatedByIdtypePartial && count($collVldPdLongsRelatedByIdtype)) {
                      $this->initVldPdLongsRelatedByIdtype(false);

                      foreach($collVldPdLongsRelatedByIdtype as $obj) {
                        if (false == $this->collVldPdLongsRelatedByIdtype->contains($obj)) {
                          $this->collVldPdLongsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPdLongsRelatedByIdtypePartial = true;
                    }

                    $collVldPdLongsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPdLongsRelatedByIdtype;
                }

                if($partial && $this->collVldPdLongsRelatedByIdtype) {
                    foreach($this->collVldPdLongsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPdLongsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPdLongsRelatedByIdtype = $collVldPdLongsRelatedByIdtype;
                $this->collVldPdLongsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPdLongsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPdLongRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPdLongsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPdLongsRelatedByIdtype(PropelCollection $vldPdLongsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPdLongsRelatedByIdtypeToDelete = $this->getVldPdLongsRelatedByIdtype(new Criteria(), $con)->diff($vldPdLongsRelatedByIdtype);

        $this->vldPdLongsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPdLongsRelatedByIdtypeToDelete));

        foreach ($vldPdLongsRelatedByIdtypeToDelete as $vldPdLongRelatedByIdtypeRemoved) {
            $vldPdLongRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPdLongsRelatedByIdtype = null;
        foreach ($vldPdLongsRelatedByIdtype as $vldPdLongRelatedByIdtype) {
            $this->addVldPdLongRelatedByIdtype($vldPdLongRelatedByIdtype);
        }

        $this->collVldPdLongsRelatedByIdtype = $vldPdLongsRelatedByIdtype;
        $this->collVldPdLongsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPdLong objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPdLong objects.
     * @throws PropelException
     */
    public function countVldPdLongsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPdLongsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPdLongsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPdLongsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPdLongsRelatedByIdtype());
            }
            $query = VldPdLongQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPdLongsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPdLong object to this object
     * through the VldPdLong foreign key attribute.
     *
     * @param    VldPdLong $l VldPdLong
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPdLongRelatedByIdtype(VldPdLong $l)
    {
        if ($this->collVldPdLongsRelatedByIdtype === null) {
            $this->initVldPdLongsRelatedByIdtype();
            $this->collVldPdLongsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPdLongsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPdLongRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPdLongRelatedByIdtype $vldPdLongRelatedByIdtype The vldPdLongRelatedByIdtype object to add.
     */
    protected function doAddVldPdLongRelatedByIdtype($vldPdLongRelatedByIdtype)
    {
        $this->collVldPdLongsRelatedByIdtype[]= $vldPdLongRelatedByIdtype;
        $vldPdLongRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPdLongRelatedByIdtype $vldPdLongRelatedByIdtype The vldPdLongRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPdLongRelatedByIdtype($vldPdLongRelatedByIdtype)
    {
        if ($this->getVldPdLongsRelatedByIdtype()->contains($vldPdLongRelatedByIdtype)) {
            $this->collVldPdLongsRelatedByIdtype->remove($this->collVldPdLongsRelatedByIdtype->search($vldPdLongRelatedByIdtype));
            if (null === $this->vldPdLongsRelatedByIdtypeScheduledForDeletion) {
                $this->vldPdLongsRelatedByIdtypeScheduledForDeletion = clone $this->collVldPdLongsRelatedByIdtype;
                $this->vldPdLongsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPdLongsRelatedByIdtypeScheduledForDeletion[]= clone $vldPdLongRelatedByIdtype;
            $vldPdLongRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPdLongsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPdLong[] List of VldPdLong objects
     */
    public function getVldPdLongsRelatedByIdtypeJoinPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPdLongQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId', $join_behavior);

        return $this->getVldPdLongsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPdLongsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPdLong[] List of VldPdLong objects
     */
    public function getVldPdLongsRelatedByIdtypeJoinPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPdLongQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId', $join_behavior);

        return $this->getVldPdLongsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldPdLongsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldPdLongsRelatedByIdtype()
     */
    public function clearVldPdLongsRelatedByIdtype()
    {
        $this->collVldPdLongsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldPdLongsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldPdLongsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPdLongsRelatedByIdtype($v = true)
    {
        $this->collVldPdLongsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldPdLongsRelatedByIdtype collection.
     *
     * By default this just sets the collVldPdLongsRelatedByIdtype collection to an empty array (like clearcollVldPdLongsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPdLongsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldPdLongsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldPdLongsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldPdLongsRelatedByIdtype->setModel('VldPdLong');
    }

    /**
     * Gets an array of VldPdLong objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPdLong[] List of VldPdLong objects
     * @throws PropelException
     */
    public function getVldPdLongsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPdLongsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPdLongsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPdLongsRelatedByIdtype) {
                // return empty collection
                $this->initVldPdLongsRelatedByIdtype();
            } else {
                $collVldPdLongsRelatedByIdtype = VldPdLongQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPdLongsRelatedByIdtypePartial && count($collVldPdLongsRelatedByIdtype)) {
                      $this->initVldPdLongsRelatedByIdtype(false);

                      foreach($collVldPdLongsRelatedByIdtype as $obj) {
                        if (false == $this->collVldPdLongsRelatedByIdtype->contains($obj)) {
                          $this->collVldPdLongsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldPdLongsRelatedByIdtypePartial = true;
                    }

                    $collVldPdLongsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldPdLongsRelatedByIdtype;
                }

                if($partial && $this->collVldPdLongsRelatedByIdtype) {
                    foreach($this->collVldPdLongsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldPdLongsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldPdLongsRelatedByIdtype = $collVldPdLongsRelatedByIdtype;
                $this->collVldPdLongsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldPdLongsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldPdLongRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPdLongsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldPdLongsRelatedByIdtype(PropelCollection $vldPdLongsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldPdLongsRelatedByIdtypeToDelete = $this->getVldPdLongsRelatedByIdtype(new Criteria(), $con)->diff($vldPdLongsRelatedByIdtype);

        $this->vldPdLongsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldPdLongsRelatedByIdtypeToDelete));

        foreach ($vldPdLongsRelatedByIdtypeToDelete as $vldPdLongRelatedByIdtypeRemoved) {
            $vldPdLongRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldPdLongsRelatedByIdtype = null;
        foreach ($vldPdLongsRelatedByIdtype as $vldPdLongRelatedByIdtype) {
            $this->addVldPdLongRelatedByIdtype($vldPdLongRelatedByIdtype);
        }

        $this->collVldPdLongsRelatedByIdtype = $vldPdLongsRelatedByIdtype;
        $this->collVldPdLongsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPdLong objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPdLong objects.
     * @throws PropelException
     */
    public function countVldPdLongsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPdLongsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldPdLongsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPdLongsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPdLongsRelatedByIdtype());
            }
            $query = VldPdLongQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldPdLongsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldPdLong object to this object
     * through the VldPdLong foreign key attribute.
     *
     * @param    VldPdLong $l VldPdLong
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldPdLongRelatedByIdtype(VldPdLong $l)
    {
        if ($this->collVldPdLongsRelatedByIdtype === null) {
            $this->initVldPdLongsRelatedByIdtype();
            $this->collVldPdLongsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldPdLongsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPdLongRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldPdLongRelatedByIdtype $vldPdLongRelatedByIdtype The vldPdLongRelatedByIdtype object to add.
     */
    protected function doAddVldPdLongRelatedByIdtype($vldPdLongRelatedByIdtype)
    {
        $this->collVldPdLongsRelatedByIdtype[]= $vldPdLongRelatedByIdtype;
        $vldPdLongRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldPdLongRelatedByIdtype $vldPdLongRelatedByIdtype The vldPdLongRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldPdLongRelatedByIdtype($vldPdLongRelatedByIdtype)
    {
        if ($this->getVldPdLongsRelatedByIdtype()->contains($vldPdLongRelatedByIdtype)) {
            $this->collVldPdLongsRelatedByIdtype->remove($this->collVldPdLongsRelatedByIdtype->search($vldPdLongRelatedByIdtype));
            if (null === $this->vldPdLongsRelatedByIdtypeScheduledForDeletion) {
                $this->vldPdLongsRelatedByIdtypeScheduledForDeletion = clone $this->collVldPdLongsRelatedByIdtype;
                $this->vldPdLongsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldPdLongsRelatedByIdtypeScheduledForDeletion[]= clone $vldPdLongRelatedByIdtype;
            $vldPdLongRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPdLongsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPdLong[] List of VldPdLong objects
     */
    public function getVldPdLongsRelatedByIdtypeJoinPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPdLongQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId', $join_behavior);

        return $this->getVldPdLongsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldPdLongsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPdLong[] List of VldPdLong objects
     */
    public function getVldPdLongsRelatedByIdtypeJoinPesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPdLongQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikLongitudinalRelatedByPesertaDidikIdSemesterId', $join_behavior);

        return $this->getVldPdLongsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldNonsekolahsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldNonsekolahsRelatedByIdtype()
     */
    public function clearVldNonsekolahsRelatedByIdtype()
    {
        $this->collVldNonsekolahsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldNonsekolahsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldNonsekolahsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldNonsekolahsRelatedByIdtype($v = true)
    {
        $this->collVldNonsekolahsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldNonsekolahsRelatedByIdtype collection.
     *
     * By default this just sets the collVldNonsekolahsRelatedByIdtype collection to an empty array (like clearcollVldNonsekolahsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldNonsekolahsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldNonsekolahsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldNonsekolahsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldNonsekolahsRelatedByIdtype->setModel('VldNonsekolah');
    }

    /**
     * Gets an array of VldNonsekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldNonsekolah[] List of VldNonsekolah objects
     * @throws PropelException
     */
    public function getVldNonsekolahsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldNonsekolahsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNonsekolahsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldNonsekolahsRelatedByIdtype) {
                // return empty collection
                $this->initVldNonsekolahsRelatedByIdtype();
            } else {
                $collVldNonsekolahsRelatedByIdtype = VldNonsekolahQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldNonsekolahsRelatedByIdtypePartial && count($collVldNonsekolahsRelatedByIdtype)) {
                      $this->initVldNonsekolahsRelatedByIdtype(false);

                      foreach($collVldNonsekolahsRelatedByIdtype as $obj) {
                        if (false == $this->collVldNonsekolahsRelatedByIdtype->contains($obj)) {
                          $this->collVldNonsekolahsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldNonsekolahsRelatedByIdtypePartial = true;
                    }

                    $collVldNonsekolahsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldNonsekolahsRelatedByIdtype;
                }

                if($partial && $this->collVldNonsekolahsRelatedByIdtype) {
                    foreach($this->collVldNonsekolahsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldNonsekolahsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldNonsekolahsRelatedByIdtype = $collVldNonsekolahsRelatedByIdtype;
                $this->collVldNonsekolahsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldNonsekolahsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldNonsekolahRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldNonsekolahsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldNonsekolahsRelatedByIdtype(PropelCollection $vldNonsekolahsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldNonsekolahsRelatedByIdtypeToDelete = $this->getVldNonsekolahsRelatedByIdtype(new Criteria(), $con)->diff($vldNonsekolahsRelatedByIdtype);

        $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldNonsekolahsRelatedByIdtypeToDelete));

        foreach ($vldNonsekolahsRelatedByIdtypeToDelete as $vldNonsekolahRelatedByIdtypeRemoved) {
            $vldNonsekolahRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldNonsekolahsRelatedByIdtype = null;
        foreach ($vldNonsekolahsRelatedByIdtype as $vldNonsekolahRelatedByIdtype) {
            $this->addVldNonsekolahRelatedByIdtype($vldNonsekolahRelatedByIdtype);
        }

        $this->collVldNonsekolahsRelatedByIdtype = $vldNonsekolahsRelatedByIdtype;
        $this->collVldNonsekolahsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldNonsekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldNonsekolah objects.
     * @throws PropelException
     */
    public function countVldNonsekolahsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldNonsekolahsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNonsekolahsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldNonsekolahsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldNonsekolahsRelatedByIdtype());
            }
            $query = VldNonsekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldNonsekolahsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldNonsekolah object to this object
     * through the VldNonsekolah foreign key attribute.
     *
     * @param    VldNonsekolah $l VldNonsekolah
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldNonsekolahRelatedByIdtype(VldNonsekolah $l)
    {
        if ($this->collVldNonsekolahsRelatedByIdtype === null) {
            $this->initVldNonsekolahsRelatedByIdtype();
            $this->collVldNonsekolahsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldNonsekolahsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldNonsekolahRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldNonsekolahRelatedByIdtype $vldNonsekolahRelatedByIdtype The vldNonsekolahRelatedByIdtype object to add.
     */
    protected function doAddVldNonsekolahRelatedByIdtype($vldNonsekolahRelatedByIdtype)
    {
        $this->collVldNonsekolahsRelatedByIdtype[]= $vldNonsekolahRelatedByIdtype;
        $vldNonsekolahRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldNonsekolahRelatedByIdtype $vldNonsekolahRelatedByIdtype The vldNonsekolahRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldNonsekolahRelatedByIdtype($vldNonsekolahRelatedByIdtype)
    {
        if ($this->getVldNonsekolahsRelatedByIdtype()->contains($vldNonsekolahRelatedByIdtype)) {
            $this->collVldNonsekolahsRelatedByIdtype->remove($this->collVldNonsekolahsRelatedByIdtype->search($vldNonsekolahRelatedByIdtype));
            if (null === $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion) {
                $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion = clone $this->collVldNonsekolahsRelatedByIdtype;
                $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion[]= clone $vldNonsekolahRelatedByIdtype;
            $vldNonsekolahRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldNonsekolahsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNonsekolah[] List of VldNonsekolah objects
     */
    public function getVldNonsekolahsRelatedByIdtypeJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNonsekolahQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getVldNonsekolahsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldNonsekolahsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNonsekolah[] List of VldNonsekolah objects
     */
    public function getVldNonsekolahsRelatedByIdtypeJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNonsekolahQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getVldNonsekolahsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldNonsekolahsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldNonsekolahsRelatedByIdtype()
     */
    public function clearVldNonsekolahsRelatedByIdtype()
    {
        $this->collVldNonsekolahsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldNonsekolahsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldNonsekolahsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldNonsekolahsRelatedByIdtype($v = true)
    {
        $this->collVldNonsekolahsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldNonsekolahsRelatedByIdtype collection.
     *
     * By default this just sets the collVldNonsekolahsRelatedByIdtype collection to an empty array (like clearcollVldNonsekolahsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldNonsekolahsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldNonsekolahsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldNonsekolahsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldNonsekolahsRelatedByIdtype->setModel('VldNonsekolah');
    }

    /**
     * Gets an array of VldNonsekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldNonsekolah[] List of VldNonsekolah objects
     * @throws PropelException
     */
    public function getVldNonsekolahsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldNonsekolahsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNonsekolahsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldNonsekolahsRelatedByIdtype) {
                // return empty collection
                $this->initVldNonsekolahsRelatedByIdtype();
            } else {
                $collVldNonsekolahsRelatedByIdtype = VldNonsekolahQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldNonsekolahsRelatedByIdtypePartial && count($collVldNonsekolahsRelatedByIdtype)) {
                      $this->initVldNonsekolahsRelatedByIdtype(false);

                      foreach($collVldNonsekolahsRelatedByIdtype as $obj) {
                        if (false == $this->collVldNonsekolahsRelatedByIdtype->contains($obj)) {
                          $this->collVldNonsekolahsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldNonsekolahsRelatedByIdtypePartial = true;
                    }

                    $collVldNonsekolahsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldNonsekolahsRelatedByIdtype;
                }

                if($partial && $this->collVldNonsekolahsRelatedByIdtype) {
                    foreach($this->collVldNonsekolahsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldNonsekolahsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldNonsekolahsRelatedByIdtype = $collVldNonsekolahsRelatedByIdtype;
                $this->collVldNonsekolahsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldNonsekolahsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldNonsekolahRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldNonsekolahsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldNonsekolahsRelatedByIdtype(PropelCollection $vldNonsekolahsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldNonsekolahsRelatedByIdtypeToDelete = $this->getVldNonsekolahsRelatedByIdtype(new Criteria(), $con)->diff($vldNonsekolahsRelatedByIdtype);

        $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldNonsekolahsRelatedByIdtypeToDelete));

        foreach ($vldNonsekolahsRelatedByIdtypeToDelete as $vldNonsekolahRelatedByIdtypeRemoved) {
            $vldNonsekolahRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldNonsekolahsRelatedByIdtype = null;
        foreach ($vldNonsekolahsRelatedByIdtype as $vldNonsekolahRelatedByIdtype) {
            $this->addVldNonsekolahRelatedByIdtype($vldNonsekolahRelatedByIdtype);
        }

        $this->collVldNonsekolahsRelatedByIdtype = $vldNonsekolahsRelatedByIdtype;
        $this->collVldNonsekolahsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldNonsekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldNonsekolah objects.
     * @throws PropelException
     */
    public function countVldNonsekolahsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldNonsekolahsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNonsekolahsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldNonsekolahsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldNonsekolahsRelatedByIdtype());
            }
            $query = VldNonsekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldNonsekolahsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldNonsekolah object to this object
     * through the VldNonsekolah foreign key attribute.
     *
     * @param    VldNonsekolah $l VldNonsekolah
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldNonsekolahRelatedByIdtype(VldNonsekolah $l)
    {
        if ($this->collVldNonsekolahsRelatedByIdtype === null) {
            $this->initVldNonsekolahsRelatedByIdtype();
            $this->collVldNonsekolahsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldNonsekolahsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldNonsekolahRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldNonsekolahRelatedByIdtype $vldNonsekolahRelatedByIdtype The vldNonsekolahRelatedByIdtype object to add.
     */
    protected function doAddVldNonsekolahRelatedByIdtype($vldNonsekolahRelatedByIdtype)
    {
        $this->collVldNonsekolahsRelatedByIdtype[]= $vldNonsekolahRelatedByIdtype;
        $vldNonsekolahRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldNonsekolahRelatedByIdtype $vldNonsekolahRelatedByIdtype The vldNonsekolahRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldNonsekolahRelatedByIdtype($vldNonsekolahRelatedByIdtype)
    {
        if ($this->getVldNonsekolahsRelatedByIdtype()->contains($vldNonsekolahRelatedByIdtype)) {
            $this->collVldNonsekolahsRelatedByIdtype->remove($this->collVldNonsekolahsRelatedByIdtype->search($vldNonsekolahRelatedByIdtype));
            if (null === $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion) {
                $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion = clone $this->collVldNonsekolahsRelatedByIdtype;
                $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldNonsekolahsRelatedByIdtypeScheduledForDeletion[]= clone $vldNonsekolahRelatedByIdtype;
            $vldNonsekolahRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldNonsekolahsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNonsekolah[] List of VldNonsekolah objects
     */
    public function getVldNonsekolahsRelatedByIdtypeJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNonsekolahQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getVldNonsekolahsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldNonsekolahsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNonsekolah[] List of VldNonsekolah objects
     */
    public function getVldNonsekolahsRelatedByIdtypeJoinLembagaNonSekolahRelatedByLembagaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNonsekolahQuery::create(null, $criteria);
        $query->joinWith('LembagaNonSekolahRelatedByLembagaId', $join_behavior);

        return $this->getVldNonsekolahsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldNilaiTestsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldNilaiTestsRelatedByIdtype()
     */
    public function clearVldNilaiTestsRelatedByIdtype()
    {
        $this->collVldNilaiTestsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldNilaiTestsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldNilaiTestsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldNilaiTestsRelatedByIdtype($v = true)
    {
        $this->collVldNilaiTestsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldNilaiTestsRelatedByIdtype collection.
     *
     * By default this just sets the collVldNilaiTestsRelatedByIdtype collection to an empty array (like clearcollVldNilaiTestsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldNilaiTestsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldNilaiTestsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldNilaiTestsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldNilaiTestsRelatedByIdtype->setModel('VldNilaiTest');
    }

    /**
     * Gets an array of VldNilaiTest objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     * @throws PropelException
     */
    public function getVldNilaiTestsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiTestsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNilaiTestsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiTestsRelatedByIdtype) {
                // return empty collection
                $this->initVldNilaiTestsRelatedByIdtype();
            } else {
                $collVldNilaiTestsRelatedByIdtype = VldNilaiTestQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldNilaiTestsRelatedByIdtypePartial && count($collVldNilaiTestsRelatedByIdtype)) {
                      $this->initVldNilaiTestsRelatedByIdtype(false);

                      foreach($collVldNilaiTestsRelatedByIdtype as $obj) {
                        if (false == $this->collVldNilaiTestsRelatedByIdtype->contains($obj)) {
                          $this->collVldNilaiTestsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldNilaiTestsRelatedByIdtypePartial = true;
                    }

                    $collVldNilaiTestsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldNilaiTestsRelatedByIdtype;
                }

                if($partial && $this->collVldNilaiTestsRelatedByIdtype) {
                    foreach($this->collVldNilaiTestsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldNilaiTestsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldNilaiTestsRelatedByIdtype = $collVldNilaiTestsRelatedByIdtype;
                $this->collVldNilaiTestsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldNilaiTestsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldNilaiTestRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldNilaiTestsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldNilaiTestsRelatedByIdtype(PropelCollection $vldNilaiTestsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldNilaiTestsRelatedByIdtypeToDelete = $this->getVldNilaiTestsRelatedByIdtype(new Criteria(), $con)->diff($vldNilaiTestsRelatedByIdtype);

        $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldNilaiTestsRelatedByIdtypeToDelete));

        foreach ($vldNilaiTestsRelatedByIdtypeToDelete as $vldNilaiTestRelatedByIdtypeRemoved) {
            $vldNilaiTestRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldNilaiTestsRelatedByIdtype = null;
        foreach ($vldNilaiTestsRelatedByIdtype as $vldNilaiTestRelatedByIdtype) {
            $this->addVldNilaiTestRelatedByIdtype($vldNilaiTestRelatedByIdtype);
        }

        $this->collVldNilaiTestsRelatedByIdtype = $vldNilaiTestsRelatedByIdtype;
        $this->collVldNilaiTestsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldNilaiTest objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldNilaiTest objects.
     * @throws PropelException
     */
    public function countVldNilaiTestsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiTestsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNilaiTestsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiTestsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldNilaiTestsRelatedByIdtype());
            }
            $query = VldNilaiTestQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldNilaiTestsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldNilaiTest object to this object
     * through the VldNilaiTest foreign key attribute.
     *
     * @param    VldNilaiTest $l VldNilaiTest
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldNilaiTestRelatedByIdtype(VldNilaiTest $l)
    {
        if ($this->collVldNilaiTestsRelatedByIdtype === null) {
            $this->initVldNilaiTestsRelatedByIdtype();
            $this->collVldNilaiTestsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldNilaiTestsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldNilaiTestRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldNilaiTestRelatedByIdtype $vldNilaiTestRelatedByIdtype The vldNilaiTestRelatedByIdtype object to add.
     */
    protected function doAddVldNilaiTestRelatedByIdtype($vldNilaiTestRelatedByIdtype)
    {
        $this->collVldNilaiTestsRelatedByIdtype[]= $vldNilaiTestRelatedByIdtype;
        $vldNilaiTestRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldNilaiTestRelatedByIdtype $vldNilaiTestRelatedByIdtype The vldNilaiTestRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldNilaiTestRelatedByIdtype($vldNilaiTestRelatedByIdtype)
    {
        if ($this->getVldNilaiTestsRelatedByIdtype()->contains($vldNilaiTestRelatedByIdtype)) {
            $this->collVldNilaiTestsRelatedByIdtype->remove($this->collVldNilaiTestsRelatedByIdtype->search($vldNilaiTestRelatedByIdtype));
            if (null === $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion) {
                $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion = clone $this->collVldNilaiTestsRelatedByIdtype;
                $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion[]= clone $vldNilaiTestRelatedByIdtype;
            $vldNilaiTestRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldNilaiTestsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     */
    public function getVldNilaiTestsRelatedByIdtypeJoinNilaiTestRelatedByNilaiTestId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNilaiTestQuery::create(null, $criteria);
        $query->joinWith('NilaiTestRelatedByNilaiTestId', $join_behavior);

        return $this->getVldNilaiTestsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldNilaiTestsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     */
    public function getVldNilaiTestsRelatedByIdtypeJoinNilaiTestRelatedByNilaiTestId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNilaiTestQuery::create(null, $criteria);
        $query->joinWith('NilaiTestRelatedByNilaiTestId', $join_behavior);

        return $this->getVldNilaiTestsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldNilaiTestsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldNilaiTestsRelatedByIdtype()
     */
    public function clearVldNilaiTestsRelatedByIdtype()
    {
        $this->collVldNilaiTestsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldNilaiTestsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldNilaiTestsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldNilaiTestsRelatedByIdtype($v = true)
    {
        $this->collVldNilaiTestsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldNilaiTestsRelatedByIdtype collection.
     *
     * By default this just sets the collVldNilaiTestsRelatedByIdtype collection to an empty array (like clearcollVldNilaiTestsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldNilaiTestsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldNilaiTestsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldNilaiTestsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldNilaiTestsRelatedByIdtype->setModel('VldNilaiTest');
    }

    /**
     * Gets an array of VldNilaiTest objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     * @throws PropelException
     */
    public function getVldNilaiTestsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiTestsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNilaiTestsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiTestsRelatedByIdtype) {
                // return empty collection
                $this->initVldNilaiTestsRelatedByIdtype();
            } else {
                $collVldNilaiTestsRelatedByIdtype = VldNilaiTestQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldNilaiTestsRelatedByIdtypePartial && count($collVldNilaiTestsRelatedByIdtype)) {
                      $this->initVldNilaiTestsRelatedByIdtype(false);

                      foreach($collVldNilaiTestsRelatedByIdtype as $obj) {
                        if (false == $this->collVldNilaiTestsRelatedByIdtype->contains($obj)) {
                          $this->collVldNilaiTestsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldNilaiTestsRelatedByIdtypePartial = true;
                    }

                    $collVldNilaiTestsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldNilaiTestsRelatedByIdtype;
                }

                if($partial && $this->collVldNilaiTestsRelatedByIdtype) {
                    foreach($this->collVldNilaiTestsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldNilaiTestsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldNilaiTestsRelatedByIdtype = $collVldNilaiTestsRelatedByIdtype;
                $this->collVldNilaiTestsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldNilaiTestsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldNilaiTestRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldNilaiTestsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldNilaiTestsRelatedByIdtype(PropelCollection $vldNilaiTestsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldNilaiTestsRelatedByIdtypeToDelete = $this->getVldNilaiTestsRelatedByIdtype(new Criteria(), $con)->diff($vldNilaiTestsRelatedByIdtype);

        $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldNilaiTestsRelatedByIdtypeToDelete));

        foreach ($vldNilaiTestsRelatedByIdtypeToDelete as $vldNilaiTestRelatedByIdtypeRemoved) {
            $vldNilaiTestRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldNilaiTestsRelatedByIdtype = null;
        foreach ($vldNilaiTestsRelatedByIdtype as $vldNilaiTestRelatedByIdtype) {
            $this->addVldNilaiTestRelatedByIdtype($vldNilaiTestRelatedByIdtype);
        }

        $this->collVldNilaiTestsRelatedByIdtype = $vldNilaiTestsRelatedByIdtype;
        $this->collVldNilaiTestsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldNilaiTest objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldNilaiTest objects.
     * @throws PropelException
     */
    public function countVldNilaiTestsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiTestsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNilaiTestsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiTestsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldNilaiTestsRelatedByIdtype());
            }
            $query = VldNilaiTestQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldNilaiTestsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldNilaiTest object to this object
     * through the VldNilaiTest foreign key attribute.
     *
     * @param    VldNilaiTest $l VldNilaiTest
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldNilaiTestRelatedByIdtype(VldNilaiTest $l)
    {
        if ($this->collVldNilaiTestsRelatedByIdtype === null) {
            $this->initVldNilaiTestsRelatedByIdtype();
            $this->collVldNilaiTestsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldNilaiTestsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldNilaiTestRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldNilaiTestRelatedByIdtype $vldNilaiTestRelatedByIdtype The vldNilaiTestRelatedByIdtype object to add.
     */
    protected function doAddVldNilaiTestRelatedByIdtype($vldNilaiTestRelatedByIdtype)
    {
        $this->collVldNilaiTestsRelatedByIdtype[]= $vldNilaiTestRelatedByIdtype;
        $vldNilaiTestRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldNilaiTestRelatedByIdtype $vldNilaiTestRelatedByIdtype The vldNilaiTestRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldNilaiTestRelatedByIdtype($vldNilaiTestRelatedByIdtype)
    {
        if ($this->getVldNilaiTestsRelatedByIdtype()->contains($vldNilaiTestRelatedByIdtype)) {
            $this->collVldNilaiTestsRelatedByIdtype->remove($this->collVldNilaiTestsRelatedByIdtype->search($vldNilaiTestRelatedByIdtype));
            if (null === $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion) {
                $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion = clone $this->collVldNilaiTestsRelatedByIdtype;
                $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldNilaiTestsRelatedByIdtypeScheduledForDeletion[]= clone $vldNilaiTestRelatedByIdtype;
            $vldNilaiTestRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldNilaiTestsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     */
    public function getVldNilaiTestsRelatedByIdtypeJoinNilaiTestRelatedByNilaiTestId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNilaiTestQuery::create(null, $criteria);
        $query->joinWith('NilaiTestRelatedByNilaiTestId', $join_behavior);

        return $this->getVldNilaiTestsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldNilaiTestsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldNilaiTest[] List of VldNilaiTest objects
     */
    public function getVldNilaiTestsRelatedByIdtypeJoinNilaiTestRelatedByNilaiTestId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldNilaiTestQuery::create(null, $criteria);
        $query->joinWith('NilaiTestRelatedByNilaiTestId', $join_behavior);

        return $this->getVldNilaiTestsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldNilaiRaporsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldNilaiRaporsRelatedByIdtype()
     */
    public function clearVldNilaiRaporsRelatedByIdtype()
    {
        $this->collVldNilaiRaporsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldNilaiRaporsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldNilaiRaporsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldNilaiRaporsRelatedByIdtype($v = true)
    {
        $this->collVldNilaiRaporsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldNilaiRaporsRelatedByIdtype collection.
     *
     * By default this just sets the collVldNilaiRaporsRelatedByIdtype collection to an empty array (like clearcollVldNilaiRaporsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldNilaiRaporsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldNilaiRaporsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldNilaiRaporsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldNilaiRaporsRelatedByIdtype->setModel('VldNilaiRapor');
    }

    /**
     * Gets an array of VldNilaiRapor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldNilaiRapor[] List of VldNilaiRapor objects
     * @throws PropelException
     */
    public function getVldNilaiRaporsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiRaporsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNilaiRaporsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiRaporsRelatedByIdtype) {
                // return empty collection
                $this->initVldNilaiRaporsRelatedByIdtype();
            } else {
                $collVldNilaiRaporsRelatedByIdtype = VldNilaiRaporQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldNilaiRaporsRelatedByIdtypePartial && count($collVldNilaiRaporsRelatedByIdtype)) {
                      $this->initVldNilaiRaporsRelatedByIdtype(false);

                      foreach($collVldNilaiRaporsRelatedByIdtype as $obj) {
                        if (false == $this->collVldNilaiRaporsRelatedByIdtype->contains($obj)) {
                          $this->collVldNilaiRaporsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldNilaiRaporsRelatedByIdtypePartial = true;
                    }

                    $collVldNilaiRaporsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldNilaiRaporsRelatedByIdtype;
                }

                if($partial && $this->collVldNilaiRaporsRelatedByIdtype) {
                    foreach($this->collVldNilaiRaporsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldNilaiRaporsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldNilaiRaporsRelatedByIdtype = $collVldNilaiRaporsRelatedByIdtype;
                $this->collVldNilaiRaporsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldNilaiRaporsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldNilaiRaporRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldNilaiRaporsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldNilaiRaporsRelatedByIdtype(PropelCollection $vldNilaiRaporsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldNilaiRaporsRelatedByIdtypeToDelete = $this->getVldNilaiRaporsRelatedByIdtype(new Criteria(), $con)->diff($vldNilaiRaporsRelatedByIdtype);

        $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldNilaiRaporsRelatedByIdtypeToDelete));

        foreach ($vldNilaiRaporsRelatedByIdtypeToDelete as $vldNilaiRaporRelatedByIdtypeRemoved) {
            $vldNilaiRaporRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldNilaiRaporsRelatedByIdtype = null;
        foreach ($vldNilaiRaporsRelatedByIdtype as $vldNilaiRaporRelatedByIdtype) {
            $this->addVldNilaiRaporRelatedByIdtype($vldNilaiRaporRelatedByIdtype);
        }

        $this->collVldNilaiRaporsRelatedByIdtype = $vldNilaiRaporsRelatedByIdtype;
        $this->collVldNilaiRaporsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldNilaiRapor objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldNilaiRapor objects.
     * @throws PropelException
     */
    public function countVldNilaiRaporsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiRaporsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNilaiRaporsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiRaporsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldNilaiRaporsRelatedByIdtype());
            }
            $query = VldNilaiRaporQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldNilaiRaporsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldNilaiRapor object to this object
     * through the VldNilaiRapor foreign key attribute.
     *
     * @param    VldNilaiRapor $l VldNilaiRapor
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldNilaiRaporRelatedByIdtype(VldNilaiRapor $l)
    {
        if ($this->collVldNilaiRaporsRelatedByIdtype === null) {
            $this->initVldNilaiRaporsRelatedByIdtype();
            $this->collVldNilaiRaporsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldNilaiRaporsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldNilaiRaporRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldNilaiRaporRelatedByIdtype $vldNilaiRaporRelatedByIdtype The vldNilaiRaporRelatedByIdtype object to add.
     */
    protected function doAddVldNilaiRaporRelatedByIdtype($vldNilaiRaporRelatedByIdtype)
    {
        $this->collVldNilaiRaporsRelatedByIdtype[]= $vldNilaiRaporRelatedByIdtype;
        $vldNilaiRaporRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldNilaiRaporRelatedByIdtype $vldNilaiRaporRelatedByIdtype The vldNilaiRaporRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldNilaiRaporRelatedByIdtype($vldNilaiRaporRelatedByIdtype)
    {
        if ($this->getVldNilaiRaporsRelatedByIdtype()->contains($vldNilaiRaporRelatedByIdtype)) {
            $this->collVldNilaiRaporsRelatedByIdtype->remove($this->collVldNilaiRaporsRelatedByIdtype->search($vldNilaiRaporRelatedByIdtype));
            if (null === $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion) {
                $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion = clone $this->collVldNilaiRaporsRelatedByIdtype;
                $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion[]= clone $vldNilaiRaporRelatedByIdtype;
            $vldNilaiRaporRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }

    /**
     * Clears out the collVldNilaiRaporsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldNilaiRaporsRelatedByIdtype()
     */
    public function clearVldNilaiRaporsRelatedByIdtype()
    {
        $this->collVldNilaiRaporsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldNilaiRaporsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldNilaiRaporsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldNilaiRaporsRelatedByIdtype($v = true)
    {
        $this->collVldNilaiRaporsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldNilaiRaporsRelatedByIdtype collection.
     *
     * By default this just sets the collVldNilaiRaporsRelatedByIdtype collection to an empty array (like clearcollVldNilaiRaporsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldNilaiRaporsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldNilaiRaporsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldNilaiRaporsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldNilaiRaporsRelatedByIdtype->setModel('VldNilaiRapor');
    }

    /**
     * Gets an array of VldNilaiRapor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldNilaiRapor[] List of VldNilaiRapor objects
     * @throws PropelException
     */
    public function getVldNilaiRaporsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiRaporsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNilaiRaporsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiRaporsRelatedByIdtype) {
                // return empty collection
                $this->initVldNilaiRaporsRelatedByIdtype();
            } else {
                $collVldNilaiRaporsRelatedByIdtype = VldNilaiRaporQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldNilaiRaporsRelatedByIdtypePartial && count($collVldNilaiRaporsRelatedByIdtype)) {
                      $this->initVldNilaiRaporsRelatedByIdtype(false);

                      foreach($collVldNilaiRaporsRelatedByIdtype as $obj) {
                        if (false == $this->collVldNilaiRaporsRelatedByIdtype->contains($obj)) {
                          $this->collVldNilaiRaporsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldNilaiRaporsRelatedByIdtypePartial = true;
                    }

                    $collVldNilaiRaporsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldNilaiRaporsRelatedByIdtype;
                }

                if($partial && $this->collVldNilaiRaporsRelatedByIdtype) {
                    foreach($this->collVldNilaiRaporsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldNilaiRaporsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldNilaiRaporsRelatedByIdtype = $collVldNilaiRaporsRelatedByIdtype;
                $this->collVldNilaiRaporsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldNilaiRaporsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldNilaiRaporRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldNilaiRaporsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldNilaiRaporsRelatedByIdtype(PropelCollection $vldNilaiRaporsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldNilaiRaporsRelatedByIdtypeToDelete = $this->getVldNilaiRaporsRelatedByIdtype(new Criteria(), $con)->diff($vldNilaiRaporsRelatedByIdtype);

        $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldNilaiRaporsRelatedByIdtypeToDelete));

        foreach ($vldNilaiRaporsRelatedByIdtypeToDelete as $vldNilaiRaporRelatedByIdtypeRemoved) {
            $vldNilaiRaporRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldNilaiRaporsRelatedByIdtype = null;
        foreach ($vldNilaiRaporsRelatedByIdtype as $vldNilaiRaporRelatedByIdtype) {
            $this->addVldNilaiRaporRelatedByIdtype($vldNilaiRaporRelatedByIdtype);
        }

        $this->collVldNilaiRaporsRelatedByIdtype = $vldNilaiRaporsRelatedByIdtype;
        $this->collVldNilaiRaporsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldNilaiRapor objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldNilaiRapor objects.
     * @throws PropelException
     */
    public function countVldNilaiRaporsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldNilaiRaporsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldNilaiRaporsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldNilaiRaporsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldNilaiRaporsRelatedByIdtype());
            }
            $query = VldNilaiRaporQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldNilaiRaporsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldNilaiRapor object to this object
     * through the VldNilaiRapor foreign key attribute.
     *
     * @param    VldNilaiRapor $l VldNilaiRapor
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldNilaiRaporRelatedByIdtype(VldNilaiRapor $l)
    {
        if ($this->collVldNilaiRaporsRelatedByIdtype === null) {
            $this->initVldNilaiRaporsRelatedByIdtype();
            $this->collVldNilaiRaporsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldNilaiRaporsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldNilaiRaporRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldNilaiRaporRelatedByIdtype $vldNilaiRaporRelatedByIdtype The vldNilaiRaporRelatedByIdtype object to add.
     */
    protected function doAddVldNilaiRaporRelatedByIdtype($vldNilaiRaporRelatedByIdtype)
    {
        $this->collVldNilaiRaporsRelatedByIdtype[]= $vldNilaiRaporRelatedByIdtype;
        $vldNilaiRaporRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldNilaiRaporRelatedByIdtype $vldNilaiRaporRelatedByIdtype The vldNilaiRaporRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldNilaiRaporRelatedByIdtype($vldNilaiRaporRelatedByIdtype)
    {
        if ($this->getVldNilaiRaporsRelatedByIdtype()->contains($vldNilaiRaporRelatedByIdtype)) {
            $this->collVldNilaiRaporsRelatedByIdtype->remove($this->collVldNilaiRaporsRelatedByIdtype->search($vldNilaiRaporRelatedByIdtype));
            if (null === $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion) {
                $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion = clone $this->collVldNilaiRaporsRelatedByIdtype;
                $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldNilaiRaporsRelatedByIdtypeScheduledForDeletion[]= clone $vldNilaiRaporRelatedByIdtype;
            $vldNilaiRaporRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }

    /**
     * Clears out the collVldMousRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldMousRelatedByIdtype()
     */
    public function clearVldMousRelatedByIdtype()
    {
        $this->collVldMousRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldMousRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldMousRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldMousRelatedByIdtype($v = true)
    {
        $this->collVldMousRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldMousRelatedByIdtype collection.
     *
     * By default this just sets the collVldMousRelatedByIdtype collection to an empty array (like clearcollVldMousRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldMousRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldMousRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldMousRelatedByIdtype = new PropelObjectCollection();
        $this->collVldMousRelatedByIdtype->setModel('VldMou');
    }

    /**
     * Gets an array of VldMou objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     * @throws PropelException
     */
    public function getVldMousRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldMousRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldMousRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldMousRelatedByIdtype) {
                // return empty collection
                $this->initVldMousRelatedByIdtype();
            } else {
                $collVldMousRelatedByIdtype = VldMouQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldMousRelatedByIdtypePartial && count($collVldMousRelatedByIdtype)) {
                      $this->initVldMousRelatedByIdtype(false);

                      foreach($collVldMousRelatedByIdtype as $obj) {
                        if (false == $this->collVldMousRelatedByIdtype->contains($obj)) {
                          $this->collVldMousRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldMousRelatedByIdtypePartial = true;
                    }

                    $collVldMousRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldMousRelatedByIdtype;
                }

                if($partial && $this->collVldMousRelatedByIdtype) {
                    foreach($this->collVldMousRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldMousRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldMousRelatedByIdtype = $collVldMousRelatedByIdtype;
                $this->collVldMousRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldMousRelatedByIdtype;
    }

    /**
     * Sets a collection of VldMouRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldMousRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldMousRelatedByIdtype(PropelCollection $vldMousRelatedByIdtype, PropelPDO $con = null)
    {
        $vldMousRelatedByIdtypeToDelete = $this->getVldMousRelatedByIdtype(new Criteria(), $con)->diff($vldMousRelatedByIdtype);

        $this->vldMousRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldMousRelatedByIdtypeToDelete));

        foreach ($vldMousRelatedByIdtypeToDelete as $vldMouRelatedByIdtypeRemoved) {
            $vldMouRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldMousRelatedByIdtype = null;
        foreach ($vldMousRelatedByIdtype as $vldMouRelatedByIdtype) {
            $this->addVldMouRelatedByIdtype($vldMouRelatedByIdtype);
        }

        $this->collVldMousRelatedByIdtype = $vldMousRelatedByIdtype;
        $this->collVldMousRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldMou objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldMou objects.
     * @throws PropelException
     */
    public function countVldMousRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldMousRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldMousRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldMousRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldMousRelatedByIdtype());
            }
            $query = VldMouQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldMousRelatedByIdtype);
    }

    /**
     * Method called to associate a VldMou object to this object
     * through the VldMou foreign key attribute.
     *
     * @param    VldMou $l VldMou
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldMouRelatedByIdtype(VldMou $l)
    {
        if ($this->collVldMousRelatedByIdtype === null) {
            $this->initVldMousRelatedByIdtype();
            $this->collVldMousRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldMousRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldMouRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldMouRelatedByIdtype $vldMouRelatedByIdtype The vldMouRelatedByIdtype object to add.
     */
    protected function doAddVldMouRelatedByIdtype($vldMouRelatedByIdtype)
    {
        $this->collVldMousRelatedByIdtype[]= $vldMouRelatedByIdtype;
        $vldMouRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldMouRelatedByIdtype $vldMouRelatedByIdtype The vldMouRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldMouRelatedByIdtype($vldMouRelatedByIdtype)
    {
        if ($this->getVldMousRelatedByIdtype()->contains($vldMouRelatedByIdtype)) {
            $this->collVldMousRelatedByIdtype->remove($this->collVldMousRelatedByIdtype->search($vldMouRelatedByIdtype));
            if (null === $this->vldMousRelatedByIdtypeScheduledForDeletion) {
                $this->vldMousRelatedByIdtypeScheduledForDeletion = clone $this->collVldMousRelatedByIdtype;
                $this->vldMousRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldMousRelatedByIdtypeScheduledForDeletion[]= clone $vldMouRelatedByIdtype;
            $vldMouRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldMousRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     */
    public function getVldMousRelatedByIdtypeJoinMouRelatedByMouId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldMouQuery::create(null, $criteria);
        $query->joinWith('MouRelatedByMouId', $join_behavior);

        return $this->getVldMousRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldMousRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     */
    public function getVldMousRelatedByIdtypeJoinMouRelatedByMouId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldMouQuery::create(null, $criteria);
        $query->joinWith('MouRelatedByMouId', $join_behavior);

        return $this->getVldMousRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldMousRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldMousRelatedByIdtype()
     */
    public function clearVldMousRelatedByIdtype()
    {
        $this->collVldMousRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldMousRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldMousRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldMousRelatedByIdtype($v = true)
    {
        $this->collVldMousRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldMousRelatedByIdtype collection.
     *
     * By default this just sets the collVldMousRelatedByIdtype collection to an empty array (like clearcollVldMousRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldMousRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldMousRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldMousRelatedByIdtype = new PropelObjectCollection();
        $this->collVldMousRelatedByIdtype->setModel('VldMou');
    }

    /**
     * Gets an array of VldMou objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     * @throws PropelException
     */
    public function getVldMousRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldMousRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldMousRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldMousRelatedByIdtype) {
                // return empty collection
                $this->initVldMousRelatedByIdtype();
            } else {
                $collVldMousRelatedByIdtype = VldMouQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldMousRelatedByIdtypePartial && count($collVldMousRelatedByIdtype)) {
                      $this->initVldMousRelatedByIdtype(false);

                      foreach($collVldMousRelatedByIdtype as $obj) {
                        if (false == $this->collVldMousRelatedByIdtype->contains($obj)) {
                          $this->collVldMousRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldMousRelatedByIdtypePartial = true;
                    }

                    $collVldMousRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldMousRelatedByIdtype;
                }

                if($partial && $this->collVldMousRelatedByIdtype) {
                    foreach($this->collVldMousRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldMousRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldMousRelatedByIdtype = $collVldMousRelatedByIdtype;
                $this->collVldMousRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldMousRelatedByIdtype;
    }

    /**
     * Sets a collection of VldMouRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldMousRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldMousRelatedByIdtype(PropelCollection $vldMousRelatedByIdtype, PropelPDO $con = null)
    {
        $vldMousRelatedByIdtypeToDelete = $this->getVldMousRelatedByIdtype(new Criteria(), $con)->diff($vldMousRelatedByIdtype);

        $this->vldMousRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldMousRelatedByIdtypeToDelete));

        foreach ($vldMousRelatedByIdtypeToDelete as $vldMouRelatedByIdtypeRemoved) {
            $vldMouRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldMousRelatedByIdtype = null;
        foreach ($vldMousRelatedByIdtype as $vldMouRelatedByIdtype) {
            $this->addVldMouRelatedByIdtype($vldMouRelatedByIdtype);
        }

        $this->collVldMousRelatedByIdtype = $vldMousRelatedByIdtype;
        $this->collVldMousRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldMou objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldMou objects.
     * @throws PropelException
     */
    public function countVldMousRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldMousRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldMousRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldMousRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldMousRelatedByIdtype());
            }
            $query = VldMouQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldMousRelatedByIdtype);
    }

    /**
     * Method called to associate a VldMou object to this object
     * through the VldMou foreign key attribute.
     *
     * @param    VldMou $l VldMou
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldMouRelatedByIdtype(VldMou $l)
    {
        if ($this->collVldMousRelatedByIdtype === null) {
            $this->initVldMousRelatedByIdtype();
            $this->collVldMousRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldMousRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldMouRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldMouRelatedByIdtype $vldMouRelatedByIdtype The vldMouRelatedByIdtype object to add.
     */
    protected function doAddVldMouRelatedByIdtype($vldMouRelatedByIdtype)
    {
        $this->collVldMousRelatedByIdtype[]= $vldMouRelatedByIdtype;
        $vldMouRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldMouRelatedByIdtype $vldMouRelatedByIdtype The vldMouRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldMouRelatedByIdtype($vldMouRelatedByIdtype)
    {
        if ($this->getVldMousRelatedByIdtype()->contains($vldMouRelatedByIdtype)) {
            $this->collVldMousRelatedByIdtype->remove($this->collVldMousRelatedByIdtype->search($vldMouRelatedByIdtype));
            if (null === $this->vldMousRelatedByIdtypeScheduledForDeletion) {
                $this->vldMousRelatedByIdtypeScheduledForDeletion = clone $this->collVldMousRelatedByIdtype;
                $this->vldMousRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldMousRelatedByIdtypeScheduledForDeletion[]= clone $vldMouRelatedByIdtype;
            $vldMouRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldMousRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     */
    public function getVldMousRelatedByIdtypeJoinMouRelatedByMouId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldMouQuery::create(null, $criteria);
        $query->joinWith('MouRelatedByMouId', $join_behavior);

        return $this->getVldMousRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldMousRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldMou[] List of VldMou objects
     */
    public function getVldMousRelatedByIdtypeJoinMouRelatedByMouId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldMouQuery::create(null, $criteria);
        $query->joinWith('MouRelatedByMouId', $join_behavior);

        return $this->getVldMousRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldKesejahteraansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldKesejahteraansRelatedByIdtype()
     */
    public function clearVldKesejahteraansRelatedByIdtype()
    {
        $this->collVldKesejahteraansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldKesejahteraansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldKesejahteraansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldKesejahteraansRelatedByIdtype($v = true)
    {
        $this->collVldKesejahteraansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldKesejahteraansRelatedByIdtype collection.
     *
     * By default this just sets the collVldKesejahteraansRelatedByIdtype collection to an empty array (like clearcollVldKesejahteraansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldKesejahteraansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldKesejahteraansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldKesejahteraansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldKesejahteraansRelatedByIdtype->setModel('VldKesejahteraan');
    }

    /**
     * Gets an array of VldKesejahteraan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldKesejahteraan[] List of VldKesejahteraan objects
     * @throws PropelException
     */
    public function getVldKesejahteraansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldKesejahteraansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldKesejahteraansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldKesejahteraansRelatedByIdtype) {
                // return empty collection
                $this->initVldKesejahteraansRelatedByIdtype();
            } else {
                $collVldKesejahteraansRelatedByIdtype = VldKesejahteraanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldKesejahteraansRelatedByIdtypePartial && count($collVldKesejahteraansRelatedByIdtype)) {
                      $this->initVldKesejahteraansRelatedByIdtype(false);

                      foreach($collVldKesejahteraansRelatedByIdtype as $obj) {
                        if (false == $this->collVldKesejahteraansRelatedByIdtype->contains($obj)) {
                          $this->collVldKesejahteraansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldKesejahteraansRelatedByIdtypePartial = true;
                    }

                    $collVldKesejahteraansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldKesejahteraansRelatedByIdtype;
                }

                if($partial && $this->collVldKesejahteraansRelatedByIdtype) {
                    foreach($this->collVldKesejahteraansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldKesejahteraansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldKesejahteraansRelatedByIdtype = $collVldKesejahteraansRelatedByIdtype;
                $this->collVldKesejahteraansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldKesejahteraansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldKesejahteraanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldKesejahteraansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldKesejahteraansRelatedByIdtype(PropelCollection $vldKesejahteraansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldKesejahteraansRelatedByIdtypeToDelete = $this->getVldKesejahteraansRelatedByIdtype(new Criteria(), $con)->diff($vldKesejahteraansRelatedByIdtype);

        $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldKesejahteraansRelatedByIdtypeToDelete));

        foreach ($vldKesejahteraansRelatedByIdtypeToDelete as $vldKesejahteraanRelatedByIdtypeRemoved) {
            $vldKesejahteraanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldKesejahteraansRelatedByIdtype = null;
        foreach ($vldKesejahteraansRelatedByIdtype as $vldKesejahteraanRelatedByIdtype) {
            $this->addVldKesejahteraanRelatedByIdtype($vldKesejahteraanRelatedByIdtype);
        }

        $this->collVldKesejahteraansRelatedByIdtype = $vldKesejahteraansRelatedByIdtype;
        $this->collVldKesejahteraansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldKesejahteraan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldKesejahteraan objects.
     * @throws PropelException
     */
    public function countVldKesejahteraansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldKesejahteraansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldKesejahteraansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldKesejahteraansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldKesejahteraansRelatedByIdtype());
            }
            $query = VldKesejahteraanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldKesejahteraansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldKesejahteraan object to this object
     * through the VldKesejahteraan foreign key attribute.
     *
     * @param    VldKesejahteraan $l VldKesejahteraan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldKesejahteraanRelatedByIdtype(VldKesejahteraan $l)
    {
        if ($this->collVldKesejahteraansRelatedByIdtype === null) {
            $this->initVldKesejahteraansRelatedByIdtype();
            $this->collVldKesejahteraansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldKesejahteraansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldKesejahteraanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldKesejahteraanRelatedByIdtype $vldKesejahteraanRelatedByIdtype The vldKesejahteraanRelatedByIdtype object to add.
     */
    protected function doAddVldKesejahteraanRelatedByIdtype($vldKesejahteraanRelatedByIdtype)
    {
        $this->collVldKesejahteraansRelatedByIdtype[]= $vldKesejahteraanRelatedByIdtype;
        $vldKesejahteraanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldKesejahteraanRelatedByIdtype $vldKesejahteraanRelatedByIdtype The vldKesejahteraanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldKesejahteraanRelatedByIdtype($vldKesejahteraanRelatedByIdtype)
    {
        if ($this->getVldKesejahteraansRelatedByIdtype()->contains($vldKesejahteraanRelatedByIdtype)) {
            $this->collVldKesejahteraansRelatedByIdtype->remove($this->collVldKesejahteraansRelatedByIdtype->search($vldKesejahteraanRelatedByIdtype));
            if (null === $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion) {
                $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion = clone $this->collVldKesejahteraansRelatedByIdtype;
                $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion[]= clone $vldKesejahteraanRelatedByIdtype;
            $vldKesejahteraanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldKesejahteraansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldKesejahteraan[] List of VldKesejahteraan objects
     */
    public function getVldKesejahteraansRelatedByIdtypeJoinKesejahteraanRelatedByKesejahteraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldKesejahteraanQuery::create(null, $criteria);
        $query->joinWith('KesejahteraanRelatedByKesejahteraanId', $join_behavior);

        return $this->getVldKesejahteraansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldKesejahteraansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldKesejahteraan[] List of VldKesejahteraan objects
     */
    public function getVldKesejahteraansRelatedByIdtypeJoinKesejahteraanRelatedByKesejahteraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldKesejahteraanQuery::create(null, $criteria);
        $query->joinWith('KesejahteraanRelatedByKesejahteraanId', $join_behavior);

        return $this->getVldKesejahteraansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldKesejahteraansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldKesejahteraansRelatedByIdtype()
     */
    public function clearVldKesejahteraansRelatedByIdtype()
    {
        $this->collVldKesejahteraansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldKesejahteraansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldKesejahteraansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldKesejahteraansRelatedByIdtype($v = true)
    {
        $this->collVldKesejahteraansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldKesejahteraansRelatedByIdtype collection.
     *
     * By default this just sets the collVldKesejahteraansRelatedByIdtype collection to an empty array (like clearcollVldKesejahteraansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldKesejahteraansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldKesejahteraansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldKesejahteraansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldKesejahteraansRelatedByIdtype->setModel('VldKesejahteraan');
    }

    /**
     * Gets an array of VldKesejahteraan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldKesejahteraan[] List of VldKesejahteraan objects
     * @throws PropelException
     */
    public function getVldKesejahteraansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldKesejahteraansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldKesejahteraansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldKesejahteraansRelatedByIdtype) {
                // return empty collection
                $this->initVldKesejahteraansRelatedByIdtype();
            } else {
                $collVldKesejahteraansRelatedByIdtype = VldKesejahteraanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldKesejahteraansRelatedByIdtypePartial && count($collVldKesejahteraansRelatedByIdtype)) {
                      $this->initVldKesejahteraansRelatedByIdtype(false);

                      foreach($collVldKesejahteraansRelatedByIdtype as $obj) {
                        if (false == $this->collVldKesejahteraansRelatedByIdtype->contains($obj)) {
                          $this->collVldKesejahteraansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldKesejahteraansRelatedByIdtypePartial = true;
                    }

                    $collVldKesejahteraansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldKesejahteraansRelatedByIdtype;
                }

                if($partial && $this->collVldKesejahteraansRelatedByIdtype) {
                    foreach($this->collVldKesejahteraansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldKesejahteraansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldKesejahteraansRelatedByIdtype = $collVldKesejahteraansRelatedByIdtype;
                $this->collVldKesejahteraansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldKesejahteraansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldKesejahteraanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldKesejahteraansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldKesejahteraansRelatedByIdtype(PropelCollection $vldKesejahteraansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldKesejahteraansRelatedByIdtypeToDelete = $this->getVldKesejahteraansRelatedByIdtype(new Criteria(), $con)->diff($vldKesejahteraansRelatedByIdtype);

        $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldKesejahteraansRelatedByIdtypeToDelete));

        foreach ($vldKesejahteraansRelatedByIdtypeToDelete as $vldKesejahteraanRelatedByIdtypeRemoved) {
            $vldKesejahteraanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldKesejahteraansRelatedByIdtype = null;
        foreach ($vldKesejahteraansRelatedByIdtype as $vldKesejahteraanRelatedByIdtype) {
            $this->addVldKesejahteraanRelatedByIdtype($vldKesejahteraanRelatedByIdtype);
        }

        $this->collVldKesejahteraansRelatedByIdtype = $vldKesejahteraansRelatedByIdtype;
        $this->collVldKesejahteraansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldKesejahteraan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldKesejahteraan objects.
     * @throws PropelException
     */
    public function countVldKesejahteraansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldKesejahteraansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldKesejahteraansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldKesejahteraansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldKesejahteraansRelatedByIdtype());
            }
            $query = VldKesejahteraanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldKesejahteraansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldKesejahteraan object to this object
     * through the VldKesejahteraan foreign key attribute.
     *
     * @param    VldKesejahteraan $l VldKesejahteraan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldKesejahteraanRelatedByIdtype(VldKesejahteraan $l)
    {
        if ($this->collVldKesejahteraansRelatedByIdtype === null) {
            $this->initVldKesejahteraansRelatedByIdtype();
            $this->collVldKesejahteraansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldKesejahteraansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldKesejahteraanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldKesejahteraanRelatedByIdtype $vldKesejahteraanRelatedByIdtype The vldKesejahteraanRelatedByIdtype object to add.
     */
    protected function doAddVldKesejahteraanRelatedByIdtype($vldKesejahteraanRelatedByIdtype)
    {
        $this->collVldKesejahteraansRelatedByIdtype[]= $vldKesejahteraanRelatedByIdtype;
        $vldKesejahteraanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldKesejahteraanRelatedByIdtype $vldKesejahteraanRelatedByIdtype The vldKesejahteraanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldKesejahteraanRelatedByIdtype($vldKesejahteraanRelatedByIdtype)
    {
        if ($this->getVldKesejahteraansRelatedByIdtype()->contains($vldKesejahteraanRelatedByIdtype)) {
            $this->collVldKesejahteraansRelatedByIdtype->remove($this->collVldKesejahteraansRelatedByIdtype->search($vldKesejahteraanRelatedByIdtype));
            if (null === $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion) {
                $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion = clone $this->collVldKesejahteraansRelatedByIdtype;
                $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldKesejahteraansRelatedByIdtypeScheduledForDeletion[]= clone $vldKesejahteraanRelatedByIdtype;
            $vldKesejahteraanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldKesejahteraansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldKesejahteraan[] List of VldKesejahteraan objects
     */
    public function getVldKesejahteraansRelatedByIdtypeJoinKesejahteraanRelatedByKesejahteraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldKesejahteraanQuery::create(null, $criteria);
        $query->joinWith('KesejahteraanRelatedByKesejahteraanId', $join_behavior);

        return $this->getVldKesejahteraansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldKesejahteraansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldKesejahteraan[] List of VldKesejahteraan objects
     */
    public function getVldKesejahteraansRelatedByIdtypeJoinKesejahteraanRelatedByKesejahteraanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldKesejahteraanQuery::create(null, $criteria);
        $query->joinWith('KesejahteraanRelatedByKesejahteraanId', $join_behavior);

        return $this->getVldKesejahteraansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldKaryaTulissRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldKaryaTulissRelatedByIdtype()
     */
    public function clearVldKaryaTulissRelatedByIdtype()
    {
        $this->collVldKaryaTulissRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldKaryaTulissRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldKaryaTulissRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldKaryaTulissRelatedByIdtype($v = true)
    {
        $this->collVldKaryaTulissRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldKaryaTulissRelatedByIdtype collection.
     *
     * By default this just sets the collVldKaryaTulissRelatedByIdtype collection to an empty array (like clearcollVldKaryaTulissRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldKaryaTulissRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldKaryaTulissRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldKaryaTulissRelatedByIdtype = new PropelObjectCollection();
        $this->collVldKaryaTulissRelatedByIdtype->setModel('VldKaryaTulis');
    }

    /**
     * Gets an array of VldKaryaTulis objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldKaryaTulis[] List of VldKaryaTulis objects
     * @throws PropelException
     */
    public function getVldKaryaTulissRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldKaryaTulissRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldKaryaTulissRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldKaryaTulissRelatedByIdtype) {
                // return empty collection
                $this->initVldKaryaTulissRelatedByIdtype();
            } else {
                $collVldKaryaTulissRelatedByIdtype = VldKaryaTulisQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldKaryaTulissRelatedByIdtypePartial && count($collVldKaryaTulissRelatedByIdtype)) {
                      $this->initVldKaryaTulissRelatedByIdtype(false);

                      foreach($collVldKaryaTulissRelatedByIdtype as $obj) {
                        if (false == $this->collVldKaryaTulissRelatedByIdtype->contains($obj)) {
                          $this->collVldKaryaTulissRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldKaryaTulissRelatedByIdtypePartial = true;
                    }

                    $collVldKaryaTulissRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldKaryaTulissRelatedByIdtype;
                }

                if($partial && $this->collVldKaryaTulissRelatedByIdtype) {
                    foreach($this->collVldKaryaTulissRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldKaryaTulissRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldKaryaTulissRelatedByIdtype = $collVldKaryaTulissRelatedByIdtype;
                $this->collVldKaryaTulissRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldKaryaTulissRelatedByIdtype;
    }

    /**
     * Sets a collection of VldKaryaTulisRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldKaryaTulissRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldKaryaTulissRelatedByIdtype(PropelCollection $vldKaryaTulissRelatedByIdtype, PropelPDO $con = null)
    {
        $vldKaryaTulissRelatedByIdtypeToDelete = $this->getVldKaryaTulissRelatedByIdtype(new Criteria(), $con)->diff($vldKaryaTulissRelatedByIdtype);

        $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldKaryaTulissRelatedByIdtypeToDelete));

        foreach ($vldKaryaTulissRelatedByIdtypeToDelete as $vldKaryaTulisRelatedByIdtypeRemoved) {
            $vldKaryaTulisRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldKaryaTulissRelatedByIdtype = null;
        foreach ($vldKaryaTulissRelatedByIdtype as $vldKaryaTulisRelatedByIdtype) {
            $this->addVldKaryaTulisRelatedByIdtype($vldKaryaTulisRelatedByIdtype);
        }

        $this->collVldKaryaTulissRelatedByIdtype = $vldKaryaTulissRelatedByIdtype;
        $this->collVldKaryaTulissRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldKaryaTulis objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldKaryaTulis objects.
     * @throws PropelException
     */
    public function countVldKaryaTulissRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldKaryaTulissRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldKaryaTulissRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldKaryaTulissRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldKaryaTulissRelatedByIdtype());
            }
            $query = VldKaryaTulisQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldKaryaTulissRelatedByIdtype);
    }

    /**
     * Method called to associate a VldKaryaTulis object to this object
     * through the VldKaryaTulis foreign key attribute.
     *
     * @param    VldKaryaTulis $l VldKaryaTulis
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldKaryaTulisRelatedByIdtype(VldKaryaTulis $l)
    {
        if ($this->collVldKaryaTulissRelatedByIdtype === null) {
            $this->initVldKaryaTulissRelatedByIdtype();
            $this->collVldKaryaTulissRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldKaryaTulissRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldKaryaTulisRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldKaryaTulisRelatedByIdtype $vldKaryaTulisRelatedByIdtype The vldKaryaTulisRelatedByIdtype object to add.
     */
    protected function doAddVldKaryaTulisRelatedByIdtype($vldKaryaTulisRelatedByIdtype)
    {
        $this->collVldKaryaTulissRelatedByIdtype[]= $vldKaryaTulisRelatedByIdtype;
        $vldKaryaTulisRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldKaryaTulisRelatedByIdtype $vldKaryaTulisRelatedByIdtype The vldKaryaTulisRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldKaryaTulisRelatedByIdtype($vldKaryaTulisRelatedByIdtype)
    {
        if ($this->getVldKaryaTulissRelatedByIdtype()->contains($vldKaryaTulisRelatedByIdtype)) {
            $this->collVldKaryaTulissRelatedByIdtype->remove($this->collVldKaryaTulissRelatedByIdtype->search($vldKaryaTulisRelatedByIdtype));
            if (null === $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion) {
                $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion = clone $this->collVldKaryaTulissRelatedByIdtype;
                $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion[]= clone $vldKaryaTulisRelatedByIdtype;
            $vldKaryaTulisRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldKaryaTulissRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldKaryaTulis[] List of VldKaryaTulis objects
     */
    public function getVldKaryaTulissRelatedByIdtypeJoinKaryaTulisRelatedByKaryaTulisId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldKaryaTulisQuery::create(null, $criteria);
        $query->joinWith('KaryaTulisRelatedByKaryaTulisId', $join_behavior);

        return $this->getVldKaryaTulissRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldKaryaTulissRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldKaryaTulis[] List of VldKaryaTulis objects
     */
    public function getVldKaryaTulissRelatedByIdtypeJoinKaryaTulisRelatedByKaryaTulisId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldKaryaTulisQuery::create(null, $criteria);
        $query->joinWith('KaryaTulisRelatedByKaryaTulisId', $join_behavior);

        return $this->getVldKaryaTulissRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldKaryaTulissRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldKaryaTulissRelatedByIdtype()
     */
    public function clearVldKaryaTulissRelatedByIdtype()
    {
        $this->collVldKaryaTulissRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldKaryaTulissRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldKaryaTulissRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldKaryaTulissRelatedByIdtype($v = true)
    {
        $this->collVldKaryaTulissRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldKaryaTulissRelatedByIdtype collection.
     *
     * By default this just sets the collVldKaryaTulissRelatedByIdtype collection to an empty array (like clearcollVldKaryaTulissRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldKaryaTulissRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldKaryaTulissRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldKaryaTulissRelatedByIdtype = new PropelObjectCollection();
        $this->collVldKaryaTulissRelatedByIdtype->setModel('VldKaryaTulis');
    }

    /**
     * Gets an array of VldKaryaTulis objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldKaryaTulis[] List of VldKaryaTulis objects
     * @throws PropelException
     */
    public function getVldKaryaTulissRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldKaryaTulissRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldKaryaTulissRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldKaryaTulissRelatedByIdtype) {
                // return empty collection
                $this->initVldKaryaTulissRelatedByIdtype();
            } else {
                $collVldKaryaTulissRelatedByIdtype = VldKaryaTulisQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldKaryaTulissRelatedByIdtypePartial && count($collVldKaryaTulissRelatedByIdtype)) {
                      $this->initVldKaryaTulissRelatedByIdtype(false);

                      foreach($collVldKaryaTulissRelatedByIdtype as $obj) {
                        if (false == $this->collVldKaryaTulissRelatedByIdtype->contains($obj)) {
                          $this->collVldKaryaTulissRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldKaryaTulissRelatedByIdtypePartial = true;
                    }

                    $collVldKaryaTulissRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldKaryaTulissRelatedByIdtype;
                }

                if($partial && $this->collVldKaryaTulissRelatedByIdtype) {
                    foreach($this->collVldKaryaTulissRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldKaryaTulissRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldKaryaTulissRelatedByIdtype = $collVldKaryaTulissRelatedByIdtype;
                $this->collVldKaryaTulissRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldKaryaTulissRelatedByIdtype;
    }

    /**
     * Sets a collection of VldKaryaTulisRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldKaryaTulissRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldKaryaTulissRelatedByIdtype(PropelCollection $vldKaryaTulissRelatedByIdtype, PropelPDO $con = null)
    {
        $vldKaryaTulissRelatedByIdtypeToDelete = $this->getVldKaryaTulissRelatedByIdtype(new Criteria(), $con)->diff($vldKaryaTulissRelatedByIdtype);

        $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldKaryaTulissRelatedByIdtypeToDelete));

        foreach ($vldKaryaTulissRelatedByIdtypeToDelete as $vldKaryaTulisRelatedByIdtypeRemoved) {
            $vldKaryaTulisRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldKaryaTulissRelatedByIdtype = null;
        foreach ($vldKaryaTulissRelatedByIdtype as $vldKaryaTulisRelatedByIdtype) {
            $this->addVldKaryaTulisRelatedByIdtype($vldKaryaTulisRelatedByIdtype);
        }

        $this->collVldKaryaTulissRelatedByIdtype = $vldKaryaTulissRelatedByIdtype;
        $this->collVldKaryaTulissRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldKaryaTulis objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldKaryaTulis objects.
     * @throws PropelException
     */
    public function countVldKaryaTulissRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldKaryaTulissRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldKaryaTulissRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldKaryaTulissRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldKaryaTulissRelatedByIdtype());
            }
            $query = VldKaryaTulisQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldKaryaTulissRelatedByIdtype);
    }

    /**
     * Method called to associate a VldKaryaTulis object to this object
     * through the VldKaryaTulis foreign key attribute.
     *
     * @param    VldKaryaTulis $l VldKaryaTulis
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldKaryaTulisRelatedByIdtype(VldKaryaTulis $l)
    {
        if ($this->collVldKaryaTulissRelatedByIdtype === null) {
            $this->initVldKaryaTulissRelatedByIdtype();
            $this->collVldKaryaTulissRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldKaryaTulissRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldKaryaTulisRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldKaryaTulisRelatedByIdtype $vldKaryaTulisRelatedByIdtype The vldKaryaTulisRelatedByIdtype object to add.
     */
    protected function doAddVldKaryaTulisRelatedByIdtype($vldKaryaTulisRelatedByIdtype)
    {
        $this->collVldKaryaTulissRelatedByIdtype[]= $vldKaryaTulisRelatedByIdtype;
        $vldKaryaTulisRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldKaryaTulisRelatedByIdtype $vldKaryaTulisRelatedByIdtype The vldKaryaTulisRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldKaryaTulisRelatedByIdtype($vldKaryaTulisRelatedByIdtype)
    {
        if ($this->getVldKaryaTulissRelatedByIdtype()->contains($vldKaryaTulisRelatedByIdtype)) {
            $this->collVldKaryaTulissRelatedByIdtype->remove($this->collVldKaryaTulissRelatedByIdtype->search($vldKaryaTulisRelatedByIdtype));
            if (null === $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion) {
                $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion = clone $this->collVldKaryaTulissRelatedByIdtype;
                $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldKaryaTulissRelatedByIdtypeScheduledForDeletion[]= clone $vldKaryaTulisRelatedByIdtype;
            $vldKaryaTulisRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldKaryaTulissRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldKaryaTulis[] List of VldKaryaTulis objects
     */
    public function getVldKaryaTulissRelatedByIdtypeJoinKaryaTulisRelatedByKaryaTulisId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldKaryaTulisQuery::create(null, $criteria);
        $query->joinWith('KaryaTulisRelatedByKaryaTulisId', $join_behavior);

        return $this->getVldKaryaTulissRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldKaryaTulissRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldKaryaTulis[] List of VldKaryaTulis objects
     */
    public function getVldKaryaTulissRelatedByIdtypeJoinKaryaTulisRelatedByKaryaTulisId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldKaryaTulisQuery::create(null, $criteria);
        $query->joinWith('KaryaTulisRelatedByKaryaTulisId', $join_behavior);

        return $this->getVldKaryaTulissRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldJurusanSpsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldJurusanSpsRelatedByIdtype()
     */
    public function clearVldJurusanSpsRelatedByIdtype()
    {
        $this->collVldJurusanSpsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldJurusanSpsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldJurusanSpsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldJurusanSpsRelatedByIdtype($v = true)
    {
        $this->collVldJurusanSpsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldJurusanSpsRelatedByIdtype collection.
     *
     * By default this just sets the collVldJurusanSpsRelatedByIdtype collection to an empty array (like clearcollVldJurusanSpsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldJurusanSpsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldJurusanSpsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldJurusanSpsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldJurusanSpsRelatedByIdtype->setModel('VldJurusanSp');
    }

    /**
     * Gets an array of VldJurusanSp objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     * @throws PropelException
     */
    public function getVldJurusanSpsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldJurusanSpsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldJurusanSpsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldJurusanSpsRelatedByIdtype) {
                // return empty collection
                $this->initVldJurusanSpsRelatedByIdtype();
            } else {
                $collVldJurusanSpsRelatedByIdtype = VldJurusanSpQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldJurusanSpsRelatedByIdtypePartial && count($collVldJurusanSpsRelatedByIdtype)) {
                      $this->initVldJurusanSpsRelatedByIdtype(false);

                      foreach($collVldJurusanSpsRelatedByIdtype as $obj) {
                        if (false == $this->collVldJurusanSpsRelatedByIdtype->contains($obj)) {
                          $this->collVldJurusanSpsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldJurusanSpsRelatedByIdtypePartial = true;
                    }

                    $collVldJurusanSpsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldJurusanSpsRelatedByIdtype;
                }

                if($partial && $this->collVldJurusanSpsRelatedByIdtype) {
                    foreach($this->collVldJurusanSpsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldJurusanSpsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldJurusanSpsRelatedByIdtype = $collVldJurusanSpsRelatedByIdtype;
                $this->collVldJurusanSpsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldJurusanSpsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldJurusanSpRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldJurusanSpsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldJurusanSpsRelatedByIdtype(PropelCollection $vldJurusanSpsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldJurusanSpsRelatedByIdtypeToDelete = $this->getVldJurusanSpsRelatedByIdtype(new Criteria(), $con)->diff($vldJurusanSpsRelatedByIdtype);

        $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldJurusanSpsRelatedByIdtypeToDelete));

        foreach ($vldJurusanSpsRelatedByIdtypeToDelete as $vldJurusanSpRelatedByIdtypeRemoved) {
            $vldJurusanSpRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldJurusanSpsRelatedByIdtype = null;
        foreach ($vldJurusanSpsRelatedByIdtype as $vldJurusanSpRelatedByIdtype) {
            $this->addVldJurusanSpRelatedByIdtype($vldJurusanSpRelatedByIdtype);
        }

        $this->collVldJurusanSpsRelatedByIdtype = $vldJurusanSpsRelatedByIdtype;
        $this->collVldJurusanSpsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldJurusanSp objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldJurusanSp objects.
     * @throws PropelException
     */
    public function countVldJurusanSpsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldJurusanSpsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldJurusanSpsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldJurusanSpsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldJurusanSpsRelatedByIdtype());
            }
            $query = VldJurusanSpQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldJurusanSpsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldJurusanSp object to this object
     * through the VldJurusanSp foreign key attribute.
     *
     * @param    VldJurusanSp $l VldJurusanSp
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldJurusanSpRelatedByIdtype(VldJurusanSp $l)
    {
        if ($this->collVldJurusanSpsRelatedByIdtype === null) {
            $this->initVldJurusanSpsRelatedByIdtype();
            $this->collVldJurusanSpsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldJurusanSpsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldJurusanSpRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldJurusanSpRelatedByIdtype $vldJurusanSpRelatedByIdtype The vldJurusanSpRelatedByIdtype object to add.
     */
    protected function doAddVldJurusanSpRelatedByIdtype($vldJurusanSpRelatedByIdtype)
    {
        $this->collVldJurusanSpsRelatedByIdtype[]= $vldJurusanSpRelatedByIdtype;
        $vldJurusanSpRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldJurusanSpRelatedByIdtype $vldJurusanSpRelatedByIdtype The vldJurusanSpRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldJurusanSpRelatedByIdtype($vldJurusanSpRelatedByIdtype)
    {
        if ($this->getVldJurusanSpsRelatedByIdtype()->contains($vldJurusanSpRelatedByIdtype)) {
            $this->collVldJurusanSpsRelatedByIdtype->remove($this->collVldJurusanSpsRelatedByIdtype->search($vldJurusanSpRelatedByIdtype));
            if (null === $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion) {
                $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion = clone $this->collVldJurusanSpsRelatedByIdtype;
                $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion[]= clone $vldJurusanSpRelatedByIdtype;
            $vldJurusanSpRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldJurusanSpsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     */
    public function getVldJurusanSpsRelatedByIdtypeJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldJurusanSpQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getVldJurusanSpsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldJurusanSpsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     */
    public function getVldJurusanSpsRelatedByIdtypeJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldJurusanSpQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getVldJurusanSpsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldJurusanSpsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldJurusanSpsRelatedByIdtype()
     */
    public function clearVldJurusanSpsRelatedByIdtype()
    {
        $this->collVldJurusanSpsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldJurusanSpsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldJurusanSpsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldJurusanSpsRelatedByIdtype($v = true)
    {
        $this->collVldJurusanSpsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldJurusanSpsRelatedByIdtype collection.
     *
     * By default this just sets the collVldJurusanSpsRelatedByIdtype collection to an empty array (like clearcollVldJurusanSpsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldJurusanSpsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldJurusanSpsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldJurusanSpsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldJurusanSpsRelatedByIdtype->setModel('VldJurusanSp');
    }

    /**
     * Gets an array of VldJurusanSp objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     * @throws PropelException
     */
    public function getVldJurusanSpsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldJurusanSpsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldJurusanSpsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldJurusanSpsRelatedByIdtype) {
                // return empty collection
                $this->initVldJurusanSpsRelatedByIdtype();
            } else {
                $collVldJurusanSpsRelatedByIdtype = VldJurusanSpQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldJurusanSpsRelatedByIdtypePartial && count($collVldJurusanSpsRelatedByIdtype)) {
                      $this->initVldJurusanSpsRelatedByIdtype(false);

                      foreach($collVldJurusanSpsRelatedByIdtype as $obj) {
                        if (false == $this->collVldJurusanSpsRelatedByIdtype->contains($obj)) {
                          $this->collVldJurusanSpsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldJurusanSpsRelatedByIdtypePartial = true;
                    }

                    $collVldJurusanSpsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldJurusanSpsRelatedByIdtype;
                }

                if($partial && $this->collVldJurusanSpsRelatedByIdtype) {
                    foreach($this->collVldJurusanSpsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldJurusanSpsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldJurusanSpsRelatedByIdtype = $collVldJurusanSpsRelatedByIdtype;
                $this->collVldJurusanSpsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldJurusanSpsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldJurusanSpRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldJurusanSpsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldJurusanSpsRelatedByIdtype(PropelCollection $vldJurusanSpsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldJurusanSpsRelatedByIdtypeToDelete = $this->getVldJurusanSpsRelatedByIdtype(new Criteria(), $con)->diff($vldJurusanSpsRelatedByIdtype);

        $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldJurusanSpsRelatedByIdtypeToDelete));

        foreach ($vldJurusanSpsRelatedByIdtypeToDelete as $vldJurusanSpRelatedByIdtypeRemoved) {
            $vldJurusanSpRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldJurusanSpsRelatedByIdtype = null;
        foreach ($vldJurusanSpsRelatedByIdtype as $vldJurusanSpRelatedByIdtype) {
            $this->addVldJurusanSpRelatedByIdtype($vldJurusanSpRelatedByIdtype);
        }

        $this->collVldJurusanSpsRelatedByIdtype = $vldJurusanSpsRelatedByIdtype;
        $this->collVldJurusanSpsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldJurusanSp objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldJurusanSp objects.
     * @throws PropelException
     */
    public function countVldJurusanSpsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldJurusanSpsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldJurusanSpsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldJurusanSpsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldJurusanSpsRelatedByIdtype());
            }
            $query = VldJurusanSpQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldJurusanSpsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldJurusanSp object to this object
     * through the VldJurusanSp foreign key attribute.
     *
     * @param    VldJurusanSp $l VldJurusanSp
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldJurusanSpRelatedByIdtype(VldJurusanSp $l)
    {
        if ($this->collVldJurusanSpsRelatedByIdtype === null) {
            $this->initVldJurusanSpsRelatedByIdtype();
            $this->collVldJurusanSpsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldJurusanSpsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldJurusanSpRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldJurusanSpRelatedByIdtype $vldJurusanSpRelatedByIdtype The vldJurusanSpRelatedByIdtype object to add.
     */
    protected function doAddVldJurusanSpRelatedByIdtype($vldJurusanSpRelatedByIdtype)
    {
        $this->collVldJurusanSpsRelatedByIdtype[]= $vldJurusanSpRelatedByIdtype;
        $vldJurusanSpRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldJurusanSpRelatedByIdtype $vldJurusanSpRelatedByIdtype The vldJurusanSpRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldJurusanSpRelatedByIdtype($vldJurusanSpRelatedByIdtype)
    {
        if ($this->getVldJurusanSpsRelatedByIdtype()->contains($vldJurusanSpRelatedByIdtype)) {
            $this->collVldJurusanSpsRelatedByIdtype->remove($this->collVldJurusanSpsRelatedByIdtype->search($vldJurusanSpRelatedByIdtype));
            if (null === $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion) {
                $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion = clone $this->collVldJurusanSpsRelatedByIdtype;
                $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldJurusanSpsRelatedByIdtypeScheduledForDeletion[]= clone $vldJurusanSpRelatedByIdtype;
            $vldJurusanSpRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldJurusanSpsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     */
    public function getVldJurusanSpsRelatedByIdtypeJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldJurusanSpQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getVldJurusanSpsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldJurusanSpsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     */
    public function getVldJurusanSpsRelatedByIdtypeJoinJurusanSpRelatedByJurusanSpId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldJurusanSpQuery::create(null, $criteria);
        $query->joinWith('JurusanSpRelatedByJurusanSpId', $join_behavior);

        return $this->getVldJurusanSpsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldInpassingsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldInpassingsRelatedByIdtype()
     */
    public function clearVldInpassingsRelatedByIdtype()
    {
        $this->collVldInpassingsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldInpassingsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldInpassingsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldInpassingsRelatedByIdtype($v = true)
    {
        $this->collVldInpassingsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldInpassingsRelatedByIdtype collection.
     *
     * By default this just sets the collVldInpassingsRelatedByIdtype collection to an empty array (like clearcollVldInpassingsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldInpassingsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldInpassingsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldInpassingsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldInpassingsRelatedByIdtype->setModel('VldInpassing');
    }

    /**
     * Gets an array of VldInpassing objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     * @throws PropelException
     */
    public function getVldInpassingsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldInpassingsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldInpassingsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldInpassingsRelatedByIdtype) {
                // return empty collection
                $this->initVldInpassingsRelatedByIdtype();
            } else {
                $collVldInpassingsRelatedByIdtype = VldInpassingQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldInpassingsRelatedByIdtypePartial && count($collVldInpassingsRelatedByIdtype)) {
                      $this->initVldInpassingsRelatedByIdtype(false);

                      foreach($collVldInpassingsRelatedByIdtype as $obj) {
                        if (false == $this->collVldInpassingsRelatedByIdtype->contains($obj)) {
                          $this->collVldInpassingsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldInpassingsRelatedByIdtypePartial = true;
                    }

                    $collVldInpassingsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldInpassingsRelatedByIdtype;
                }

                if($partial && $this->collVldInpassingsRelatedByIdtype) {
                    foreach($this->collVldInpassingsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldInpassingsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldInpassingsRelatedByIdtype = $collVldInpassingsRelatedByIdtype;
                $this->collVldInpassingsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldInpassingsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldInpassingRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldInpassingsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldInpassingsRelatedByIdtype(PropelCollection $vldInpassingsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldInpassingsRelatedByIdtypeToDelete = $this->getVldInpassingsRelatedByIdtype(new Criteria(), $con)->diff($vldInpassingsRelatedByIdtype);

        $this->vldInpassingsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldInpassingsRelatedByIdtypeToDelete));

        foreach ($vldInpassingsRelatedByIdtypeToDelete as $vldInpassingRelatedByIdtypeRemoved) {
            $vldInpassingRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldInpassingsRelatedByIdtype = null;
        foreach ($vldInpassingsRelatedByIdtype as $vldInpassingRelatedByIdtype) {
            $this->addVldInpassingRelatedByIdtype($vldInpassingRelatedByIdtype);
        }

        $this->collVldInpassingsRelatedByIdtype = $vldInpassingsRelatedByIdtype;
        $this->collVldInpassingsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldInpassing objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldInpassing objects.
     * @throws PropelException
     */
    public function countVldInpassingsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldInpassingsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldInpassingsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldInpassingsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldInpassingsRelatedByIdtype());
            }
            $query = VldInpassingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldInpassingsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldInpassing object to this object
     * through the VldInpassing foreign key attribute.
     *
     * @param    VldInpassing $l VldInpassing
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldInpassingRelatedByIdtype(VldInpassing $l)
    {
        if ($this->collVldInpassingsRelatedByIdtype === null) {
            $this->initVldInpassingsRelatedByIdtype();
            $this->collVldInpassingsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldInpassingsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldInpassingRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldInpassingRelatedByIdtype $vldInpassingRelatedByIdtype The vldInpassingRelatedByIdtype object to add.
     */
    protected function doAddVldInpassingRelatedByIdtype($vldInpassingRelatedByIdtype)
    {
        $this->collVldInpassingsRelatedByIdtype[]= $vldInpassingRelatedByIdtype;
        $vldInpassingRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldInpassingRelatedByIdtype $vldInpassingRelatedByIdtype The vldInpassingRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldInpassingRelatedByIdtype($vldInpassingRelatedByIdtype)
    {
        if ($this->getVldInpassingsRelatedByIdtype()->contains($vldInpassingRelatedByIdtype)) {
            $this->collVldInpassingsRelatedByIdtype->remove($this->collVldInpassingsRelatedByIdtype->search($vldInpassingRelatedByIdtype));
            if (null === $this->vldInpassingsRelatedByIdtypeScheduledForDeletion) {
                $this->vldInpassingsRelatedByIdtypeScheduledForDeletion = clone $this->collVldInpassingsRelatedByIdtype;
                $this->vldInpassingsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldInpassingsRelatedByIdtypeScheduledForDeletion[]= clone $vldInpassingRelatedByIdtype;
            $vldInpassingRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldInpassingsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     */
    public function getVldInpassingsRelatedByIdtypeJoinInpassingRelatedByInpassingId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldInpassingQuery::create(null, $criteria);
        $query->joinWith('InpassingRelatedByInpassingId', $join_behavior);

        return $this->getVldInpassingsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldInpassingsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     */
    public function getVldInpassingsRelatedByIdtypeJoinInpassingRelatedByInpassingId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldInpassingQuery::create(null, $criteria);
        $query->joinWith('InpassingRelatedByInpassingId', $join_behavior);

        return $this->getVldInpassingsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldInpassingsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldInpassingsRelatedByIdtype()
     */
    public function clearVldInpassingsRelatedByIdtype()
    {
        $this->collVldInpassingsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldInpassingsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldInpassingsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldInpassingsRelatedByIdtype($v = true)
    {
        $this->collVldInpassingsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldInpassingsRelatedByIdtype collection.
     *
     * By default this just sets the collVldInpassingsRelatedByIdtype collection to an empty array (like clearcollVldInpassingsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldInpassingsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldInpassingsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldInpassingsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldInpassingsRelatedByIdtype->setModel('VldInpassing');
    }

    /**
     * Gets an array of VldInpassing objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     * @throws PropelException
     */
    public function getVldInpassingsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldInpassingsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldInpassingsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldInpassingsRelatedByIdtype) {
                // return empty collection
                $this->initVldInpassingsRelatedByIdtype();
            } else {
                $collVldInpassingsRelatedByIdtype = VldInpassingQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldInpassingsRelatedByIdtypePartial && count($collVldInpassingsRelatedByIdtype)) {
                      $this->initVldInpassingsRelatedByIdtype(false);

                      foreach($collVldInpassingsRelatedByIdtype as $obj) {
                        if (false == $this->collVldInpassingsRelatedByIdtype->contains($obj)) {
                          $this->collVldInpassingsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldInpassingsRelatedByIdtypePartial = true;
                    }

                    $collVldInpassingsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldInpassingsRelatedByIdtype;
                }

                if($partial && $this->collVldInpassingsRelatedByIdtype) {
                    foreach($this->collVldInpassingsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldInpassingsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldInpassingsRelatedByIdtype = $collVldInpassingsRelatedByIdtype;
                $this->collVldInpassingsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldInpassingsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldInpassingRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldInpassingsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldInpassingsRelatedByIdtype(PropelCollection $vldInpassingsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldInpassingsRelatedByIdtypeToDelete = $this->getVldInpassingsRelatedByIdtype(new Criteria(), $con)->diff($vldInpassingsRelatedByIdtype);

        $this->vldInpassingsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldInpassingsRelatedByIdtypeToDelete));

        foreach ($vldInpassingsRelatedByIdtypeToDelete as $vldInpassingRelatedByIdtypeRemoved) {
            $vldInpassingRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldInpassingsRelatedByIdtype = null;
        foreach ($vldInpassingsRelatedByIdtype as $vldInpassingRelatedByIdtype) {
            $this->addVldInpassingRelatedByIdtype($vldInpassingRelatedByIdtype);
        }

        $this->collVldInpassingsRelatedByIdtype = $vldInpassingsRelatedByIdtype;
        $this->collVldInpassingsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldInpassing objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldInpassing objects.
     * @throws PropelException
     */
    public function countVldInpassingsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldInpassingsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldInpassingsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldInpassingsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldInpassingsRelatedByIdtype());
            }
            $query = VldInpassingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldInpassingsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldInpassing object to this object
     * through the VldInpassing foreign key attribute.
     *
     * @param    VldInpassing $l VldInpassing
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldInpassingRelatedByIdtype(VldInpassing $l)
    {
        if ($this->collVldInpassingsRelatedByIdtype === null) {
            $this->initVldInpassingsRelatedByIdtype();
            $this->collVldInpassingsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldInpassingsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldInpassingRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldInpassingRelatedByIdtype $vldInpassingRelatedByIdtype The vldInpassingRelatedByIdtype object to add.
     */
    protected function doAddVldInpassingRelatedByIdtype($vldInpassingRelatedByIdtype)
    {
        $this->collVldInpassingsRelatedByIdtype[]= $vldInpassingRelatedByIdtype;
        $vldInpassingRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldInpassingRelatedByIdtype $vldInpassingRelatedByIdtype The vldInpassingRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldInpassingRelatedByIdtype($vldInpassingRelatedByIdtype)
    {
        if ($this->getVldInpassingsRelatedByIdtype()->contains($vldInpassingRelatedByIdtype)) {
            $this->collVldInpassingsRelatedByIdtype->remove($this->collVldInpassingsRelatedByIdtype->search($vldInpassingRelatedByIdtype));
            if (null === $this->vldInpassingsRelatedByIdtypeScheduledForDeletion) {
                $this->vldInpassingsRelatedByIdtypeScheduledForDeletion = clone $this->collVldInpassingsRelatedByIdtype;
                $this->vldInpassingsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldInpassingsRelatedByIdtypeScheduledForDeletion[]= clone $vldInpassingRelatedByIdtype;
            $vldInpassingRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldInpassingsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     */
    public function getVldInpassingsRelatedByIdtypeJoinInpassingRelatedByInpassingId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldInpassingQuery::create(null, $criteria);
        $query->joinWith('InpassingRelatedByInpassingId', $join_behavior);

        return $this->getVldInpassingsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldInpassingsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldInpassing[] List of VldInpassing objects
     */
    public function getVldInpassingsRelatedByIdtypeJoinInpassingRelatedByInpassingId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldInpassingQuery::create(null, $criteria);
        $query->joinWith('InpassingRelatedByInpassingId', $join_behavior);

        return $this->getVldInpassingsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldDemografisRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldDemografisRelatedByIdtype()
     */
    public function clearVldDemografisRelatedByIdtype()
    {
        $this->collVldDemografisRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldDemografisRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldDemografisRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldDemografisRelatedByIdtype($v = true)
    {
        $this->collVldDemografisRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldDemografisRelatedByIdtype collection.
     *
     * By default this just sets the collVldDemografisRelatedByIdtype collection to an empty array (like clearcollVldDemografisRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldDemografisRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldDemografisRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldDemografisRelatedByIdtype = new PropelObjectCollection();
        $this->collVldDemografisRelatedByIdtype->setModel('VldDemografi');
    }

    /**
     * Gets an array of VldDemografi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     * @throws PropelException
     */
    public function getVldDemografisRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldDemografisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldDemografisRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldDemografisRelatedByIdtype) {
                // return empty collection
                $this->initVldDemografisRelatedByIdtype();
            } else {
                $collVldDemografisRelatedByIdtype = VldDemografiQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldDemografisRelatedByIdtypePartial && count($collVldDemografisRelatedByIdtype)) {
                      $this->initVldDemografisRelatedByIdtype(false);

                      foreach($collVldDemografisRelatedByIdtype as $obj) {
                        if (false == $this->collVldDemografisRelatedByIdtype->contains($obj)) {
                          $this->collVldDemografisRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldDemografisRelatedByIdtypePartial = true;
                    }

                    $collVldDemografisRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldDemografisRelatedByIdtype;
                }

                if($partial && $this->collVldDemografisRelatedByIdtype) {
                    foreach($this->collVldDemografisRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldDemografisRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldDemografisRelatedByIdtype = $collVldDemografisRelatedByIdtype;
                $this->collVldDemografisRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldDemografisRelatedByIdtype;
    }

    /**
     * Sets a collection of VldDemografiRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldDemografisRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldDemografisRelatedByIdtype(PropelCollection $vldDemografisRelatedByIdtype, PropelPDO $con = null)
    {
        $vldDemografisRelatedByIdtypeToDelete = $this->getVldDemografisRelatedByIdtype(new Criteria(), $con)->diff($vldDemografisRelatedByIdtype);

        $this->vldDemografisRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldDemografisRelatedByIdtypeToDelete));

        foreach ($vldDemografisRelatedByIdtypeToDelete as $vldDemografiRelatedByIdtypeRemoved) {
            $vldDemografiRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldDemografisRelatedByIdtype = null;
        foreach ($vldDemografisRelatedByIdtype as $vldDemografiRelatedByIdtype) {
            $this->addVldDemografiRelatedByIdtype($vldDemografiRelatedByIdtype);
        }

        $this->collVldDemografisRelatedByIdtype = $vldDemografisRelatedByIdtype;
        $this->collVldDemografisRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldDemografi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldDemografi objects.
     * @throws PropelException
     */
    public function countVldDemografisRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldDemografisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldDemografisRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldDemografisRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldDemografisRelatedByIdtype());
            }
            $query = VldDemografiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldDemografisRelatedByIdtype);
    }

    /**
     * Method called to associate a VldDemografi object to this object
     * through the VldDemografi foreign key attribute.
     *
     * @param    VldDemografi $l VldDemografi
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldDemografiRelatedByIdtype(VldDemografi $l)
    {
        if ($this->collVldDemografisRelatedByIdtype === null) {
            $this->initVldDemografisRelatedByIdtype();
            $this->collVldDemografisRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldDemografisRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldDemografiRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldDemografiRelatedByIdtype $vldDemografiRelatedByIdtype The vldDemografiRelatedByIdtype object to add.
     */
    protected function doAddVldDemografiRelatedByIdtype($vldDemografiRelatedByIdtype)
    {
        $this->collVldDemografisRelatedByIdtype[]= $vldDemografiRelatedByIdtype;
        $vldDemografiRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldDemografiRelatedByIdtype $vldDemografiRelatedByIdtype The vldDemografiRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldDemografiRelatedByIdtype($vldDemografiRelatedByIdtype)
    {
        if ($this->getVldDemografisRelatedByIdtype()->contains($vldDemografiRelatedByIdtype)) {
            $this->collVldDemografisRelatedByIdtype->remove($this->collVldDemografisRelatedByIdtype->search($vldDemografiRelatedByIdtype));
            if (null === $this->vldDemografisRelatedByIdtypeScheduledForDeletion) {
                $this->vldDemografisRelatedByIdtypeScheduledForDeletion = clone $this->collVldDemografisRelatedByIdtype;
                $this->vldDemografisRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldDemografisRelatedByIdtypeScheduledForDeletion[]= clone $vldDemografiRelatedByIdtype;
            $vldDemografiRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldDemografisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     */
    public function getVldDemografisRelatedByIdtypeJoinDemografiRelatedByDemografiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldDemografiQuery::create(null, $criteria);
        $query->joinWith('DemografiRelatedByDemografiId', $join_behavior);

        return $this->getVldDemografisRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldDemografisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     */
    public function getVldDemografisRelatedByIdtypeJoinDemografiRelatedByDemografiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldDemografiQuery::create(null, $criteria);
        $query->joinWith('DemografiRelatedByDemografiId', $join_behavior);

        return $this->getVldDemografisRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldDemografisRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldDemografisRelatedByIdtype()
     */
    public function clearVldDemografisRelatedByIdtype()
    {
        $this->collVldDemografisRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldDemografisRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldDemografisRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldDemografisRelatedByIdtype($v = true)
    {
        $this->collVldDemografisRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldDemografisRelatedByIdtype collection.
     *
     * By default this just sets the collVldDemografisRelatedByIdtype collection to an empty array (like clearcollVldDemografisRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldDemografisRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldDemografisRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldDemografisRelatedByIdtype = new PropelObjectCollection();
        $this->collVldDemografisRelatedByIdtype->setModel('VldDemografi');
    }

    /**
     * Gets an array of VldDemografi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     * @throws PropelException
     */
    public function getVldDemografisRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldDemografisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldDemografisRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldDemografisRelatedByIdtype) {
                // return empty collection
                $this->initVldDemografisRelatedByIdtype();
            } else {
                $collVldDemografisRelatedByIdtype = VldDemografiQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldDemografisRelatedByIdtypePartial && count($collVldDemografisRelatedByIdtype)) {
                      $this->initVldDemografisRelatedByIdtype(false);

                      foreach($collVldDemografisRelatedByIdtype as $obj) {
                        if (false == $this->collVldDemografisRelatedByIdtype->contains($obj)) {
                          $this->collVldDemografisRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldDemografisRelatedByIdtypePartial = true;
                    }

                    $collVldDemografisRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldDemografisRelatedByIdtype;
                }

                if($partial && $this->collVldDemografisRelatedByIdtype) {
                    foreach($this->collVldDemografisRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldDemografisRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldDemografisRelatedByIdtype = $collVldDemografisRelatedByIdtype;
                $this->collVldDemografisRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldDemografisRelatedByIdtype;
    }

    /**
     * Sets a collection of VldDemografiRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldDemografisRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldDemografisRelatedByIdtype(PropelCollection $vldDemografisRelatedByIdtype, PropelPDO $con = null)
    {
        $vldDemografisRelatedByIdtypeToDelete = $this->getVldDemografisRelatedByIdtype(new Criteria(), $con)->diff($vldDemografisRelatedByIdtype);

        $this->vldDemografisRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldDemografisRelatedByIdtypeToDelete));

        foreach ($vldDemografisRelatedByIdtypeToDelete as $vldDemografiRelatedByIdtypeRemoved) {
            $vldDemografiRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldDemografisRelatedByIdtype = null;
        foreach ($vldDemografisRelatedByIdtype as $vldDemografiRelatedByIdtype) {
            $this->addVldDemografiRelatedByIdtype($vldDemografiRelatedByIdtype);
        }

        $this->collVldDemografisRelatedByIdtype = $vldDemografisRelatedByIdtype;
        $this->collVldDemografisRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldDemografi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldDemografi objects.
     * @throws PropelException
     */
    public function countVldDemografisRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldDemografisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldDemografisRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldDemografisRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldDemografisRelatedByIdtype());
            }
            $query = VldDemografiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldDemografisRelatedByIdtype);
    }

    /**
     * Method called to associate a VldDemografi object to this object
     * through the VldDemografi foreign key attribute.
     *
     * @param    VldDemografi $l VldDemografi
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldDemografiRelatedByIdtype(VldDemografi $l)
    {
        if ($this->collVldDemografisRelatedByIdtype === null) {
            $this->initVldDemografisRelatedByIdtype();
            $this->collVldDemografisRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldDemografisRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldDemografiRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldDemografiRelatedByIdtype $vldDemografiRelatedByIdtype The vldDemografiRelatedByIdtype object to add.
     */
    protected function doAddVldDemografiRelatedByIdtype($vldDemografiRelatedByIdtype)
    {
        $this->collVldDemografisRelatedByIdtype[]= $vldDemografiRelatedByIdtype;
        $vldDemografiRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldDemografiRelatedByIdtype $vldDemografiRelatedByIdtype The vldDemografiRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldDemografiRelatedByIdtype($vldDemografiRelatedByIdtype)
    {
        if ($this->getVldDemografisRelatedByIdtype()->contains($vldDemografiRelatedByIdtype)) {
            $this->collVldDemografisRelatedByIdtype->remove($this->collVldDemografisRelatedByIdtype->search($vldDemografiRelatedByIdtype));
            if (null === $this->vldDemografisRelatedByIdtypeScheduledForDeletion) {
                $this->vldDemografisRelatedByIdtypeScheduledForDeletion = clone $this->collVldDemografisRelatedByIdtype;
                $this->vldDemografisRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldDemografisRelatedByIdtypeScheduledForDeletion[]= clone $vldDemografiRelatedByIdtype;
            $vldDemografiRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldDemografisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     */
    public function getVldDemografisRelatedByIdtypeJoinDemografiRelatedByDemografiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldDemografiQuery::create(null, $criteria);
        $query->joinWith('DemografiRelatedByDemografiId', $join_behavior);

        return $this->getVldDemografisRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldDemografisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldDemografi[] List of VldDemografi objects
     */
    public function getVldDemografisRelatedByIdtypeJoinDemografiRelatedByDemografiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldDemografiQuery::create(null, $criteria);
        $query->joinWith('DemografiRelatedByDemografiId', $join_behavior);

        return $this->getVldDemografisRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldBukuPtksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldBukuPtksRelatedByIdtype()
     */
    public function clearVldBukuPtksRelatedByIdtype()
    {
        $this->collVldBukuPtksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldBukuPtksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldBukuPtksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldBukuPtksRelatedByIdtype($v = true)
    {
        $this->collVldBukuPtksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldBukuPtksRelatedByIdtype collection.
     *
     * By default this just sets the collVldBukuPtksRelatedByIdtype collection to an empty array (like clearcollVldBukuPtksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldBukuPtksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldBukuPtksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldBukuPtksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldBukuPtksRelatedByIdtype->setModel('VldBukuPtk');
    }

    /**
     * Gets an array of VldBukuPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     * @throws PropelException
     */
    public function getVldBukuPtksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldBukuPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBukuPtksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldBukuPtksRelatedByIdtype) {
                // return empty collection
                $this->initVldBukuPtksRelatedByIdtype();
            } else {
                $collVldBukuPtksRelatedByIdtype = VldBukuPtkQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldBukuPtksRelatedByIdtypePartial && count($collVldBukuPtksRelatedByIdtype)) {
                      $this->initVldBukuPtksRelatedByIdtype(false);

                      foreach($collVldBukuPtksRelatedByIdtype as $obj) {
                        if (false == $this->collVldBukuPtksRelatedByIdtype->contains($obj)) {
                          $this->collVldBukuPtksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldBukuPtksRelatedByIdtypePartial = true;
                    }

                    $collVldBukuPtksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldBukuPtksRelatedByIdtype;
                }

                if($partial && $this->collVldBukuPtksRelatedByIdtype) {
                    foreach($this->collVldBukuPtksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldBukuPtksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldBukuPtksRelatedByIdtype = $collVldBukuPtksRelatedByIdtype;
                $this->collVldBukuPtksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldBukuPtksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldBukuPtkRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldBukuPtksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldBukuPtksRelatedByIdtype(PropelCollection $vldBukuPtksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldBukuPtksRelatedByIdtypeToDelete = $this->getVldBukuPtksRelatedByIdtype(new Criteria(), $con)->diff($vldBukuPtksRelatedByIdtype);

        $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldBukuPtksRelatedByIdtypeToDelete));

        foreach ($vldBukuPtksRelatedByIdtypeToDelete as $vldBukuPtkRelatedByIdtypeRemoved) {
            $vldBukuPtkRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldBukuPtksRelatedByIdtype = null;
        foreach ($vldBukuPtksRelatedByIdtype as $vldBukuPtkRelatedByIdtype) {
            $this->addVldBukuPtkRelatedByIdtype($vldBukuPtkRelatedByIdtype);
        }

        $this->collVldBukuPtksRelatedByIdtype = $vldBukuPtksRelatedByIdtype;
        $this->collVldBukuPtksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldBukuPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldBukuPtk objects.
     * @throws PropelException
     */
    public function countVldBukuPtksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldBukuPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBukuPtksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldBukuPtksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldBukuPtksRelatedByIdtype());
            }
            $query = VldBukuPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldBukuPtksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldBukuPtk object to this object
     * through the VldBukuPtk foreign key attribute.
     *
     * @param    VldBukuPtk $l VldBukuPtk
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldBukuPtkRelatedByIdtype(VldBukuPtk $l)
    {
        if ($this->collVldBukuPtksRelatedByIdtype === null) {
            $this->initVldBukuPtksRelatedByIdtype();
            $this->collVldBukuPtksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldBukuPtksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldBukuPtkRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldBukuPtkRelatedByIdtype $vldBukuPtkRelatedByIdtype The vldBukuPtkRelatedByIdtype object to add.
     */
    protected function doAddVldBukuPtkRelatedByIdtype($vldBukuPtkRelatedByIdtype)
    {
        $this->collVldBukuPtksRelatedByIdtype[]= $vldBukuPtkRelatedByIdtype;
        $vldBukuPtkRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldBukuPtkRelatedByIdtype $vldBukuPtkRelatedByIdtype The vldBukuPtkRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldBukuPtkRelatedByIdtype($vldBukuPtkRelatedByIdtype)
    {
        if ($this->getVldBukuPtksRelatedByIdtype()->contains($vldBukuPtkRelatedByIdtype)) {
            $this->collVldBukuPtksRelatedByIdtype->remove($this->collVldBukuPtksRelatedByIdtype->search($vldBukuPtkRelatedByIdtype));
            if (null === $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion) {
                $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion = clone $this->collVldBukuPtksRelatedByIdtype;
                $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion[]= clone $vldBukuPtkRelatedByIdtype;
            $vldBukuPtkRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBukuPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     */
    public function getVldBukuPtksRelatedByIdtypeJoinBukuPtkRelatedByBukuId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBukuPtkQuery::create(null, $criteria);
        $query->joinWith('BukuPtkRelatedByBukuId', $join_behavior);

        return $this->getVldBukuPtksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBukuPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     */
    public function getVldBukuPtksRelatedByIdtypeJoinBukuPtkRelatedByBukuId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBukuPtkQuery::create(null, $criteria);
        $query->joinWith('BukuPtkRelatedByBukuId', $join_behavior);

        return $this->getVldBukuPtksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldBukuPtksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldBukuPtksRelatedByIdtype()
     */
    public function clearVldBukuPtksRelatedByIdtype()
    {
        $this->collVldBukuPtksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldBukuPtksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldBukuPtksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldBukuPtksRelatedByIdtype($v = true)
    {
        $this->collVldBukuPtksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldBukuPtksRelatedByIdtype collection.
     *
     * By default this just sets the collVldBukuPtksRelatedByIdtype collection to an empty array (like clearcollVldBukuPtksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldBukuPtksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldBukuPtksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldBukuPtksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldBukuPtksRelatedByIdtype->setModel('VldBukuPtk');
    }

    /**
     * Gets an array of VldBukuPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     * @throws PropelException
     */
    public function getVldBukuPtksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldBukuPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBukuPtksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldBukuPtksRelatedByIdtype) {
                // return empty collection
                $this->initVldBukuPtksRelatedByIdtype();
            } else {
                $collVldBukuPtksRelatedByIdtype = VldBukuPtkQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldBukuPtksRelatedByIdtypePartial && count($collVldBukuPtksRelatedByIdtype)) {
                      $this->initVldBukuPtksRelatedByIdtype(false);

                      foreach($collVldBukuPtksRelatedByIdtype as $obj) {
                        if (false == $this->collVldBukuPtksRelatedByIdtype->contains($obj)) {
                          $this->collVldBukuPtksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldBukuPtksRelatedByIdtypePartial = true;
                    }

                    $collVldBukuPtksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldBukuPtksRelatedByIdtype;
                }

                if($partial && $this->collVldBukuPtksRelatedByIdtype) {
                    foreach($this->collVldBukuPtksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldBukuPtksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldBukuPtksRelatedByIdtype = $collVldBukuPtksRelatedByIdtype;
                $this->collVldBukuPtksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldBukuPtksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldBukuPtkRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldBukuPtksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldBukuPtksRelatedByIdtype(PropelCollection $vldBukuPtksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldBukuPtksRelatedByIdtypeToDelete = $this->getVldBukuPtksRelatedByIdtype(new Criteria(), $con)->diff($vldBukuPtksRelatedByIdtype);

        $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldBukuPtksRelatedByIdtypeToDelete));

        foreach ($vldBukuPtksRelatedByIdtypeToDelete as $vldBukuPtkRelatedByIdtypeRemoved) {
            $vldBukuPtkRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldBukuPtksRelatedByIdtype = null;
        foreach ($vldBukuPtksRelatedByIdtype as $vldBukuPtkRelatedByIdtype) {
            $this->addVldBukuPtkRelatedByIdtype($vldBukuPtkRelatedByIdtype);
        }

        $this->collVldBukuPtksRelatedByIdtype = $vldBukuPtksRelatedByIdtype;
        $this->collVldBukuPtksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldBukuPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldBukuPtk objects.
     * @throws PropelException
     */
    public function countVldBukuPtksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldBukuPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBukuPtksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldBukuPtksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldBukuPtksRelatedByIdtype());
            }
            $query = VldBukuPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldBukuPtksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldBukuPtk object to this object
     * through the VldBukuPtk foreign key attribute.
     *
     * @param    VldBukuPtk $l VldBukuPtk
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldBukuPtkRelatedByIdtype(VldBukuPtk $l)
    {
        if ($this->collVldBukuPtksRelatedByIdtype === null) {
            $this->initVldBukuPtksRelatedByIdtype();
            $this->collVldBukuPtksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldBukuPtksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldBukuPtkRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldBukuPtkRelatedByIdtype $vldBukuPtkRelatedByIdtype The vldBukuPtkRelatedByIdtype object to add.
     */
    protected function doAddVldBukuPtkRelatedByIdtype($vldBukuPtkRelatedByIdtype)
    {
        $this->collVldBukuPtksRelatedByIdtype[]= $vldBukuPtkRelatedByIdtype;
        $vldBukuPtkRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldBukuPtkRelatedByIdtype $vldBukuPtkRelatedByIdtype The vldBukuPtkRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldBukuPtkRelatedByIdtype($vldBukuPtkRelatedByIdtype)
    {
        if ($this->getVldBukuPtksRelatedByIdtype()->contains($vldBukuPtkRelatedByIdtype)) {
            $this->collVldBukuPtksRelatedByIdtype->remove($this->collVldBukuPtksRelatedByIdtype->search($vldBukuPtkRelatedByIdtype));
            if (null === $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion) {
                $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion = clone $this->collVldBukuPtksRelatedByIdtype;
                $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldBukuPtksRelatedByIdtypeScheduledForDeletion[]= clone $vldBukuPtkRelatedByIdtype;
            $vldBukuPtkRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBukuPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     */
    public function getVldBukuPtksRelatedByIdtypeJoinBukuPtkRelatedByBukuId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBukuPtkQuery::create(null, $criteria);
        $query->joinWith('BukuPtkRelatedByBukuId', $join_behavior);

        return $this->getVldBukuPtksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBukuPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBukuPtk[] List of VldBukuPtk objects
     */
    public function getVldBukuPtksRelatedByIdtypeJoinBukuPtkRelatedByBukuId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBukuPtkQuery::create(null, $criteria);
        $query->joinWith('BukuPtkRelatedByBukuId', $join_behavior);

        return $this->getVldBukuPtksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldBeaPtksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldBeaPtksRelatedByIdtype()
     */
    public function clearVldBeaPtksRelatedByIdtype()
    {
        $this->collVldBeaPtksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldBeaPtksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldBeaPtksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldBeaPtksRelatedByIdtype($v = true)
    {
        $this->collVldBeaPtksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldBeaPtksRelatedByIdtype collection.
     *
     * By default this just sets the collVldBeaPtksRelatedByIdtype collection to an empty array (like clearcollVldBeaPtksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldBeaPtksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldBeaPtksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldBeaPtksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldBeaPtksRelatedByIdtype->setModel('VldBeaPtk');
    }

    /**
     * Gets an array of VldBeaPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldBeaPtk[] List of VldBeaPtk objects
     * @throws PropelException
     */
    public function getVldBeaPtksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldBeaPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBeaPtksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldBeaPtksRelatedByIdtype) {
                // return empty collection
                $this->initVldBeaPtksRelatedByIdtype();
            } else {
                $collVldBeaPtksRelatedByIdtype = VldBeaPtkQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldBeaPtksRelatedByIdtypePartial && count($collVldBeaPtksRelatedByIdtype)) {
                      $this->initVldBeaPtksRelatedByIdtype(false);

                      foreach($collVldBeaPtksRelatedByIdtype as $obj) {
                        if (false == $this->collVldBeaPtksRelatedByIdtype->contains($obj)) {
                          $this->collVldBeaPtksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldBeaPtksRelatedByIdtypePartial = true;
                    }

                    $collVldBeaPtksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldBeaPtksRelatedByIdtype;
                }

                if($partial && $this->collVldBeaPtksRelatedByIdtype) {
                    foreach($this->collVldBeaPtksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldBeaPtksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldBeaPtksRelatedByIdtype = $collVldBeaPtksRelatedByIdtype;
                $this->collVldBeaPtksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldBeaPtksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldBeaPtkRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldBeaPtksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldBeaPtksRelatedByIdtype(PropelCollection $vldBeaPtksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldBeaPtksRelatedByIdtypeToDelete = $this->getVldBeaPtksRelatedByIdtype(new Criteria(), $con)->diff($vldBeaPtksRelatedByIdtype);

        $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldBeaPtksRelatedByIdtypeToDelete));

        foreach ($vldBeaPtksRelatedByIdtypeToDelete as $vldBeaPtkRelatedByIdtypeRemoved) {
            $vldBeaPtkRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldBeaPtksRelatedByIdtype = null;
        foreach ($vldBeaPtksRelatedByIdtype as $vldBeaPtkRelatedByIdtype) {
            $this->addVldBeaPtkRelatedByIdtype($vldBeaPtkRelatedByIdtype);
        }

        $this->collVldBeaPtksRelatedByIdtype = $vldBeaPtksRelatedByIdtype;
        $this->collVldBeaPtksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldBeaPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldBeaPtk objects.
     * @throws PropelException
     */
    public function countVldBeaPtksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldBeaPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBeaPtksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldBeaPtksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldBeaPtksRelatedByIdtype());
            }
            $query = VldBeaPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldBeaPtksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldBeaPtk object to this object
     * through the VldBeaPtk foreign key attribute.
     *
     * @param    VldBeaPtk $l VldBeaPtk
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldBeaPtkRelatedByIdtype(VldBeaPtk $l)
    {
        if ($this->collVldBeaPtksRelatedByIdtype === null) {
            $this->initVldBeaPtksRelatedByIdtype();
            $this->collVldBeaPtksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldBeaPtksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldBeaPtkRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldBeaPtkRelatedByIdtype $vldBeaPtkRelatedByIdtype The vldBeaPtkRelatedByIdtype object to add.
     */
    protected function doAddVldBeaPtkRelatedByIdtype($vldBeaPtkRelatedByIdtype)
    {
        $this->collVldBeaPtksRelatedByIdtype[]= $vldBeaPtkRelatedByIdtype;
        $vldBeaPtkRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldBeaPtkRelatedByIdtype $vldBeaPtkRelatedByIdtype The vldBeaPtkRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldBeaPtkRelatedByIdtype($vldBeaPtkRelatedByIdtype)
    {
        if ($this->getVldBeaPtksRelatedByIdtype()->contains($vldBeaPtkRelatedByIdtype)) {
            $this->collVldBeaPtksRelatedByIdtype->remove($this->collVldBeaPtksRelatedByIdtype->search($vldBeaPtkRelatedByIdtype));
            if (null === $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion) {
                $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion = clone $this->collVldBeaPtksRelatedByIdtype;
                $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion[]= clone $vldBeaPtkRelatedByIdtype;
            $vldBeaPtkRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBeaPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBeaPtk[] List of VldBeaPtk objects
     */
    public function getVldBeaPtksRelatedByIdtypeJoinBeasiswaPtkRelatedByBeasiswaPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBeaPtkQuery::create(null, $criteria);
        $query->joinWith('BeasiswaPtkRelatedByBeasiswaPtkId', $join_behavior);

        return $this->getVldBeaPtksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBeaPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBeaPtk[] List of VldBeaPtk objects
     */
    public function getVldBeaPtksRelatedByIdtypeJoinBeasiswaPtkRelatedByBeasiswaPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBeaPtkQuery::create(null, $criteria);
        $query->joinWith('BeasiswaPtkRelatedByBeasiswaPtkId', $join_behavior);

        return $this->getVldBeaPtksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldBeaPtksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldBeaPtksRelatedByIdtype()
     */
    public function clearVldBeaPtksRelatedByIdtype()
    {
        $this->collVldBeaPtksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldBeaPtksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldBeaPtksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldBeaPtksRelatedByIdtype($v = true)
    {
        $this->collVldBeaPtksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldBeaPtksRelatedByIdtype collection.
     *
     * By default this just sets the collVldBeaPtksRelatedByIdtype collection to an empty array (like clearcollVldBeaPtksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldBeaPtksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldBeaPtksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldBeaPtksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldBeaPtksRelatedByIdtype->setModel('VldBeaPtk');
    }

    /**
     * Gets an array of VldBeaPtk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldBeaPtk[] List of VldBeaPtk objects
     * @throws PropelException
     */
    public function getVldBeaPtksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldBeaPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBeaPtksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldBeaPtksRelatedByIdtype) {
                // return empty collection
                $this->initVldBeaPtksRelatedByIdtype();
            } else {
                $collVldBeaPtksRelatedByIdtype = VldBeaPtkQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldBeaPtksRelatedByIdtypePartial && count($collVldBeaPtksRelatedByIdtype)) {
                      $this->initVldBeaPtksRelatedByIdtype(false);

                      foreach($collVldBeaPtksRelatedByIdtype as $obj) {
                        if (false == $this->collVldBeaPtksRelatedByIdtype->contains($obj)) {
                          $this->collVldBeaPtksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldBeaPtksRelatedByIdtypePartial = true;
                    }

                    $collVldBeaPtksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldBeaPtksRelatedByIdtype;
                }

                if($partial && $this->collVldBeaPtksRelatedByIdtype) {
                    foreach($this->collVldBeaPtksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldBeaPtksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldBeaPtksRelatedByIdtype = $collVldBeaPtksRelatedByIdtype;
                $this->collVldBeaPtksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldBeaPtksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldBeaPtkRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldBeaPtksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldBeaPtksRelatedByIdtype(PropelCollection $vldBeaPtksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldBeaPtksRelatedByIdtypeToDelete = $this->getVldBeaPtksRelatedByIdtype(new Criteria(), $con)->diff($vldBeaPtksRelatedByIdtype);

        $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldBeaPtksRelatedByIdtypeToDelete));

        foreach ($vldBeaPtksRelatedByIdtypeToDelete as $vldBeaPtkRelatedByIdtypeRemoved) {
            $vldBeaPtkRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldBeaPtksRelatedByIdtype = null;
        foreach ($vldBeaPtksRelatedByIdtype as $vldBeaPtkRelatedByIdtype) {
            $this->addVldBeaPtkRelatedByIdtype($vldBeaPtkRelatedByIdtype);
        }

        $this->collVldBeaPtksRelatedByIdtype = $vldBeaPtksRelatedByIdtype;
        $this->collVldBeaPtksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldBeaPtk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldBeaPtk objects.
     * @throws PropelException
     */
    public function countVldBeaPtksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldBeaPtksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBeaPtksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldBeaPtksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldBeaPtksRelatedByIdtype());
            }
            $query = VldBeaPtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldBeaPtksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldBeaPtk object to this object
     * through the VldBeaPtk foreign key attribute.
     *
     * @param    VldBeaPtk $l VldBeaPtk
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldBeaPtkRelatedByIdtype(VldBeaPtk $l)
    {
        if ($this->collVldBeaPtksRelatedByIdtype === null) {
            $this->initVldBeaPtksRelatedByIdtype();
            $this->collVldBeaPtksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldBeaPtksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldBeaPtkRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldBeaPtkRelatedByIdtype $vldBeaPtkRelatedByIdtype The vldBeaPtkRelatedByIdtype object to add.
     */
    protected function doAddVldBeaPtkRelatedByIdtype($vldBeaPtkRelatedByIdtype)
    {
        $this->collVldBeaPtksRelatedByIdtype[]= $vldBeaPtkRelatedByIdtype;
        $vldBeaPtkRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldBeaPtkRelatedByIdtype $vldBeaPtkRelatedByIdtype The vldBeaPtkRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldBeaPtkRelatedByIdtype($vldBeaPtkRelatedByIdtype)
    {
        if ($this->getVldBeaPtksRelatedByIdtype()->contains($vldBeaPtkRelatedByIdtype)) {
            $this->collVldBeaPtksRelatedByIdtype->remove($this->collVldBeaPtksRelatedByIdtype->search($vldBeaPtkRelatedByIdtype));
            if (null === $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion) {
                $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion = clone $this->collVldBeaPtksRelatedByIdtype;
                $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldBeaPtksRelatedByIdtypeScheduledForDeletion[]= clone $vldBeaPtkRelatedByIdtype;
            $vldBeaPtkRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBeaPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBeaPtk[] List of VldBeaPtk objects
     */
    public function getVldBeaPtksRelatedByIdtypeJoinBeasiswaPtkRelatedByBeasiswaPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBeaPtkQuery::create(null, $criteria);
        $query->joinWith('BeasiswaPtkRelatedByBeasiswaPtkId', $join_behavior);

        return $this->getVldBeaPtksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBeaPtksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBeaPtk[] List of VldBeaPtk objects
     */
    public function getVldBeaPtksRelatedByIdtypeJoinBeasiswaPtkRelatedByBeasiswaPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBeaPtkQuery::create(null, $criteria);
        $query->joinWith('BeasiswaPtkRelatedByBeasiswaPtkId', $join_behavior);

        return $this->getVldBeaPtksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldBeaPdsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldBeaPdsRelatedByIdtype()
     */
    public function clearVldBeaPdsRelatedByIdtype()
    {
        $this->collVldBeaPdsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldBeaPdsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldBeaPdsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldBeaPdsRelatedByIdtype($v = true)
    {
        $this->collVldBeaPdsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldBeaPdsRelatedByIdtype collection.
     *
     * By default this just sets the collVldBeaPdsRelatedByIdtype collection to an empty array (like clearcollVldBeaPdsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldBeaPdsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldBeaPdsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldBeaPdsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldBeaPdsRelatedByIdtype->setModel('VldBeaPd');
    }

    /**
     * Gets an array of VldBeaPd objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldBeaPd[] List of VldBeaPd objects
     * @throws PropelException
     */
    public function getVldBeaPdsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldBeaPdsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBeaPdsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldBeaPdsRelatedByIdtype) {
                // return empty collection
                $this->initVldBeaPdsRelatedByIdtype();
            } else {
                $collVldBeaPdsRelatedByIdtype = VldBeaPdQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldBeaPdsRelatedByIdtypePartial && count($collVldBeaPdsRelatedByIdtype)) {
                      $this->initVldBeaPdsRelatedByIdtype(false);

                      foreach($collVldBeaPdsRelatedByIdtype as $obj) {
                        if (false == $this->collVldBeaPdsRelatedByIdtype->contains($obj)) {
                          $this->collVldBeaPdsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldBeaPdsRelatedByIdtypePartial = true;
                    }

                    $collVldBeaPdsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldBeaPdsRelatedByIdtype;
                }

                if($partial && $this->collVldBeaPdsRelatedByIdtype) {
                    foreach($this->collVldBeaPdsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldBeaPdsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldBeaPdsRelatedByIdtype = $collVldBeaPdsRelatedByIdtype;
                $this->collVldBeaPdsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldBeaPdsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldBeaPdRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldBeaPdsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldBeaPdsRelatedByIdtype(PropelCollection $vldBeaPdsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldBeaPdsRelatedByIdtypeToDelete = $this->getVldBeaPdsRelatedByIdtype(new Criteria(), $con)->diff($vldBeaPdsRelatedByIdtype);

        $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldBeaPdsRelatedByIdtypeToDelete));

        foreach ($vldBeaPdsRelatedByIdtypeToDelete as $vldBeaPdRelatedByIdtypeRemoved) {
            $vldBeaPdRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldBeaPdsRelatedByIdtype = null;
        foreach ($vldBeaPdsRelatedByIdtype as $vldBeaPdRelatedByIdtype) {
            $this->addVldBeaPdRelatedByIdtype($vldBeaPdRelatedByIdtype);
        }

        $this->collVldBeaPdsRelatedByIdtype = $vldBeaPdsRelatedByIdtype;
        $this->collVldBeaPdsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldBeaPd objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldBeaPd objects.
     * @throws PropelException
     */
    public function countVldBeaPdsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldBeaPdsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBeaPdsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldBeaPdsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldBeaPdsRelatedByIdtype());
            }
            $query = VldBeaPdQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldBeaPdsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldBeaPd object to this object
     * through the VldBeaPd foreign key attribute.
     *
     * @param    VldBeaPd $l VldBeaPd
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldBeaPdRelatedByIdtype(VldBeaPd $l)
    {
        if ($this->collVldBeaPdsRelatedByIdtype === null) {
            $this->initVldBeaPdsRelatedByIdtype();
            $this->collVldBeaPdsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldBeaPdsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldBeaPdRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldBeaPdRelatedByIdtype $vldBeaPdRelatedByIdtype The vldBeaPdRelatedByIdtype object to add.
     */
    protected function doAddVldBeaPdRelatedByIdtype($vldBeaPdRelatedByIdtype)
    {
        $this->collVldBeaPdsRelatedByIdtype[]= $vldBeaPdRelatedByIdtype;
        $vldBeaPdRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldBeaPdRelatedByIdtype $vldBeaPdRelatedByIdtype The vldBeaPdRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldBeaPdRelatedByIdtype($vldBeaPdRelatedByIdtype)
    {
        if ($this->getVldBeaPdsRelatedByIdtype()->contains($vldBeaPdRelatedByIdtype)) {
            $this->collVldBeaPdsRelatedByIdtype->remove($this->collVldBeaPdsRelatedByIdtype->search($vldBeaPdRelatedByIdtype));
            if (null === $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion) {
                $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion = clone $this->collVldBeaPdsRelatedByIdtype;
                $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion[]= clone $vldBeaPdRelatedByIdtype;
            $vldBeaPdRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBeaPdsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBeaPd[] List of VldBeaPd objects
     */
    public function getVldBeaPdsRelatedByIdtypeJoinBeasiswaPesertaDidikRelatedByBeasiswaPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBeaPdQuery::create(null, $criteria);
        $query->joinWith('BeasiswaPesertaDidikRelatedByBeasiswaPesertaDidikId', $join_behavior);

        return $this->getVldBeaPdsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBeaPdsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBeaPd[] List of VldBeaPd objects
     */
    public function getVldBeaPdsRelatedByIdtypeJoinBeasiswaPesertaDidikRelatedByBeasiswaPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBeaPdQuery::create(null, $criteria);
        $query->joinWith('BeasiswaPesertaDidikRelatedByBeasiswaPesertaDidikId', $join_behavior);

        return $this->getVldBeaPdsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldBeaPdsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldBeaPdsRelatedByIdtype()
     */
    public function clearVldBeaPdsRelatedByIdtype()
    {
        $this->collVldBeaPdsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldBeaPdsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldBeaPdsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldBeaPdsRelatedByIdtype($v = true)
    {
        $this->collVldBeaPdsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldBeaPdsRelatedByIdtype collection.
     *
     * By default this just sets the collVldBeaPdsRelatedByIdtype collection to an empty array (like clearcollVldBeaPdsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldBeaPdsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldBeaPdsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldBeaPdsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldBeaPdsRelatedByIdtype->setModel('VldBeaPd');
    }

    /**
     * Gets an array of VldBeaPd objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldBeaPd[] List of VldBeaPd objects
     * @throws PropelException
     */
    public function getVldBeaPdsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldBeaPdsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBeaPdsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldBeaPdsRelatedByIdtype) {
                // return empty collection
                $this->initVldBeaPdsRelatedByIdtype();
            } else {
                $collVldBeaPdsRelatedByIdtype = VldBeaPdQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldBeaPdsRelatedByIdtypePartial && count($collVldBeaPdsRelatedByIdtype)) {
                      $this->initVldBeaPdsRelatedByIdtype(false);

                      foreach($collVldBeaPdsRelatedByIdtype as $obj) {
                        if (false == $this->collVldBeaPdsRelatedByIdtype->contains($obj)) {
                          $this->collVldBeaPdsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldBeaPdsRelatedByIdtypePartial = true;
                    }

                    $collVldBeaPdsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldBeaPdsRelatedByIdtype;
                }

                if($partial && $this->collVldBeaPdsRelatedByIdtype) {
                    foreach($this->collVldBeaPdsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldBeaPdsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldBeaPdsRelatedByIdtype = $collVldBeaPdsRelatedByIdtype;
                $this->collVldBeaPdsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldBeaPdsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldBeaPdRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldBeaPdsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldBeaPdsRelatedByIdtype(PropelCollection $vldBeaPdsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldBeaPdsRelatedByIdtypeToDelete = $this->getVldBeaPdsRelatedByIdtype(new Criteria(), $con)->diff($vldBeaPdsRelatedByIdtype);

        $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldBeaPdsRelatedByIdtypeToDelete));

        foreach ($vldBeaPdsRelatedByIdtypeToDelete as $vldBeaPdRelatedByIdtypeRemoved) {
            $vldBeaPdRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldBeaPdsRelatedByIdtype = null;
        foreach ($vldBeaPdsRelatedByIdtype as $vldBeaPdRelatedByIdtype) {
            $this->addVldBeaPdRelatedByIdtype($vldBeaPdRelatedByIdtype);
        }

        $this->collVldBeaPdsRelatedByIdtype = $vldBeaPdsRelatedByIdtype;
        $this->collVldBeaPdsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldBeaPd objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldBeaPd objects.
     * @throws PropelException
     */
    public function countVldBeaPdsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldBeaPdsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldBeaPdsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldBeaPdsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldBeaPdsRelatedByIdtype());
            }
            $query = VldBeaPdQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldBeaPdsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldBeaPd object to this object
     * through the VldBeaPd foreign key attribute.
     *
     * @param    VldBeaPd $l VldBeaPd
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldBeaPdRelatedByIdtype(VldBeaPd $l)
    {
        if ($this->collVldBeaPdsRelatedByIdtype === null) {
            $this->initVldBeaPdsRelatedByIdtype();
            $this->collVldBeaPdsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldBeaPdsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldBeaPdRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldBeaPdRelatedByIdtype $vldBeaPdRelatedByIdtype The vldBeaPdRelatedByIdtype object to add.
     */
    protected function doAddVldBeaPdRelatedByIdtype($vldBeaPdRelatedByIdtype)
    {
        $this->collVldBeaPdsRelatedByIdtype[]= $vldBeaPdRelatedByIdtype;
        $vldBeaPdRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldBeaPdRelatedByIdtype $vldBeaPdRelatedByIdtype The vldBeaPdRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldBeaPdRelatedByIdtype($vldBeaPdRelatedByIdtype)
    {
        if ($this->getVldBeaPdsRelatedByIdtype()->contains($vldBeaPdRelatedByIdtype)) {
            $this->collVldBeaPdsRelatedByIdtype->remove($this->collVldBeaPdsRelatedByIdtype->search($vldBeaPdRelatedByIdtype));
            if (null === $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion) {
                $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion = clone $this->collVldBeaPdsRelatedByIdtype;
                $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldBeaPdsRelatedByIdtypeScheduledForDeletion[]= clone $vldBeaPdRelatedByIdtype;
            $vldBeaPdRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBeaPdsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBeaPd[] List of VldBeaPd objects
     */
    public function getVldBeaPdsRelatedByIdtypeJoinBeasiswaPesertaDidikRelatedByBeasiswaPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBeaPdQuery::create(null, $criteria);
        $query->joinWith('BeasiswaPesertaDidikRelatedByBeasiswaPesertaDidikId', $join_behavior);

        return $this->getVldBeaPdsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldBeaPdsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldBeaPd[] List of VldBeaPd objects
     */
    public function getVldBeaPdsRelatedByIdtypeJoinBeasiswaPesertaDidikRelatedByBeasiswaPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldBeaPdQuery::create(null, $criteria);
        $query->joinWith('BeasiswaPesertaDidikRelatedByBeasiswaPesertaDidikId', $join_behavior);

        return $this->getVldBeaPdsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldAnaksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldAnaksRelatedByIdtype()
     */
    public function clearVldAnaksRelatedByIdtype()
    {
        $this->collVldAnaksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldAnaksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldAnaksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldAnaksRelatedByIdtype($v = true)
    {
        $this->collVldAnaksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldAnaksRelatedByIdtype collection.
     *
     * By default this just sets the collVldAnaksRelatedByIdtype collection to an empty array (like clearcollVldAnaksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldAnaksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldAnaksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldAnaksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldAnaksRelatedByIdtype->setModel('VldAnak');
    }

    /**
     * Gets an array of VldAnak objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     * @throws PropelException
     */
    public function getVldAnaksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldAnaksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldAnaksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldAnaksRelatedByIdtype) {
                // return empty collection
                $this->initVldAnaksRelatedByIdtype();
            } else {
                $collVldAnaksRelatedByIdtype = VldAnakQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldAnaksRelatedByIdtypePartial && count($collVldAnaksRelatedByIdtype)) {
                      $this->initVldAnaksRelatedByIdtype(false);

                      foreach($collVldAnaksRelatedByIdtype as $obj) {
                        if (false == $this->collVldAnaksRelatedByIdtype->contains($obj)) {
                          $this->collVldAnaksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldAnaksRelatedByIdtypePartial = true;
                    }

                    $collVldAnaksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldAnaksRelatedByIdtype;
                }

                if($partial && $this->collVldAnaksRelatedByIdtype) {
                    foreach($this->collVldAnaksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldAnaksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldAnaksRelatedByIdtype = $collVldAnaksRelatedByIdtype;
                $this->collVldAnaksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldAnaksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldAnakRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldAnaksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldAnaksRelatedByIdtype(PropelCollection $vldAnaksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldAnaksRelatedByIdtypeToDelete = $this->getVldAnaksRelatedByIdtype(new Criteria(), $con)->diff($vldAnaksRelatedByIdtype);

        $this->vldAnaksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldAnaksRelatedByIdtypeToDelete));

        foreach ($vldAnaksRelatedByIdtypeToDelete as $vldAnakRelatedByIdtypeRemoved) {
            $vldAnakRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldAnaksRelatedByIdtype = null;
        foreach ($vldAnaksRelatedByIdtype as $vldAnakRelatedByIdtype) {
            $this->addVldAnakRelatedByIdtype($vldAnakRelatedByIdtype);
        }

        $this->collVldAnaksRelatedByIdtype = $vldAnaksRelatedByIdtype;
        $this->collVldAnaksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldAnak objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldAnak objects.
     * @throws PropelException
     */
    public function countVldAnaksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldAnaksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldAnaksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldAnaksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldAnaksRelatedByIdtype());
            }
            $query = VldAnakQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldAnaksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldAnak object to this object
     * through the VldAnak foreign key attribute.
     *
     * @param    VldAnak $l VldAnak
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldAnakRelatedByIdtype(VldAnak $l)
    {
        if ($this->collVldAnaksRelatedByIdtype === null) {
            $this->initVldAnaksRelatedByIdtype();
            $this->collVldAnaksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldAnaksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldAnakRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldAnakRelatedByIdtype $vldAnakRelatedByIdtype The vldAnakRelatedByIdtype object to add.
     */
    protected function doAddVldAnakRelatedByIdtype($vldAnakRelatedByIdtype)
    {
        $this->collVldAnaksRelatedByIdtype[]= $vldAnakRelatedByIdtype;
        $vldAnakRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldAnakRelatedByIdtype $vldAnakRelatedByIdtype The vldAnakRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldAnakRelatedByIdtype($vldAnakRelatedByIdtype)
    {
        if ($this->getVldAnaksRelatedByIdtype()->contains($vldAnakRelatedByIdtype)) {
            $this->collVldAnaksRelatedByIdtype->remove($this->collVldAnaksRelatedByIdtype->search($vldAnakRelatedByIdtype));
            if (null === $this->vldAnaksRelatedByIdtypeScheduledForDeletion) {
                $this->vldAnaksRelatedByIdtypeScheduledForDeletion = clone $this->collVldAnaksRelatedByIdtype;
                $this->vldAnaksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldAnaksRelatedByIdtypeScheduledForDeletion[]= clone $vldAnakRelatedByIdtype;
            $vldAnakRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldAnaksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     */
    public function getVldAnaksRelatedByIdtypeJoinAnakRelatedByAnakId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldAnakQuery::create(null, $criteria);
        $query->joinWith('AnakRelatedByAnakId', $join_behavior);

        return $this->getVldAnaksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldAnaksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     */
    public function getVldAnaksRelatedByIdtypeJoinAnakRelatedByAnakId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldAnakQuery::create(null, $criteria);
        $query->joinWith('AnakRelatedByAnakId', $join_behavior);

        return $this->getVldAnaksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldAnaksRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldAnaksRelatedByIdtype()
     */
    public function clearVldAnaksRelatedByIdtype()
    {
        $this->collVldAnaksRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldAnaksRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldAnaksRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldAnaksRelatedByIdtype($v = true)
    {
        $this->collVldAnaksRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldAnaksRelatedByIdtype collection.
     *
     * By default this just sets the collVldAnaksRelatedByIdtype collection to an empty array (like clearcollVldAnaksRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldAnaksRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldAnaksRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldAnaksRelatedByIdtype = new PropelObjectCollection();
        $this->collVldAnaksRelatedByIdtype->setModel('VldAnak');
    }

    /**
     * Gets an array of VldAnak objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     * @throws PropelException
     */
    public function getVldAnaksRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldAnaksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldAnaksRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldAnaksRelatedByIdtype) {
                // return empty collection
                $this->initVldAnaksRelatedByIdtype();
            } else {
                $collVldAnaksRelatedByIdtype = VldAnakQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldAnaksRelatedByIdtypePartial && count($collVldAnaksRelatedByIdtype)) {
                      $this->initVldAnaksRelatedByIdtype(false);

                      foreach($collVldAnaksRelatedByIdtype as $obj) {
                        if (false == $this->collVldAnaksRelatedByIdtype->contains($obj)) {
                          $this->collVldAnaksRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldAnaksRelatedByIdtypePartial = true;
                    }

                    $collVldAnaksRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldAnaksRelatedByIdtype;
                }

                if($partial && $this->collVldAnaksRelatedByIdtype) {
                    foreach($this->collVldAnaksRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldAnaksRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldAnaksRelatedByIdtype = $collVldAnaksRelatedByIdtype;
                $this->collVldAnaksRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldAnaksRelatedByIdtype;
    }

    /**
     * Sets a collection of VldAnakRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldAnaksRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldAnaksRelatedByIdtype(PropelCollection $vldAnaksRelatedByIdtype, PropelPDO $con = null)
    {
        $vldAnaksRelatedByIdtypeToDelete = $this->getVldAnaksRelatedByIdtype(new Criteria(), $con)->diff($vldAnaksRelatedByIdtype);

        $this->vldAnaksRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldAnaksRelatedByIdtypeToDelete));

        foreach ($vldAnaksRelatedByIdtypeToDelete as $vldAnakRelatedByIdtypeRemoved) {
            $vldAnakRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldAnaksRelatedByIdtype = null;
        foreach ($vldAnaksRelatedByIdtype as $vldAnakRelatedByIdtype) {
            $this->addVldAnakRelatedByIdtype($vldAnakRelatedByIdtype);
        }

        $this->collVldAnaksRelatedByIdtype = $vldAnaksRelatedByIdtype;
        $this->collVldAnaksRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldAnak objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldAnak objects.
     * @throws PropelException
     */
    public function countVldAnaksRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldAnaksRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldAnaksRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldAnaksRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldAnaksRelatedByIdtype());
            }
            $query = VldAnakQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldAnaksRelatedByIdtype);
    }

    /**
     * Method called to associate a VldAnak object to this object
     * through the VldAnak foreign key attribute.
     *
     * @param    VldAnak $l VldAnak
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldAnakRelatedByIdtype(VldAnak $l)
    {
        if ($this->collVldAnaksRelatedByIdtype === null) {
            $this->initVldAnaksRelatedByIdtype();
            $this->collVldAnaksRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldAnaksRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldAnakRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldAnakRelatedByIdtype $vldAnakRelatedByIdtype The vldAnakRelatedByIdtype object to add.
     */
    protected function doAddVldAnakRelatedByIdtype($vldAnakRelatedByIdtype)
    {
        $this->collVldAnaksRelatedByIdtype[]= $vldAnakRelatedByIdtype;
        $vldAnakRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldAnakRelatedByIdtype $vldAnakRelatedByIdtype The vldAnakRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldAnakRelatedByIdtype($vldAnakRelatedByIdtype)
    {
        if ($this->getVldAnaksRelatedByIdtype()->contains($vldAnakRelatedByIdtype)) {
            $this->collVldAnaksRelatedByIdtype->remove($this->collVldAnaksRelatedByIdtype->search($vldAnakRelatedByIdtype));
            if (null === $this->vldAnaksRelatedByIdtypeScheduledForDeletion) {
                $this->vldAnaksRelatedByIdtypeScheduledForDeletion = clone $this->collVldAnaksRelatedByIdtype;
                $this->vldAnaksRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldAnaksRelatedByIdtypeScheduledForDeletion[]= clone $vldAnakRelatedByIdtype;
            $vldAnakRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldAnaksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     */
    public function getVldAnaksRelatedByIdtypeJoinAnakRelatedByAnakId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldAnakQuery::create(null, $criteria);
        $query->joinWith('AnakRelatedByAnakId', $join_behavior);

        return $this->getVldAnaksRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldAnaksRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldAnak[] List of VldAnak objects
     */
    public function getVldAnaksRelatedByIdtypeJoinAnakRelatedByAnakId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldAnakQuery::create(null, $criteria);
        $query->joinWith('AnakRelatedByAnakId', $join_behavior);

        return $this->getVldAnaksRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldYayasansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldYayasansRelatedByIdtype()
     */
    public function clearVldYayasansRelatedByIdtype()
    {
        $this->collVldYayasansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldYayasansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldYayasansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldYayasansRelatedByIdtype($v = true)
    {
        $this->collVldYayasansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldYayasansRelatedByIdtype collection.
     *
     * By default this just sets the collVldYayasansRelatedByIdtype collection to an empty array (like clearcollVldYayasansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldYayasansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldYayasansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldYayasansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldYayasansRelatedByIdtype->setModel('VldYayasan');
    }

    /**
     * Gets an array of VldYayasan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldYayasan[] List of VldYayasan objects
     * @throws PropelException
     */
    public function getVldYayasansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldYayasansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldYayasansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldYayasansRelatedByIdtype) {
                // return empty collection
                $this->initVldYayasansRelatedByIdtype();
            } else {
                $collVldYayasansRelatedByIdtype = VldYayasanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldYayasansRelatedByIdtypePartial && count($collVldYayasansRelatedByIdtype)) {
                      $this->initVldYayasansRelatedByIdtype(false);

                      foreach($collVldYayasansRelatedByIdtype as $obj) {
                        if (false == $this->collVldYayasansRelatedByIdtype->contains($obj)) {
                          $this->collVldYayasansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldYayasansRelatedByIdtypePartial = true;
                    }

                    $collVldYayasansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldYayasansRelatedByIdtype;
                }

                if($partial && $this->collVldYayasansRelatedByIdtype) {
                    foreach($this->collVldYayasansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldYayasansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldYayasansRelatedByIdtype = $collVldYayasansRelatedByIdtype;
                $this->collVldYayasansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldYayasansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldYayasanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldYayasansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldYayasansRelatedByIdtype(PropelCollection $vldYayasansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldYayasansRelatedByIdtypeToDelete = $this->getVldYayasansRelatedByIdtype(new Criteria(), $con)->diff($vldYayasansRelatedByIdtype);

        $this->vldYayasansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldYayasansRelatedByIdtypeToDelete));

        foreach ($vldYayasansRelatedByIdtypeToDelete as $vldYayasanRelatedByIdtypeRemoved) {
            $vldYayasanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldYayasansRelatedByIdtype = null;
        foreach ($vldYayasansRelatedByIdtype as $vldYayasanRelatedByIdtype) {
            $this->addVldYayasanRelatedByIdtype($vldYayasanRelatedByIdtype);
        }

        $this->collVldYayasansRelatedByIdtype = $vldYayasansRelatedByIdtype;
        $this->collVldYayasansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldYayasan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldYayasan objects.
     * @throws PropelException
     */
    public function countVldYayasansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldYayasansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldYayasansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldYayasansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldYayasansRelatedByIdtype());
            }
            $query = VldYayasanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldYayasansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldYayasan object to this object
     * through the VldYayasan foreign key attribute.
     *
     * @param    VldYayasan $l VldYayasan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldYayasanRelatedByIdtype(VldYayasan $l)
    {
        if ($this->collVldYayasansRelatedByIdtype === null) {
            $this->initVldYayasansRelatedByIdtype();
            $this->collVldYayasansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldYayasansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldYayasanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldYayasanRelatedByIdtype $vldYayasanRelatedByIdtype The vldYayasanRelatedByIdtype object to add.
     */
    protected function doAddVldYayasanRelatedByIdtype($vldYayasanRelatedByIdtype)
    {
        $this->collVldYayasansRelatedByIdtype[]= $vldYayasanRelatedByIdtype;
        $vldYayasanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldYayasanRelatedByIdtype $vldYayasanRelatedByIdtype The vldYayasanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldYayasanRelatedByIdtype($vldYayasanRelatedByIdtype)
    {
        if ($this->getVldYayasansRelatedByIdtype()->contains($vldYayasanRelatedByIdtype)) {
            $this->collVldYayasansRelatedByIdtype->remove($this->collVldYayasansRelatedByIdtype->search($vldYayasanRelatedByIdtype));
            if (null === $this->vldYayasansRelatedByIdtypeScheduledForDeletion) {
                $this->vldYayasansRelatedByIdtypeScheduledForDeletion = clone $this->collVldYayasansRelatedByIdtype;
                $this->vldYayasansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldYayasansRelatedByIdtypeScheduledForDeletion[]= clone $vldYayasanRelatedByIdtype;
            $vldYayasanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldYayasansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldYayasan[] List of VldYayasan objects
     */
    public function getVldYayasansRelatedByIdtypeJoinYayasanRelatedByYayasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldYayasanQuery::create(null, $criteria);
        $query->joinWith('YayasanRelatedByYayasanId', $join_behavior);

        return $this->getVldYayasansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldYayasansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldYayasan[] List of VldYayasan objects
     */
    public function getVldYayasansRelatedByIdtypeJoinYayasanRelatedByYayasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldYayasanQuery::create(null, $criteria);
        $query->joinWith('YayasanRelatedByYayasanId', $join_behavior);

        return $this->getVldYayasansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldYayasansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldYayasansRelatedByIdtype()
     */
    public function clearVldYayasansRelatedByIdtype()
    {
        $this->collVldYayasansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldYayasansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldYayasansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldYayasansRelatedByIdtype($v = true)
    {
        $this->collVldYayasansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldYayasansRelatedByIdtype collection.
     *
     * By default this just sets the collVldYayasansRelatedByIdtype collection to an empty array (like clearcollVldYayasansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldYayasansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldYayasansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldYayasansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldYayasansRelatedByIdtype->setModel('VldYayasan');
    }

    /**
     * Gets an array of VldYayasan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldYayasan[] List of VldYayasan objects
     * @throws PropelException
     */
    public function getVldYayasansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldYayasansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldYayasansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldYayasansRelatedByIdtype) {
                // return empty collection
                $this->initVldYayasansRelatedByIdtype();
            } else {
                $collVldYayasansRelatedByIdtype = VldYayasanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldYayasansRelatedByIdtypePartial && count($collVldYayasansRelatedByIdtype)) {
                      $this->initVldYayasansRelatedByIdtype(false);

                      foreach($collVldYayasansRelatedByIdtype as $obj) {
                        if (false == $this->collVldYayasansRelatedByIdtype->contains($obj)) {
                          $this->collVldYayasansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldYayasansRelatedByIdtypePartial = true;
                    }

                    $collVldYayasansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldYayasansRelatedByIdtype;
                }

                if($partial && $this->collVldYayasansRelatedByIdtype) {
                    foreach($this->collVldYayasansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldYayasansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldYayasansRelatedByIdtype = $collVldYayasansRelatedByIdtype;
                $this->collVldYayasansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldYayasansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldYayasanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldYayasansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldYayasansRelatedByIdtype(PropelCollection $vldYayasansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldYayasansRelatedByIdtypeToDelete = $this->getVldYayasansRelatedByIdtype(new Criteria(), $con)->diff($vldYayasansRelatedByIdtype);

        $this->vldYayasansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldYayasansRelatedByIdtypeToDelete));

        foreach ($vldYayasansRelatedByIdtypeToDelete as $vldYayasanRelatedByIdtypeRemoved) {
            $vldYayasanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldYayasansRelatedByIdtype = null;
        foreach ($vldYayasansRelatedByIdtype as $vldYayasanRelatedByIdtype) {
            $this->addVldYayasanRelatedByIdtype($vldYayasanRelatedByIdtype);
        }

        $this->collVldYayasansRelatedByIdtype = $vldYayasansRelatedByIdtype;
        $this->collVldYayasansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldYayasan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldYayasan objects.
     * @throws PropelException
     */
    public function countVldYayasansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldYayasansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldYayasansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldYayasansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldYayasansRelatedByIdtype());
            }
            $query = VldYayasanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldYayasansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldYayasan object to this object
     * through the VldYayasan foreign key attribute.
     *
     * @param    VldYayasan $l VldYayasan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldYayasanRelatedByIdtype(VldYayasan $l)
    {
        if ($this->collVldYayasansRelatedByIdtype === null) {
            $this->initVldYayasansRelatedByIdtype();
            $this->collVldYayasansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldYayasansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldYayasanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldYayasanRelatedByIdtype $vldYayasanRelatedByIdtype The vldYayasanRelatedByIdtype object to add.
     */
    protected function doAddVldYayasanRelatedByIdtype($vldYayasanRelatedByIdtype)
    {
        $this->collVldYayasansRelatedByIdtype[]= $vldYayasanRelatedByIdtype;
        $vldYayasanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldYayasanRelatedByIdtype $vldYayasanRelatedByIdtype The vldYayasanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldYayasanRelatedByIdtype($vldYayasanRelatedByIdtype)
    {
        if ($this->getVldYayasansRelatedByIdtype()->contains($vldYayasanRelatedByIdtype)) {
            $this->collVldYayasansRelatedByIdtype->remove($this->collVldYayasansRelatedByIdtype->search($vldYayasanRelatedByIdtype));
            if (null === $this->vldYayasansRelatedByIdtypeScheduledForDeletion) {
                $this->vldYayasansRelatedByIdtypeScheduledForDeletion = clone $this->collVldYayasansRelatedByIdtype;
                $this->vldYayasansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldYayasansRelatedByIdtypeScheduledForDeletion[]= clone $vldYayasanRelatedByIdtype;
            $vldYayasanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldYayasansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldYayasan[] List of VldYayasan objects
     */
    public function getVldYayasansRelatedByIdtypeJoinYayasanRelatedByYayasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldYayasanQuery::create(null, $criteria);
        $query->joinWith('YayasanRelatedByYayasanId', $join_behavior);

        return $this->getVldYayasansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldYayasansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldYayasan[] List of VldYayasan objects
     */
    public function getVldYayasansRelatedByIdtypeJoinYayasanRelatedByYayasanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldYayasanQuery::create(null, $criteria);
        $query->joinWith('YayasanRelatedByYayasanId', $join_behavior);

        return $this->getVldYayasansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldUnsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldUnsRelatedByIdtype()
     */
    public function clearVldUnsRelatedByIdtype()
    {
        $this->collVldUnsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldUnsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldUnsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldUnsRelatedByIdtype($v = true)
    {
        $this->collVldUnsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldUnsRelatedByIdtype collection.
     *
     * By default this just sets the collVldUnsRelatedByIdtype collection to an empty array (like clearcollVldUnsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldUnsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldUnsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldUnsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldUnsRelatedByIdtype->setModel('VldUn');
    }

    /**
     * Gets an array of VldUn objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldUn[] List of VldUn objects
     * @throws PropelException
     */
    public function getVldUnsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldUnsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldUnsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldUnsRelatedByIdtype) {
                // return empty collection
                $this->initVldUnsRelatedByIdtype();
            } else {
                $collVldUnsRelatedByIdtype = VldUnQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldUnsRelatedByIdtypePartial && count($collVldUnsRelatedByIdtype)) {
                      $this->initVldUnsRelatedByIdtype(false);

                      foreach($collVldUnsRelatedByIdtype as $obj) {
                        if (false == $this->collVldUnsRelatedByIdtype->contains($obj)) {
                          $this->collVldUnsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldUnsRelatedByIdtypePartial = true;
                    }

                    $collVldUnsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldUnsRelatedByIdtype;
                }

                if($partial && $this->collVldUnsRelatedByIdtype) {
                    foreach($this->collVldUnsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldUnsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldUnsRelatedByIdtype = $collVldUnsRelatedByIdtype;
                $this->collVldUnsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldUnsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldUnRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldUnsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldUnsRelatedByIdtype(PropelCollection $vldUnsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldUnsRelatedByIdtypeToDelete = $this->getVldUnsRelatedByIdtype(new Criteria(), $con)->diff($vldUnsRelatedByIdtype);

        $this->vldUnsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldUnsRelatedByIdtypeToDelete));

        foreach ($vldUnsRelatedByIdtypeToDelete as $vldUnRelatedByIdtypeRemoved) {
            $vldUnRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldUnsRelatedByIdtype = null;
        foreach ($vldUnsRelatedByIdtype as $vldUnRelatedByIdtype) {
            $this->addVldUnRelatedByIdtype($vldUnRelatedByIdtype);
        }

        $this->collVldUnsRelatedByIdtype = $vldUnsRelatedByIdtype;
        $this->collVldUnsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldUn objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldUn objects.
     * @throws PropelException
     */
    public function countVldUnsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldUnsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldUnsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldUnsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldUnsRelatedByIdtype());
            }
            $query = VldUnQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldUnsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldUn object to this object
     * through the VldUn foreign key attribute.
     *
     * @param    VldUn $l VldUn
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldUnRelatedByIdtype(VldUn $l)
    {
        if ($this->collVldUnsRelatedByIdtype === null) {
            $this->initVldUnsRelatedByIdtype();
            $this->collVldUnsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldUnsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldUnRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldUnRelatedByIdtype $vldUnRelatedByIdtype The vldUnRelatedByIdtype object to add.
     */
    protected function doAddVldUnRelatedByIdtype($vldUnRelatedByIdtype)
    {
        $this->collVldUnsRelatedByIdtype[]= $vldUnRelatedByIdtype;
        $vldUnRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldUnRelatedByIdtype $vldUnRelatedByIdtype The vldUnRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldUnRelatedByIdtype($vldUnRelatedByIdtype)
    {
        if ($this->getVldUnsRelatedByIdtype()->contains($vldUnRelatedByIdtype)) {
            $this->collVldUnsRelatedByIdtype->remove($this->collVldUnsRelatedByIdtype->search($vldUnRelatedByIdtype));
            if (null === $this->vldUnsRelatedByIdtypeScheduledForDeletion) {
                $this->vldUnsRelatedByIdtypeScheduledForDeletion = clone $this->collVldUnsRelatedByIdtype;
                $this->vldUnsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldUnsRelatedByIdtypeScheduledForDeletion[]= clone $vldUnRelatedByIdtype;
            $vldUnRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }

    /**
     * Clears out the collVldUnsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldUnsRelatedByIdtype()
     */
    public function clearVldUnsRelatedByIdtype()
    {
        $this->collVldUnsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldUnsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldUnsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldUnsRelatedByIdtype($v = true)
    {
        $this->collVldUnsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldUnsRelatedByIdtype collection.
     *
     * By default this just sets the collVldUnsRelatedByIdtype collection to an empty array (like clearcollVldUnsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldUnsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldUnsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldUnsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldUnsRelatedByIdtype->setModel('VldUn');
    }

    /**
     * Gets an array of VldUn objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldUn[] List of VldUn objects
     * @throws PropelException
     */
    public function getVldUnsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldUnsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldUnsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldUnsRelatedByIdtype) {
                // return empty collection
                $this->initVldUnsRelatedByIdtype();
            } else {
                $collVldUnsRelatedByIdtype = VldUnQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldUnsRelatedByIdtypePartial && count($collVldUnsRelatedByIdtype)) {
                      $this->initVldUnsRelatedByIdtype(false);

                      foreach($collVldUnsRelatedByIdtype as $obj) {
                        if (false == $this->collVldUnsRelatedByIdtype->contains($obj)) {
                          $this->collVldUnsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldUnsRelatedByIdtypePartial = true;
                    }

                    $collVldUnsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldUnsRelatedByIdtype;
                }

                if($partial && $this->collVldUnsRelatedByIdtype) {
                    foreach($this->collVldUnsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldUnsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldUnsRelatedByIdtype = $collVldUnsRelatedByIdtype;
                $this->collVldUnsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldUnsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldUnRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldUnsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldUnsRelatedByIdtype(PropelCollection $vldUnsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldUnsRelatedByIdtypeToDelete = $this->getVldUnsRelatedByIdtype(new Criteria(), $con)->diff($vldUnsRelatedByIdtype);

        $this->vldUnsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldUnsRelatedByIdtypeToDelete));

        foreach ($vldUnsRelatedByIdtypeToDelete as $vldUnRelatedByIdtypeRemoved) {
            $vldUnRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldUnsRelatedByIdtype = null;
        foreach ($vldUnsRelatedByIdtype as $vldUnRelatedByIdtype) {
            $this->addVldUnRelatedByIdtype($vldUnRelatedByIdtype);
        }

        $this->collVldUnsRelatedByIdtype = $vldUnsRelatedByIdtype;
        $this->collVldUnsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldUn objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldUn objects.
     * @throws PropelException
     */
    public function countVldUnsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldUnsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldUnsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldUnsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldUnsRelatedByIdtype());
            }
            $query = VldUnQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldUnsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldUn object to this object
     * through the VldUn foreign key attribute.
     *
     * @param    VldUn $l VldUn
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldUnRelatedByIdtype(VldUn $l)
    {
        if ($this->collVldUnsRelatedByIdtype === null) {
            $this->initVldUnsRelatedByIdtype();
            $this->collVldUnsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldUnsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldUnRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldUnRelatedByIdtype $vldUnRelatedByIdtype The vldUnRelatedByIdtype object to add.
     */
    protected function doAddVldUnRelatedByIdtype($vldUnRelatedByIdtype)
    {
        $this->collVldUnsRelatedByIdtype[]= $vldUnRelatedByIdtype;
        $vldUnRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldUnRelatedByIdtype $vldUnRelatedByIdtype The vldUnRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldUnRelatedByIdtype($vldUnRelatedByIdtype)
    {
        if ($this->getVldUnsRelatedByIdtype()->contains($vldUnRelatedByIdtype)) {
            $this->collVldUnsRelatedByIdtype->remove($this->collVldUnsRelatedByIdtype->search($vldUnRelatedByIdtype));
            if (null === $this->vldUnsRelatedByIdtypeScheduledForDeletion) {
                $this->vldUnsRelatedByIdtypeScheduledForDeletion = clone $this->collVldUnsRelatedByIdtype;
                $this->vldUnsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldUnsRelatedByIdtypeScheduledForDeletion[]= clone $vldUnRelatedByIdtype;
            $vldUnRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }

    /**
     * Clears out the collVldTunjangansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldTunjangansRelatedByIdtype()
     */
    public function clearVldTunjangansRelatedByIdtype()
    {
        $this->collVldTunjangansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldTunjangansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldTunjangansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldTunjangansRelatedByIdtype($v = true)
    {
        $this->collVldTunjangansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldTunjangansRelatedByIdtype collection.
     *
     * By default this just sets the collVldTunjangansRelatedByIdtype collection to an empty array (like clearcollVldTunjangansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldTunjangansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldTunjangansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldTunjangansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldTunjangansRelatedByIdtype->setModel('VldTunjangan');
    }

    /**
     * Gets an array of VldTunjangan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldTunjangan[] List of VldTunjangan objects
     * @throws PropelException
     */
    public function getVldTunjangansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldTunjangansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldTunjangansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldTunjangansRelatedByIdtype) {
                // return empty collection
                $this->initVldTunjangansRelatedByIdtype();
            } else {
                $collVldTunjangansRelatedByIdtype = VldTunjanganQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldTunjangansRelatedByIdtypePartial && count($collVldTunjangansRelatedByIdtype)) {
                      $this->initVldTunjangansRelatedByIdtype(false);

                      foreach($collVldTunjangansRelatedByIdtype as $obj) {
                        if (false == $this->collVldTunjangansRelatedByIdtype->contains($obj)) {
                          $this->collVldTunjangansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldTunjangansRelatedByIdtypePartial = true;
                    }

                    $collVldTunjangansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldTunjangansRelatedByIdtype;
                }

                if($partial && $this->collVldTunjangansRelatedByIdtype) {
                    foreach($this->collVldTunjangansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldTunjangansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldTunjangansRelatedByIdtype = $collVldTunjangansRelatedByIdtype;
                $this->collVldTunjangansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldTunjangansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldTunjanganRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldTunjangansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldTunjangansRelatedByIdtype(PropelCollection $vldTunjangansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldTunjangansRelatedByIdtypeToDelete = $this->getVldTunjangansRelatedByIdtype(new Criteria(), $con)->diff($vldTunjangansRelatedByIdtype);

        $this->vldTunjangansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldTunjangansRelatedByIdtypeToDelete));

        foreach ($vldTunjangansRelatedByIdtypeToDelete as $vldTunjanganRelatedByIdtypeRemoved) {
            $vldTunjanganRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldTunjangansRelatedByIdtype = null;
        foreach ($vldTunjangansRelatedByIdtype as $vldTunjanganRelatedByIdtype) {
            $this->addVldTunjanganRelatedByIdtype($vldTunjanganRelatedByIdtype);
        }

        $this->collVldTunjangansRelatedByIdtype = $vldTunjangansRelatedByIdtype;
        $this->collVldTunjangansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldTunjangan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldTunjangan objects.
     * @throws PropelException
     */
    public function countVldTunjangansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldTunjangansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldTunjangansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldTunjangansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldTunjangansRelatedByIdtype());
            }
            $query = VldTunjanganQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldTunjangansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldTunjangan object to this object
     * through the VldTunjangan foreign key attribute.
     *
     * @param    VldTunjangan $l VldTunjangan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldTunjanganRelatedByIdtype(VldTunjangan $l)
    {
        if ($this->collVldTunjangansRelatedByIdtype === null) {
            $this->initVldTunjangansRelatedByIdtype();
            $this->collVldTunjangansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldTunjangansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldTunjanganRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldTunjanganRelatedByIdtype $vldTunjanganRelatedByIdtype The vldTunjanganRelatedByIdtype object to add.
     */
    protected function doAddVldTunjanganRelatedByIdtype($vldTunjanganRelatedByIdtype)
    {
        $this->collVldTunjangansRelatedByIdtype[]= $vldTunjanganRelatedByIdtype;
        $vldTunjanganRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldTunjanganRelatedByIdtype $vldTunjanganRelatedByIdtype The vldTunjanganRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldTunjanganRelatedByIdtype($vldTunjanganRelatedByIdtype)
    {
        if ($this->getVldTunjangansRelatedByIdtype()->contains($vldTunjanganRelatedByIdtype)) {
            $this->collVldTunjangansRelatedByIdtype->remove($this->collVldTunjangansRelatedByIdtype->search($vldTunjanganRelatedByIdtype));
            if (null === $this->vldTunjangansRelatedByIdtypeScheduledForDeletion) {
                $this->vldTunjangansRelatedByIdtypeScheduledForDeletion = clone $this->collVldTunjangansRelatedByIdtype;
                $this->vldTunjangansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldTunjangansRelatedByIdtypeScheduledForDeletion[]= clone $vldTunjanganRelatedByIdtype;
            $vldTunjanganRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldTunjangansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldTunjangan[] List of VldTunjangan objects
     */
    public function getVldTunjangansRelatedByIdtypeJoinTunjanganRelatedByTunjanganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldTunjanganQuery::create(null, $criteria);
        $query->joinWith('TunjanganRelatedByTunjanganId', $join_behavior);

        return $this->getVldTunjangansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldTunjangansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldTunjangan[] List of VldTunjangan objects
     */
    public function getVldTunjangansRelatedByIdtypeJoinTunjanganRelatedByTunjanganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldTunjanganQuery::create(null, $criteria);
        $query->joinWith('TunjanganRelatedByTunjanganId', $join_behavior);

        return $this->getVldTunjangansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldTunjangansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldTunjangansRelatedByIdtype()
     */
    public function clearVldTunjangansRelatedByIdtype()
    {
        $this->collVldTunjangansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldTunjangansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldTunjangansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldTunjangansRelatedByIdtype($v = true)
    {
        $this->collVldTunjangansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldTunjangansRelatedByIdtype collection.
     *
     * By default this just sets the collVldTunjangansRelatedByIdtype collection to an empty array (like clearcollVldTunjangansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldTunjangansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldTunjangansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldTunjangansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldTunjangansRelatedByIdtype->setModel('VldTunjangan');
    }

    /**
     * Gets an array of VldTunjangan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldTunjangan[] List of VldTunjangan objects
     * @throws PropelException
     */
    public function getVldTunjangansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldTunjangansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldTunjangansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldTunjangansRelatedByIdtype) {
                // return empty collection
                $this->initVldTunjangansRelatedByIdtype();
            } else {
                $collVldTunjangansRelatedByIdtype = VldTunjanganQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldTunjangansRelatedByIdtypePartial && count($collVldTunjangansRelatedByIdtype)) {
                      $this->initVldTunjangansRelatedByIdtype(false);

                      foreach($collVldTunjangansRelatedByIdtype as $obj) {
                        if (false == $this->collVldTunjangansRelatedByIdtype->contains($obj)) {
                          $this->collVldTunjangansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldTunjangansRelatedByIdtypePartial = true;
                    }

                    $collVldTunjangansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldTunjangansRelatedByIdtype;
                }

                if($partial && $this->collVldTunjangansRelatedByIdtype) {
                    foreach($this->collVldTunjangansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldTunjangansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldTunjangansRelatedByIdtype = $collVldTunjangansRelatedByIdtype;
                $this->collVldTunjangansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldTunjangansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldTunjanganRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldTunjangansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldTunjangansRelatedByIdtype(PropelCollection $vldTunjangansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldTunjangansRelatedByIdtypeToDelete = $this->getVldTunjangansRelatedByIdtype(new Criteria(), $con)->diff($vldTunjangansRelatedByIdtype);

        $this->vldTunjangansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldTunjangansRelatedByIdtypeToDelete));

        foreach ($vldTunjangansRelatedByIdtypeToDelete as $vldTunjanganRelatedByIdtypeRemoved) {
            $vldTunjanganRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldTunjangansRelatedByIdtype = null;
        foreach ($vldTunjangansRelatedByIdtype as $vldTunjanganRelatedByIdtype) {
            $this->addVldTunjanganRelatedByIdtype($vldTunjanganRelatedByIdtype);
        }

        $this->collVldTunjangansRelatedByIdtype = $vldTunjangansRelatedByIdtype;
        $this->collVldTunjangansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldTunjangan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldTunjangan objects.
     * @throws PropelException
     */
    public function countVldTunjangansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldTunjangansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldTunjangansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldTunjangansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldTunjangansRelatedByIdtype());
            }
            $query = VldTunjanganQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldTunjangansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldTunjangan object to this object
     * through the VldTunjangan foreign key attribute.
     *
     * @param    VldTunjangan $l VldTunjangan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldTunjanganRelatedByIdtype(VldTunjangan $l)
    {
        if ($this->collVldTunjangansRelatedByIdtype === null) {
            $this->initVldTunjangansRelatedByIdtype();
            $this->collVldTunjangansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldTunjangansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldTunjanganRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldTunjanganRelatedByIdtype $vldTunjanganRelatedByIdtype The vldTunjanganRelatedByIdtype object to add.
     */
    protected function doAddVldTunjanganRelatedByIdtype($vldTunjanganRelatedByIdtype)
    {
        $this->collVldTunjangansRelatedByIdtype[]= $vldTunjanganRelatedByIdtype;
        $vldTunjanganRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldTunjanganRelatedByIdtype $vldTunjanganRelatedByIdtype The vldTunjanganRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldTunjanganRelatedByIdtype($vldTunjanganRelatedByIdtype)
    {
        if ($this->getVldTunjangansRelatedByIdtype()->contains($vldTunjanganRelatedByIdtype)) {
            $this->collVldTunjangansRelatedByIdtype->remove($this->collVldTunjangansRelatedByIdtype->search($vldTunjanganRelatedByIdtype));
            if (null === $this->vldTunjangansRelatedByIdtypeScheduledForDeletion) {
                $this->vldTunjangansRelatedByIdtypeScheduledForDeletion = clone $this->collVldTunjangansRelatedByIdtype;
                $this->vldTunjangansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldTunjangansRelatedByIdtypeScheduledForDeletion[]= clone $vldTunjanganRelatedByIdtype;
            $vldTunjanganRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldTunjangansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldTunjangan[] List of VldTunjangan objects
     */
    public function getVldTunjangansRelatedByIdtypeJoinTunjanganRelatedByTunjanganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldTunjanganQuery::create(null, $criteria);
        $query->joinWith('TunjanganRelatedByTunjanganId', $join_behavior);

        return $this->getVldTunjangansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldTunjangansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldTunjangan[] List of VldTunjangan objects
     */
    public function getVldTunjangansRelatedByIdtypeJoinTunjanganRelatedByTunjanganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldTunjanganQuery::create(null, $criteria);
        $query->joinWith('TunjanganRelatedByTunjanganId', $join_behavior);

        return $this->getVldTunjangansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldTugasTambahansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldTugasTambahansRelatedByIdtype()
     */
    public function clearVldTugasTambahansRelatedByIdtype()
    {
        $this->collVldTugasTambahansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldTugasTambahansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldTugasTambahansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldTugasTambahansRelatedByIdtype($v = true)
    {
        $this->collVldTugasTambahansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldTugasTambahansRelatedByIdtype collection.
     *
     * By default this just sets the collVldTugasTambahansRelatedByIdtype collection to an empty array (like clearcollVldTugasTambahansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldTugasTambahansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldTugasTambahansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldTugasTambahansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldTugasTambahansRelatedByIdtype->setModel('VldTugasTambahan');
    }

    /**
     * Gets an array of VldTugasTambahan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldTugasTambahan[] List of VldTugasTambahan objects
     * @throws PropelException
     */
    public function getVldTugasTambahansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldTugasTambahansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldTugasTambahansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldTugasTambahansRelatedByIdtype) {
                // return empty collection
                $this->initVldTugasTambahansRelatedByIdtype();
            } else {
                $collVldTugasTambahansRelatedByIdtype = VldTugasTambahanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldTugasTambahansRelatedByIdtypePartial && count($collVldTugasTambahansRelatedByIdtype)) {
                      $this->initVldTugasTambahansRelatedByIdtype(false);

                      foreach($collVldTugasTambahansRelatedByIdtype as $obj) {
                        if (false == $this->collVldTugasTambahansRelatedByIdtype->contains($obj)) {
                          $this->collVldTugasTambahansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldTugasTambahansRelatedByIdtypePartial = true;
                    }

                    $collVldTugasTambahansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldTugasTambahansRelatedByIdtype;
                }

                if($partial && $this->collVldTugasTambahansRelatedByIdtype) {
                    foreach($this->collVldTugasTambahansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldTugasTambahansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldTugasTambahansRelatedByIdtype = $collVldTugasTambahansRelatedByIdtype;
                $this->collVldTugasTambahansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldTugasTambahansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldTugasTambahanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldTugasTambahansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldTugasTambahansRelatedByIdtype(PropelCollection $vldTugasTambahansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldTugasTambahansRelatedByIdtypeToDelete = $this->getVldTugasTambahansRelatedByIdtype(new Criteria(), $con)->diff($vldTugasTambahansRelatedByIdtype);

        $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldTugasTambahansRelatedByIdtypeToDelete));

        foreach ($vldTugasTambahansRelatedByIdtypeToDelete as $vldTugasTambahanRelatedByIdtypeRemoved) {
            $vldTugasTambahanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldTugasTambahansRelatedByIdtype = null;
        foreach ($vldTugasTambahansRelatedByIdtype as $vldTugasTambahanRelatedByIdtype) {
            $this->addVldTugasTambahanRelatedByIdtype($vldTugasTambahanRelatedByIdtype);
        }

        $this->collVldTugasTambahansRelatedByIdtype = $vldTugasTambahansRelatedByIdtype;
        $this->collVldTugasTambahansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldTugasTambahan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldTugasTambahan objects.
     * @throws PropelException
     */
    public function countVldTugasTambahansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldTugasTambahansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldTugasTambahansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldTugasTambahansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldTugasTambahansRelatedByIdtype());
            }
            $query = VldTugasTambahanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldTugasTambahansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldTugasTambahan object to this object
     * through the VldTugasTambahan foreign key attribute.
     *
     * @param    VldTugasTambahan $l VldTugasTambahan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldTugasTambahanRelatedByIdtype(VldTugasTambahan $l)
    {
        if ($this->collVldTugasTambahansRelatedByIdtype === null) {
            $this->initVldTugasTambahansRelatedByIdtype();
            $this->collVldTugasTambahansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldTugasTambahansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldTugasTambahanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldTugasTambahanRelatedByIdtype $vldTugasTambahanRelatedByIdtype The vldTugasTambahanRelatedByIdtype object to add.
     */
    protected function doAddVldTugasTambahanRelatedByIdtype($vldTugasTambahanRelatedByIdtype)
    {
        $this->collVldTugasTambahansRelatedByIdtype[]= $vldTugasTambahanRelatedByIdtype;
        $vldTugasTambahanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldTugasTambahanRelatedByIdtype $vldTugasTambahanRelatedByIdtype The vldTugasTambahanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldTugasTambahanRelatedByIdtype($vldTugasTambahanRelatedByIdtype)
    {
        if ($this->getVldTugasTambahansRelatedByIdtype()->contains($vldTugasTambahanRelatedByIdtype)) {
            $this->collVldTugasTambahansRelatedByIdtype->remove($this->collVldTugasTambahansRelatedByIdtype->search($vldTugasTambahanRelatedByIdtype));
            if (null === $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion) {
                $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion = clone $this->collVldTugasTambahansRelatedByIdtype;
                $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion[]= clone $vldTugasTambahanRelatedByIdtype;
            $vldTugasTambahanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldTugasTambahansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldTugasTambahan[] List of VldTugasTambahan objects
     */
    public function getVldTugasTambahansRelatedByIdtypeJoinTugasTambahanRelatedByPtkTugasTambahanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldTugasTambahanQuery::create(null, $criteria);
        $query->joinWith('TugasTambahanRelatedByPtkTugasTambahanId', $join_behavior);

        return $this->getVldTugasTambahansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldTugasTambahansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldTugasTambahan[] List of VldTugasTambahan objects
     */
    public function getVldTugasTambahansRelatedByIdtypeJoinTugasTambahanRelatedByPtkTugasTambahanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldTugasTambahanQuery::create(null, $criteria);
        $query->joinWith('TugasTambahanRelatedByPtkTugasTambahanId', $join_behavior);

        return $this->getVldTugasTambahansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldTugasTambahansRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldTugasTambahansRelatedByIdtype()
     */
    public function clearVldTugasTambahansRelatedByIdtype()
    {
        $this->collVldTugasTambahansRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldTugasTambahansRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldTugasTambahansRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldTugasTambahansRelatedByIdtype($v = true)
    {
        $this->collVldTugasTambahansRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldTugasTambahansRelatedByIdtype collection.
     *
     * By default this just sets the collVldTugasTambahansRelatedByIdtype collection to an empty array (like clearcollVldTugasTambahansRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldTugasTambahansRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldTugasTambahansRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldTugasTambahansRelatedByIdtype = new PropelObjectCollection();
        $this->collVldTugasTambahansRelatedByIdtype->setModel('VldTugasTambahan');
    }

    /**
     * Gets an array of VldTugasTambahan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldTugasTambahan[] List of VldTugasTambahan objects
     * @throws PropelException
     */
    public function getVldTugasTambahansRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldTugasTambahansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldTugasTambahansRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldTugasTambahansRelatedByIdtype) {
                // return empty collection
                $this->initVldTugasTambahansRelatedByIdtype();
            } else {
                $collVldTugasTambahansRelatedByIdtype = VldTugasTambahanQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldTugasTambahansRelatedByIdtypePartial && count($collVldTugasTambahansRelatedByIdtype)) {
                      $this->initVldTugasTambahansRelatedByIdtype(false);

                      foreach($collVldTugasTambahansRelatedByIdtype as $obj) {
                        if (false == $this->collVldTugasTambahansRelatedByIdtype->contains($obj)) {
                          $this->collVldTugasTambahansRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldTugasTambahansRelatedByIdtypePartial = true;
                    }

                    $collVldTugasTambahansRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldTugasTambahansRelatedByIdtype;
                }

                if($partial && $this->collVldTugasTambahansRelatedByIdtype) {
                    foreach($this->collVldTugasTambahansRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldTugasTambahansRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldTugasTambahansRelatedByIdtype = $collVldTugasTambahansRelatedByIdtype;
                $this->collVldTugasTambahansRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldTugasTambahansRelatedByIdtype;
    }

    /**
     * Sets a collection of VldTugasTambahanRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldTugasTambahansRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldTugasTambahansRelatedByIdtype(PropelCollection $vldTugasTambahansRelatedByIdtype, PropelPDO $con = null)
    {
        $vldTugasTambahansRelatedByIdtypeToDelete = $this->getVldTugasTambahansRelatedByIdtype(new Criteria(), $con)->diff($vldTugasTambahansRelatedByIdtype);

        $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldTugasTambahansRelatedByIdtypeToDelete));

        foreach ($vldTugasTambahansRelatedByIdtypeToDelete as $vldTugasTambahanRelatedByIdtypeRemoved) {
            $vldTugasTambahanRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldTugasTambahansRelatedByIdtype = null;
        foreach ($vldTugasTambahansRelatedByIdtype as $vldTugasTambahanRelatedByIdtype) {
            $this->addVldTugasTambahanRelatedByIdtype($vldTugasTambahanRelatedByIdtype);
        }

        $this->collVldTugasTambahansRelatedByIdtype = $vldTugasTambahansRelatedByIdtype;
        $this->collVldTugasTambahansRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldTugasTambahan objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldTugasTambahan objects.
     * @throws PropelException
     */
    public function countVldTugasTambahansRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldTugasTambahansRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldTugasTambahansRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldTugasTambahansRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldTugasTambahansRelatedByIdtype());
            }
            $query = VldTugasTambahanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldTugasTambahansRelatedByIdtype);
    }

    /**
     * Method called to associate a VldTugasTambahan object to this object
     * through the VldTugasTambahan foreign key attribute.
     *
     * @param    VldTugasTambahan $l VldTugasTambahan
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldTugasTambahanRelatedByIdtype(VldTugasTambahan $l)
    {
        if ($this->collVldTugasTambahansRelatedByIdtype === null) {
            $this->initVldTugasTambahansRelatedByIdtype();
            $this->collVldTugasTambahansRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldTugasTambahansRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldTugasTambahanRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldTugasTambahanRelatedByIdtype $vldTugasTambahanRelatedByIdtype The vldTugasTambahanRelatedByIdtype object to add.
     */
    protected function doAddVldTugasTambahanRelatedByIdtype($vldTugasTambahanRelatedByIdtype)
    {
        $this->collVldTugasTambahansRelatedByIdtype[]= $vldTugasTambahanRelatedByIdtype;
        $vldTugasTambahanRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldTugasTambahanRelatedByIdtype $vldTugasTambahanRelatedByIdtype The vldTugasTambahanRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldTugasTambahanRelatedByIdtype($vldTugasTambahanRelatedByIdtype)
    {
        if ($this->getVldTugasTambahansRelatedByIdtype()->contains($vldTugasTambahanRelatedByIdtype)) {
            $this->collVldTugasTambahansRelatedByIdtype->remove($this->collVldTugasTambahansRelatedByIdtype->search($vldTugasTambahanRelatedByIdtype));
            if (null === $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion) {
                $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion = clone $this->collVldTugasTambahansRelatedByIdtype;
                $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldTugasTambahansRelatedByIdtypeScheduledForDeletion[]= clone $vldTugasTambahanRelatedByIdtype;
            $vldTugasTambahanRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldTugasTambahansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldTugasTambahan[] List of VldTugasTambahan objects
     */
    public function getVldTugasTambahansRelatedByIdtypeJoinTugasTambahanRelatedByPtkTugasTambahanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldTugasTambahanQuery::create(null, $criteria);
        $query->joinWith('TugasTambahanRelatedByPtkTugasTambahanId', $join_behavior);

        return $this->getVldTugasTambahansRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldTugasTambahansRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldTugasTambahan[] List of VldTugasTambahan objects
     */
    public function getVldTugasTambahansRelatedByIdtypeJoinTugasTambahanRelatedByPtkTugasTambahanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldTugasTambahanQuery::create(null, $criteria);
        $query->joinWith('TugasTambahanRelatedByPtkTugasTambahanId', $join_behavior);

        return $this->getVldTugasTambahansRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldSekolahsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldSekolahsRelatedByIdtype()
     */
    public function clearVldSekolahsRelatedByIdtype()
    {
        $this->collVldSekolahsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldSekolahsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldSekolahsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldSekolahsRelatedByIdtype($v = true)
    {
        $this->collVldSekolahsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldSekolahsRelatedByIdtype collection.
     *
     * By default this just sets the collVldSekolahsRelatedByIdtype collection to an empty array (like clearcollVldSekolahsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldSekolahsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldSekolahsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldSekolahsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldSekolahsRelatedByIdtype->setModel('VldSekolah');
    }

    /**
     * Gets an array of VldSekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldSekolah[] List of VldSekolah objects
     * @throws PropelException
     */
    public function getVldSekolahsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldSekolahsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldSekolahsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldSekolahsRelatedByIdtype) {
                // return empty collection
                $this->initVldSekolahsRelatedByIdtype();
            } else {
                $collVldSekolahsRelatedByIdtype = VldSekolahQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldSekolahsRelatedByIdtypePartial && count($collVldSekolahsRelatedByIdtype)) {
                      $this->initVldSekolahsRelatedByIdtype(false);

                      foreach($collVldSekolahsRelatedByIdtype as $obj) {
                        if (false == $this->collVldSekolahsRelatedByIdtype->contains($obj)) {
                          $this->collVldSekolahsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldSekolahsRelatedByIdtypePartial = true;
                    }

                    $collVldSekolahsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldSekolahsRelatedByIdtype;
                }

                if($partial && $this->collVldSekolahsRelatedByIdtype) {
                    foreach($this->collVldSekolahsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldSekolahsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldSekolahsRelatedByIdtype = $collVldSekolahsRelatedByIdtype;
                $this->collVldSekolahsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldSekolahsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldSekolahRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldSekolahsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldSekolahsRelatedByIdtype(PropelCollection $vldSekolahsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldSekolahsRelatedByIdtypeToDelete = $this->getVldSekolahsRelatedByIdtype(new Criteria(), $con)->diff($vldSekolahsRelatedByIdtype);

        $this->vldSekolahsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldSekolahsRelatedByIdtypeToDelete));

        foreach ($vldSekolahsRelatedByIdtypeToDelete as $vldSekolahRelatedByIdtypeRemoved) {
            $vldSekolahRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldSekolahsRelatedByIdtype = null;
        foreach ($vldSekolahsRelatedByIdtype as $vldSekolahRelatedByIdtype) {
            $this->addVldSekolahRelatedByIdtype($vldSekolahRelatedByIdtype);
        }

        $this->collVldSekolahsRelatedByIdtype = $vldSekolahsRelatedByIdtype;
        $this->collVldSekolahsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldSekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldSekolah objects.
     * @throws PropelException
     */
    public function countVldSekolahsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldSekolahsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldSekolahsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldSekolahsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldSekolahsRelatedByIdtype());
            }
            $query = VldSekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldSekolahsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldSekolah object to this object
     * through the VldSekolah foreign key attribute.
     *
     * @param    VldSekolah $l VldSekolah
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldSekolahRelatedByIdtype(VldSekolah $l)
    {
        if ($this->collVldSekolahsRelatedByIdtype === null) {
            $this->initVldSekolahsRelatedByIdtype();
            $this->collVldSekolahsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldSekolahsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldSekolahRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldSekolahRelatedByIdtype $vldSekolahRelatedByIdtype The vldSekolahRelatedByIdtype object to add.
     */
    protected function doAddVldSekolahRelatedByIdtype($vldSekolahRelatedByIdtype)
    {
        $this->collVldSekolahsRelatedByIdtype[]= $vldSekolahRelatedByIdtype;
        $vldSekolahRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldSekolahRelatedByIdtype $vldSekolahRelatedByIdtype The vldSekolahRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldSekolahRelatedByIdtype($vldSekolahRelatedByIdtype)
    {
        if ($this->getVldSekolahsRelatedByIdtype()->contains($vldSekolahRelatedByIdtype)) {
            $this->collVldSekolahsRelatedByIdtype->remove($this->collVldSekolahsRelatedByIdtype->search($vldSekolahRelatedByIdtype));
            if (null === $this->vldSekolahsRelatedByIdtypeScheduledForDeletion) {
                $this->vldSekolahsRelatedByIdtypeScheduledForDeletion = clone $this->collVldSekolahsRelatedByIdtype;
                $this->vldSekolahsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldSekolahsRelatedByIdtypeScheduledForDeletion[]= clone $vldSekolahRelatedByIdtype;
            $vldSekolahRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldSekolahsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSekolah[] List of VldSekolah objects
     */
    public function getVldSekolahsRelatedByIdtypeJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSekolahQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getVldSekolahsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldSekolahsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSekolah[] List of VldSekolah objects
     */
    public function getVldSekolahsRelatedByIdtypeJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSekolahQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getVldSekolahsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldSekolahsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldSekolahsRelatedByIdtype()
     */
    public function clearVldSekolahsRelatedByIdtype()
    {
        $this->collVldSekolahsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldSekolahsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldSekolahsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldSekolahsRelatedByIdtype($v = true)
    {
        $this->collVldSekolahsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldSekolahsRelatedByIdtype collection.
     *
     * By default this just sets the collVldSekolahsRelatedByIdtype collection to an empty array (like clearcollVldSekolahsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldSekolahsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldSekolahsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldSekolahsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldSekolahsRelatedByIdtype->setModel('VldSekolah');
    }

    /**
     * Gets an array of VldSekolah objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldSekolah[] List of VldSekolah objects
     * @throws PropelException
     */
    public function getVldSekolahsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldSekolahsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldSekolahsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldSekolahsRelatedByIdtype) {
                // return empty collection
                $this->initVldSekolahsRelatedByIdtype();
            } else {
                $collVldSekolahsRelatedByIdtype = VldSekolahQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldSekolahsRelatedByIdtypePartial && count($collVldSekolahsRelatedByIdtype)) {
                      $this->initVldSekolahsRelatedByIdtype(false);

                      foreach($collVldSekolahsRelatedByIdtype as $obj) {
                        if (false == $this->collVldSekolahsRelatedByIdtype->contains($obj)) {
                          $this->collVldSekolahsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldSekolahsRelatedByIdtypePartial = true;
                    }

                    $collVldSekolahsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldSekolahsRelatedByIdtype;
                }

                if($partial && $this->collVldSekolahsRelatedByIdtype) {
                    foreach($this->collVldSekolahsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldSekolahsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldSekolahsRelatedByIdtype = $collVldSekolahsRelatedByIdtype;
                $this->collVldSekolahsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldSekolahsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldSekolahRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldSekolahsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldSekolahsRelatedByIdtype(PropelCollection $vldSekolahsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldSekolahsRelatedByIdtypeToDelete = $this->getVldSekolahsRelatedByIdtype(new Criteria(), $con)->diff($vldSekolahsRelatedByIdtype);

        $this->vldSekolahsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldSekolahsRelatedByIdtypeToDelete));

        foreach ($vldSekolahsRelatedByIdtypeToDelete as $vldSekolahRelatedByIdtypeRemoved) {
            $vldSekolahRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldSekolahsRelatedByIdtype = null;
        foreach ($vldSekolahsRelatedByIdtype as $vldSekolahRelatedByIdtype) {
            $this->addVldSekolahRelatedByIdtype($vldSekolahRelatedByIdtype);
        }

        $this->collVldSekolahsRelatedByIdtype = $vldSekolahsRelatedByIdtype;
        $this->collVldSekolahsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldSekolah objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldSekolah objects.
     * @throws PropelException
     */
    public function countVldSekolahsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldSekolahsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldSekolahsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldSekolahsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldSekolahsRelatedByIdtype());
            }
            $query = VldSekolahQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldSekolahsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldSekolah object to this object
     * through the VldSekolah foreign key attribute.
     *
     * @param    VldSekolah $l VldSekolah
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldSekolahRelatedByIdtype(VldSekolah $l)
    {
        if ($this->collVldSekolahsRelatedByIdtype === null) {
            $this->initVldSekolahsRelatedByIdtype();
            $this->collVldSekolahsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldSekolahsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldSekolahRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldSekolahRelatedByIdtype $vldSekolahRelatedByIdtype The vldSekolahRelatedByIdtype object to add.
     */
    protected function doAddVldSekolahRelatedByIdtype($vldSekolahRelatedByIdtype)
    {
        $this->collVldSekolahsRelatedByIdtype[]= $vldSekolahRelatedByIdtype;
        $vldSekolahRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldSekolahRelatedByIdtype $vldSekolahRelatedByIdtype The vldSekolahRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldSekolahRelatedByIdtype($vldSekolahRelatedByIdtype)
    {
        if ($this->getVldSekolahsRelatedByIdtype()->contains($vldSekolahRelatedByIdtype)) {
            $this->collVldSekolahsRelatedByIdtype->remove($this->collVldSekolahsRelatedByIdtype->search($vldSekolahRelatedByIdtype));
            if (null === $this->vldSekolahsRelatedByIdtypeScheduledForDeletion) {
                $this->vldSekolahsRelatedByIdtypeScheduledForDeletion = clone $this->collVldSekolahsRelatedByIdtype;
                $this->vldSekolahsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldSekolahsRelatedByIdtypeScheduledForDeletion[]= clone $vldSekolahRelatedByIdtype;
            $vldSekolahRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldSekolahsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSekolah[] List of VldSekolah objects
     */
    public function getVldSekolahsRelatedByIdtypeJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSekolahQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getVldSekolahsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldSekolahsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSekolah[] List of VldSekolah objects
     */
    public function getVldSekolahsRelatedByIdtypeJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSekolahQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getVldSekolahsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldSaranasRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldSaranasRelatedByIdtype()
     */
    public function clearVldSaranasRelatedByIdtype()
    {
        $this->collVldSaranasRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldSaranasRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldSaranasRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldSaranasRelatedByIdtype($v = true)
    {
        $this->collVldSaranasRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldSaranasRelatedByIdtype collection.
     *
     * By default this just sets the collVldSaranasRelatedByIdtype collection to an empty array (like clearcollVldSaranasRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldSaranasRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldSaranasRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldSaranasRelatedByIdtype = new PropelObjectCollection();
        $this->collVldSaranasRelatedByIdtype->setModel('VldSarana');
    }

    /**
     * Gets an array of VldSarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     * @throws PropelException
     */
    public function getVldSaranasRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldSaranasRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldSaranasRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldSaranasRelatedByIdtype) {
                // return empty collection
                $this->initVldSaranasRelatedByIdtype();
            } else {
                $collVldSaranasRelatedByIdtype = VldSaranaQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldSaranasRelatedByIdtypePartial && count($collVldSaranasRelatedByIdtype)) {
                      $this->initVldSaranasRelatedByIdtype(false);

                      foreach($collVldSaranasRelatedByIdtype as $obj) {
                        if (false == $this->collVldSaranasRelatedByIdtype->contains($obj)) {
                          $this->collVldSaranasRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldSaranasRelatedByIdtypePartial = true;
                    }

                    $collVldSaranasRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldSaranasRelatedByIdtype;
                }

                if($partial && $this->collVldSaranasRelatedByIdtype) {
                    foreach($this->collVldSaranasRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldSaranasRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldSaranasRelatedByIdtype = $collVldSaranasRelatedByIdtype;
                $this->collVldSaranasRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldSaranasRelatedByIdtype;
    }

    /**
     * Sets a collection of VldSaranaRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldSaranasRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldSaranasRelatedByIdtype(PropelCollection $vldSaranasRelatedByIdtype, PropelPDO $con = null)
    {
        $vldSaranasRelatedByIdtypeToDelete = $this->getVldSaranasRelatedByIdtype(new Criteria(), $con)->diff($vldSaranasRelatedByIdtype);

        $this->vldSaranasRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldSaranasRelatedByIdtypeToDelete));

        foreach ($vldSaranasRelatedByIdtypeToDelete as $vldSaranaRelatedByIdtypeRemoved) {
            $vldSaranaRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldSaranasRelatedByIdtype = null;
        foreach ($vldSaranasRelatedByIdtype as $vldSaranaRelatedByIdtype) {
            $this->addVldSaranaRelatedByIdtype($vldSaranaRelatedByIdtype);
        }

        $this->collVldSaranasRelatedByIdtype = $vldSaranasRelatedByIdtype;
        $this->collVldSaranasRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldSarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldSarana objects.
     * @throws PropelException
     */
    public function countVldSaranasRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldSaranasRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldSaranasRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldSaranasRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldSaranasRelatedByIdtype());
            }
            $query = VldSaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldSaranasRelatedByIdtype);
    }

    /**
     * Method called to associate a VldSarana object to this object
     * through the VldSarana foreign key attribute.
     *
     * @param    VldSarana $l VldSarana
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldSaranaRelatedByIdtype(VldSarana $l)
    {
        if ($this->collVldSaranasRelatedByIdtype === null) {
            $this->initVldSaranasRelatedByIdtype();
            $this->collVldSaranasRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldSaranasRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldSaranaRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldSaranaRelatedByIdtype $vldSaranaRelatedByIdtype The vldSaranaRelatedByIdtype object to add.
     */
    protected function doAddVldSaranaRelatedByIdtype($vldSaranaRelatedByIdtype)
    {
        $this->collVldSaranasRelatedByIdtype[]= $vldSaranaRelatedByIdtype;
        $vldSaranaRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldSaranaRelatedByIdtype $vldSaranaRelatedByIdtype The vldSaranaRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldSaranaRelatedByIdtype($vldSaranaRelatedByIdtype)
    {
        if ($this->getVldSaranasRelatedByIdtype()->contains($vldSaranaRelatedByIdtype)) {
            $this->collVldSaranasRelatedByIdtype->remove($this->collVldSaranasRelatedByIdtype->search($vldSaranaRelatedByIdtype));
            if (null === $this->vldSaranasRelatedByIdtypeScheduledForDeletion) {
                $this->vldSaranasRelatedByIdtypeScheduledForDeletion = clone $this->collVldSaranasRelatedByIdtype;
                $this->vldSaranasRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldSaranasRelatedByIdtypeScheduledForDeletion[]= clone $vldSaranaRelatedByIdtype;
            $vldSaranaRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldSaranasRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     */
    public function getVldSaranasRelatedByIdtypeJoinSaranaRelatedBySaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSaranaQuery::create(null, $criteria);
        $query->joinWith('SaranaRelatedBySaranaId', $join_behavior);

        return $this->getVldSaranasRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldSaranasRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     */
    public function getVldSaranasRelatedByIdtypeJoinSaranaRelatedBySaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSaranaQuery::create(null, $criteria);
        $query->joinWith('SaranaRelatedBySaranaId', $join_behavior);

        return $this->getVldSaranasRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldSaranasRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldSaranasRelatedByIdtype()
     */
    public function clearVldSaranasRelatedByIdtype()
    {
        $this->collVldSaranasRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldSaranasRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldSaranasRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldSaranasRelatedByIdtype($v = true)
    {
        $this->collVldSaranasRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldSaranasRelatedByIdtype collection.
     *
     * By default this just sets the collVldSaranasRelatedByIdtype collection to an empty array (like clearcollVldSaranasRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldSaranasRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldSaranasRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldSaranasRelatedByIdtype = new PropelObjectCollection();
        $this->collVldSaranasRelatedByIdtype->setModel('VldSarana');
    }

    /**
     * Gets an array of VldSarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     * @throws PropelException
     */
    public function getVldSaranasRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldSaranasRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldSaranasRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldSaranasRelatedByIdtype) {
                // return empty collection
                $this->initVldSaranasRelatedByIdtype();
            } else {
                $collVldSaranasRelatedByIdtype = VldSaranaQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldSaranasRelatedByIdtypePartial && count($collVldSaranasRelatedByIdtype)) {
                      $this->initVldSaranasRelatedByIdtype(false);

                      foreach($collVldSaranasRelatedByIdtype as $obj) {
                        if (false == $this->collVldSaranasRelatedByIdtype->contains($obj)) {
                          $this->collVldSaranasRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldSaranasRelatedByIdtypePartial = true;
                    }

                    $collVldSaranasRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldSaranasRelatedByIdtype;
                }

                if($partial && $this->collVldSaranasRelatedByIdtype) {
                    foreach($this->collVldSaranasRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldSaranasRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldSaranasRelatedByIdtype = $collVldSaranasRelatedByIdtype;
                $this->collVldSaranasRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldSaranasRelatedByIdtype;
    }

    /**
     * Sets a collection of VldSaranaRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldSaranasRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldSaranasRelatedByIdtype(PropelCollection $vldSaranasRelatedByIdtype, PropelPDO $con = null)
    {
        $vldSaranasRelatedByIdtypeToDelete = $this->getVldSaranasRelatedByIdtype(new Criteria(), $con)->diff($vldSaranasRelatedByIdtype);

        $this->vldSaranasRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldSaranasRelatedByIdtypeToDelete));

        foreach ($vldSaranasRelatedByIdtypeToDelete as $vldSaranaRelatedByIdtypeRemoved) {
            $vldSaranaRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldSaranasRelatedByIdtype = null;
        foreach ($vldSaranasRelatedByIdtype as $vldSaranaRelatedByIdtype) {
            $this->addVldSaranaRelatedByIdtype($vldSaranaRelatedByIdtype);
        }

        $this->collVldSaranasRelatedByIdtype = $vldSaranasRelatedByIdtype;
        $this->collVldSaranasRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldSarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldSarana objects.
     * @throws PropelException
     */
    public function countVldSaranasRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldSaranasRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldSaranasRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldSaranasRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldSaranasRelatedByIdtype());
            }
            $query = VldSaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldSaranasRelatedByIdtype);
    }

    /**
     * Method called to associate a VldSarana object to this object
     * through the VldSarana foreign key attribute.
     *
     * @param    VldSarana $l VldSarana
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldSaranaRelatedByIdtype(VldSarana $l)
    {
        if ($this->collVldSaranasRelatedByIdtype === null) {
            $this->initVldSaranasRelatedByIdtype();
            $this->collVldSaranasRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldSaranasRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldSaranaRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldSaranaRelatedByIdtype $vldSaranaRelatedByIdtype The vldSaranaRelatedByIdtype object to add.
     */
    protected function doAddVldSaranaRelatedByIdtype($vldSaranaRelatedByIdtype)
    {
        $this->collVldSaranasRelatedByIdtype[]= $vldSaranaRelatedByIdtype;
        $vldSaranaRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldSaranaRelatedByIdtype $vldSaranaRelatedByIdtype The vldSaranaRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldSaranaRelatedByIdtype($vldSaranaRelatedByIdtype)
    {
        if ($this->getVldSaranasRelatedByIdtype()->contains($vldSaranaRelatedByIdtype)) {
            $this->collVldSaranasRelatedByIdtype->remove($this->collVldSaranasRelatedByIdtype->search($vldSaranaRelatedByIdtype));
            if (null === $this->vldSaranasRelatedByIdtypeScheduledForDeletion) {
                $this->vldSaranasRelatedByIdtypeScheduledForDeletion = clone $this->collVldSaranasRelatedByIdtype;
                $this->vldSaranasRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldSaranasRelatedByIdtypeScheduledForDeletion[]= clone $vldSaranaRelatedByIdtype;
            $vldSaranaRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldSaranasRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     */
    public function getVldSaranasRelatedByIdtypeJoinSaranaRelatedBySaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSaranaQuery::create(null, $criteria);
        $query->joinWith('SaranaRelatedBySaranaId', $join_behavior);

        return $this->getVldSaranasRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldSaranasRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldSarana[] List of VldSarana objects
     */
    public function getVldSaranasRelatedByIdtypeJoinSaranaRelatedBySaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldSaranaQuery::create(null, $criteria);
        $query->joinWith('SaranaRelatedBySaranaId', $join_behavior);

        return $this->getVldSaranasRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwyStrukturalsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwyStrukturalsRelatedByIdtype()
     */
    public function clearVldRwyStrukturalsRelatedByIdtype()
    {
        $this->collVldRwyStrukturalsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwyStrukturalsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwyStrukturalsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwyStrukturalsRelatedByIdtype($v = true)
    {
        $this->collVldRwyStrukturalsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwyStrukturalsRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwyStrukturalsRelatedByIdtype collection to an empty array (like clearcollVldRwyStrukturalsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwyStrukturalsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwyStrukturalsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwyStrukturalsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwyStrukturalsRelatedByIdtype->setModel('VldRwyStruktural');
    }

    /**
     * Gets an array of VldRwyStruktural objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwyStruktural[] List of VldRwyStruktural objects
     * @throws PropelException
     */
    public function getVldRwyStrukturalsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyStrukturalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyStrukturalsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwyStrukturalsRelatedByIdtype) {
                // return empty collection
                $this->initVldRwyStrukturalsRelatedByIdtype();
            } else {
                $collVldRwyStrukturalsRelatedByIdtype = VldRwyStrukturalQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwyStrukturalsRelatedByIdtypePartial && count($collVldRwyStrukturalsRelatedByIdtype)) {
                      $this->initVldRwyStrukturalsRelatedByIdtype(false);

                      foreach($collVldRwyStrukturalsRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwyStrukturalsRelatedByIdtype->contains($obj)) {
                          $this->collVldRwyStrukturalsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwyStrukturalsRelatedByIdtypePartial = true;
                    }

                    $collVldRwyStrukturalsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwyStrukturalsRelatedByIdtype;
                }

                if($partial && $this->collVldRwyStrukturalsRelatedByIdtype) {
                    foreach($this->collVldRwyStrukturalsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwyStrukturalsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwyStrukturalsRelatedByIdtype = $collVldRwyStrukturalsRelatedByIdtype;
                $this->collVldRwyStrukturalsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwyStrukturalsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwyStrukturalRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwyStrukturalsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwyStrukturalsRelatedByIdtype(PropelCollection $vldRwyStrukturalsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwyStrukturalsRelatedByIdtypeToDelete = $this->getVldRwyStrukturalsRelatedByIdtype(new Criteria(), $con)->diff($vldRwyStrukturalsRelatedByIdtype);

        $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwyStrukturalsRelatedByIdtypeToDelete));

        foreach ($vldRwyStrukturalsRelatedByIdtypeToDelete as $vldRwyStrukturalRelatedByIdtypeRemoved) {
            $vldRwyStrukturalRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwyStrukturalsRelatedByIdtype = null;
        foreach ($vldRwyStrukturalsRelatedByIdtype as $vldRwyStrukturalRelatedByIdtype) {
            $this->addVldRwyStrukturalRelatedByIdtype($vldRwyStrukturalRelatedByIdtype);
        }

        $this->collVldRwyStrukturalsRelatedByIdtype = $vldRwyStrukturalsRelatedByIdtype;
        $this->collVldRwyStrukturalsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwyStruktural objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwyStruktural objects.
     * @throws PropelException
     */
    public function countVldRwyStrukturalsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyStrukturalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyStrukturalsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwyStrukturalsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwyStrukturalsRelatedByIdtype());
            }
            $query = VldRwyStrukturalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwyStrukturalsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwyStruktural object to this object
     * through the VldRwyStruktural foreign key attribute.
     *
     * @param    VldRwyStruktural $l VldRwyStruktural
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwyStrukturalRelatedByIdtype(VldRwyStruktural $l)
    {
        if ($this->collVldRwyStrukturalsRelatedByIdtype === null) {
            $this->initVldRwyStrukturalsRelatedByIdtype();
            $this->collVldRwyStrukturalsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwyStrukturalsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwyStrukturalRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwyStrukturalRelatedByIdtype $vldRwyStrukturalRelatedByIdtype The vldRwyStrukturalRelatedByIdtype object to add.
     */
    protected function doAddVldRwyStrukturalRelatedByIdtype($vldRwyStrukturalRelatedByIdtype)
    {
        $this->collVldRwyStrukturalsRelatedByIdtype[]= $vldRwyStrukturalRelatedByIdtype;
        $vldRwyStrukturalRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwyStrukturalRelatedByIdtype $vldRwyStrukturalRelatedByIdtype The vldRwyStrukturalRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwyStrukturalRelatedByIdtype($vldRwyStrukturalRelatedByIdtype)
    {
        if ($this->getVldRwyStrukturalsRelatedByIdtype()->contains($vldRwyStrukturalRelatedByIdtype)) {
            $this->collVldRwyStrukturalsRelatedByIdtype->remove($this->collVldRwyStrukturalsRelatedByIdtype->search($vldRwyStrukturalRelatedByIdtype));
            if (null === $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwyStrukturalsRelatedByIdtype;
                $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion[]= clone $vldRwyStrukturalRelatedByIdtype;
            $vldRwyStrukturalRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyStrukturalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyStruktural[] List of VldRwyStruktural objects
     */
    public function getVldRwyStrukturalsRelatedByIdtypeJoinRwyStrukturalRelatedByRiwayatStrukturalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyStrukturalQuery::create(null, $criteria);
        $query->joinWith('RwyStrukturalRelatedByRiwayatStrukturalId', $join_behavior);

        return $this->getVldRwyStrukturalsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyStrukturalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyStruktural[] List of VldRwyStruktural objects
     */
    public function getVldRwyStrukturalsRelatedByIdtypeJoinRwyStrukturalRelatedByRiwayatStrukturalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyStrukturalQuery::create(null, $criteria);
        $query->joinWith('RwyStrukturalRelatedByRiwayatStrukturalId', $join_behavior);

        return $this->getVldRwyStrukturalsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwyStrukturalsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwyStrukturalsRelatedByIdtype()
     */
    public function clearVldRwyStrukturalsRelatedByIdtype()
    {
        $this->collVldRwyStrukturalsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwyStrukturalsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwyStrukturalsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwyStrukturalsRelatedByIdtype($v = true)
    {
        $this->collVldRwyStrukturalsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwyStrukturalsRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwyStrukturalsRelatedByIdtype collection to an empty array (like clearcollVldRwyStrukturalsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwyStrukturalsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwyStrukturalsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwyStrukturalsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwyStrukturalsRelatedByIdtype->setModel('VldRwyStruktural');
    }

    /**
     * Gets an array of VldRwyStruktural objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwyStruktural[] List of VldRwyStruktural objects
     * @throws PropelException
     */
    public function getVldRwyStrukturalsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyStrukturalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyStrukturalsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwyStrukturalsRelatedByIdtype) {
                // return empty collection
                $this->initVldRwyStrukturalsRelatedByIdtype();
            } else {
                $collVldRwyStrukturalsRelatedByIdtype = VldRwyStrukturalQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwyStrukturalsRelatedByIdtypePartial && count($collVldRwyStrukturalsRelatedByIdtype)) {
                      $this->initVldRwyStrukturalsRelatedByIdtype(false);

                      foreach($collVldRwyStrukturalsRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwyStrukturalsRelatedByIdtype->contains($obj)) {
                          $this->collVldRwyStrukturalsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwyStrukturalsRelatedByIdtypePartial = true;
                    }

                    $collVldRwyStrukturalsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwyStrukturalsRelatedByIdtype;
                }

                if($partial && $this->collVldRwyStrukturalsRelatedByIdtype) {
                    foreach($this->collVldRwyStrukturalsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwyStrukturalsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwyStrukturalsRelatedByIdtype = $collVldRwyStrukturalsRelatedByIdtype;
                $this->collVldRwyStrukturalsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwyStrukturalsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwyStrukturalRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwyStrukturalsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwyStrukturalsRelatedByIdtype(PropelCollection $vldRwyStrukturalsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwyStrukturalsRelatedByIdtypeToDelete = $this->getVldRwyStrukturalsRelatedByIdtype(new Criteria(), $con)->diff($vldRwyStrukturalsRelatedByIdtype);

        $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwyStrukturalsRelatedByIdtypeToDelete));

        foreach ($vldRwyStrukturalsRelatedByIdtypeToDelete as $vldRwyStrukturalRelatedByIdtypeRemoved) {
            $vldRwyStrukturalRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwyStrukturalsRelatedByIdtype = null;
        foreach ($vldRwyStrukturalsRelatedByIdtype as $vldRwyStrukturalRelatedByIdtype) {
            $this->addVldRwyStrukturalRelatedByIdtype($vldRwyStrukturalRelatedByIdtype);
        }

        $this->collVldRwyStrukturalsRelatedByIdtype = $vldRwyStrukturalsRelatedByIdtype;
        $this->collVldRwyStrukturalsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwyStruktural objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwyStruktural objects.
     * @throws PropelException
     */
    public function countVldRwyStrukturalsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyStrukturalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyStrukturalsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwyStrukturalsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwyStrukturalsRelatedByIdtype());
            }
            $query = VldRwyStrukturalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwyStrukturalsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwyStruktural object to this object
     * through the VldRwyStruktural foreign key attribute.
     *
     * @param    VldRwyStruktural $l VldRwyStruktural
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwyStrukturalRelatedByIdtype(VldRwyStruktural $l)
    {
        if ($this->collVldRwyStrukturalsRelatedByIdtype === null) {
            $this->initVldRwyStrukturalsRelatedByIdtype();
            $this->collVldRwyStrukturalsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwyStrukturalsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwyStrukturalRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwyStrukturalRelatedByIdtype $vldRwyStrukturalRelatedByIdtype The vldRwyStrukturalRelatedByIdtype object to add.
     */
    protected function doAddVldRwyStrukturalRelatedByIdtype($vldRwyStrukturalRelatedByIdtype)
    {
        $this->collVldRwyStrukturalsRelatedByIdtype[]= $vldRwyStrukturalRelatedByIdtype;
        $vldRwyStrukturalRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwyStrukturalRelatedByIdtype $vldRwyStrukturalRelatedByIdtype The vldRwyStrukturalRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwyStrukturalRelatedByIdtype($vldRwyStrukturalRelatedByIdtype)
    {
        if ($this->getVldRwyStrukturalsRelatedByIdtype()->contains($vldRwyStrukturalRelatedByIdtype)) {
            $this->collVldRwyStrukturalsRelatedByIdtype->remove($this->collVldRwyStrukturalsRelatedByIdtype->search($vldRwyStrukturalRelatedByIdtype));
            if (null === $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwyStrukturalsRelatedByIdtype;
                $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwyStrukturalsRelatedByIdtypeScheduledForDeletion[]= clone $vldRwyStrukturalRelatedByIdtype;
            $vldRwyStrukturalRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyStrukturalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyStruktural[] List of VldRwyStruktural objects
     */
    public function getVldRwyStrukturalsRelatedByIdtypeJoinRwyStrukturalRelatedByRiwayatStrukturalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyStrukturalQuery::create(null, $criteria);
        $query->joinWith('RwyStrukturalRelatedByRiwayatStrukturalId', $join_behavior);

        return $this->getVldRwyStrukturalsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyStrukturalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyStruktural[] List of VldRwyStruktural objects
     */
    public function getVldRwyStrukturalsRelatedByIdtypeJoinRwyStrukturalRelatedByRiwayatStrukturalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyStrukturalQuery::create(null, $criteria);
        $query->joinWith('RwyStrukturalRelatedByRiwayatStrukturalId', $join_behavior);

        return $this->getVldRwyStrukturalsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwySertifikasisRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwySertifikasisRelatedByIdtype()
     */
    public function clearVldRwySertifikasisRelatedByIdtype()
    {
        $this->collVldRwySertifikasisRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwySertifikasisRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwySertifikasisRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwySertifikasisRelatedByIdtype($v = true)
    {
        $this->collVldRwySertifikasisRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwySertifikasisRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwySertifikasisRelatedByIdtype collection to an empty array (like clearcollVldRwySertifikasisRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwySertifikasisRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwySertifikasisRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwySertifikasisRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwySertifikasisRelatedByIdtype->setModel('VldRwySertifikasi');
    }

    /**
     * Gets an array of VldRwySertifikasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwySertifikasi[] List of VldRwySertifikasi objects
     * @throws PropelException
     */
    public function getVldRwySertifikasisRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwySertifikasisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwySertifikasisRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwySertifikasisRelatedByIdtype) {
                // return empty collection
                $this->initVldRwySertifikasisRelatedByIdtype();
            } else {
                $collVldRwySertifikasisRelatedByIdtype = VldRwySertifikasiQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwySertifikasisRelatedByIdtypePartial && count($collVldRwySertifikasisRelatedByIdtype)) {
                      $this->initVldRwySertifikasisRelatedByIdtype(false);

                      foreach($collVldRwySertifikasisRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwySertifikasisRelatedByIdtype->contains($obj)) {
                          $this->collVldRwySertifikasisRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwySertifikasisRelatedByIdtypePartial = true;
                    }

                    $collVldRwySertifikasisRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwySertifikasisRelatedByIdtype;
                }

                if($partial && $this->collVldRwySertifikasisRelatedByIdtype) {
                    foreach($this->collVldRwySertifikasisRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwySertifikasisRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwySertifikasisRelatedByIdtype = $collVldRwySertifikasisRelatedByIdtype;
                $this->collVldRwySertifikasisRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwySertifikasisRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwySertifikasiRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwySertifikasisRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwySertifikasisRelatedByIdtype(PropelCollection $vldRwySertifikasisRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwySertifikasisRelatedByIdtypeToDelete = $this->getVldRwySertifikasisRelatedByIdtype(new Criteria(), $con)->diff($vldRwySertifikasisRelatedByIdtype);

        $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwySertifikasisRelatedByIdtypeToDelete));

        foreach ($vldRwySertifikasisRelatedByIdtypeToDelete as $vldRwySertifikasiRelatedByIdtypeRemoved) {
            $vldRwySertifikasiRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwySertifikasisRelatedByIdtype = null;
        foreach ($vldRwySertifikasisRelatedByIdtype as $vldRwySertifikasiRelatedByIdtype) {
            $this->addVldRwySertifikasiRelatedByIdtype($vldRwySertifikasiRelatedByIdtype);
        }

        $this->collVldRwySertifikasisRelatedByIdtype = $vldRwySertifikasisRelatedByIdtype;
        $this->collVldRwySertifikasisRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwySertifikasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwySertifikasi objects.
     * @throws PropelException
     */
    public function countVldRwySertifikasisRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwySertifikasisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwySertifikasisRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwySertifikasisRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwySertifikasisRelatedByIdtype());
            }
            $query = VldRwySertifikasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwySertifikasisRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwySertifikasi object to this object
     * through the VldRwySertifikasi foreign key attribute.
     *
     * @param    VldRwySertifikasi $l VldRwySertifikasi
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwySertifikasiRelatedByIdtype(VldRwySertifikasi $l)
    {
        if ($this->collVldRwySertifikasisRelatedByIdtype === null) {
            $this->initVldRwySertifikasisRelatedByIdtype();
            $this->collVldRwySertifikasisRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwySertifikasisRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwySertifikasiRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwySertifikasiRelatedByIdtype $vldRwySertifikasiRelatedByIdtype The vldRwySertifikasiRelatedByIdtype object to add.
     */
    protected function doAddVldRwySertifikasiRelatedByIdtype($vldRwySertifikasiRelatedByIdtype)
    {
        $this->collVldRwySertifikasisRelatedByIdtype[]= $vldRwySertifikasiRelatedByIdtype;
        $vldRwySertifikasiRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwySertifikasiRelatedByIdtype $vldRwySertifikasiRelatedByIdtype The vldRwySertifikasiRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwySertifikasiRelatedByIdtype($vldRwySertifikasiRelatedByIdtype)
    {
        if ($this->getVldRwySertifikasisRelatedByIdtype()->contains($vldRwySertifikasiRelatedByIdtype)) {
            $this->collVldRwySertifikasisRelatedByIdtype->remove($this->collVldRwySertifikasisRelatedByIdtype->search($vldRwySertifikasiRelatedByIdtype));
            if (null === $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwySertifikasisRelatedByIdtype;
                $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion[]= clone $vldRwySertifikasiRelatedByIdtype;
            $vldRwySertifikasiRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwySertifikasisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwySertifikasi[] List of VldRwySertifikasi objects
     */
    public function getVldRwySertifikasisRelatedByIdtypeJoinRwySertifikasiRelatedByRiwayatSertifikasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('RwySertifikasiRelatedByRiwayatSertifikasiId', $join_behavior);

        return $this->getVldRwySertifikasisRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwySertifikasisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwySertifikasi[] List of VldRwySertifikasi objects
     */
    public function getVldRwySertifikasisRelatedByIdtypeJoinRwySertifikasiRelatedByRiwayatSertifikasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('RwySertifikasiRelatedByRiwayatSertifikasiId', $join_behavior);

        return $this->getVldRwySertifikasisRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwySertifikasisRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwySertifikasisRelatedByIdtype()
     */
    public function clearVldRwySertifikasisRelatedByIdtype()
    {
        $this->collVldRwySertifikasisRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwySertifikasisRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwySertifikasisRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwySertifikasisRelatedByIdtype($v = true)
    {
        $this->collVldRwySertifikasisRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwySertifikasisRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwySertifikasisRelatedByIdtype collection to an empty array (like clearcollVldRwySertifikasisRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwySertifikasisRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwySertifikasisRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwySertifikasisRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwySertifikasisRelatedByIdtype->setModel('VldRwySertifikasi');
    }

    /**
     * Gets an array of VldRwySertifikasi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwySertifikasi[] List of VldRwySertifikasi objects
     * @throws PropelException
     */
    public function getVldRwySertifikasisRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwySertifikasisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwySertifikasisRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwySertifikasisRelatedByIdtype) {
                // return empty collection
                $this->initVldRwySertifikasisRelatedByIdtype();
            } else {
                $collVldRwySertifikasisRelatedByIdtype = VldRwySertifikasiQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwySertifikasisRelatedByIdtypePartial && count($collVldRwySertifikasisRelatedByIdtype)) {
                      $this->initVldRwySertifikasisRelatedByIdtype(false);

                      foreach($collVldRwySertifikasisRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwySertifikasisRelatedByIdtype->contains($obj)) {
                          $this->collVldRwySertifikasisRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwySertifikasisRelatedByIdtypePartial = true;
                    }

                    $collVldRwySertifikasisRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwySertifikasisRelatedByIdtype;
                }

                if($partial && $this->collVldRwySertifikasisRelatedByIdtype) {
                    foreach($this->collVldRwySertifikasisRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwySertifikasisRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwySertifikasisRelatedByIdtype = $collVldRwySertifikasisRelatedByIdtype;
                $this->collVldRwySertifikasisRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwySertifikasisRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwySertifikasiRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwySertifikasisRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwySertifikasisRelatedByIdtype(PropelCollection $vldRwySertifikasisRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwySertifikasisRelatedByIdtypeToDelete = $this->getVldRwySertifikasisRelatedByIdtype(new Criteria(), $con)->diff($vldRwySertifikasisRelatedByIdtype);

        $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwySertifikasisRelatedByIdtypeToDelete));

        foreach ($vldRwySertifikasisRelatedByIdtypeToDelete as $vldRwySertifikasiRelatedByIdtypeRemoved) {
            $vldRwySertifikasiRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwySertifikasisRelatedByIdtype = null;
        foreach ($vldRwySertifikasisRelatedByIdtype as $vldRwySertifikasiRelatedByIdtype) {
            $this->addVldRwySertifikasiRelatedByIdtype($vldRwySertifikasiRelatedByIdtype);
        }

        $this->collVldRwySertifikasisRelatedByIdtype = $vldRwySertifikasisRelatedByIdtype;
        $this->collVldRwySertifikasisRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwySertifikasi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwySertifikasi objects.
     * @throws PropelException
     */
    public function countVldRwySertifikasisRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwySertifikasisRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwySertifikasisRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwySertifikasisRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwySertifikasisRelatedByIdtype());
            }
            $query = VldRwySertifikasiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwySertifikasisRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwySertifikasi object to this object
     * through the VldRwySertifikasi foreign key attribute.
     *
     * @param    VldRwySertifikasi $l VldRwySertifikasi
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwySertifikasiRelatedByIdtype(VldRwySertifikasi $l)
    {
        if ($this->collVldRwySertifikasisRelatedByIdtype === null) {
            $this->initVldRwySertifikasisRelatedByIdtype();
            $this->collVldRwySertifikasisRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwySertifikasisRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwySertifikasiRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwySertifikasiRelatedByIdtype $vldRwySertifikasiRelatedByIdtype The vldRwySertifikasiRelatedByIdtype object to add.
     */
    protected function doAddVldRwySertifikasiRelatedByIdtype($vldRwySertifikasiRelatedByIdtype)
    {
        $this->collVldRwySertifikasisRelatedByIdtype[]= $vldRwySertifikasiRelatedByIdtype;
        $vldRwySertifikasiRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwySertifikasiRelatedByIdtype $vldRwySertifikasiRelatedByIdtype The vldRwySertifikasiRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwySertifikasiRelatedByIdtype($vldRwySertifikasiRelatedByIdtype)
    {
        if ($this->getVldRwySertifikasisRelatedByIdtype()->contains($vldRwySertifikasiRelatedByIdtype)) {
            $this->collVldRwySertifikasisRelatedByIdtype->remove($this->collVldRwySertifikasisRelatedByIdtype->search($vldRwySertifikasiRelatedByIdtype));
            if (null === $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwySertifikasisRelatedByIdtype;
                $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwySertifikasisRelatedByIdtypeScheduledForDeletion[]= clone $vldRwySertifikasiRelatedByIdtype;
            $vldRwySertifikasiRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwySertifikasisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwySertifikasi[] List of VldRwySertifikasi objects
     */
    public function getVldRwySertifikasisRelatedByIdtypeJoinRwySertifikasiRelatedByRiwayatSertifikasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('RwySertifikasiRelatedByRiwayatSertifikasiId', $join_behavior);

        return $this->getVldRwySertifikasisRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwySertifikasisRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwySertifikasi[] List of VldRwySertifikasi objects
     */
    public function getVldRwySertifikasisRelatedByIdtypeJoinRwySertifikasiRelatedByRiwayatSertifikasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwySertifikasiQuery::create(null, $criteria);
        $query->joinWith('RwySertifikasiRelatedByRiwayatSertifikasiId', $join_behavior);

        return $this->getVldRwySertifikasisRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwyPendFormalsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwyPendFormalsRelatedByIdtype()
     */
    public function clearVldRwyPendFormalsRelatedByIdtype()
    {
        $this->collVldRwyPendFormalsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwyPendFormalsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwyPendFormalsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwyPendFormalsRelatedByIdtype($v = true)
    {
        $this->collVldRwyPendFormalsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwyPendFormalsRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwyPendFormalsRelatedByIdtype collection to an empty array (like clearcollVldRwyPendFormalsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwyPendFormalsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwyPendFormalsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwyPendFormalsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwyPendFormalsRelatedByIdtype->setModel('VldRwyPendFormal');
    }

    /**
     * Gets an array of VldRwyPendFormal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwyPendFormal[] List of VldRwyPendFormal objects
     * @throws PropelException
     */
    public function getVldRwyPendFormalsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyPendFormalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyPendFormalsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwyPendFormalsRelatedByIdtype) {
                // return empty collection
                $this->initVldRwyPendFormalsRelatedByIdtype();
            } else {
                $collVldRwyPendFormalsRelatedByIdtype = VldRwyPendFormalQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwyPendFormalsRelatedByIdtypePartial && count($collVldRwyPendFormalsRelatedByIdtype)) {
                      $this->initVldRwyPendFormalsRelatedByIdtype(false);

                      foreach($collVldRwyPendFormalsRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwyPendFormalsRelatedByIdtype->contains($obj)) {
                          $this->collVldRwyPendFormalsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwyPendFormalsRelatedByIdtypePartial = true;
                    }

                    $collVldRwyPendFormalsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwyPendFormalsRelatedByIdtype;
                }

                if($partial && $this->collVldRwyPendFormalsRelatedByIdtype) {
                    foreach($this->collVldRwyPendFormalsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwyPendFormalsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwyPendFormalsRelatedByIdtype = $collVldRwyPendFormalsRelatedByIdtype;
                $this->collVldRwyPendFormalsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwyPendFormalsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwyPendFormalRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwyPendFormalsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwyPendFormalsRelatedByIdtype(PropelCollection $vldRwyPendFormalsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwyPendFormalsRelatedByIdtypeToDelete = $this->getVldRwyPendFormalsRelatedByIdtype(new Criteria(), $con)->diff($vldRwyPendFormalsRelatedByIdtype);

        $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwyPendFormalsRelatedByIdtypeToDelete));

        foreach ($vldRwyPendFormalsRelatedByIdtypeToDelete as $vldRwyPendFormalRelatedByIdtypeRemoved) {
            $vldRwyPendFormalRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwyPendFormalsRelatedByIdtype = null;
        foreach ($vldRwyPendFormalsRelatedByIdtype as $vldRwyPendFormalRelatedByIdtype) {
            $this->addVldRwyPendFormalRelatedByIdtype($vldRwyPendFormalRelatedByIdtype);
        }

        $this->collVldRwyPendFormalsRelatedByIdtype = $vldRwyPendFormalsRelatedByIdtype;
        $this->collVldRwyPendFormalsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwyPendFormal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwyPendFormal objects.
     * @throws PropelException
     */
    public function countVldRwyPendFormalsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyPendFormalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyPendFormalsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwyPendFormalsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwyPendFormalsRelatedByIdtype());
            }
            $query = VldRwyPendFormalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwyPendFormalsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwyPendFormal object to this object
     * through the VldRwyPendFormal foreign key attribute.
     *
     * @param    VldRwyPendFormal $l VldRwyPendFormal
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwyPendFormalRelatedByIdtype(VldRwyPendFormal $l)
    {
        if ($this->collVldRwyPendFormalsRelatedByIdtype === null) {
            $this->initVldRwyPendFormalsRelatedByIdtype();
            $this->collVldRwyPendFormalsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwyPendFormalsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwyPendFormalRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwyPendFormalRelatedByIdtype $vldRwyPendFormalRelatedByIdtype The vldRwyPendFormalRelatedByIdtype object to add.
     */
    protected function doAddVldRwyPendFormalRelatedByIdtype($vldRwyPendFormalRelatedByIdtype)
    {
        $this->collVldRwyPendFormalsRelatedByIdtype[]= $vldRwyPendFormalRelatedByIdtype;
        $vldRwyPendFormalRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwyPendFormalRelatedByIdtype $vldRwyPendFormalRelatedByIdtype The vldRwyPendFormalRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwyPendFormalRelatedByIdtype($vldRwyPendFormalRelatedByIdtype)
    {
        if ($this->getVldRwyPendFormalsRelatedByIdtype()->contains($vldRwyPendFormalRelatedByIdtype)) {
            $this->collVldRwyPendFormalsRelatedByIdtype->remove($this->collVldRwyPendFormalsRelatedByIdtype->search($vldRwyPendFormalRelatedByIdtype));
            if (null === $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwyPendFormalsRelatedByIdtype;
                $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion[]= clone $vldRwyPendFormalRelatedByIdtype;
            $vldRwyPendFormalRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyPendFormalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyPendFormal[] List of VldRwyPendFormal objects
     */
    public function getVldRwyPendFormalsRelatedByIdtypeJoinRwyPendFormalRelatedByRiwayatPendidikanFormalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('RwyPendFormalRelatedByRiwayatPendidikanFormalId', $join_behavior);

        return $this->getVldRwyPendFormalsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyPendFormalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyPendFormal[] List of VldRwyPendFormal objects
     */
    public function getVldRwyPendFormalsRelatedByIdtypeJoinRwyPendFormalRelatedByRiwayatPendidikanFormalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('RwyPendFormalRelatedByRiwayatPendidikanFormalId', $join_behavior);

        return $this->getVldRwyPendFormalsRelatedByIdtype($query, $con);
    }

    /**
     * Clears out the collVldRwyPendFormalsRelatedByIdtype collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Errortype The current object (for fluent API support)
     * @see        addVldRwyPendFormalsRelatedByIdtype()
     */
    public function clearVldRwyPendFormalsRelatedByIdtype()
    {
        $this->collVldRwyPendFormalsRelatedByIdtype = null; // important to set this to null since that means it is uninitialized
        $this->collVldRwyPendFormalsRelatedByIdtypePartial = null;

        return $this;
    }

    /**
     * reset is the collVldRwyPendFormalsRelatedByIdtype collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRwyPendFormalsRelatedByIdtype($v = true)
    {
        $this->collVldRwyPendFormalsRelatedByIdtypePartial = $v;
    }

    /**
     * Initializes the collVldRwyPendFormalsRelatedByIdtype collection.
     *
     * By default this just sets the collVldRwyPendFormalsRelatedByIdtype collection to an empty array (like clearcollVldRwyPendFormalsRelatedByIdtype());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRwyPendFormalsRelatedByIdtype($overrideExisting = true)
    {
        if (null !== $this->collVldRwyPendFormalsRelatedByIdtype && !$overrideExisting) {
            return;
        }
        $this->collVldRwyPendFormalsRelatedByIdtype = new PropelObjectCollection();
        $this->collVldRwyPendFormalsRelatedByIdtype->setModel('VldRwyPendFormal');
    }

    /**
     * Gets an array of VldRwyPendFormal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Errortype is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRwyPendFormal[] List of VldRwyPendFormal objects
     * @throws PropelException
     */
    public function getVldRwyPendFormalsRelatedByIdtype($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyPendFormalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyPendFormalsRelatedByIdtype || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRwyPendFormalsRelatedByIdtype) {
                // return empty collection
                $this->initVldRwyPendFormalsRelatedByIdtype();
            } else {
                $collVldRwyPendFormalsRelatedByIdtype = VldRwyPendFormalQuery::create(null, $criteria)
                    ->filterByErrortypeRelatedByIdtype($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRwyPendFormalsRelatedByIdtypePartial && count($collVldRwyPendFormalsRelatedByIdtype)) {
                      $this->initVldRwyPendFormalsRelatedByIdtype(false);

                      foreach($collVldRwyPendFormalsRelatedByIdtype as $obj) {
                        if (false == $this->collVldRwyPendFormalsRelatedByIdtype->contains($obj)) {
                          $this->collVldRwyPendFormalsRelatedByIdtype->append($obj);
                        }
                      }

                      $this->collVldRwyPendFormalsRelatedByIdtypePartial = true;
                    }

                    $collVldRwyPendFormalsRelatedByIdtype->getInternalIterator()->rewind();
                    return $collVldRwyPendFormalsRelatedByIdtype;
                }

                if($partial && $this->collVldRwyPendFormalsRelatedByIdtype) {
                    foreach($this->collVldRwyPendFormalsRelatedByIdtype as $obj) {
                        if($obj->isNew()) {
                            $collVldRwyPendFormalsRelatedByIdtype[] = $obj;
                        }
                    }
                }

                $this->collVldRwyPendFormalsRelatedByIdtype = $collVldRwyPendFormalsRelatedByIdtype;
                $this->collVldRwyPendFormalsRelatedByIdtypePartial = false;
            }
        }

        return $this->collVldRwyPendFormalsRelatedByIdtype;
    }

    /**
     * Sets a collection of VldRwyPendFormalRelatedByIdtype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRwyPendFormalsRelatedByIdtype A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Errortype The current object (for fluent API support)
     */
    public function setVldRwyPendFormalsRelatedByIdtype(PropelCollection $vldRwyPendFormalsRelatedByIdtype, PropelPDO $con = null)
    {
        $vldRwyPendFormalsRelatedByIdtypeToDelete = $this->getVldRwyPendFormalsRelatedByIdtype(new Criteria(), $con)->diff($vldRwyPendFormalsRelatedByIdtype);

        $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion = unserialize(serialize($vldRwyPendFormalsRelatedByIdtypeToDelete));

        foreach ($vldRwyPendFormalsRelatedByIdtypeToDelete as $vldRwyPendFormalRelatedByIdtypeRemoved) {
            $vldRwyPendFormalRelatedByIdtypeRemoved->setErrortypeRelatedByIdtype(null);
        }

        $this->collVldRwyPendFormalsRelatedByIdtype = null;
        foreach ($vldRwyPendFormalsRelatedByIdtype as $vldRwyPendFormalRelatedByIdtype) {
            $this->addVldRwyPendFormalRelatedByIdtype($vldRwyPendFormalRelatedByIdtype);
        }

        $this->collVldRwyPendFormalsRelatedByIdtype = $vldRwyPendFormalsRelatedByIdtype;
        $this->collVldRwyPendFormalsRelatedByIdtypePartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRwyPendFormal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRwyPendFormal objects.
     * @throws PropelException
     */
    public function countVldRwyPendFormalsRelatedByIdtype(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRwyPendFormalsRelatedByIdtypePartial && !$this->isNew();
        if (null === $this->collVldRwyPendFormalsRelatedByIdtype || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRwyPendFormalsRelatedByIdtype) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRwyPendFormalsRelatedByIdtype());
            }
            $query = VldRwyPendFormalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByErrortypeRelatedByIdtype($this)
                ->count($con);
        }

        return count($this->collVldRwyPendFormalsRelatedByIdtype);
    }

    /**
     * Method called to associate a VldRwyPendFormal object to this object
     * through the VldRwyPendFormal foreign key attribute.
     *
     * @param    VldRwyPendFormal $l VldRwyPendFormal
     * @return Errortype The current object (for fluent API support)
     */
    public function addVldRwyPendFormalRelatedByIdtype(VldRwyPendFormal $l)
    {
        if ($this->collVldRwyPendFormalsRelatedByIdtype === null) {
            $this->initVldRwyPendFormalsRelatedByIdtype();
            $this->collVldRwyPendFormalsRelatedByIdtypePartial = true;
        }
        if (!in_array($l, $this->collVldRwyPendFormalsRelatedByIdtype->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRwyPendFormalRelatedByIdtype($l);
        }

        return $this;
    }

    /**
     * @param	VldRwyPendFormalRelatedByIdtype $vldRwyPendFormalRelatedByIdtype The vldRwyPendFormalRelatedByIdtype object to add.
     */
    protected function doAddVldRwyPendFormalRelatedByIdtype($vldRwyPendFormalRelatedByIdtype)
    {
        $this->collVldRwyPendFormalsRelatedByIdtype[]= $vldRwyPendFormalRelatedByIdtype;
        $vldRwyPendFormalRelatedByIdtype->setErrortypeRelatedByIdtype($this);
    }

    /**
     * @param	VldRwyPendFormalRelatedByIdtype $vldRwyPendFormalRelatedByIdtype The vldRwyPendFormalRelatedByIdtype object to remove.
     * @return Errortype The current object (for fluent API support)
     */
    public function removeVldRwyPendFormalRelatedByIdtype($vldRwyPendFormalRelatedByIdtype)
    {
        if ($this->getVldRwyPendFormalsRelatedByIdtype()->contains($vldRwyPendFormalRelatedByIdtype)) {
            $this->collVldRwyPendFormalsRelatedByIdtype->remove($this->collVldRwyPendFormalsRelatedByIdtype->search($vldRwyPendFormalRelatedByIdtype));
            if (null === $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion) {
                $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion = clone $this->collVldRwyPendFormalsRelatedByIdtype;
                $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion->clear();
            }
            $this->vldRwyPendFormalsRelatedByIdtypeScheduledForDeletion[]= clone $vldRwyPendFormalRelatedByIdtype;
            $vldRwyPendFormalRelatedByIdtype->setErrortypeRelatedByIdtype(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyPendFormalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyPendFormal[] List of VldRwyPendFormal objects
     */
    public function getVldRwyPendFormalsRelatedByIdtypeJoinRwyPendFormalRelatedByRiwayatPendidikanFormalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('RwyPendFormalRelatedByRiwayatPendidikanFormalId', $join_behavior);

        return $this->getVldRwyPendFormalsRelatedByIdtype($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Errortype is new, it will return
     * an empty collection; or if this Errortype has previously
     * been saved, it will retrieve related VldRwyPendFormalsRelatedByIdtype from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Errortype.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRwyPendFormal[] List of VldRwyPendFormal objects
     */
    public function getVldRwyPendFormalsRelatedByIdtypeJoinRwyPendFormalRelatedByRiwayatPendidikanFormalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRwyPendFormalQuery::create(null, $criteria);
        $query->joinWith('RwyPendFormalRelatedByRiwayatPendidikanFormalId', $join_behavior);

        return $this->getVldRwyPendFormalsRelatedByIdtype($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->idtype = null;
        $this->kategori_error = null;
        $this->keterangan = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldRwyKepangkatansRelatedByIdtype) {
                foreach ($this->collVldRwyKepangkatansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwyKepangkatansRelatedByIdtype) {
                foreach ($this->collVldRwyKepangkatansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwyFungsionalsRelatedByIdtype) {
                foreach ($this->collVldRwyFungsionalsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwyFungsionalsRelatedByIdtype) {
                foreach ($this->collVldRwyFungsionalsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRombelsRelatedByIdtype) {
                foreach ($this->collVldRombelsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRombelsRelatedByIdtype) {
                foreach ($this->collVldRombelsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPtksRelatedByIdtype) {
                foreach ($this->collVldPtksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPtksRelatedByIdtype) {
                foreach ($this->collVldPtksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPrestasisRelatedByIdtype) {
                foreach ($this->collVldPrestasisRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPrestasisRelatedByIdtype) {
                foreach ($this->collVldPrestasisRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPrasaranasRelatedByIdtype) {
                foreach ($this->collVldPrasaranasRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPrasaranasRelatedByIdtype) {
                foreach ($this->collVldPrasaranasRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPesertaDidiksRelatedByIdtype) {
                foreach ($this->collVldPesertaDidiksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPesertaDidiksRelatedByIdtype) {
                foreach ($this->collVldPesertaDidiksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPenghargaansRelatedByIdtype) {
                foreach ($this->collVldPenghargaansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPenghargaansRelatedByIdtype) {
                foreach ($this->collVldPenghargaansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPembelajaransRelatedByIdtype) {
                foreach ($this->collVldPembelajaransRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPembelajaransRelatedByIdtype) {
                foreach ($this->collVldPembelajaransRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPdLongsRelatedByIdtype) {
                foreach ($this->collVldPdLongsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPdLongsRelatedByIdtype) {
                foreach ($this->collVldPdLongsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldNonsekolahsRelatedByIdtype) {
                foreach ($this->collVldNonsekolahsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldNonsekolahsRelatedByIdtype) {
                foreach ($this->collVldNonsekolahsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldNilaiTestsRelatedByIdtype) {
                foreach ($this->collVldNilaiTestsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldNilaiTestsRelatedByIdtype) {
                foreach ($this->collVldNilaiTestsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldNilaiRaporsRelatedByIdtype) {
                foreach ($this->collVldNilaiRaporsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldNilaiRaporsRelatedByIdtype) {
                foreach ($this->collVldNilaiRaporsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldMousRelatedByIdtype) {
                foreach ($this->collVldMousRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldMousRelatedByIdtype) {
                foreach ($this->collVldMousRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldKesejahteraansRelatedByIdtype) {
                foreach ($this->collVldKesejahteraansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldKesejahteraansRelatedByIdtype) {
                foreach ($this->collVldKesejahteraansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldKaryaTulissRelatedByIdtype) {
                foreach ($this->collVldKaryaTulissRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldKaryaTulissRelatedByIdtype) {
                foreach ($this->collVldKaryaTulissRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldJurusanSpsRelatedByIdtype) {
                foreach ($this->collVldJurusanSpsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldJurusanSpsRelatedByIdtype) {
                foreach ($this->collVldJurusanSpsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldInpassingsRelatedByIdtype) {
                foreach ($this->collVldInpassingsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldInpassingsRelatedByIdtype) {
                foreach ($this->collVldInpassingsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldDemografisRelatedByIdtype) {
                foreach ($this->collVldDemografisRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldDemografisRelatedByIdtype) {
                foreach ($this->collVldDemografisRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldBukuPtksRelatedByIdtype) {
                foreach ($this->collVldBukuPtksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldBukuPtksRelatedByIdtype) {
                foreach ($this->collVldBukuPtksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldBeaPtksRelatedByIdtype) {
                foreach ($this->collVldBeaPtksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldBeaPtksRelatedByIdtype) {
                foreach ($this->collVldBeaPtksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldBeaPdsRelatedByIdtype) {
                foreach ($this->collVldBeaPdsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldBeaPdsRelatedByIdtype) {
                foreach ($this->collVldBeaPdsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldAnaksRelatedByIdtype) {
                foreach ($this->collVldAnaksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldAnaksRelatedByIdtype) {
                foreach ($this->collVldAnaksRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldYayasansRelatedByIdtype) {
                foreach ($this->collVldYayasansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldYayasansRelatedByIdtype) {
                foreach ($this->collVldYayasansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldUnsRelatedByIdtype) {
                foreach ($this->collVldUnsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldUnsRelatedByIdtype) {
                foreach ($this->collVldUnsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldTunjangansRelatedByIdtype) {
                foreach ($this->collVldTunjangansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldTunjangansRelatedByIdtype) {
                foreach ($this->collVldTunjangansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldTugasTambahansRelatedByIdtype) {
                foreach ($this->collVldTugasTambahansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldTugasTambahansRelatedByIdtype) {
                foreach ($this->collVldTugasTambahansRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldSekolahsRelatedByIdtype) {
                foreach ($this->collVldSekolahsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldSekolahsRelatedByIdtype) {
                foreach ($this->collVldSekolahsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldSaranasRelatedByIdtype) {
                foreach ($this->collVldSaranasRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldSaranasRelatedByIdtype) {
                foreach ($this->collVldSaranasRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwyStrukturalsRelatedByIdtype) {
                foreach ($this->collVldRwyStrukturalsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwyStrukturalsRelatedByIdtype) {
                foreach ($this->collVldRwyStrukturalsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwySertifikasisRelatedByIdtype) {
                foreach ($this->collVldRwySertifikasisRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwySertifikasisRelatedByIdtype) {
                foreach ($this->collVldRwySertifikasisRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwyPendFormalsRelatedByIdtype) {
                foreach ($this->collVldRwyPendFormalsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldRwyPendFormalsRelatedByIdtype) {
                foreach ($this->collVldRwyPendFormalsRelatedByIdtype as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldRwyKepangkatansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwyKepangkatansRelatedByIdtype->clearIterator();
        }
        $this->collVldRwyKepangkatansRelatedByIdtype = null;
        if ($this->collVldRwyKepangkatansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwyKepangkatansRelatedByIdtype->clearIterator();
        }
        $this->collVldRwyKepangkatansRelatedByIdtype = null;
        if ($this->collVldRwyFungsionalsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwyFungsionalsRelatedByIdtype->clearIterator();
        }
        $this->collVldRwyFungsionalsRelatedByIdtype = null;
        if ($this->collVldRwyFungsionalsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwyFungsionalsRelatedByIdtype->clearIterator();
        }
        $this->collVldRwyFungsionalsRelatedByIdtype = null;
        if ($this->collVldRombelsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRombelsRelatedByIdtype->clearIterator();
        }
        $this->collVldRombelsRelatedByIdtype = null;
        if ($this->collVldRombelsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRombelsRelatedByIdtype->clearIterator();
        }
        $this->collVldRombelsRelatedByIdtype = null;
        if ($this->collVldPtksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPtksRelatedByIdtype->clearIterator();
        }
        $this->collVldPtksRelatedByIdtype = null;
        if ($this->collVldPtksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPtksRelatedByIdtype->clearIterator();
        }
        $this->collVldPtksRelatedByIdtype = null;
        if ($this->collVldPrestasisRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPrestasisRelatedByIdtype->clearIterator();
        }
        $this->collVldPrestasisRelatedByIdtype = null;
        if ($this->collVldPrestasisRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPrestasisRelatedByIdtype->clearIterator();
        }
        $this->collVldPrestasisRelatedByIdtype = null;
        if ($this->collVldPrasaranasRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPrasaranasRelatedByIdtype->clearIterator();
        }
        $this->collVldPrasaranasRelatedByIdtype = null;
        if ($this->collVldPrasaranasRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPrasaranasRelatedByIdtype->clearIterator();
        }
        $this->collVldPrasaranasRelatedByIdtype = null;
        if ($this->collVldPesertaDidiksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPesertaDidiksRelatedByIdtype->clearIterator();
        }
        $this->collVldPesertaDidiksRelatedByIdtype = null;
        if ($this->collVldPesertaDidiksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPesertaDidiksRelatedByIdtype->clearIterator();
        }
        $this->collVldPesertaDidiksRelatedByIdtype = null;
        if ($this->collVldPenghargaansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPenghargaansRelatedByIdtype->clearIterator();
        }
        $this->collVldPenghargaansRelatedByIdtype = null;
        if ($this->collVldPenghargaansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPenghargaansRelatedByIdtype->clearIterator();
        }
        $this->collVldPenghargaansRelatedByIdtype = null;
        if ($this->collVldPembelajaransRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPembelajaransRelatedByIdtype->clearIterator();
        }
        $this->collVldPembelajaransRelatedByIdtype = null;
        if ($this->collVldPembelajaransRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPembelajaransRelatedByIdtype->clearIterator();
        }
        $this->collVldPembelajaransRelatedByIdtype = null;
        if ($this->collVldPdLongsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPdLongsRelatedByIdtype->clearIterator();
        }
        $this->collVldPdLongsRelatedByIdtype = null;
        if ($this->collVldPdLongsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldPdLongsRelatedByIdtype->clearIterator();
        }
        $this->collVldPdLongsRelatedByIdtype = null;
        if ($this->collVldNonsekolahsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldNonsekolahsRelatedByIdtype->clearIterator();
        }
        $this->collVldNonsekolahsRelatedByIdtype = null;
        if ($this->collVldNonsekolahsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldNonsekolahsRelatedByIdtype->clearIterator();
        }
        $this->collVldNonsekolahsRelatedByIdtype = null;
        if ($this->collVldNilaiTestsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldNilaiTestsRelatedByIdtype->clearIterator();
        }
        $this->collVldNilaiTestsRelatedByIdtype = null;
        if ($this->collVldNilaiTestsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldNilaiTestsRelatedByIdtype->clearIterator();
        }
        $this->collVldNilaiTestsRelatedByIdtype = null;
        if ($this->collVldNilaiRaporsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldNilaiRaporsRelatedByIdtype->clearIterator();
        }
        $this->collVldNilaiRaporsRelatedByIdtype = null;
        if ($this->collVldNilaiRaporsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldNilaiRaporsRelatedByIdtype->clearIterator();
        }
        $this->collVldNilaiRaporsRelatedByIdtype = null;
        if ($this->collVldMousRelatedByIdtype instanceof PropelCollection) {
            $this->collVldMousRelatedByIdtype->clearIterator();
        }
        $this->collVldMousRelatedByIdtype = null;
        if ($this->collVldMousRelatedByIdtype instanceof PropelCollection) {
            $this->collVldMousRelatedByIdtype->clearIterator();
        }
        $this->collVldMousRelatedByIdtype = null;
        if ($this->collVldKesejahteraansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldKesejahteraansRelatedByIdtype->clearIterator();
        }
        $this->collVldKesejahteraansRelatedByIdtype = null;
        if ($this->collVldKesejahteraansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldKesejahteraansRelatedByIdtype->clearIterator();
        }
        $this->collVldKesejahteraansRelatedByIdtype = null;
        if ($this->collVldKaryaTulissRelatedByIdtype instanceof PropelCollection) {
            $this->collVldKaryaTulissRelatedByIdtype->clearIterator();
        }
        $this->collVldKaryaTulissRelatedByIdtype = null;
        if ($this->collVldKaryaTulissRelatedByIdtype instanceof PropelCollection) {
            $this->collVldKaryaTulissRelatedByIdtype->clearIterator();
        }
        $this->collVldKaryaTulissRelatedByIdtype = null;
        if ($this->collVldJurusanSpsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldJurusanSpsRelatedByIdtype->clearIterator();
        }
        $this->collVldJurusanSpsRelatedByIdtype = null;
        if ($this->collVldJurusanSpsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldJurusanSpsRelatedByIdtype->clearIterator();
        }
        $this->collVldJurusanSpsRelatedByIdtype = null;
        if ($this->collVldInpassingsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldInpassingsRelatedByIdtype->clearIterator();
        }
        $this->collVldInpassingsRelatedByIdtype = null;
        if ($this->collVldInpassingsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldInpassingsRelatedByIdtype->clearIterator();
        }
        $this->collVldInpassingsRelatedByIdtype = null;
        if ($this->collVldDemografisRelatedByIdtype instanceof PropelCollection) {
            $this->collVldDemografisRelatedByIdtype->clearIterator();
        }
        $this->collVldDemografisRelatedByIdtype = null;
        if ($this->collVldDemografisRelatedByIdtype instanceof PropelCollection) {
            $this->collVldDemografisRelatedByIdtype->clearIterator();
        }
        $this->collVldDemografisRelatedByIdtype = null;
        if ($this->collVldBukuPtksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldBukuPtksRelatedByIdtype->clearIterator();
        }
        $this->collVldBukuPtksRelatedByIdtype = null;
        if ($this->collVldBukuPtksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldBukuPtksRelatedByIdtype->clearIterator();
        }
        $this->collVldBukuPtksRelatedByIdtype = null;
        if ($this->collVldBeaPtksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldBeaPtksRelatedByIdtype->clearIterator();
        }
        $this->collVldBeaPtksRelatedByIdtype = null;
        if ($this->collVldBeaPtksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldBeaPtksRelatedByIdtype->clearIterator();
        }
        $this->collVldBeaPtksRelatedByIdtype = null;
        if ($this->collVldBeaPdsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldBeaPdsRelatedByIdtype->clearIterator();
        }
        $this->collVldBeaPdsRelatedByIdtype = null;
        if ($this->collVldBeaPdsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldBeaPdsRelatedByIdtype->clearIterator();
        }
        $this->collVldBeaPdsRelatedByIdtype = null;
        if ($this->collVldAnaksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldAnaksRelatedByIdtype->clearIterator();
        }
        $this->collVldAnaksRelatedByIdtype = null;
        if ($this->collVldAnaksRelatedByIdtype instanceof PropelCollection) {
            $this->collVldAnaksRelatedByIdtype->clearIterator();
        }
        $this->collVldAnaksRelatedByIdtype = null;
        if ($this->collVldYayasansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldYayasansRelatedByIdtype->clearIterator();
        }
        $this->collVldYayasansRelatedByIdtype = null;
        if ($this->collVldYayasansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldYayasansRelatedByIdtype->clearIterator();
        }
        $this->collVldYayasansRelatedByIdtype = null;
        if ($this->collVldUnsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldUnsRelatedByIdtype->clearIterator();
        }
        $this->collVldUnsRelatedByIdtype = null;
        if ($this->collVldUnsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldUnsRelatedByIdtype->clearIterator();
        }
        $this->collVldUnsRelatedByIdtype = null;
        if ($this->collVldTunjangansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldTunjangansRelatedByIdtype->clearIterator();
        }
        $this->collVldTunjangansRelatedByIdtype = null;
        if ($this->collVldTunjangansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldTunjangansRelatedByIdtype->clearIterator();
        }
        $this->collVldTunjangansRelatedByIdtype = null;
        if ($this->collVldTugasTambahansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldTugasTambahansRelatedByIdtype->clearIterator();
        }
        $this->collVldTugasTambahansRelatedByIdtype = null;
        if ($this->collVldTugasTambahansRelatedByIdtype instanceof PropelCollection) {
            $this->collVldTugasTambahansRelatedByIdtype->clearIterator();
        }
        $this->collVldTugasTambahansRelatedByIdtype = null;
        if ($this->collVldSekolahsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldSekolahsRelatedByIdtype->clearIterator();
        }
        $this->collVldSekolahsRelatedByIdtype = null;
        if ($this->collVldSekolahsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldSekolahsRelatedByIdtype->clearIterator();
        }
        $this->collVldSekolahsRelatedByIdtype = null;
        if ($this->collVldSaranasRelatedByIdtype instanceof PropelCollection) {
            $this->collVldSaranasRelatedByIdtype->clearIterator();
        }
        $this->collVldSaranasRelatedByIdtype = null;
        if ($this->collVldSaranasRelatedByIdtype instanceof PropelCollection) {
            $this->collVldSaranasRelatedByIdtype->clearIterator();
        }
        $this->collVldSaranasRelatedByIdtype = null;
        if ($this->collVldRwyStrukturalsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwyStrukturalsRelatedByIdtype->clearIterator();
        }
        $this->collVldRwyStrukturalsRelatedByIdtype = null;
        if ($this->collVldRwyStrukturalsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwyStrukturalsRelatedByIdtype->clearIterator();
        }
        $this->collVldRwyStrukturalsRelatedByIdtype = null;
        if ($this->collVldRwySertifikasisRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwySertifikasisRelatedByIdtype->clearIterator();
        }
        $this->collVldRwySertifikasisRelatedByIdtype = null;
        if ($this->collVldRwySertifikasisRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwySertifikasisRelatedByIdtype->clearIterator();
        }
        $this->collVldRwySertifikasisRelatedByIdtype = null;
        if ($this->collVldRwyPendFormalsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwyPendFormalsRelatedByIdtype->clearIterator();
        }
        $this->collVldRwyPendFormalsRelatedByIdtype = null;
        if ($this->collVldRwyPendFormalsRelatedByIdtype instanceof PropelCollection) {
            $this->collVldRwyPendFormalsRelatedByIdtype->clearIterator();
        }
        $this->collVldRwyPendFormalsRelatedByIdtype = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ErrortypePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
