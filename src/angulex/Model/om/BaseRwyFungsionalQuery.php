<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\JabatanFungsional;
use angulex\Model\Ptk;
use angulex\Model\RwyFungsional;
use angulex\Model\RwyFungsionalPeer;
use angulex\Model\RwyFungsionalQuery;
use angulex\Model\VldRwyFungsional;

/**
 * Base class that represents a query for the 'rwy_fungsional' table.
 *
 * 
 *
 * @method RwyFungsionalQuery orderByRiwayatFungsionalId($order = Criteria::ASC) Order by the riwayat_fungsional_id column
 * @method RwyFungsionalQuery orderByPtkId($order = Criteria::ASC) Order by the ptk_id column
 * @method RwyFungsionalQuery orderByJabatanFungsionalId($order = Criteria::ASC) Order by the jabatan_fungsional_id column
 * @method RwyFungsionalQuery orderBySkJabfung($order = Criteria::ASC) Order by the sk_jabfung column
 * @method RwyFungsionalQuery orderByTmtJabatan($order = Criteria::ASC) Order by the tmt_jabatan column
 * @method RwyFungsionalQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method RwyFungsionalQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method RwyFungsionalQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method RwyFungsionalQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method RwyFungsionalQuery groupByRiwayatFungsionalId() Group by the riwayat_fungsional_id column
 * @method RwyFungsionalQuery groupByPtkId() Group by the ptk_id column
 * @method RwyFungsionalQuery groupByJabatanFungsionalId() Group by the jabatan_fungsional_id column
 * @method RwyFungsionalQuery groupBySkJabfung() Group by the sk_jabfung column
 * @method RwyFungsionalQuery groupByTmtJabatan() Group by the tmt_jabatan column
 * @method RwyFungsionalQuery groupByLastUpdate() Group by the Last_update column
 * @method RwyFungsionalQuery groupBySoftDelete() Group by the Soft_delete column
 * @method RwyFungsionalQuery groupByLastSync() Group by the last_sync column
 * @method RwyFungsionalQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method RwyFungsionalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RwyFungsionalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RwyFungsionalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RwyFungsionalQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyFungsionalQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyFungsionalQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwyFungsionalQuery leftJoinPtkRelatedByPtkId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyFungsionalQuery rightJoinPtkRelatedByPtkId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByPtkId relation
 * @method RwyFungsionalQuery innerJoinPtkRelatedByPtkId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByPtkId relation
 *
 * @method RwyFungsionalQuery leftJoinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JabatanFungsionalRelatedByJabatanFungsionalId relation
 * @method RwyFungsionalQuery rightJoinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JabatanFungsionalRelatedByJabatanFungsionalId relation
 * @method RwyFungsionalQuery innerJoinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias = null) Adds a INNER JOIN clause to the query using the JabatanFungsionalRelatedByJabatanFungsionalId relation
 *
 * @method RwyFungsionalQuery leftJoinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JabatanFungsionalRelatedByJabatanFungsionalId relation
 * @method RwyFungsionalQuery rightJoinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JabatanFungsionalRelatedByJabatanFungsionalId relation
 * @method RwyFungsionalQuery innerJoinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias = null) Adds a INNER JOIN clause to the query using the JabatanFungsionalRelatedByJabatanFungsionalId relation
 *
 * @method RwyFungsionalQuery leftJoinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyFungsionalRelatedByRiwayatFungsionalId relation
 * @method RwyFungsionalQuery rightJoinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyFungsionalRelatedByRiwayatFungsionalId relation
 * @method RwyFungsionalQuery innerJoinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyFungsionalRelatedByRiwayatFungsionalId relation
 *
 * @method RwyFungsionalQuery leftJoinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldRwyFungsionalRelatedByRiwayatFungsionalId relation
 * @method RwyFungsionalQuery rightJoinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldRwyFungsionalRelatedByRiwayatFungsionalId relation
 * @method RwyFungsionalQuery innerJoinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldRwyFungsionalRelatedByRiwayatFungsionalId relation
 *
 * @method RwyFungsional findOne(PropelPDO $con = null) Return the first RwyFungsional matching the query
 * @method RwyFungsional findOneOrCreate(PropelPDO $con = null) Return the first RwyFungsional matching the query, or a new RwyFungsional object populated from the query conditions when no match is found
 *
 * @method RwyFungsional findOneByPtkId(string $ptk_id) Return the first RwyFungsional filtered by the ptk_id column
 * @method RwyFungsional findOneByJabatanFungsionalId(string $jabatan_fungsional_id) Return the first RwyFungsional filtered by the jabatan_fungsional_id column
 * @method RwyFungsional findOneBySkJabfung(string $sk_jabfung) Return the first RwyFungsional filtered by the sk_jabfung column
 * @method RwyFungsional findOneByTmtJabatan(string $tmt_jabatan) Return the first RwyFungsional filtered by the tmt_jabatan column
 * @method RwyFungsional findOneByLastUpdate(string $Last_update) Return the first RwyFungsional filtered by the Last_update column
 * @method RwyFungsional findOneBySoftDelete(string $Soft_delete) Return the first RwyFungsional filtered by the Soft_delete column
 * @method RwyFungsional findOneByLastSync(string $last_sync) Return the first RwyFungsional filtered by the last_sync column
 * @method RwyFungsional findOneByUpdaterId(string $Updater_ID) Return the first RwyFungsional filtered by the Updater_ID column
 *
 * @method array findByRiwayatFungsionalId(string $riwayat_fungsional_id) Return RwyFungsional objects filtered by the riwayat_fungsional_id column
 * @method array findByPtkId(string $ptk_id) Return RwyFungsional objects filtered by the ptk_id column
 * @method array findByJabatanFungsionalId(string $jabatan_fungsional_id) Return RwyFungsional objects filtered by the jabatan_fungsional_id column
 * @method array findBySkJabfung(string $sk_jabfung) Return RwyFungsional objects filtered by the sk_jabfung column
 * @method array findByTmtJabatan(string $tmt_jabatan) Return RwyFungsional objects filtered by the tmt_jabatan column
 * @method array findByLastUpdate(string $Last_update) Return RwyFungsional objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return RwyFungsional objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return RwyFungsional objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return RwyFungsional objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseRwyFungsionalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRwyFungsionalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\RwyFungsional', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RwyFungsionalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RwyFungsionalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RwyFungsionalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RwyFungsionalQuery) {
            return $criteria;
        }
        $query = new RwyFungsionalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RwyFungsional|RwyFungsional[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RwyFungsionalPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RwyFungsionalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwyFungsional A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByRiwayatFungsionalId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RwyFungsional A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [riwayat_fungsional_id], [ptk_id], [jabatan_fungsional_id], [sk_jabfung], [tmt_jabatan], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [rwy_fungsional] WHERE [riwayat_fungsional_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RwyFungsional();
            $obj->hydrate($row);
            RwyFungsionalPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RwyFungsional|RwyFungsional[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RwyFungsional[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RwyFungsionalPeer::RIWAYAT_FUNGSIONAL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RwyFungsionalPeer::RIWAYAT_FUNGSIONAL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the riwayat_fungsional_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRiwayatFungsionalId('fooValue');   // WHERE riwayat_fungsional_id = 'fooValue'
     * $query->filterByRiwayatFungsionalId('%fooValue%'); // WHERE riwayat_fungsional_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $riwayatFungsionalId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByRiwayatFungsionalId($riwayatFungsionalId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($riwayatFungsionalId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $riwayatFungsionalId)) {
                $riwayatFungsionalId = str_replace('*', '%', $riwayatFungsionalId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::RIWAYAT_FUNGSIONAL_ID, $riwayatFungsionalId, $comparison);
    }

    /**
     * Filter the query on the ptk_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPtkId('fooValue');   // WHERE ptk_id = 'fooValue'
     * $query->filterByPtkId('%fooValue%'); // WHERE ptk_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ptkId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByPtkId($ptkId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ptkId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ptkId)) {
                $ptkId = str_replace('*', '%', $ptkId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::PTK_ID, $ptkId, $comparison);
    }

    /**
     * Filter the query on the jabatan_fungsional_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJabatanFungsionalId(1234); // WHERE jabatan_fungsional_id = 1234
     * $query->filterByJabatanFungsionalId(array(12, 34)); // WHERE jabatan_fungsional_id IN (12, 34)
     * $query->filterByJabatanFungsionalId(array('min' => 12)); // WHERE jabatan_fungsional_id >= 12
     * $query->filterByJabatanFungsionalId(array('max' => 12)); // WHERE jabatan_fungsional_id <= 12
     * </code>
     *
     * @see       filterByJabatanFungsionalRelatedByJabatanFungsionalId()
     *
     * @see       filterByJabatanFungsionalRelatedByJabatanFungsionalId()
     *
     * @param     mixed $jabatanFungsionalId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByJabatanFungsionalId($jabatanFungsionalId = null, $comparison = null)
    {
        if (is_array($jabatanFungsionalId)) {
            $useMinMax = false;
            if (isset($jabatanFungsionalId['min'])) {
                $this->addUsingAlias(RwyFungsionalPeer::JABATAN_FUNGSIONAL_ID, $jabatanFungsionalId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jabatanFungsionalId['max'])) {
                $this->addUsingAlias(RwyFungsionalPeer::JABATAN_FUNGSIONAL_ID, $jabatanFungsionalId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::JABATAN_FUNGSIONAL_ID, $jabatanFungsionalId, $comparison);
    }

    /**
     * Filter the query on the sk_jabfung column
     *
     * Example usage:
     * <code>
     * $query->filterBySkJabfung('fooValue');   // WHERE sk_jabfung = 'fooValue'
     * $query->filterBySkJabfung('%fooValue%'); // WHERE sk_jabfung LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skJabfung The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterBySkJabfung($skJabfung = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skJabfung)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $skJabfung)) {
                $skJabfung = str_replace('*', '%', $skJabfung);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::SK_JABFUNG, $skJabfung, $comparison);
    }

    /**
     * Filter the query on the tmt_jabatan column
     *
     * Example usage:
     * <code>
     * $query->filterByTmtJabatan('fooValue');   // WHERE tmt_jabatan = 'fooValue'
     * $query->filterByTmtJabatan('%fooValue%'); // WHERE tmt_jabatan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tmtJabatan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByTmtJabatan($tmtJabatan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tmtJabatan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tmtJabatan)) {
                $tmtJabatan = str_replace('*', '%', $tmtJabatan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::TMT_JABATAN, $tmtJabatan, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(RwyFungsionalPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(RwyFungsionalPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(RwyFungsionalPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(RwyFungsionalPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(RwyFungsionalPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(RwyFungsionalPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RwyFungsionalPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyFungsionalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwyFungsionalPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyFungsionalPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyFungsionalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByPtkId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(RwyFungsionalPeer::PTK_ID, $ptk->getPtkId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyFungsionalPeer::PTK_ID, $ptk->toKeyValue('PrimaryKey', 'PtkId'), $comparison);
        } else {
            throw new PropelException('filterByPtkRelatedByPtkId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByPtkId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByPtkId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByPtkId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByPtkId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByPtkId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByPtkIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByPtkId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByPtkId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related JabatanFungsional object
     *
     * @param   JabatanFungsional|PropelObjectCollection $jabatanFungsional The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyFungsionalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJabatanFungsionalRelatedByJabatanFungsionalId($jabatanFungsional, $comparison = null)
    {
        if ($jabatanFungsional instanceof JabatanFungsional) {
            return $this
                ->addUsingAlias(RwyFungsionalPeer::JABATAN_FUNGSIONAL_ID, $jabatanFungsional->getJabatanFungsionalId(), $comparison);
        } elseif ($jabatanFungsional instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyFungsionalPeer::JABATAN_FUNGSIONAL_ID, $jabatanFungsional->toKeyValue('PrimaryKey', 'JabatanFungsionalId'), $comparison);
        } else {
            throw new PropelException('filterByJabatanFungsionalRelatedByJabatanFungsionalId() only accepts arguments of type JabatanFungsional or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JabatanFungsionalRelatedByJabatanFungsionalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function joinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JabatanFungsionalRelatedByJabatanFungsionalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JabatanFungsionalRelatedByJabatanFungsionalId');
        }

        return $this;
    }

    /**
     * Use the JabatanFungsionalRelatedByJabatanFungsionalId relation JabatanFungsional object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JabatanFungsionalQuery A secondary query class using the current class as primary query
     */
    public function useJabatanFungsionalRelatedByJabatanFungsionalIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JabatanFungsionalRelatedByJabatanFungsionalId', '\angulex\Model\JabatanFungsionalQuery');
    }

    /**
     * Filter the query by a related JabatanFungsional object
     *
     * @param   JabatanFungsional|PropelObjectCollection $jabatanFungsional The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyFungsionalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJabatanFungsionalRelatedByJabatanFungsionalId($jabatanFungsional, $comparison = null)
    {
        if ($jabatanFungsional instanceof JabatanFungsional) {
            return $this
                ->addUsingAlias(RwyFungsionalPeer::JABATAN_FUNGSIONAL_ID, $jabatanFungsional->getJabatanFungsionalId(), $comparison);
        } elseif ($jabatanFungsional instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RwyFungsionalPeer::JABATAN_FUNGSIONAL_ID, $jabatanFungsional->toKeyValue('PrimaryKey', 'JabatanFungsionalId'), $comparison);
        } else {
            throw new PropelException('filterByJabatanFungsionalRelatedByJabatanFungsionalId() only accepts arguments of type JabatanFungsional or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JabatanFungsionalRelatedByJabatanFungsionalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function joinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JabatanFungsionalRelatedByJabatanFungsionalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JabatanFungsionalRelatedByJabatanFungsionalId');
        }

        return $this;
    }

    /**
     * Use the JabatanFungsionalRelatedByJabatanFungsionalId relation JabatanFungsional object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JabatanFungsionalQuery A secondary query class using the current class as primary query
     */
    public function useJabatanFungsionalRelatedByJabatanFungsionalIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJabatanFungsionalRelatedByJabatanFungsionalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JabatanFungsionalRelatedByJabatanFungsionalId', '\angulex\Model\JabatanFungsionalQuery');
    }

    /**
     * Filter the query by a related VldRwyFungsional object
     *
     * @param   VldRwyFungsional|PropelObjectCollection $vldRwyFungsional  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyFungsionalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyFungsionalRelatedByRiwayatFungsionalId($vldRwyFungsional, $comparison = null)
    {
        if ($vldRwyFungsional instanceof VldRwyFungsional) {
            return $this
                ->addUsingAlias(RwyFungsionalPeer::RIWAYAT_FUNGSIONAL_ID, $vldRwyFungsional->getRiwayatFungsionalId(), $comparison);
        } elseif ($vldRwyFungsional instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyFungsionalRelatedByRiwayatFungsionalIdQuery()
                ->filterByPrimaryKeys($vldRwyFungsional->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyFungsionalRelatedByRiwayatFungsionalId() only accepts arguments of type VldRwyFungsional or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyFungsionalRelatedByRiwayatFungsionalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function joinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyFungsionalRelatedByRiwayatFungsionalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyFungsionalRelatedByRiwayatFungsionalId');
        }

        return $this;
    }

    /**
     * Use the VldRwyFungsionalRelatedByRiwayatFungsionalId relation VldRwyFungsional object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyFungsionalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyFungsionalRelatedByRiwayatFungsionalIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyFungsionalRelatedByRiwayatFungsionalId', '\angulex\Model\VldRwyFungsionalQuery');
    }

    /**
     * Filter the query by a related VldRwyFungsional object
     *
     * @param   VldRwyFungsional|PropelObjectCollection $vldRwyFungsional  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RwyFungsionalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldRwyFungsionalRelatedByRiwayatFungsionalId($vldRwyFungsional, $comparison = null)
    {
        if ($vldRwyFungsional instanceof VldRwyFungsional) {
            return $this
                ->addUsingAlias(RwyFungsionalPeer::RIWAYAT_FUNGSIONAL_ID, $vldRwyFungsional->getRiwayatFungsionalId(), $comparison);
        } elseif ($vldRwyFungsional instanceof PropelObjectCollection) {
            return $this
                ->useVldRwyFungsionalRelatedByRiwayatFungsionalIdQuery()
                ->filterByPrimaryKeys($vldRwyFungsional->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldRwyFungsionalRelatedByRiwayatFungsionalId() only accepts arguments of type VldRwyFungsional or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldRwyFungsionalRelatedByRiwayatFungsionalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function joinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldRwyFungsionalRelatedByRiwayatFungsionalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldRwyFungsionalRelatedByRiwayatFungsionalId');
        }

        return $this;
    }

    /**
     * Use the VldRwyFungsionalRelatedByRiwayatFungsionalId relation VldRwyFungsional object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldRwyFungsionalQuery A secondary query class using the current class as primary query
     */
    public function useVldRwyFungsionalRelatedByRiwayatFungsionalIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldRwyFungsionalRelatedByRiwayatFungsionalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldRwyFungsionalRelatedByRiwayatFungsionalId', '\angulex\Model\VldRwyFungsionalQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RwyFungsional $rwyFungsional Object to remove from the list of results
     *
     * @return RwyFungsionalQuery The current query, for fluid interface
     */
    public function prune($rwyFungsional = null)
    {
        if ($rwyFungsional) {
            $this->addUsingAlias(RwyFungsionalPeer::RIWAYAT_FUNGSIONAL_ID, $rwyFungsional->getRiwayatFungsionalId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
