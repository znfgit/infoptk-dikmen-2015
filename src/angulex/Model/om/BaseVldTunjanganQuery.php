<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Errortype;
use angulex\Model\Tunjangan;
use angulex\Model\VldTunjangan;
use angulex\Model\VldTunjanganPeer;
use angulex\Model\VldTunjanganQuery;

/**
 * Base class that represents a query for the 'vld_tunjangan' table.
 *
 * 
 *
 * @method VldTunjanganQuery orderByTunjanganId($order = Criteria::ASC) Order by the tunjangan_id column
 * @method VldTunjanganQuery orderByLogid($order = Criteria::ASC) Order by the logid column
 * @method VldTunjanganQuery orderByIdtype($order = Criteria::ASC) Order by the idtype column
 * @method VldTunjanganQuery orderByStatusValidasi($order = Criteria::ASC) Order by the status_validasi column
 * @method VldTunjanganQuery orderByStatusVerifikasi($order = Criteria::ASC) Order by the status_verifikasi column
 * @method VldTunjanganQuery orderByFieldError($order = Criteria::ASC) Order by the field_error column
 * @method VldTunjanganQuery orderByErrorMessage($order = Criteria::ASC) Order by the error_message column
 * @method VldTunjanganQuery orderByKeterangan($order = Criteria::ASC) Order by the keterangan column
 * @method VldTunjanganQuery orderByCreateDate($order = Criteria::ASC) Order by the create_date column
 * @method VldTunjanganQuery orderByLastUpdate($order = Criteria::ASC) Order by the last_update column
 * @method VldTunjanganQuery orderByExpiredDate($order = Criteria::ASC) Order by the expired_date column
 * @method VldTunjanganQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method VldTunjanganQuery orderByAppUsername($order = Criteria::ASC) Order by the app_username column
 *
 * @method VldTunjanganQuery groupByTunjanganId() Group by the tunjangan_id column
 * @method VldTunjanganQuery groupByLogid() Group by the logid column
 * @method VldTunjanganQuery groupByIdtype() Group by the idtype column
 * @method VldTunjanganQuery groupByStatusValidasi() Group by the status_validasi column
 * @method VldTunjanganQuery groupByStatusVerifikasi() Group by the status_verifikasi column
 * @method VldTunjanganQuery groupByFieldError() Group by the field_error column
 * @method VldTunjanganQuery groupByErrorMessage() Group by the error_message column
 * @method VldTunjanganQuery groupByKeterangan() Group by the keterangan column
 * @method VldTunjanganQuery groupByCreateDate() Group by the create_date column
 * @method VldTunjanganQuery groupByLastUpdate() Group by the last_update column
 * @method VldTunjanganQuery groupByExpiredDate() Group by the expired_date column
 * @method VldTunjanganQuery groupByLastSync() Group by the last_sync column
 * @method VldTunjanganQuery groupByAppUsername() Group by the app_username column
 *
 * @method VldTunjanganQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method VldTunjanganQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method VldTunjanganQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method VldTunjanganQuery leftJoinTunjanganRelatedByTunjanganId($relationAlias = null) Adds a LEFT JOIN clause to the query using the TunjanganRelatedByTunjanganId relation
 * @method VldTunjanganQuery rightJoinTunjanganRelatedByTunjanganId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TunjanganRelatedByTunjanganId relation
 * @method VldTunjanganQuery innerJoinTunjanganRelatedByTunjanganId($relationAlias = null) Adds a INNER JOIN clause to the query using the TunjanganRelatedByTunjanganId relation
 *
 * @method VldTunjanganQuery leftJoinTunjanganRelatedByTunjanganId($relationAlias = null) Adds a LEFT JOIN clause to the query using the TunjanganRelatedByTunjanganId relation
 * @method VldTunjanganQuery rightJoinTunjanganRelatedByTunjanganId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TunjanganRelatedByTunjanganId relation
 * @method VldTunjanganQuery innerJoinTunjanganRelatedByTunjanganId($relationAlias = null) Adds a INNER JOIN clause to the query using the TunjanganRelatedByTunjanganId relation
 *
 * @method VldTunjanganQuery leftJoinErrortypeRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the ErrortypeRelatedByIdtype relation
 * @method VldTunjanganQuery rightJoinErrortypeRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ErrortypeRelatedByIdtype relation
 * @method VldTunjanganQuery innerJoinErrortypeRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the ErrortypeRelatedByIdtype relation
 *
 * @method VldTunjanganQuery leftJoinErrortypeRelatedByIdtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the ErrortypeRelatedByIdtype relation
 * @method VldTunjanganQuery rightJoinErrortypeRelatedByIdtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ErrortypeRelatedByIdtype relation
 * @method VldTunjanganQuery innerJoinErrortypeRelatedByIdtype($relationAlias = null) Adds a INNER JOIN clause to the query using the ErrortypeRelatedByIdtype relation
 *
 * @method VldTunjangan findOne(PropelPDO $con = null) Return the first VldTunjangan matching the query
 * @method VldTunjangan findOneOrCreate(PropelPDO $con = null) Return the first VldTunjangan matching the query, or a new VldTunjangan object populated from the query conditions when no match is found
 *
 * @method VldTunjangan findOneByTunjanganId(string $tunjangan_id) Return the first VldTunjangan filtered by the tunjangan_id column
 * @method VldTunjangan findOneByLogid(string $logid) Return the first VldTunjangan filtered by the logid column
 * @method VldTunjangan findOneByIdtype(int $idtype) Return the first VldTunjangan filtered by the idtype column
 * @method VldTunjangan findOneByStatusValidasi(string $status_validasi) Return the first VldTunjangan filtered by the status_validasi column
 * @method VldTunjangan findOneByStatusVerifikasi(string $status_verifikasi) Return the first VldTunjangan filtered by the status_verifikasi column
 * @method VldTunjangan findOneByFieldError(string $field_error) Return the first VldTunjangan filtered by the field_error column
 * @method VldTunjangan findOneByErrorMessage(string $error_message) Return the first VldTunjangan filtered by the error_message column
 * @method VldTunjangan findOneByKeterangan(string $keterangan) Return the first VldTunjangan filtered by the keterangan column
 * @method VldTunjangan findOneByCreateDate(string $create_date) Return the first VldTunjangan filtered by the create_date column
 * @method VldTunjangan findOneByLastUpdate(string $last_update) Return the first VldTunjangan filtered by the last_update column
 * @method VldTunjangan findOneByExpiredDate(string $expired_date) Return the first VldTunjangan filtered by the expired_date column
 * @method VldTunjangan findOneByLastSync(string $last_sync) Return the first VldTunjangan filtered by the last_sync column
 * @method VldTunjangan findOneByAppUsername(string $app_username) Return the first VldTunjangan filtered by the app_username column
 *
 * @method array findByTunjanganId(string $tunjangan_id) Return VldTunjangan objects filtered by the tunjangan_id column
 * @method array findByLogid(string $logid) Return VldTunjangan objects filtered by the logid column
 * @method array findByIdtype(int $idtype) Return VldTunjangan objects filtered by the idtype column
 * @method array findByStatusValidasi(string $status_validasi) Return VldTunjangan objects filtered by the status_validasi column
 * @method array findByStatusVerifikasi(string $status_verifikasi) Return VldTunjangan objects filtered by the status_verifikasi column
 * @method array findByFieldError(string $field_error) Return VldTunjangan objects filtered by the field_error column
 * @method array findByErrorMessage(string $error_message) Return VldTunjangan objects filtered by the error_message column
 * @method array findByKeterangan(string $keterangan) Return VldTunjangan objects filtered by the keterangan column
 * @method array findByCreateDate(string $create_date) Return VldTunjangan objects filtered by the create_date column
 * @method array findByLastUpdate(string $last_update) Return VldTunjangan objects filtered by the last_update column
 * @method array findByExpiredDate(string $expired_date) Return VldTunjangan objects filtered by the expired_date column
 * @method array findByLastSync(string $last_sync) Return VldTunjangan objects filtered by the last_sync column
 * @method array findByAppUsername(string $app_username) Return VldTunjangan objects filtered by the app_username column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseVldTunjanganQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseVldTunjanganQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\VldTunjangan', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new VldTunjanganQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   VldTunjanganQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return VldTunjanganQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof VldTunjanganQuery) {
            return $criteria;
        }
        $query = new VldTunjanganQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$tunjangan_id, $logid]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   VldTunjangan|VldTunjangan[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = VldTunjanganPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(VldTunjanganPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 VldTunjangan A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [tunjangan_id], [logid], [idtype], [status_validasi], [status_verifikasi], [field_error], [error_message], [keterangan], [create_date], [last_update], [expired_date], [last_sync], [app_username] FROM [vld_tunjangan] WHERE [tunjangan_id] = :p0 AND [logid] = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new VldTunjangan();
            $obj->hydrate($row);
            VldTunjanganPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return VldTunjangan|VldTunjangan[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|VldTunjangan[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(VldTunjanganPeer::TUNJANGAN_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(VldTunjanganPeer::LOGID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(VldTunjanganPeer::TUNJANGAN_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(VldTunjanganPeer::LOGID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the tunjangan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTunjanganId('fooValue');   // WHERE tunjangan_id = 'fooValue'
     * $query->filterByTunjanganId('%fooValue%'); // WHERE tunjangan_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tunjanganId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByTunjanganId($tunjanganId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tunjanganId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tunjanganId)) {
                $tunjanganId = str_replace('*', '%', $tunjanganId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::TUNJANGAN_ID, $tunjanganId, $comparison);
    }

    /**
     * Filter the query on the logid column
     *
     * Example usage:
     * <code>
     * $query->filterByLogid('fooValue');   // WHERE logid = 'fooValue'
     * $query->filterByLogid('%fooValue%'); // WHERE logid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $logid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByLogid($logid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($logid)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $logid)) {
                $logid = str_replace('*', '%', $logid);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::LOGID, $logid, $comparison);
    }

    /**
     * Filter the query on the idtype column
     *
     * Example usage:
     * <code>
     * $query->filterByIdtype(1234); // WHERE idtype = 1234
     * $query->filterByIdtype(array(12, 34)); // WHERE idtype IN (12, 34)
     * $query->filterByIdtype(array('min' => 12)); // WHERE idtype >= 12
     * $query->filterByIdtype(array('max' => 12)); // WHERE idtype <= 12
     * </code>
     *
     * @see       filterByErrortypeRelatedByIdtype()
     *
     * @see       filterByErrortypeRelatedByIdtype()
     *
     * @param     mixed $idtype The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByIdtype($idtype = null, $comparison = null)
    {
        if (is_array($idtype)) {
            $useMinMax = false;
            if (isset($idtype['min'])) {
                $this->addUsingAlias(VldTunjanganPeer::IDTYPE, $idtype['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idtype['max'])) {
                $this->addUsingAlias(VldTunjanganPeer::IDTYPE, $idtype['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::IDTYPE, $idtype, $comparison);
    }

    /**
     * Filter the query on the status_validasi column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusValidasi(1234); // WHERE status_validasi = 1234
     * $query->filterByStatusValidasi(array(12, 34)); // WHERE status_validasi IN (12, 34)
     * $query->filterByStatusValidasi(array('min' => 12)); // WHERE status_validasi >= 12
     * $query->filterByStatusValidasi(array('max' => 12)); // WHERE status_validasi <= 12
     * </code>
     *
     * @param     mixed $statusValidasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByStatusValidasi($statusValidasi = null, $comparison = null)
    {
        if (is_array($statusValidasi)) {
            $useMinMax = false;
            if (isset($statusValidasi['min'])) {
                $this->addUsingAlias(VldTunjanganPeer::STATUS_VALIDASI, $statusValidasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusValidasi['max'])) {
                $this->addUsingAlias(VldTunjanganPeer::STATUS_VALIDASI, $statusValidasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::STATUS_VALIDASI, $statusValidasi, $comparison);
    }

    /**
     * Filter the query on the status_verifikasi column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusVerifikasi(1234); // WHERE status_verifikasi = 1234
     * $query->filterByStatusVerifikasi(array(12, 34)); // WHERE status_verifikasi IN (12, 34)
     * $query->filterByStatusVerifikasi(array('min' => 12)); // WHERE status_verifikasi >= 12
     * $query->filterByStatusVerifikasi(array('max' => 12)); // WHERE status_verifikasi <= 12
     * </code>
     *
     * @param     mixed $statusVerifikasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByStatusVerifikasi($statusVerifikasi = null, $comparison = null)
    {
        if (is_array($statusVerifikasi)) {
            $useMinMax = false;
            if (isset($statusVerifikasi['min'])) {
                $this->addUsingAlias(VldTunjanganPeer::STATUS_VERIFIKASI, $statusVerifikasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusVerifikasi['max'])) {
                $this->addUsingAlias(VldTunjanganPeer::STATUS_VERIFIKASI, $statusVerifikasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::STATUS_VERIFIKASI, $statusVerifikasi, $comparison);
    }

    /**
     * Filter the query on the field_error column
     *
     * Example usage:
     * <code>
     * $query->filterByFieldError('fooValue');   // WHERE field_error = 'fooValue'
     * $query->filterByFieldError('%fooValue%'); // WHERE field_error LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fieldError The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByFieldError($fieldError = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fieldError)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fieldError)) {
                $fieldError = str_replace('*', '%', $fieldError);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::FIELD_ERROR, $fieldError, $comparison);
    }

    /**
     * Filter the query on the error_message column
     *
     * Example usage:
     * <code>
     * $query->filterByErrorMessage('fooValue');   // WHERE error_message = 'fooValue'
     * $query->filterByErrorMessage('%fooValue%'); // WHERE error_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $errorMessage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByErrorMessage($errorMessage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($errorMessage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $errorMessage)) {
                $errorMessage = str_replace('*', '%', $errorMessage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::ERROR_MESSAGE, $errorMessage, $comparison);
    }

    /**
     * Filter the query on the keterangan column
     *
     * Example usage:
     * <code>
     * $query->filterByKeterangan('fooValue');   // WHERE keterangan = 'fooValue'
     * $query->filterByKeterangan('%fooValue%'); // WHERE keterangan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keterangan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByKeterangan($keterangan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keterangan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $keterangan)) {
                $keterangan = str_replace('*', '%', $keterangan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::KETERANGAN, $keterangan, $comparison);
    }

    /**
     * Filter the query on the create_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateDate('2011-03-14'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate('now'); // WHERE create_date = '2011-03-14'
     * $query->filterByCreateDate(array('max' => 'yesterday')); // WHERE create_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByCreateDate($createDate = null, $comparison = null)
    {
        if (is_array($createDate)) {
            $useMinMax = false;
            if (isset($createDate['min'])) {
                $this->addUsingAlias(VldTunjanganPeer::CREATE_DATE, $createDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createDate['max'])) {
                $this->addUsingAlias(VldTunjanganPeer::CREATE_DATE, $createDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::CREATE_DATE, $createDate, $comparison);
    }

    /**
     * Filter the query on the last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(VldTunjanganPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(VldTunjanganPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the expired_date column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredDate('2011-03-14'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate('now'); // WHERE expired_date = '2011-03-14'
     * $query->filterByExpiredDate(array('max' => 'yesterday')); // WHERE expired_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByExpiredDate($expiredDate = null, $comparison = null)
    {
        if (is_array($expiredDate)) {
            $useMinMax = false;
            if (isset($expiredDate['min'])) {
                $this->addUsingAlias(VldTunjanganPeer::EXPIRED_DATE, $expiredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredDate['max'])) {
                $this->addUsingAlias(VldTunjanganPeer::EXPIRED_DATE, $expiredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::EXPIRED_DATE, $expiredDate, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(VldTunjanganPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(VldTunjanganPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the app_username column
     *
     * Example usage:
     * <code>
     * $query->filterByAppUsername('fooValue');   // WHERE app_username = 'fooValue'
     * $query->filterByAppUsername('%fooValue%'); // WHERE app_username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appUsername The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function filterByAppUsername($appUsername = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appUsername)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $appUsername)) {
                $appUsername = str_replace('*', '%', $appUsername);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VldTunjanganPeer::APP_USERNAME, $appUsername, $comparison);
    }

    /**
     * Filter the query by a related Tunjangan object
     *
     * @param   Tunjangan|PropelObjectCollection $tunjangan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 VldTunjanganQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTunjanganRelatedByTunjanganId($tunjangan, $comparison = null)
    {
        if ($tunjangan instanceof Tunjangan) {
            return $this
                ->addUsingAlias(VldTunjanganPeer::TUNJANGAN_ID, $tunjangan->getTunjanganId(), $comparison);
        } elseif ($tunjangan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VldTunjanganPeer::TUNJANGAN_ID, $tunjangan->toKeyValue('PrimaryKey', 'TunjanganId'), $comparison);
        } else {
            throw new PropelException('filterByTunjanganRelatedByTunjanganId() only accepts arguments of type Tunjangan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TunjanganRelatedByTunjanganId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function joinTunjanganRelatedByTunjanganId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TunjanganRelatedByTunjanganId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TunjanganRelatedByTunjanganId');
        }

        return $this;
    }

    /**
     * Use the TunjanganRelatedByTunjanganId relation Tunjangan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TunjanganQuery A secondary query class using the current class as primary query
     */
    public function useTunjanganRelatedByTunjanganIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTunjanganRelatedByTunjanganId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TunjanganRelatedByTunjanganId', '\angulex\Model\TunjanganQuery');
    }

    /**
     * Filter the query by a related Tunjangan object
     *
     * @param   Tunjangan|PropelObjectCollection $tunjangan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 VldTunjanganQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTunjanganRelatedByTunjanganId($tunjangan, $comparison = null)
    {
        if ($tunjangan instanceof Tunjangan) {
            return $this
                ->addUsingAlias(VldTunjanganPeer::TUNJANGAN_ID, $tunjangan->getTunjanganId(), $comparison);
        } elseif ($tunjangan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VldTunjanganPeer::TUNJANGAN_ID, $tunjangan->toKeyValue('PrimaryKey', 'TunjanganId'), $comparison);
        } else {
            throw new PropelException('filterByTunjanganRelatedByTunjanganId() only accepts arguments of type Tunjangan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TunjanganRelatedByTunjanganId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function joinTunjanganRelatedByTunjanganId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TunjanganRelatedByTunjanganId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TunjanganRelatedByTunjanganId');
        }

        return $this;
    }

    /**
     * Use the TunjanganRelatedByTunjanganId relation Tunjangan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TunjanganQuery A secondary query class using the current class as primary query
     */
    public function useTunjanganRelatedByTunjanganIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTunjanganRelatedByTunjanganId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TunjanganRelatedByTunjanganId', '\angulex\Model\TunjanganQuery');
    }

    /**
     * Filter the query by a related Errortype object
     *
     * @param   Errortype|PropelObjectCollection $errortype The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 VldTunjanganQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByErrortypeRelatedByIdtype($errortype, $comparison = null)
    {
        if ($errortype instanceof Errortype) {
            return $this
                ->addUsingAlias(VldTunjanganPeer::IDTYPE, $errortype->getIdtype(), $comparison);
        } elseif ($errortype instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VldTunjanganPeer::IDTYPE, $errortype->toKeyValue('PrimaryKey', 'Idtype'), $comparison);
        } else {
            throw new PropelException('filterByErrortypeRelatedByIdtype() only accepts arguments of type Errortype or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ErrortypeRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function joinErrortypeRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ErrortypeRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ErrortypeRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the ErrortypeRelatedByIdtype relation Errortype object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\ErrortypeQuery A secondary query class using the current class as primary query
     */
    public function useErrortypeRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinErrortypeRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ErrortypeRelatedByIdtype', '\angulex\Model\ErrortypeQuery');
    }

    /**
     * Filter the query by a related Errortype object
     *
     * @param   Errortype|PropelObjectCollection $errortype The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 VldTunjanganQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByErrortypeRelatedByIdtype($errortype, $comparison = null)
    {
        if ($errortype instanceof Errortype) {
            return $this
                ->addUsingAlias(VldTunjanganPeer::IDTYPE, $errortype->getIdtype(), $comparison);
        } elseif ($errortype instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VldTunjanganPeer::IDTYPE, $errortype->toKeyValue('PrimaryKey', 'Idtype'), $comparison);
        } else {
            throw new PropelException('filterByErrortypeRelatedByIdtype() only accepts arguments of type Errortype or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ErrortypeRelatedByIdtype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function joinErrortypeRelatedByIdtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ErrortypeRelatedByIdtype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ErrortypeRelatedByIdtype');
        }

        return $this;
    }

    /**
     * Use the ErrortypeRelatedByIdtype relation Errortype object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\ErrortypeQuery A secondary query class using the current class as primary query
     */
    public function useErrortypeRelatedByIdtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinErrortypeRelatedByIdtype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ErrortypeRelatedByIdtype', '\angulex\Model\ErrortypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   VldTunjangan $vldTunjangan Object to remove from the list of results
     *
     * @return VldTunjanganQuery The current query, for fluid interface
     */
    public function prune($vldTunjangan = null)
    {
        if ($vldTunjangan) {
            $this->addCond('pruneCond0', $this->getAliasedColName(VldTunjanganPeer::TUNJANGAN_ID), $vldTunjangan->getTunjanganId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(VldTunjanganPeer::LOGID), $vldTunjangan->getLogid(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
