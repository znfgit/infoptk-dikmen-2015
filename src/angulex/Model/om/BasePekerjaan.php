<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\Pekerjaan;
use angulex\Model\PekerjaanPeer;
use angulex\Model\PekerjaanQuery;
use angulex\Model\PesertaDidik;
use angulex\Model\PesertaDidikQuery;
use angulex\Model\Ptk;
use angulex\Model\PtkQuery;

/**
 * Base class that represents a row from the 'ref.pekerjaan' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BasePekerjaan extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\PekerjaanPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PekerjaanPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the pekerjaan_id field.
     * @var        int
     */
    protected $pekerjaan_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the create_date field.
     * @var        string
     */
    protected $create_date;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the expired_date field.
     * @var        string
     */
    protected $expired_date;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * @var        PropelObjectCollection|PesertaDidik[] Collection to store aggregation of PesertaDidik objects.
     */
    protected $collPesertaDidiksRelatedByPekerjaanIdAyah;
    protected $collPesertaDidiksRelatedByPekerjaanIdAyahPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidik[] Collection to store aggregation of PesertaDidik objects.
     */
    protected $collPesertaDidiksRelatedByPekerjaanIdIbu;
    protected $collPesertaDidiksRelatedByPekerjaanIdIbuPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidik[] Collection to store aggregation of PesertaDidik objects.
     */
    protected $collPesertaDidiksRelatedByPekerjaanIdWali;
    protected $collPesertaDidiksRelatedByPekerjaanIdWaliPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidik[] Collection to store aggregation of PesertaDidik objects.
     */
    protected $collPesertaDidiksRelatedByPekerjaanIdAyah;
    protected $collPesertaDidiksRelatedByPekerjaanIdAyahPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidik[] Collection to store aggregation of PesertaDidik objects.
     */
    protected $collPesertaDidiksRelatedByPekerjaanIdIbu;
    protected $collPesertaDidiksRelatedByPekerjaanIdIbuPartial;

    /**
     * @var        PropelObjectCollection|PesertaDidik[] Collection to store aggregation of PesertaDidik objects.
     */
    protected $collPesertaDidiksRelatedByPekerjaanIdWali;
    protected $collPesertaDidiksRelatedByPekerjaanIdWaliPartial;

    /**
     * @var        PropelObjectCollection|Ptk[] Collection to store aggregation of Ptk objects.
     */
    protected $collPtksRelatedByPekerjaanSuamiIstri;
    protected $collPtksRelatedByPekerjaanSuamiIstriPartial;

    /**
     * @var        PropelObjectCollection|Ptk[] Collection to store aggregation of Ptk objects.
     */
    protected $collPtksRelatedByPekerjaanSuamiIstri;
    protected $collPtksRelatedByPekerjaanSuamiIstriPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion = null;

    /**
     * Get the [pekerjaan_id] column value.
     * 
     * @return int
     */
    public function getPekerjaanId()
    {
        return $this->pekerjaan_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [optionally formatted] temporal [create_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreateDate($format = 'Y-m-d H:i:s')
    {
        if ($this->create_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->create_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->create_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [expired_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->expired_date === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->expired_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->expired_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [pekerjaan_id] column.
     * 
     * @param int $v new value
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPekerjaanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pekerjaan_id !== $v) {
            $this->pekerjaan_id = $v;
            $this->modifiedColumns[] = PekerjaanPeer::PEKERJAAN_ID;
        }


        return $this;
    } // setPekerjaanId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PekerjaanPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Sets the value of [create_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setCreateDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->create_date !== null || $dt !== null) {
            $currentDateAsString = ($this->create_date !== null && $tmpDt = new DateTime($this->create_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->create_date = $newDateAsString;
                $this->modifiedColumns[] = PekerjaanPeer::CREATE_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreateDate()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PekerjaanPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Sets the value of [expired_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setExpiredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_date !== null || $dt !== null) {
            $currentDateAsString = ($this->expired_date !== null && $tmpDt = new DateTime($this->expired_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->expired_date = $newDateAsString;
                $this->modifiedColumns[] = PekerjaanPeer::EXPIRED_DATE;
            }
        } // if either are not null


        return $this;
    } // setExpiredDate()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PekerjaanPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->pekerjaan_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nama = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->create_date = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->last_update = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->expired_date = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->last_sync = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 6; // 6 = PekerjaanPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Pekerjaan object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PekerjaanPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PekerjaanPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collPesertaDidiksRelatedByPekerjaanIdAyah = null;

            $this->collPesertaDidiksRelatedByPekerjaanIdIbu = null;

            $this->collPesertaDidiksRelatedByPekerjaanIdWali = null;

            $this->collPesertaDidiksRelatedByPekerjaanIdAyah = null;

            $this->collPesertaDidiksRelatedByPekerjaanIdIbu = null;

            $this->collPesertaDidiksRelatedByPekerjaanIdWali = null;

            $this->collPtksRelatedByPekerjaanSuamiIstri = null;

            $this->collPtksRelatedByPekerjaanSuamiIstri = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PekerjaanPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PekerjaanQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PekerjaanPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PekerjaanPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion !== null) {
                if (!$this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion->isEmpty()) {
                    foreach ($this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion as $pesertaDidikRelatedByPekerjaanIdAyah) {
                        // need to save related object because we set the relation to null
                        $pesertaDidikRelatedByPekerjaanIdAyah->save($con);
                    }
                    $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah !== null) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdAyah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion !== null) {
                if (!$this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion->isEmpty()) {
                    foreach ($this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion as $pesertaDidikRelatedByPekerjaanIdIbu) {
                        // need to save related object because we set the relation to null
                        $pesertaDidikRelatedByPekerjaanIdIbu->save($con);
                    }
                    $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu !== null) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdIbu as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion !== null) {
                if (!$this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion->isEmpty()) {
                    foreach ($this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion as $pesertaDidikRelatedByPekerjaanIdWali) {
                        // need to save related object because we set the relation to null
                        $pesertaDidikRelatedByPekerjaanIdWali->save($con);
                    }
                    $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidiksRelatedByPekerjaanIdWali !== null) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdWali as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion !== null) {
                if (!$this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion->isEmpty()) {
                    foreach ($this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion as $pesertaDidikRelatedByPekerjaanIdAyah) {
                        // need to save related object because we set the relation to null
                        $pesertaDidikRelatedByPekerjaanIdAyah->save($con);
                    }
                    $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah !== null) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdAyah as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion !== null) {
                if (!$this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion->isEmpty()) {
                    foreach ($this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion as $pesertaDidikRelatedByPekerjaanIdIbu) {
                        // need to save related object because we set the relation to null
                        $pesertaDidikRelatedByPekerjaanIdIbu->save($con);
                    }
                    $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu !== null) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdIbu as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion !== null) {
                if (!$this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion->isEmpty()) {
                    foreach ($this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion as $pesertaDidikRelatedByPekerjaanIdWali) {
                        // need to save related object because we set the relation to null
                        $pesertaDidikRelatedByPekerjaanIdWali->save($con);
                    }
                    $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion = null;
                }
            }

            if ($this->collPesertaDidiksRelatedByPekerjaanIdWali !== null) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdWali as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion !== null) {
                if (!$this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion->isEmpty()) {
                    PtkQuery::create()
                        ->filterByPrimaryKeys($this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion = null;
                }
            }

            if ($this->collPtksRelatedByPekerjaanSuamiIstri !== null) {
                foreach ($this->collPtksRelatedByPekerjaanSuamiIstri as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion !== null) {
                if (!$this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion->isEmpty()) {
                    PtkQuery::create()
                        ->filterByPrimaryKeys($this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion = null;
                }
            }

            if ($this->collPtksRelatedByPekerjaanSuamiIstri !== null) {
                foreach ($this->collPtksRelatedByPekerjaanSuamiIstri as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = PekerjaanPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah !== null) {
                    foreach ($this->collPesertaDidiksRelatedByPekerjaanIdAyah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu !== null) {
                    foreach ($this->collPesertaDidiksRelatedByPekerjaanIdIbu as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidiksRelatedByPekerjaanIdWali !== null) {
                    foreach ($this->collPesertaDidiksRelatedByPekerjaanIdWali as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah !== null) {
                    foreach ($this->collPesertaDidiksRelatedByPekerjaanIdAyah as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu !== null) {
                    foreach ($this->collPesertaDidiksRelatedByPekerjaanIdIbu as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPesertaDidiksRelatedByPekerjaanIdWali !== null) {
                    foreach ($this->collPesertaDidiksRelatedByPekerjaanIdWali as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtksRelatedByPekerjaanSuamiIstri !== null) {
                    foreach ($this->collPtksRelatedByPekerjaanSuamiIstri as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPtksRelatedByPekerjaanSuamiIstri !== null) {
                    foreach ($this->collPtksRelatedByPekerjaanSuamiIstri as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PekerjaanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPekerjaanId();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getCreateDate();
                break;
            case 3:
                return $this->getLastUpdate();
                break;
            case 4:
                return $this->getExpiredDate();
                break;
            case 5:
                return $this->getLastSync();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Pekerjaan'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Pekerjaan'][$this->getPrimaryKey()] = true;
        $keys = PekerjaanPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPekerjaanId(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getCreateDate(),
            $keys[3] => $this->getLastUpdate(),
            $keys[4] => $this->getExpiredDate(),
            $keys[5] => $this->getLastSync(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                $result['PesertaDidiksRelatedByPekerjaanIdAyah'] = $this->collPesertaDidiksRelatedByPekerjaanIdAyah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                $result['PesertaDidiksRelatedByPekerjaanIdIbu'] = $this->collPesertaDidiksRelatedByPekerjaanIdIbu->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                $result['PesertaDidiksRelatedByPekerjaanIdWali'] = $this->collPesertaDidiksRelatedByPekerjaanIdWali->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                $result['PesertaDidiksRelatedByPekerjaanIdAyah'] = $this->collPesertaDidiksRelatedByPekerjaanIdAyah->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                $result['PesertaDidiksRelatedByPekerjaanIdIbu'] = $this->collPesertaDidiksRelatedByPekerjaanIdIbu->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                $result['PesertaDidiksRelatedByPekerjaanIdWali'] = $this->collPesertaDidiksRelatedByPekerjaanIdWali->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtksRelatedByPekerjaanSuamiIstri) {
                $result['PtksRelatedByPekerjaanSuamiIstri'] = $this->collPtksRelatedByPekerjaanSuamiIstri->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPtksRelatedByPekerjaanSuamiIstri) {
                $result['PtksRelatedByPekerjaanSuamiIstri'] = $this->collPtksRelatedByPekerjaanSuamiIstri->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PekerjaanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPekerjaanId($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setCreateDate($value);
                break;
            case 3:
                $this->setLastUpdate($value);
                break;
            case 4:
                $this->setExpiredDate($value);
                break;
            case 5:
                $this->setLastSync($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PekerjaanPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPekerjaanId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCreateDate($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setLastUpdate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setExpiredDate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setLastSync($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PekerjaanPeer::DATABASE_NAME);

        if ($this->isColumnModified(PekerjaanPeer::PEKERJAAN_ID)) $criteria->add(PekerjaanPeer::PEKERJAAN_ID, $this->pekerjaan_id);
        if ($this->isColumnModified(PekerjaanPeer::NAMA)) $criteria->add(PekerjaanPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PekerjaanPeer::CREATE_DATE)) $criteria->add(PekerjaanPeer::CREATE_DATE, $this->create_date);
        if ($this->isColumnModified(PekerjaanPeer::LAST_UPDATE)) $criteria->add(PekerjaanPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PekerjaanPeer::EXPIRED_DATE)) $criteria->add(PekerjaanPeer::EXPIRED_DATE, $this->expired_date);
        if ($this->isColumnModified(PekerjaanPeer::LAST_SYNC)) $criteria->add(PekerjaanPeer::LAST_SYNC, $this->last_sync);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PekerjaanPeer::DATABASE_NAME);
        $criteria->add(PekerjaanPeer::PEKERJAAN_ID, $this->pekerjaan_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getPekerjaanId();
    }

    /**
     * Generic method to set the primary key (pekerjaan_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPekerjaanId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPekerjaanId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Pekerjaan (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNama($this->getNama());
        $copyObj->setCreateDate($this->getCreateDate());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setExpiredDate($this->getExpiredDate());
        $copyObj->setLastSync($this->getLastSync());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getPesertaDidiksRelatedByPekerjaanIdAyah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikRelatedByPekerjaanIdAyah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidiksRelatedByPekerjaanIdIbu() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikRelatedByPekerjaanIdIbu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidiksRelatedByPekerjaanIdWali() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikRelatedByPekerjaanIdWali($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidiksRelatedByPekerjaanIdAyah() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikRelatedByPekerjaanIdAyah($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidiksRelatedByPekerjaanIdIbu() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikRelatedByPekerjaanIdIbu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPesertaDidiksRelatedByPekerjaanIdWali() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPesertaDidikRelatedByPekerjaanIdWali($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtksRelatedByPekerjaanSuamiIstri() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkRelatedByPekerjaanSuamiIstri($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPtksRelatedByPekerjaanSuamiIstri() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPtkRelatedByPekerjaanSuamiIstri($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPekerjaanId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Pekerjaan Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PekerjaanPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PekerjaanPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('PesertaDidikRelatedByPekerjaanIdAyah' == $relationName) {
            $this->initPesertaDidiksRelatedByPekerjaanIdAyah();
        }
        if ('PesertaDidikRelatedByPekerjaanIdIbu' == $relationName) {
            $this->initPesertaDidiksRelatedByPekerjaanIdIbu();
        }
        if ('PesertaDidikRelatedByPekerjaanIdWali' == $relationName) {
            $this->initPesertaDidiksRelatedByPekerjaanIdWali();
        }
        if ('PesertaDidikRelatedByPekerjaanIdAyah' == $relationName) {
            $this->initPesertaDidiksRelatedByPekerjaanIdAyah();
        }
        if ('PesertaDidikRelatedByPekerjaanIdIbu' == $relationName) {
            $this->initPesertaDidiksRelatedByPekerjaanIdIbu();
        }
        if ('PesertaDidikRelatedByPekerjaanIdWali' == $relationName) {
            $this->initPesertaDidiksRelatedByPekerjaanIdWali();
        }
        if ('PtkRelatedByPekerjaanSuamiIstri' == $relationName) {
            $this->initPtksRelatedByPekerjaanSuamiIstri();
        }
        if ('PtkRelatedByPekerjaanSuamiIstri' == $relationName) {
            $this->initPtksRelatedByPekerjaanSuamiIstri();
        }
    }

    /**
     * Clears out the collPesertaDidiksRelatedByPekerjaanIdAyah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pekerjaan The current object (for fluent API support)
     * @see        addPesertaDidiksRelatedByPekerjaanIdAyah()
     */
    public function clearPesertaDidiksRelatedByPekerjaanIdAyah()
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidiksRelatedByPekerjaanIdAyah collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidiksRelatedByPekerjaanIdAyah($v = true)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = $v;
    }

    /**
     * Initializes the collPesertaDidiksRelatedByPekerjaanIdAyah collection.
     *
     * By default this just sets the collPesertaDidiksRelatedByPekerjaanIdAyah collection to an empty array (like clearcollPesertaDidiksRelatedByPekerjaanIdAyah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidiksRelatedByPekerjaanIdAyah($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdAyah && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = new PropelObjectCollection();
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah->setModel('PesertaDidik');
    }

    /**
     * Gets an array of PesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pekerjaan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     * @throws PropelException
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdAyah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                // return empty collection
                $this->initPesertaDidiksRelatedByPekerjaanIdAyah();
            } else {
                $collPesertaDidiksRelatedByPekerjaanIdAyah = PesertaDidikQuery::create(null, $criteria)
                    ->filterByPekerjaanRelatedByPekerjaanIdAyah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial && count($collPesertaDidiksRelatedByPekerjaanIdAyah)) {
                      $this->initPesertaDidiksRelatedByPekerjaanIdAyah(false);

                      foreach($collPesertaDidiksRelatedByPekerjaanIdAyah as $obj) {
                        if (false == $this->collPesertaDidiksRelatedByPekerjaanIdAyah->contains($obj)) {
                          $this->collPesertaDidiksRelatedByPekerjaanIdAyah->append($obj);
                        }
                      }

                      $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = true;
                    }

                    $collPesertaDidiksRelatedByPekerjaanIdAyah->getInternalIterator()->rewind();
                    return $collPesertaDidiksRelatedByPekerjaanIdAyah;
                }

                if($partial && $this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                    foreach($this->collPesertaDidiksRelatedByPekerjaanIdAyah as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidiksRelatedByPekerjaanIdAyah[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidiksRelatedByPekerjaanIdAyah = $collPesertaDidiksRelatedByPekerjaanIdAyah;
                $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = false;
            }
        }

        return $this->collPesertaDidiksRelatedByPekerjaanIdAyah;
    }

    /**
     * Sets a collection of PesertaDidikRelatedByPekerjaanIdAyah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidiksRelatedByPekerjaanIdAyah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPesertaDidiksRelatedByPekerjaanIdAyah(PropelCollection $pesertaDidiksRelatedByPekerjaanIdAyah, PropelPDO $con = null)
    {
        $pesertaDidiksRelatedByPekerjaanIdAyahToDelete = $this->getPesertaDidiksRelatedByPekerjaanIdAyah(new Criteria(), $con)->diff($pesertaDidiksRelatedByPekerjaanIdAyah);

        $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion = unserialize(serialize($pesertaDidiksRelatedByPekerjaanIdAyahToDelete));

        foreach ($pesertaDidiksRelatedByPekerjaanIdAyahToDelete as $pesertaDidikRelatedByPekerjaanIdAyahRemoved) {
            $pesertaDidikRelatedByPekerjaanIdAyahRemoved->setPekerjaanRelatedByPekerjaanIdAyah(null);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = null;
        foreach ($pesertaDidiksRelatedByPekerjaanIdAyah as $pesertaDidikRelatedByPekerjaanIdAyah) {
            $this->addPesertaDidikRelatedByPekerjaanIdAyah($pesertaDidikRelatedByPekerjaanIdAyah);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = $pesertaDidiksRelatedByPekerjaanIdAyah;
        $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidik objects.
     * @throws PropelException
     */
    public function countPesertaDidiksRelatedByPekerjaanIdAyah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdAyah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidiksRelatedByPekerjaanIdAyah());
            }
            $query = PesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPekerjaanRelatedByPekerjaanIdAyah($this)
                ->count($con);
        }

        return count($this->collPesertaDidiksRelatedByPekerjaanIdAyah);
    }

    /**
     * Method called to associate a PesertaDidik object to this object
     * through the PesertaDidik foreign key attribute.
     *
     * @param    PesertaDidik $l PesertaDidik
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function addPesertaDidikRelatedByPekerjaanIdAyah(PesertaDidik $l)
    {
        if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah === null) {
            $this->initPesertaDidiksRelatedByPekerjaanIdAyah();
            $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidiksRelatedByPekerjaanIdAyah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikRelatedByPekerjaanIdAyah($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdAyah $pesertaDidikRelatedByPekerjaanIdAyah The pesertaDidikRelatedByPekerjaanIdAyah object to add.
     */
    protected function doAddPesertaDidikRelatedByPekerjaanIdAyah($pesertaDidikRelatedByPekerjaanIdAyah)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah[]= $pesertaDidikRelatedByPekerjaanIdAyah;
        $pesertaDidikRelatedByPekerjaanIdAyah->setPekerjaanRelatedByPekerjaanIdAyah($this);
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdAyah $pesertaDidikRelatedByPekerjaanIdAyah The pesertaDidikRelatedByPekerjaanIdAyah object to remove.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function removePesertaDidikRelatedByPekerjaanIdAyah($pesertaDidikRelatedByPekerjaanIdAyah)
    {
        if ($this->getPesertaDidiksRelatedByPekerjaanIdAyah()->contains($pesertaDidikRelatedByPekerjaanIdAyah)) {
            $this->collPesertaDidiksRelatedByPekerjaanIdAyah->remove($this->collPesertaDidiksRelatedByPekerjaanIdAyah->search($pesertaDidikRelatedByPekerjaanIdAyah));
            if (null === $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion) {
                $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion = clone $this->collPesertaDidiksRelatedByPekerjaanIdAyah;
                $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion->clear();
            }
            $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion[]= $pesertaDidikRelatedByPekerjaanIdAyah;
            $pesertaDidikRelatedByPekerjaanIdAyah->setPekerjaanRelatedByPekerjaanIdAyah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }

    /**
     * Clears out the collPesertaDidiksRelatedByPekerjaanIdIbu collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pekerjaan The current object (for fluent API support)
     * @see        addPesertaDidiksRelatedByPekerjaanIdIbu()
     */
    public function clearPesertaDidiksRelatedByPekerjaanIdIbu()
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidiksRelatedByPekerjaanIdIbu collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidiksRelatedByPekerjaanIdIbu($v = true)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = $v;
    }

    /**
     * Initializes the collPesertaDidiksRelatedByPekerjaanIdIbu collection.
     *
     * By default this just sets the collPesertaDidiksRelatedByPekerjaanIdIbu collection to an empty array (like clearcollPesertaDidiksRelatedByPekerjaanIdIbu());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidiksRelatedByPekerjaanIdIbu($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdIbu && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = new PropelObjectCollection();
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu->setModel('PesertaDidik');
    }

    /**
     * Gets an array of PesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pekerjaan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     * @throws PropelException
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbu($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdIbu || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                // return empty collection
                $this->initPesertaDidiksRelatedByPekerjaanIdIbu();
            } else {
                $collPesertaDidiksRelatedByPekerjaanIdIbu = PesertaDidikQuery::create(null, $criteria)
                    ->filterByPekerjaanRelatedByPekerjaanIdIbu($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial && count($collPesertaDidiksRelatedByPekerjaanIdIbu)) {
                      $this->initPesertaDidiksRelatedByPekerjaanIdIbu(false);

                      foreach($collPesertaDidiksRelatedByPekerjaanIdIbu as $obj) {
                        if (false == $this->collPesertaDidiksRelatedByPekerjaanIdIbu->contains($obj)) {
                          $this->collPesertaDidiksRelatedByPekerjaanIdIbu->append($obj);
                        }
                      }

                      $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = true;
                    }

                    $collPesertaDidiksRelatedByPekerjaanIdIbu->getInternalIterator()->rewind();
                    return $collPesertaDidiksRelatedByPekerjaanIdIbu;
                }

                if($partial && $this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                    foreach($this->collPesertaDidiksRelatedByPekerjaanIdIbu as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidiksRelatedByPekerjaanIdIbu[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidiksRelatedByPekerjaanIdIbu = $collPesertaDidiksRelatedByPekerjaanIdIbu;
                $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = false;
            }
        }

        return $this->collPesertaDidiksRelatedByPekerjaanIdIbu;
    }

    /**
     * Sets a collection of PesertaDidikRelatedByPekerjaanIdIbu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidiksRelatedByPekerjaanIdIbu A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPesertaDidiksRelatedByPekerjaanIdIbu(PropelCollection $pesertaDidiksRelatedByPekerjaanIdIbu, PropelPDO $con = null)
    {
        $pesertaDidiksRelatedByPekerjaanIdIbuToDelete = $this->getPesertaDidiksRelatedByPekerjaanIdIbu(new Criteria(), $con)->diff($pesertaDidiksRelatedByPekerjaanIdIbu);

        $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion = unserialize(serialize($pesertaDidiksRelatedByPekerjaanIdIbuToDelete));

        foreach ($pesertaDidiksRelatedByPekerjaanIdIbuToDelete as $pesertaDidikRelatedByPekerjaanIdIbuRemoved) {
            $pesertaDidikRelatedByPekerjaanIdIbuRemoved->setPekerjaanRelatedByPekerjaanIdIbu(null);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = null;
        foreach ($pesertaDidiksRelatedByPekerjaanIdIbu as $pesertaDidikRelatedByPekerjaanIdIbu) {
            $this->addPesertaDidikRelatedByPekerjaanIdIbu($pesertaDidikRelatedByPekerjaanIdIbu);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = $pesertaDidiksRelatedByPekerjaanIdIbu;
        $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidik objects.
     * @throws PropelException
     */
    public function countPesertaDidiksRelatedByPekerjaanIdIbu(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdIbu || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidiksRelatedByPekerjaanIdIbu());
            }
            $query = PesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPekerjaanRelatedByPekerjaanIdIbu($this)
                ->count($con);
        }

        return count($this->collPesertaDidiksRelatedByPekerjaanIdIbu);
    }

    /**
     * Method called to associate a PesertaDidik object to this object
     * through the PesertaDidik foreign key attribute.
     *
     * @param    PesertaDidik $l PesertaDidik
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function addPesertaDidikRelatedByPekerjaanIdIbu(PesertaDidik $l)
    {
        if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu === null) {
            $this->initPesertaDidiksRelatedByPekerjaanIdIbu();
            $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidiksRelatedByPekerjaanIdIbu->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikRelatedByPekerjaanIdIbu($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdIbu $pesertaDidikRelatedByPekerjaanIdIbu The pesertaDidikRelatedByPekerjaanIdIbu object to add.
     */
    protected function doAddPesertaDidikRelatedByPekerjaanIdIbu($pesertaDidikRelatedByPekerjaanIdIbu)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu[]= $pesertaDidikRelatedByPekerjaanIdIbu;
        $pesertaDidikRelatedByPekerjaanIdIbu->setPekerjaanRelatedByPekerjaanIdIbu($this);
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdIbu $pesertaDidikRelatedByPekerjaanIdIbu The pesertaDidikRelatedByPekerjaanIdIbu object to remove.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function removePesertaDidikRelatedByPekerjaanIdIbu($pesertaDidikRelatedByPekerjaanIdIbu)
    {
        if ($this->getPesertaDidiksRelatedByPekerjaanIdIbu()->contains($pesertaDidikRelatedByPekerjaanIdIbu)) {
            $this->collPesertaDidiksRelatedByPekerjaanIdIbu->remove($this->collPesertaDidiksRelatedByPekerjaanIdIbu->search($pesertaDidikRelatedByPekerjaanIdIbu));
            if (null === $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion) {
                $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion = clone $this->collPesertaDidiksRelatedByPekerjaanIdIbu;
                $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion->clear();
            }
            $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion[]= $pesertaDidikRelatedByPekerjaanIdIbu;
            $pesertaDidikRelatedByPekerjaanIdIbu->setPekerjaanRelatedByPekerjaanIdIbu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }

    /**
     * Clears out the collPesertaDidiksRelatedByPekerjaanIdWali collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pekerjaan The current object (for fluent API support)
     * @see        addPesertaDidiksRelatedByPekerjaanIdWali()
     */
    public function clearPesertaDidiksRelatedByPekerjaanIdWali()
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdWali = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidiksRelatedByPekerjaanIdWali collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidiksRelatedByPekerjaanIdWali($v = true)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = $v;
    }

    /**
     * Initializes the collPesertaDidiksRelatedByPekerjaanIdWali collection.
     *
     * By default this just sets the collPesertaDidiksRelatedByPekerjaanIdWali collection to an empty array (like clearcollPesertaDidiksRelatedByPekerjaanIdWali());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidiksRelatedByPekerjaanIdWali($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdWali && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdWali = new PropelObjectCollection();
        $this->collPesertaDidiksRelatedByPekerjaanIdWali->setModel('PesertaDidik');
    }

    /**
     * Gets an array of PesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pekerjaan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     * @throws PropelException
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWali($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdWali || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                // return empty collection
                $this->initPesertaDidiksRelatedByPekerjaanIdWali();
            } else {
                $collPesertaDidiksRelatedByPekerjaanIdWali = PesertaDidikQuery::create(null, $criteria)
                    ->filterByPekerjaanRelatedByPekerjaanIdWali($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial && count($collPesertaDidiksRelatedByPekerjaanIdWali)) {
                      $this->initPesertaDidiksRelatedByPekerjaanIdWali(false);

                      foreach($collPesertaDidiksRelatedByPekerjaanIdWali as $obj) {
                        if (false == $this->collPesertaDidiksRelatedByPekerjaanIdWali->contains($obj)) {
                          $this->collPesertaDidiksRelatedByPekerjaanIdWali->append($obj);
                        }
                      }

                      $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = true;
                    }

                    $collPesertaDidiksRelatedByPekerjaanIdWali->getInternalIterator()->rewind();
                    return $collPesertaDidiksRelatedByPekerjaanIdWali;
                }

                if($partial && $this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                    foreach($this->collPesertaDidiksRelatedByPekerjaanIdWali as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidiksRelatedByPekerjaanIdWali[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidiksRelatedByPekerjaanIdWali = $collPesertaDidiksRelatedByPekerjaanIdWali;
                $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = false;
            }
        }

        return $this->collPesertaDidiksRelatedByPekerjaanIdWali;
    }

    /**
     * Sets a collection of PesertaDidikRelatedByPekerjaanIdWali objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidiksRelatedByPekerjaanIdWali A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPesertaDidiksRelatedByPekerjaanIdWali(PropelCollection $pesertaDidiksRelatedByPekerjaanIdWali, PropelPDO $con = null)
    {
        $pesertaDidiksRelatedByPekerjaanIdWaliToDelete = $this->getPesertaDidiksRelatedByPekerjaanIdWali(new Criteria(), $con)->diff($pesertaDidiksRelatedByPekerjaanIdWali);

        $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion = unserialize(serialize($pesertaDidiksRelatedByPekerjaanIdWaliToDelete));

        foreach ($pesertaDidiksRelatedByPekerjaanIdWaliToDelete as $pesertaDidikRelatedByPekerjaanIdWaliRemoved) {
            $pesertaDidikRelatedByPekerjaanIdWaliRemoved->setPekerjaanRelatedByPekerjaanIdWali(null);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdWali = null;
        foreach ($pesertaDidiksRelatedByPekerjaanIdWali as $pesertaDidikRelatedByPekerjaanIdWali) {
            $this->addPesertaDidikRelatedByPekerjaanIdWali($pesertaDidikRelatedByPekerjaanIdWali);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdWali = $pesertaDidiksRelatedByPekerjaanIdWali;
        $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidik objects.
     * @throws PropelException
     */
    public function countPesertaDidiksRelatedByPekerjaanIdWali(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdWali || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidiksRelatedByPekerjaanIdWali());
            }
            $query = PesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPekerjaanRelatedByPekerjaanIdWali($this)
                ->count($con);
        }

        return count($this->collPesertaDidiksRelatedByPekerjaanIdWali);
    }

    /**
     * Method called to associate a PesertaDidik object to this object
     * through the PesertaDidik foreign key attribute.
     *
     * @param    PesertaDidik $l PesertaDidik
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function addPesertaDidikRelatedByPekerjaanIdWali(PesertaDidik $l)
    {
        if ($this->collPesertaDidiksRelatedByPekerjaanIdWali === null) {
            $this->initPesertaDidiksRelatedByPekerjaanIdWali();
            $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidiksRelatedByPekerjaanIdWali->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikRelatedByPekerjaanIdWali($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdWali $pesertaDidikRelatedByPekerjaanIdWali The pesertaDidikRelatedByPekerjaanIdWali object to add.
     */
    protected function doAddPesertaDidikRelatedByPekerjaanIdWali($pesertaDidikRelatedByPekerjaanIdWali)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdWali[]= $pesertaDidikRelatedByPekerjaanIdWali;
        $pesertaDidikRelatedByPekerjaanIdWali->setPekerjaanRelatedByPekerjaanIdWali($this);
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdWali $pesertaDidikRelatedByPekerjaanIdWali The pesertaDidikRelatedByPekerjaanIdWali object to remove.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function removePesertaDidikRelatedByPekerjaanIdWali($pesertaDidikRelatedByPekerjaanIdWali)
    {
        if ($this->getPesertaDidiksRelatedByPekerjaanIdWali()->contains($pesertaDidikRelatedByPekerjaanIdWali)) {
            $this->collPesertaDidiksRelatedByPekerjaanIdWali->remove($this->collPesertaDidiksRelatedByPekerjaanIdWali->search($pesertaDidikRelatedByPekerjaanIdWali));
            if (null === $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion) {
                $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion = clone $this->collPesertaDidiksRelatedByPekerjaanIdWali;
                $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion->clear();
            }
            $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion[]= $pesertaDidikRelatedByPekerjaanIdWali;
            $pesertaDidikRelatedByPekerjaanIdWali->setPekerjaanRelatedByPekerjaanIdWali(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }

    /**
     * Clears out the collPesertaDidiksRelatedByPekerjaanIdAyah collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pekerjaan The current object (for fluent API support)
     * @see        addPesertaDidiksRelatedByPekerjaanIdAyah()
     */
    public function clearPesertaDidiksRelatedByPekerjaanIdAyah()
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidiksRelatedByPekerjaanIdAyah collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidiksRelatedByPekerjaanIdAyah($v = true)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = $v;
    }

    /**
     * Initializes the collPesertaDidiksRelatedByPekerjaanIdAyah collection.
     *
     * By default this just sets the collPesertaDidiksRelatedByPekerjaanIdAyah collection to an empty array (like clearcollPesertaDidiksRelatedByPekerjaanIdAyah());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidiksRelatedByPekerjaanIdAyah($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdAyah && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = new PropelObjectCollection();
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah->setModel('PesertaDidik');
    }

    /**
     * Gets an array of PesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pekerjaan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     * @throws PropelException
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyah($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdAyah || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                // return empty collection
                $this->initPesertaDidiksRelatedByPekerjaanIdAyah();
            } else {
                $collPesertaDidiksRelatedByPekerjaanIdAyah = PesertaDidikQuery::create(null, $criteria)
                    ->filterByPekerjaanRelatedByPekerjaanIdAyah($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial && count($collPesertaDidiksRelatedByPekerjaanIdAyah)) {
                      $this->initPesertaDidiksRelatedByPekerjaanIdAyah(false);

                      foreach($collPesertaDidiksRelatedByPekerjaanIdAyah as $obj) {
                        if (false == $this->collPesertaDidiksRelatedByPekerjaanIdAyah->contains($obj)) {
                          $this->collPesertaDidiksRelatedByPekerjaanIdAyah->append($obj);
                        }
                      }

                      $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = true;
                    }

                    $collPesertaDidiksRelatedByPekerjaanIdAyah->getInternalIterator()->rewind();
                    return $collPesertaDidiksRelatedByPekerjaanIdAyah;
                }

                if($partial && $this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                    foreach($this->collPesertaDidiksRelatedByPekerjaanIdAyah as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidiksRelatedByPekerjaanIdAyah[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidiksRelatedByPekerjaanIdAyah = $collPesertaDidiksRelatedByPekerjaanIdAyah;
                $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = false;
            }
        }

        return $this->collPesertaDidiksRelatedByPekerjaanIdAyah;
    }

    /**
     * Sets a collection of PesertaDidikRelatedByPekerjaanIdAyah objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidiksRelatedByPekerjaanIdAyah A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPesertaDidiksRelatedByPekerjaanIdAyah(PropelCollection $pesertaDidiksRelatedByPekerjaanIdAyah, PropelPDO $con = null)
    {
        $pesertaDidiksRelatedByPekerjaanIdAyahToDelete = $this->getPesertaDidiksRelatedByPekerjaanIdAyah(new Criteria(), $con)->diff($pesertaDidiksRelatedByPekerjaanIdAyah);

        $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion = unserialize(serialize($pesertaDidiksRelatedByPekerjaanIdAyahToDelete));

        foreach ($pesertaDidiksRelatedByPekerjaanIdAyahToDelete as $pesertaDidikRelatedByPekerjaanIdAyahRemoved) {
            $pesertaDidikRelatedByPekerjaanIdAyahRemoved->setPekerjaanRelatedByPekerjaanIdAyah(null);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = null;
        foreach ($pesertaDidiksRelatedByPekerjaanIdAyah as $pesertaDidikRelatedByPekerjaanIdAyah) {
            $this->addPesertaDidikRelatedByPekerjaanIdAyah($pesertaDidikRelatedByPekerjaanIdAyah);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = $pesertaDidiksRelatedByPekerjaanIdAyah;
        $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidik objects.
     * @throws PropelException
     */
    public function countPesertaDidiksRelatedByPekerjaanIdAyah(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdAyah || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidiksRelatedByPekerjaanIdAyah());
            }
            $query = PesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPekerjaanRelatedByPekerjaanIdAyah($this)
                ->count($con);
        }

        return count($this->collPesertaDidiksRelatedByPekerjaanIdAyah);
    }

    /**
     * Method called to associate a PesertaDidik object to this object
     * through the PesertaDidik foreign key attribute.
     *
     * @param    PesertaDidik $l PesertaDidik
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function addPesertaDidikRelatedByPekerjaanIdAyah(PesertaDidik $l)
    {
        if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah === null) {
            $this->initPesertaDidiksRelatedByPekerjaanIdAyah();
            $this->collPesertaDidiksRelatedByPekerjaanIdAyahPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidiksRelatedByPekerjaanIdAyah->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikRelatedByPekerjaanIdAyah($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdAyah $pesertaDidikRelatedByPekerjaanIdAyah The pesertaDidikRelatedByPekerjaanIdAyah object to add.
     */
    protected function doAddPesertaDidikRelatedByPekerjaanIdAyah($pesertaDidikRelatedByPekerjaanIdAyah)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah[]= $pesertaDidikRelatedByPekerjaanIdAyah;
        $pesertaDidikRelatedByPekerjaanIdAyah->setPekerjaanRelatedByPekerjaanIdAyah($this);
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdAyah $pesertaDidikRelatedByPekerjaanIdAyah The pesertaDidikRelatedByPekerjaanIdAyah object to remove.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function removePesertaDidikRelatedByPekerjaanIdAyah($pesertaDidikRelatedByPekerjaanIdAyah)
    {
        if ($this->getPesertaDidiksRelatedByPekerjaanIdAyah()->contains($pesertaDidikRelatedByPekerjaanIdAyah)) {
            $this->collPesertaDidiksRelatedByPekerjaanIdAyah->remove($this->collPesertaDidiksRelatedByPekerjaanIdAyah->search($pesertaDidikRelatedByPekerjaanIdAyah));
            if (null === $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion) {
                $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion = clone $this->collPesertaDidiksRelatedByPekerjaanIdAyah;
                $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion->clear();
            }
            $this->pesertaDidiksRelatedByPekerjaanIdAyahScheduledForDeletion[]= $pesertaDidikRelatedByPekerjaanIdAyah;
            $pesertaDidikRelatedByPekerjaanIdAyah->setPekerjaanRelatedByPekerjaanIdAyah(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdAyah from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdAyahJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdAyah($query, $con);
    }

    /**
     * Clears out the collPesertaDidiksRelatedByPekerjaanIdIbu collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pekerjaan The current object (for fluent API support)
     * @see        addPesertaDidiksRelatedByPekerjaanIdIbu()
     */
    public function clearPesertaDidiksRelatedByPekerjaanIdIbu()
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidiksRelatedByPekerjaanIdIbu collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidiksRelatedByPekerjaanIdIbu($v = true)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = $v;
    }

    /**
     * Initializes the collPesertaDidiksRelatedByPekerjaanIdIbu collection.
     *
     * By default this just sets the collPesertaDidiksRelatedByPekerjaanIdIbu collection to an empty array (like clearcollPesertaDidiksRelatedByPekerjaanIdIbu());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidiksRelatedByPekerjaanIdIbu($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdIbu && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = new PropelObjectCollection();
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu->setModel('PesertaDidik');
    }

    /**
     * Gets an array of PesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pekerjaan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     * @throws PropelException
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbu($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdIbu || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                // return empty collection
                $this->initPesertaDidiksRelatedByPekerjaanIdIbu();
            } else {
                $collPesertaDidiksRelatedByPekerjaanIdIbu = PesertaDidikQuery::create(null, $criteria)
                    ->filterByPekerjaanRelatedByPekerjaanIdIbu($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial && count($collPesertaDidiksRelatedByPekerjaanIdIbu)) {
                      $this->initPesertaDidiksRelatedByPekerjaanIdIbu(false);

                      foreach($collPesertaDidiksRelatedByPekerjaanIdIbu as $obj) {
                        if (false == $this->collPesertaDidiksRelatedByPekerjaanIdIbu->contains($obj)) {
                          $this->collPesertaDidiksRelatedByPekerjaanIdIbu->append($obj);
                        }
                      }

                      $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = true;
                    }

                    $collPesertaDidiksRelatedByPekerjaanIdIbu->getInternalIterator()->rewind();
                    return $collPesertaDidiksRelatedByPekerjaanIdIbu;
                }

                if($partial && $this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                    foreach($this->collPesertaDidiksRelatedByPekerjaanIdIbu as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidiksRelatedByPekerjaanIdIbu[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidiksRelatedByPekerjaanIdIbu = $collPesertaDidiksRelatedByPekerjaanIdIbu;
                $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = false;
            }
        }

        return $this->collPesertaDidiksRelatedByPekerjaanIdIbu;
    }

    /**
     * Sets a collection of PesertaDidikRelatedByPekerjaanIdIbu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidiksRelatedByPekerjaanIdIbu A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPesertaDidiksRelatedByPekerjaanIdIbu(PropelCollection $pesertaDidiksRelatedByPekerjaanIdIbu, PropelPDO $con = null)
    {
        $pesertaDidiksRelatedByPekerjaanIdIbuToDelete = $this->getPesertaDidiksRelatedByPekerjaanIdIbu(new Criteria(), $con)->diff($pesertaDidiksRelatedByPekerjaanIdIbu);

        $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion = unserialize(serialize($pesertaDidiksRelatedByPekerjaanIdIbuToDelete));

        foreach ($pesertaDidiksRelatedByPekerjaanIdIbuToDelete as $pesertaDidikRelatedByPekerjaanIdIbuRemoved) {
            $pesertaDidikRelatedByPekerjaanIdIbuRemoved->setPekerjaanRelatedByPekerjaanIdIbu(null);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = null;
        foreach ($pesertaDidiksRelatedByPekerjaanIdIbu as $pesertaDidikRelatedByPekerjaanIdIbu) {
            $this->addPesertaDidikRelatedByPekerjaanIdIbu($pesertaDidikRelatedByPekerjaanIdIbu);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = $pesertaDidiksRelatedByPekerjaanIdIbu;
        $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidik objects.
     * @throws PropelException
     */
    public function countPesertaDidiksRelatedByPekerjaanIdIbu(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdIbu || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidiksRelatedByPekerjaanIdIbu());
            }
            $query = PesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPekerjaanRelatedByPekerjaanIdIbu($this)
                ->count($con);
        }

        return count($this->collPesertaDidiksRelatedByPekerjaanIdIbu);
    }

    /**
     * Method called to associate a PesertaDidik object to this object
     * through the PesertaDidik foreign key attribute.
     *
     * @param    PesertaDidik $l PesertaDidik
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function addPesertaDidikRelatedByPekerjaanIdIbu(PesertaDidik $l)
    {
        if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu === null) {
            $this->initPesertaDidiksRelatedByPekerjaanIdIbu();
            $this->collPesertaDidiksRelatedByPekerjaanIdIbuPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidiksRelatedByPekerjaanIdIbu->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikRelatedByPekerjaanIdIbu($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdIbu $pesertaDidikRelatedByPekerjaanIdIbu The pesertaDidikRelatedByPekerjaanIdIbu object to add.
     */
    protected function doAddPesertaDidikRelatedByPekerjaanIdIbu($pesertaDidikRelatedByPekerjaanIdIbu)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu[]= $pesertaDidikRelatedByPekerjaanIdIbu;
        $pesertaDidikRelatedByPekerjaanIdIbu->setPekerjaanRelatedByPekerjaanIdIbu($this);
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdIbu $pesertaDidikRelatedByPekerjaanIdIbu The pesertaDidikRelatedByPekerjaanIdIbu object to remove.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function removePesertaDidikRelatedByPekerjaanIdIbu($pesertaDidikRelatedByPekerjaanIdIbu)
    {
        if ($this->getPesertaDidiksRelatedByPekerjaanIdIbu()->contains($pesertaDidikRelatedByPekerjaanIdIbu)) {
            $this->collPesertaDidiksRelatedByPekerjaanIdIbu->remove($this->collPesertaDidiksRelatedByPekerjaanIdIbu->search($pesertaDidikRelatedByPekerjaanIdIbu));
            if (null === $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion) {
                $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion = clone $this->collPesertaDidiksRelatedByPekerjaanIdIbu;
                $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion->clear();
            }
            $this->pesertaDidiksRelatedByPekerjaanIdIbuScheduledForDeletion[]= $pesertaDidikRelatedByPekerjaanIdIbu;
            $pesertaDidikRelatedByPekerjaanIdIbu->setPekerjaanRelatedByPekerjaanIdIbu(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdIbu from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdIbuJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdIbu($query, $con);
    }

    /**
     * Clears out the collPesertaDidiksRelatedByPekerjaanIdWali collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pekerjaan The current object (for fluent API support)
     * @see        addPesertaDidiksRelatedByPekerjaanIdWali()
     */
    public function clearPesertaDidiksRelatedByPekerjaanIdWali()
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdWali = null; // important to set this to null since that means it is uninitialized
        $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = null;

        return $this;
    }

    /**
     * reset is the collPesertaDidiksRelatedByPekerjaanIdWali collection loaded partially
     *
     * @return void
     */
    public function resetPartialPesertaDidiksRelatedByPekerjaanIdWali($v = true)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = $v;
    }

    /**
     * Initializes the collPesertaDidiksRelatedByPekerjaanIdWali collection.
     *
     * By default this just sets the collPesertaDidiksRelatedByPekerjaanIdWali collection to an empty array (like clearcollPesertaDidiksRelatedByPekerjaanIdWali());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPesertaDidiksRelatedByPekerjaanIdWali($overrideExisting = true)
    {
        if (null !== $this->collPesertaDidiksRelatedByPekerjaanIdWali && !$overrideExisting) {
            return;
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdWali = new PropelObjectCollection();
        $this->collPesertaDidiksRelatedByPekerjaanIdWali->setModel('PesertaDidik');
    }

    /**
     * Gets an array of PesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pekerjaan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     * @throws PropelException
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWali($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdWali || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                // return empty collection
                $this->initPesertaDidiksRelatedByPekerjaanIdWali();
            } else {
                $collPesertaDidiksRelatedByPekerjaanIdWali = PesertaDidikQuery::create(null, $criteria)
                    ->filterByPekerjaanRelatedByPekerjaanIdWali($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial && count($collPesertaDidiksRelatedByPekerjaanIdWali)) {
                      $this->initPesertaDidiksRelatedByPekerjaanIdWali(false);

                      foreach($collPesertaDidiksRelatedByPekerjaanIdWali as $obj) {
                        if (false == $this->collPesertaDidiksRelatedByPekerjaanIdWali->contains($obj)) {
                          $this->collPesertaDidiksRelatedByPekerjaanIdWali->append($obj);
                        }
                      }

                      $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = true;
                    }

                    $collPesertaDidiksRelatedByPekerjaanIdWali->getInternalIterator()->rewind();
                    return $collPesertaDidiksRelatedByPekerjaanIdWali;
                }

                if($partial && $this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                    foreach($this->collPesertaDidiksRelatedByPekerjaanIdWali as $obj) {
                        if($obj->isNew()) {
                            $collPesertaDidiksRelatedByPekerjaanIdWali[] = $obj;
                        }
                    }
                }

                $this->collPesertaDidiksRelatedByPekerjaanIdWali = $collPesertaDidiksRelatedByPekerjaanIdWali;
                $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = false;
            }
        }

        return $this->collPesertaDidiksRelatedByPekerjaanIdWali;
    }

    /**
     * Sets a collection of PesertaDidikRelatedByPekerjaanIdWali objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pesertaDidiksRelatedByPekerjaanIdWali A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPesertaDidiksRelatedByPekerjaanIdWali(PropelCollection $pesertaDidiksRelatedByPekerjaanIdWali, PropelPDO $con = null)
    {
        $pesertaDidiksRelatedByPekerjaanIdWaliToDelete = $this->getPesertaDidiksRelatedByPekerjaanIdWali(new Criteria(), $con)->diff($pesertaDidiksRelatedByPekerjaanIdWali);

        $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion = unserialize(serialize($pesertaDidiksRelatedByPekerjaanIdWaliToDelete));

        foreach ($pesertaDidiksRelatedByPekerjaanIdWaliToDelete as $pesertaDidikRelatedByPekerjaanIdWaliRemoved) {
            $pesertaDidikRelatedByPekerjaanIdWaliRemoved->setPekerjaanRelatedByPekerjaanIdWali(null);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdWali = null;
        foreach ($pesertaDidiksRelatedByPekerjaanIdWali as $pesertaDidikRelatedByPekerjaanIdWali) {
            $this->addPesertaDidikRelatedByPekerjaanIdWali($pesertaDidikRelatedByPekerjaanIdWali);
        }

        $this->collPesertaDidiksRelatedByPekerjaanIdWali = $pesertaDidiksRelatedByPekerjaanIdWali;
        $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PesertaDidik objects.
     * @throws PropelException
     */
    public function countPesertaDidiksRelatedByPekerjaanIdWali(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial && !$this->isNew();
        if (null === $this->collPesertaDidiksRelatedByPekerjaanIdWali || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPesertaDidiksRelatedByPekerjaanIdWali());
            }
            $query = PesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPekerjaanRelatedByPekerjaanIdWali($this)
                ->count($con);
        }

        return count($this->collPesertaDidiksRelatedByPekerjaanIdWali);
    }

    /**
     * Method called to associate a PesertaDidik object to this object
     * through the PesertaDidik foreign key attribute.
     *
     * @param    PesertaDidik $l PesertaDidik
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function addPesertaDidikRelatedByPekerjaanIdWali(PesertaDidik $l)
    {
        if ($this->collPesertaDidiksRelatedByPekerjaanIdWali === null) {
            $this->initPesertaDidiksRelatedByPekerjaanIdWali();
            $this->collPesertaDidiksRelatedByPekerjaanIdWaliPartial = true;
        }
        if (!in_array($l, $this->collPesertaDidiksRelatedByPekerjaanIdWali->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPesertaDidikRelatedByPekerjaanIdWali($l);
        }

        return $this;
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdWali $pesertaDidikRelatedByPekerjaanIdWali The pesertaDidikRelatedByPekerjaanIdWali object to add.
     */
    protected function doAddPesertaDidikRelatedByPekerjaanIdWali($pesertaDidikRelatedByPekerjaanIdWali)
    {
        $this->collPesertaDidiksRelatedByPekerjaanIdWali[]= $pesertaDidikRelatedByPekerjaanIdWali;
        $pesertaDidikRelatedByPekerjaanIdWali->setPekerjaanRelatedByPekerjaanIdWali($this);
    }

    /**
     * @param	PesertaDidikRelatedByPekerjaanIdWali $pesertaDidikRelatedByPekerjaanIdWali The pesertaDidikRelatedByPekerjaanIdWali object to remove.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function removePesertaDidikRelatedByPekerjaanIdWali($pesertaDidikRelatedByPekerjaanIdWali)
    {
        if ($this->getPesertaDidiksRelatedByPekerjaanIdWali()->contains($pesertaDidikRelatedByPekerjaanIdWali)) {
            $this->collPesertaDidiksRelatedByPekerjaanIdWali->remove($this->collPesertaDidiksRelatedByPekerjaanIdWali->search($pesertaDidikRelatedByPekerjaanIdWali));
            if (null === $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion) {
                $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion = clone $this->collPesertaDidiksRelatedByPekerjaanIdWali;
                $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion->clear();
            }
            $this->pesertaDidiksRelatedByPekerjaanIdWaliScheduledForDeletion[]= $pesertaDidikRelatedByPekerjaanIdWali;
            $pesertaDidikRelatedByPekerjaanIdWali->setPekerjaanRelatedByPekerjaanIdWali(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinAlatTransportasiRelatedByAlatTransportasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('AlatTransportasiRelatedByAlatTransportasiId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenisTinggalRelatedByJenisTinggalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisTinggalRelatedByJenisTinggalId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinJenjangPendidikanRelatedByJenjangPendidikanWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenjangPendidikanRelatedByJenjangPendidikanWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdWali($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PesertaDidiksRelatedByPekerjaanIdWali from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PesertaDidik[] List of PesertaDidik objects
     */
    public function getPesertaDidiksRelatedByPekerjaanIdWaliJoinPenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', $join_behavior);

        return $this->getPesertaDidiksRelatedByPekerjaanIdWali($query, $con);
    }

    /**
     * Clears out the collPtksRelatedByPekerjaanSuamiIstri collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pekerjaan The current object (for fluent API support)
     * @see        addPtksRelatedByPekerjaanSuamiIstri()
     */
    public function clearPtksRelatedByPekerjaanSuamiIstri()
    {
        $this->collPtksRelatedByPekerjaanSuamiIstri = null; // important to set this to null since that means it is uninitialized
        $this->collPtksRelatedByPekerjaanSuamiIstriPartial = null;

        return $this;
    }

    /**
     * reset is the collPtksRelatedByPekerjaanSuamiIstri collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtksRelatedByPekerjaanSuamiIstri($v = true)
    {
        $this->collPtksRelatedByPekerjaanSuamiIstriPartial = $v;
    }

    /**
     * Initializes the collPtksRelatedByPekerjaanSuamiIstri collection.
     *
     * By default this just sets the collPtksRelatedByPekerjaanSuamiIstri collection to an empty array (like clearcollPtksRelatedByPekerjaanSuamiIstri());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtksRelatedByPekerjaanSuamiIstri($overrideExisting = true)
    {
        if (null !== $this->collPtksRelatedByPekerjaanSuamiIstri && !$overrideExisting) {
            return;
        }
        $this->collPtksRelatedByPekerjaanSuamiIstri = new PropelObjectCollection();
        $this->collPtksRelatedByPekerjaanSuamiIstri->setModel('Ptk');
    }

    /**
     * Gets an array of Ptk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pekerjaan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     * @throws PropelException
     */
    public function getPtksRelatedByPekerjaanSuamiIstri($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPekerjaanSuamiIstriPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPekerjaanSuamiIstri || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPekerjaanSuamiIstri) {
                // return empty collection
                $this->initPtksRelatedByPekerjaanSuamiIstri();
            } else {
                $collPtksRelatedByPekerjaanSuamiIstri = PtkQuery::create(null, $criteria)
                    ->filterByPekerjaanRelatedByPekerjaanSuamiIstri($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtksRelatedByPekerjaanSuamiIstriPartial && count($collPtksRelatedByPekerjaanSuamiIstri)) {
                      $this->initPtksRelatedByPekerjaanSuamiIstri(false);

                      foreach($collPtksRelatedByPekerjaanSuamiIstri as $obj) {
                        if (false == $this->collPtksRelatedByPekerjaanSuamiIstri->contains($obj)) {
                          $this->collPtksRelatedByPekerjaanSuamiIstri->append($obj);
                        }
                      }

                      $this->collPtksRelatedByPekerjaanSuamiIstriPartial = true;
                    }

                    $collPtksRelatedByPekerjaanSuamiIstri->getInternalIterator()->rewind();
                    return $collPtksRelatedByPekerjaanSuamiIstri;
                }

                if($partial && $this->collPtksRelatedByPekerjaanSuamiIstri) {
                    foreach($this->collPtksRelatedByPekerjaanSuamiIstri as $obj) {
                        if($obj->isNew()) {
                            $collPtksRelatedByPekerjaanSuamiIstri[] = $obj;
                        }
                    }
                }

                $this->collPtksRelatedByPekerjaanSuamiIstri = $collPtksRelatedByPekerjaanSuamiIstri;
                $this->collPtksRelatedByPekerjaanSuamiIstriPartial = false;
            }
        }

        return $this->collPtksRelatedByPekerjaanSuamiIstri;
    }

    /**
     * Sets a collection of PtkRelatedByPekerjaanSuamiIstri objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptksRelatedByPekerjaanSuamiIstri A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPtksRelatedByPekerjaanSuamiIstri(PropelCollection $ptksRelatedByPekerjaanSuamiIstri, PropelPDO $con = null)
    {
        $ptksRelatedByPekerjaanSuamiIstriToDelete = $this->getPtksRelatedByPekerjaanSuamiIstri(new Criteria(), $con)->diff($ptksRelatedByPekerjaanSuamiIstri);

        $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion = unserialize(serialize($ptksRelatedByPekerjaanSuamiIstriToDelete));

        foreach ($ptksRelatedByPekerjaanSuamiIstriToDelete as $ptkRelatedByPekerjaanSuamiIstriRemoved) {
            $ptkRelatedByPekerjaanSuamiIstriRemoved->setPekerjaanRelatedByPekerjaanSuamiIstri(null);
        }

        $this->collPtksRelatedByPekerjaanSuamiIstri = null;
        foreach ($ptksRelatedByPekerjaanSuamiIstri as $ptkRelatedByPekerjaanSuamiIstri) {
            $this->addPtkRelatedByPekerjaanSuamiIstri($ptkRelatedByPekerjaanSuamiIstri);
        }

        $this->collPtksRelatedByPekerjaanSuamiIstri = $ptksRelatedByPekerjaanSuamiIstri;
        $this->collPtksRelatedByPekerjaanSuamiIstriPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ptk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Ptk objects.
     * @throws PropelException
     */
    public function countPtksRelatedByPekerjaanSuamiIstri(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPekerjaanSuamiIstriPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPekerjaanSuamiIstri || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPekerjaanSuamiIstri) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtksRelatedByPekerjaanSuamiIstri());
            }
            $query = PtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPekerjaanRelatedByPekerjaanSuamiIstri($this)
                ->count($con);
        }

        return count($this->collPtksRelatedByPekerjaanSuamiIstri);
    }

    /**
     * Method called to associate a Ptk object to this object
     * through the Ptk foreign key attribute.
     *
     * @param    Ptk $l Ptk
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function addPtkRelatedByPekerjaanSuamiIstri(Ptk $l)
    {
        if ($this->collPtksRelatedByPekerjaanSuamiIstri === null) {
            $this->initPtksRelatedByPekerjaanSuamiIstri();
            $this->collPtksRelatedByPekerjaanSuamiIstriPartial = true;
        }
        if (!in_array($l, $this->collPtksRelatedByPekerjaanSuamiIstri->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkRelatedByPekerjaanSuamiIstri($l);
        }

        return $this;
    }

    /**
     * @param	PtkRelatedByPekerjaanSuamiIstri $ptkRelatedByPekerjaanSuamiIstri The ptkRelatedByPekerjaanSuamiIstri object to add.
     */
    protected function doAddPtkRelatedByPekerjaanSuamiIstri($ptkRelatedByPekerjaanSuamiIstri)
    {
        $this->collPtksRelatedByPekerjaanSuamiIstri[]= $ptkRelatedByPekerjaanSuamiIstri;
        $ptkRelatedByPekerjaanSuamiIstri->setPekerjaanRelatedByPekerjaanSuamiIstri($this);
    }

    /**
     * @param	PtkRelatedByPekerjaanSuamiIstri $ptkRelatedByPekerjaanSuamiIstri The ptkRelatedByPekerjaanSuamiIstri object to remove.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function removePtkRelatedByPekerjaanSuamiIstri($ptkRelatedByPekerjaanSuamiIstri)
    {
        if ($this->getPtksRelatedByPekerjaanSuamiIstri()->contains($ptkRelatedByPekerjaanSuamiIstri)) {
            $this->collPtksRelatedByPekerjaanSuamiIstri->remove($this->collPtksRelatedByPekerjaanSuamiIstri->search($ptkRelatedByPekerjaanSuamiIstri));
            if (null === $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion) {
                $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion = clone $this->collPtksRelatedByPekerjaanSuamiIstri;
                $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion->clear();
            }
            $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion[]= clone $ptkRelatedByPekerjaanSuamiIstri;
            $ptkRelatedByPekerjaanSuamiIstri->setPekerjaanRelatedByPekerjaanSuamiIstri(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }

    /**
     * Clears out the collPtksRelatedByPekerjaanSuamiIstri collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pekerjaan The current object (for fluent API support)
     * @see        addPtksRelatedByPekerjaanSuamiIstri()
     */
    public function clearPtksRelatedByPekerjaanSuamiIstri()
    {
        $this->collPtksRelatedByPekerjaanSuamiIstri = null; // important to set this to null since that means it is uninitialized
        $this->collPtksRelatedByPekerjaanSuamiIstriPartial = null;

        return $this;
    }

    /**
     * reset is the collPtksRelatedByPekerjaanSuamiIstri collection loaded partially
     *
     * @return void
     */
    public function resetPartialPtksRelatedByPekerjaanSuamiIstri($v = true)
    {
        $this->collPtksRelatedByPekerjaanSuamiIstriPartial = $v;
    }

    /**
     * Initializes the collPtksRelatedByPekerjaanSuamiIstri collection.
     *
     * By default this just sets the collPtksRelatedByPekerjaanSuamiIstri collection to an empty array (like clearcollPtksRelatedByPekerjaanSuamiIstri());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPtksRelatedByPekerjaanSuamiIstri($overrideExisting = true)
    {
        if (null !== $this->collPtksRelatedByPekerjaanSuamiIstri && !$overrideExisting) {
            return;
        }
        $this->collPtksRelatedByPekerjaanSuamiIstri = new PropelObjectCollection();
        $this->collPtksRelatedByPekerjaanSuamiIstri->setModel('Ptk');
    }

    /**
     * Gets an array of Ptk objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pekerjaan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     * @throws PropelException
     */
    public function getPtksRelatedByPekerjaanSuamiIstri($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPekerjaanSuamiIstriPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPekerjaanSuamiIstri || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPekerjaanSuamiIstri) {
                // return empty collection
                $this->initPtksRelatedByPekerjaanSuamiIstri();
            } else {
                $collPtksRelatedByPekerjaanSuamiIstri = PtkQuery::create(null, $criteria)
                    ->filterByPekerjaanRelatedByPekerjaanSuamiIstri($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPtksRelatedByPekerjaanSuamiIstriPartial && count($collPtksRelatedByPekerjaanSuamiIstri)) {
                      $this->initPtksRelatedByPekerjaanSuamiIstri(false);

                      foreach($collPtksRelatedByPekerjaanSuamiIstri as $obj) {
                        if (false == $this->collPtksRelatedByPekerjaanSuamiIstri->contains($obj)) {
                          $this->collPtksRelatedByPekerjaanSuamiIstri->append($obj);
                        }
                      }

                      $this->collPtksRelatedByPekerjaanSuamiIstriPartial = true;
                    }

                    $collPtksRelatedByPekerjaanSuamiIstri->getInternalIterator()->rewind();
                    return $collPtksRelatedByPekerjaanSuamiIstri;
                }

                if($partial && $this->collPtksRelatedByPekerjaanSuamiIstri) {
                    foreach($this->collPtksRelatedByPekerjaanSuamiIstri as $obj) {
                        if($obj->isNew()) {
                            $collPtksRelatedByPekerjaanSuamiIstri[] = $obj;
                        }
                    }
                }

                $this->collPtksRelatedByPekerjaanSuamiIstri = $collPtksRelatedByPekerjaanSuamiIstri;
                $this->collPtksRelatedByPekerjaanSuamiIstriPartial = false;
            }
        }

        return $this->collPtksRelatedByPekerjaanSuamiIstri;
    }

    /**
     * Sets a collection of PtkRelatedByPekerjaanSuamiIstri objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $ptksRelatedByPekerjaanSuamiIstri A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function setPtksRelatedByPekerjaanSuamiIstri(PropelCollection $ptksRelatedByPekerjaanSuamiIstri, PropelPDO $con = null)
    {
        $ptksRelatedByPekerjaanSuamiIstriToDelete = $this->getPtksRelatedByPekerjaanSuamiIstri(new Criteria(), $con)->diff($ptksRelatedByPekerjaanSuamiIstri);

        $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion = unserialize(serialize($ptksRelatedByPekerjaanSuamiIstriToDelete));

        foreach ($ptksRelatedByPekerjaanSuamiIstriToDelete as $ptkRelatedByPekerjaanSuamiIstriRemoved) {
            $ptkRelatedByPekerjaanSuamiIstriRemoved->setPekerjaanRelatedByPekerjaanSuamiIstri(null);
        }

        $this->collPtksRelatedByPekerjaanSuamiIstri = null;
        foreach ($ptksRelatedByPekerjaanSuamiIstri as $ptkRelatedByPekerjaanSuamiIstri) {
            $this->addPtkRelatedByPekerjaanSuamiIstri($ptkRelatedByPekerjaanSuamiIstri);
        }

        $this->collPtksRelatedByPekerjaanSuamiIstri = $ptksRelatedByPekerjaanSuamiIstri;
        $this->collPtksRelatedByPekerjaanSuamiIstriPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ptk objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Ptk objects.
     * @throws PropelException
     */
    public function countPtksRelatedByPekerjaanSuamiIstri(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPtksRelatedByPekerjaanSuamiIstriPartial && !$this->isNew();
        if (null === $this->collPtksRelatedByPekerjaanSuamiIstri || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPtksRelatedByPekerjaanSuamiIstri) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPtksRelatedByPekerjaanSuamiIstri());
            }
            $query = PtkQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPekerjaanRelatedByPekerjaanSuamiIstri($this)
                ->count($con);
        }

        return count($this->collPtksRelatedByPekerjaanSuamiIstri);
    }

    /**
     * Method called to associate a Ptk object to this object
     * through the Ptk foreign key attribute.
     *
     * @param    Ptk $l Ptk
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function addPtkRelatedByPekerjaanSuamiIstri(Ptk $l)
    {
        if ($this->collPtksRelatedByPekerjaanSuamiIstri === null) {
            $this->initPtksRelatedByPekerjaanSuamiIstri();
            $this->collPtksRelatedByPekerjaanSuamiIstriPartial = true;
        }
        if (!in_array($l, $this->collPtksRelatedByPekerjaanSuamiIstri->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPtkRelatedByPekerjaanSuamiIstri($l);
        }

        return $this;
    }

    /**
     * @param	PtkRelatedByPekerjaanSuamiIstri $ptkRelatedByPekerjaanSuamiIstri The ptkRelatedByPekerjaanSuamiIstri object to add.
     */
    protected function doAddPtkRelatedByPekerjaanSuamiIstri($ptkRelatedByPekerjaanSuamiIstri)
    {
        $this->collPtksRelatedByPekerjaanSuamiIstri[]= $ptkRelatedByPekerjaanSuamiIstri;
        $ptkRelatedByPekerjaanSuamiIstri->setPekerjaanRelatedByPekerjaanSuamiIstri($this);
    }

    /**
     * @param	PtkRelatedByPekerjaanSuamiIstri $ptkRelatedByPekerjaanSuamiIstri The ptkRelatedByPekerjaanSuamiIstri object to remove.
     * @return Pekerjaan The current object (for fluent API support)
     */
    public function removePtkRelatedByPekerjaanSuamiIstri($ptkRelatedByPekerjaanSuamiIstri)
    {
        if ($this->getPtksRelatedByPekerjaanSuamiIstri()->contains($ptkRelatedByPekerjaanSuamiIstri)) {
            $this->collPtksRelatedByPekerjaanSuamiIstri->remove($this->collPtksRelatedByPekerjaanSuamiIstri->search($ptkRelatedByPekerjaanSuamiIstri));
            if (null === $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion) {
                $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion = clone $this->collPtksRelatedByPekerjaanSuamiIstri;
                $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion->clear();
            }
            $this->ptksRelatedByPekerjaanSuamiIstriScheduledForDeletion[]= clone $ptkRelatedByPekerjaanSuamiIstri;
            $ptkRelatedByPekerjaanSuamiIstri->setPekerjaanRelatedByPekerjaanSuamiIstri(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinSekolahRelatedByEntrySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedByEntrySekolahId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinAgamaRelatedByAgamaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('AgamaRelatedByAgamaId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinBidangStudiRelatedByPengawasBidangStudiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('BidangStudiRelatedByPengawasBidangStudiId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinJenisPtkRelatedByJenisPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('JenisPtkRelatedByJenisPtkId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinKeahlianLaboratoriumRelatedByKeahlianLaboratoriumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KeahlianLaboratoriumRelatedByKeahlianLaboratoriumId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinKebutuhanKhususRelatedByMampuHandleKk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByMampuHandleKk', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinLembagaPengangkatRelatedByLembagaPengangkatId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('LembagaPengangkatRelatedByLembagaPengangkatId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinMstWilayahRelatedByKodeWilayah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('MstWilayahRelatedByKodeWilayah', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinNegaraRelatedByKewarganegaraan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('NegaraRelatedByKewarganegaraan', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinPangkatGolonganRelatedByPangkatGolonganId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('PangkatGolonganRelatedByPangkatGolonganId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinStatusKepegawaianRelatedByStatusKepegawaianId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKepegawaianRelatedByStatusKepegawaianId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinStatusKeaktifanPegawaiRelatedByStatusKeaktifanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('StatusKeaktifanPegawaiRelatedByStatusKeaktifanId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pekerjaan is new, it will return
     * an empty collection; or if this Pekerjaan has previously
     * been saved, it will retrieve related PtksRelatedByPekerjaanSuamiIstri from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pekerjaan.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Ptk[] List of Ptk objects
     */
    public function getPtksRelatedByPekerjaanSuamiIstriJoinSumberGajiRelatedBySumberGajiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PtkQuery::create(null, $criteria);
        $query->joinWith('SumberGajiRelatedBySumberGajiId', $join_behavior);

        return $this->getPtksRelatedByPekerjaanSuamiIstri($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->pekerjaan_id = null;
        $this->nama = null;
        $this->create_date = null;
        $this->last_update = null;
        $this->expired_date = null;
        $this->last_sync = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdAyah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdIbu as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdWali as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdAyah as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdIbu as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPesertaDidiksRelatedByPekerjaanIdWali) {
                foreach ($this->collPesertaDidiksRelatedByPekerjaanIdWali as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtksRelatedByPekerjaanSuamiIstri) {
                foreach ($this->collPtksRelatedByPekerjaanSuamiIstri as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPtksRelatedByPekerjaanSuamiIstri) {
                foreach ($this->collPtksRelatedByPekerjaanSuamiIstri as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah instanceof PropelCollection) {
            $this->collPesertaDidiksRelatedByPekerjaanIdAyah->clearIterator();
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = null;
        if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu instanceof PropelCollection) {
            $this->collPesertaDidiksRelatedByPekerjaanIdIbu->clearIterator();
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = null;
        if ($this->collPesertaDidiksRelatedByPekerjaanIdWali instanceof PropelCollection) {
            $this->collPesertaDidiksRelatedByPekerjaanIdWali->clearIterator();
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdWali = null;
        if ($this->collPesertaDidiksRelatedByPekerjaanIdAyah instanceof PropelCollection) {
            $this->collPesertaDidiksRelatedByPekerjaanIdAyah->clearIterator();
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdAyah = null;
        if ($this->collPesertaDidiksRelatedByPekerjaanIdIbu instanceof PropelCollection) {
            $this->collPesertaDidiksRelatedByPekerjaanIdIbu->clearIterator();
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdIbu = null;
        if ($this->collPesertaDidiksRelatedByPekerjaanIdWali instanceof PropelCollection) {
            $this->collPesertaDidiksRelatedByPekerjaanIdWali->clearIterator();
        }
        $this->collPesertaDidiksRelatedByPekerjaanIdWali = null;
        if ($this->collPtksRelatedByPekerjaanSuamiIstri instanceof PropelCollection) {
            $this->collPtksRelatedByPekerjaanSuamiIstri->clearIterator();
        }
        $this->collPtksRelatedByPekerjaanSuamiIstri = null;
        if ($this->collPtksRelatedByPekerjaanSuamiIstri instanceof PropelCollection) {
            $this->collPtksRelatedByPekerjaanSuamiIstri->clearIterator();
        }
        $this->collPtksRelatedByPekerjaanSuamiIstri = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PekerjaanPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
