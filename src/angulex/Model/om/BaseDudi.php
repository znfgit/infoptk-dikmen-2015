<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\BidangUsaha;
use angulex\Model\BidangUsahaQuery;
use angulex\Model\Dudi;
use angulex\Model\DudiPeer;
use angulex\Model\DudiQuery;
use angulex\Model\Mou;
use angulex\Model\MouQuery;

/**
 * Base class that represents a row from the 'dudi' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseDudi extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\DudiPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        DudiPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the dudi_id field.
     * @var        string
     */
    protected $dudi_id;

    /**
     * The value for the bidang_usaha_id field.
     * @var        string
     */
    protected $bidang_usaha_id;

    /**
     * The value for the nama_dudi field.
     * @var        string
     */
    protected $nama_dudi;

    /**
     * The value for the npwp field.
     * @var        string
     */
    protected $npwp;

    /**
     * The value for the telp_kantor field.
     * @var        string
     */
    protected $telp_kantor;

    /**
     * The value for the fax field.
     * @var        string
     */
    protected $fax;

    /**
     * The value for the contact_person field.
     * @var        string
     */
    protected $contact_person;

    /**
     * The value for the telp_cp field.
     * @var        string
     */
    protected $telp_cp;

    /**
     * The value for the jabatan_cp field.
     * @var        string
     */
    protected $jabatan_cp;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        BidangUsaha
     */
    protected $aBidangUsahaRelatedByBidangUsahaId;

    /**
     * @var        BidangUsaha
     */
    protected $aBidangUsahaRelatedByBidangUsahaId;

    /**
     * @var        PropelObjectCollection|Mou[] Collection to store aggregation of Mou objects.
     */
    protected $collMousRelatedByDudiId;
    protected $collMousRelatedByDudiIdPartial;

    /**
     * @var        PropelObjectCollection|Mou[] Collection to store aggregation of Mou objects.
     */
    protected $collMousRelatedByDudiId;
    protected $collMousRelatedByDudiIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $mousRelatedByDudiIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $mousRelatedByDudiIdScheduledForDeletion = null;

    /**
     * Get the [dudi_id] column value.
     * 
     * @return string
     */
    public function getDudiId()
    {
        return $this->dudi_id;
    }

    /**
     * Get the [bidang_usaha_id] column value.
     * 
     * @return string
     */
    public function getBidangUsahaId()
    {
        return $this->bidang_usaha_id;
    }

    /**
     * Get the [nama_dudi] column value.
     * 
     * @return string
     */
    public function getNamaDudi()
    {
        return $this->nama_dudi;
    }

    /**
     * Get the [npwp] column value.
     * 
     * @return string
     */
    public function getNpwp()
    {
        return $this->npwp;
    }

    /**
     * Get the [telp_kantor] column value.
     * 
     * @return string
     */
    public function getTelpKantor()
    {
        return $this->telp_kantor;
    }

    /**
     * Get the [fax] column value.
     * 
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get the [contact_person] column value.
     * 
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contact_person;
    }

    /**
     * Get the [telp_cp] column value.
     * 
     * @return string
     */
    public function getTelpCp()
    {
        return $this->telp_cp;
    }

    /**
     * Get the [jabatan_cp] column value.
     * 
     * @return string
     */
    public function getJabatanCp()
    {
        return $this->jabatan_cp;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [dudi_id] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setDudiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->dudi_id !== $v) {
            $this->dudi_id = $v;
            $this->modifiedColumns[] = DudiPeer::DUDI_ID;
        }


        return $this;
    } // setDudiId()

    /**
     * Set the value of [bidang_usaha_id] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setBidangUsahaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->bidang_usaha_id !== $v) {
            $this->bidang_usaha_id = $v;
            $this->modifiedColumns[] = DudiPeer::BIDANG_USAHA_ID;
        }

        if ($this->aBidangUsahaRelatedByBidangUsahaId !== null && $this->aBidangUsahaRelatedByBidangUsahaId->getBidangUsahaId() !== $v) {
            $this->aBidangUsahaRelatedByBidangUsahaId = null;
        }

        if ($this->aBidangUsahaRelatedByBidangUsahaId !== null && $this->aBidangUsahaRelatedByBidangUsahaId->getBidangUsahaId() !== $v) {
            $this->aBidangUsahaRelatedByBidangUsahaId = null;
        }


        return $this;
    } // setBidangUsahaId()

    /**
     * Set the value of [nama_dudi] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setNamaDudi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_dudi !== $v) {
            $this->nama_dudi = $v;
            $this->modifiedColumns[] = DudiPeer::NAMA_DUDI;
        }


        return $this;
    } // setNamaDudi()

    /**
     * Set the value of [npwp] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setNpwp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->npwp !== $v) {
            $this->npwp = $v;
            $this->modifiedColumns[] = DudiPeer::NPWP;
        }


        return $this;
    } // setNpwp()

    /**
     * Set the value of [telp_kantor] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setTelpKantor($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telp_kantor !== $v) {
            $this->telp_kantor = $v;
            $this->modifiedColumns[] = DudiPeer::TELP_KANTOR;
        }


        return $this;
    } // setTelpKantor()

    /**
     * Set the value of [fax] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[] = DudiPeer::FAX;
        }


        return $this;
    } // setFax()

    /**
     * Set the value of [contact_person] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setContactPerson($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->contact_person !== $v) {
            $this->contact_person = $v;
            $this->modifiedColumns[] = DudiPeer::CONTACT_PERSON;
        }


        return $this;
    } // setContactPerson()

    /**
     * Set the value of [telp_cp] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setTelpCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telp_cp !== $v) {
            $this->telp_cp = $v;
            $this->modifiedColumns[] = DudiPeer::TELP_CP;
        }


        return $this;
    } // setTelpCp()

    /**
     * Set the value of [jabatan_cp] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setJabatanCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jabatan_cp !== $v) {
            $this->jabatan_cp = $v;
            $this->modifiedColumns[] = DudiPeer::JABATAN_CP;
        }


        return $this;
    } // setJabatanCp()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Dudi The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = DudiPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = DudiPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Dudi The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = DudiPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return Dudi The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = DudiPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->dudi_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->bidang_usaha_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nama_dudi = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->npwp = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->telp_kantor = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->fax = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->contact_person = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->telp_cp = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->jabatan_cp = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->last_update = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->soft_delete = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->last_sync = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->updater_id = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 13; // 13 = DudiPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Dudi object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aBidangUsahaRelatedByBidangUsahaId !== null && $this->bidang_usaha_id !== $this->aBidangUsahaRelatedByBidangUsahaId->getBidangUsahaId()) {
            $this->aBidangUsahaRelatedByBidangUsahaId = null;
        }
        if ($this->aBidangUsahaRelatedByBidangUsahaId !== null && $this->bidang_usaha_id !== $this->aBidangUsahaRelatedByBidangUsahaId->getBidangUsahaId()) {
            $this->aBidangUsahaRelatedByBidangUsahaId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DudiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = DudiPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBidangUsahaRelatedByBidangUsahaId = null;
            $this->aBidangUsahaRelatedByBidangUsahaId = null;
            $this->collMousRelatedByDudiId = null;

            $this->collMousRelatedByDudiId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DudiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = DudiQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(DudiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DudiPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBidangUsahaRelatedByBidangUsahaId !== null) {
                if ($this->aBidangUsahaRelatedByBidangUsahaId->isModified() || $this->aBidangUsahaRelatedByBidangUsahaId->isNew()) {
                    $affectedRows += $this->aBidangUsahaRelatedByBidangUsahaId->save($con);
                }
                $this->setBidangUsahaRelatedByBidangUsahaId($this->aBidangUsahaRelatedByBidangUsahaId);
            }

            if ($this->aBidangUsahaRelatedByBidangUsahaId !== null) {
                if ($this->aBidangUsahaRelatedByBidangUsahaId->isModified() || $this->aBidangUsahaRelatedByBidangUsahaId->isNew()) {
                    $affectedRows += $this->aBidangUsahaRelatedByBidangUsahaId->save($con);
                }
                $this->setBidangUsahaRelatedByBidangUsahaId($this->aBidangUsahaRelatedByBidangUsahaId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->mousRelatedByDudiIdScheduledForDeletion !== null) {
                if (!$this->mousRelatedByDudiIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->mousRelatedByDudiIdScheduledForDeletion as $mouRelatedByDudiId) {
                        // need to save related object because we set the relation to null
                        $mouRelatedByDudiId->save($con);
                    }
                    $this->mousRelatedByDudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collMousRelatedByDudiId !== null) {
                foreach ($this->collMousRelatedByDudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mousRelatedByDudiIdScheduledForDeletion !== null) {
                if (!$this->mousRelatedByDudiIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->mousRelatedByDudiIdScheduledForDeletion as $mouRelatedByDudiId) {
                        // need to save related object because we set the relation to null
                        $mouRelatedByDudiId->save($con);
                    }
                    $this->mousRelatedByDudiIdScheduledForDeletion = null;
                }
            }

            if ($this->collMousRelatedByDudiId !== null) {
                foreach ($this->collMousRelatedByDudiId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBidangUsahaRelatedByBidangUsahaId !== null) {
                if (!$this->aBidangUsahaRelatedByBidangUsahaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aBidangUsahaRelatedByBidangUsahaId->getValidationFailures());
                }
            }

            if ($this->aBidangUsahaRelatedByBidangUsahaId !== null) {
                if (!$this->aBidangUsahaRelatedByBidangUsahaId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aBidangUsahaRelatedByBidangUsahaId->getValidationFailures());
                }
            }


            if (($retval = DudiPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collMousRelatedByDudiId !== null) {
                    foreach ($this->collMousRelatedByDudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collMousRelatedByDudiId !== null) {
                    foreach ($this->collMousRelatedByDudiId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = DudiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getDudiId();
                break;
            case 1:
                return $this->getBidangUsahaId();
                break;
            case 2:
                return $this->getNamaDudi();
                break;
            case 3:
                return $this->getNpwp();
                break;
            case 4:
                return $this->getTelpKantor();
                break;
            case 5:
                return $this->getFax();
                break;
            case 6:
                return $this->getContactPerson();
                break;
            case 7:
                return $this->getTelpCp();
                break;
            case 8:
                return $this->getJabatanCp();
                break;
            case 9:
                return $this->getLastUpdate();
                break;
            case 10:
                return $this->getSoftDelete();
                break;
            case 11:
                return $this->getLastSync();
                break;
            case 12:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Dudi'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Dudi'][$this->getPrimaryKey()] = true;
        $keys = DudiPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getDudiId(),
            $keys[1] => $this->getBidangUsahaId(),
            $keys[2] => $this->getNamaDudi(),
            $keys[3] => $this->getNpwp(),
            $keys[4] => $this->getTelpKantor(),
            $keys[5] => $this->getFax(),
            $keys[6] => $this->getContactPerson(),
            $keys[7] => $this->getTelpCp(),
            $keys[8] => $this->getJabatanCp(),
            $keys[9] => $this->getLastUpdate(),
            $keys[10] => $this->getSoftDelete(),
            $keys[11] => $this->getLastSync(),
            $keys[12] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aBidangUsahaRelatedByBidangUsahaId) {
                $result['BidangUsahaRelatedByBidangUsahaId'] = $this->aBidangUsahaRelatedByBidangUsahaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBidangUsahaRelatedByBidangUsahaId) {
                $result['BidangUsahaRelatedByBidangUsahaId'] = $this->aBidangUsahaRelatedByBidangUsahaId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collMousRelatedByDudiId) {
                $result['MousRelatedByDudiId'] = $this->collMousRelatedByDudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMousRelatedByDudiId) {
                $result['MousRelatedByDudiId'] = $this->collMousRelatedByDudiId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = DudiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setDudiId($value);
                break;
            case 1:
                $this->setBidangUsahaId($value);
                break;
            case 2:
                $this->setNamaDudi($value);
                break;
            case 3:
                $this->setNpwp($value);
                break;
            case 4:
                $this->setTelpKantor($value);
                break;
            case 5:
                $this->setFax($value);
                break;
            case 6:
                $this->setContactPerson($value);
                break;
            case 7:
                $this->setTelpCp($value);
                break;
            case 8:
                $this->setJabatanCp($value);
                break;
            case 9:
                $this->setLastUpdate($value);
                break;
            case 10:
                $this->setSoftDelete($value);
                break;
            case 11:
                $this->setLastSync($value);
                break;
            case 12:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = DudiPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setDudiId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setBidangUsahaId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNamaDudi($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNpwp($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setTelpKantor($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setFax($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setContactPerson($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setTelpCp($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setJabatanCp($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setLastUpdate($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setSoftDelete($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setLastSync($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setUpdaterId($arr[$keys[12]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DudiPeer::DATABASE_NAME);

        if ($this->isColumnModified(DudiPeer::DUDI_ID)) $criteria->add(DudiPeer::DUDI_ID, $this->dudi_id);
        if ($this->isColumnModified(DudiPeer::BIDANG_USAHA_ID)) $criteria->add(DudiPeer::BIDANG_USAHA_ID, $this->bidang_usaha_id);
        if ($this->isColumnModified(DudiPeer::NAMA_DUDI)) $criteria->add(DudiPeer::NAMA_DUDI, $this->nama_dudi);
        if ($this->isColumnModified(DudiPeer::NPWP)) $criteria->add(DudiPeer::NPWP, $this->npwp);
        if ($this->isColumnModified(DudiPeer::TELP_KANTOR)) $criteria->add(DudiPeer::TELP_KANTOR, $this->telp_kantor);
        if ($this->isColumnModified(DudiPeer::FAX)) $criteria->add(DudiPeer::FAX, $this->fax);
        if ($this->isColumnModified(DudiPeer::CONTACT_PERSON)) $criteria->add(DudiPeer::CONTACT_PERSON, $this->contact_person);
        if ($this->isColumnModified(DudiPeer::TELP_CP)) $criteria->add(DudiPeer::TELP_CP, $this->telp_cp);
        if ($this->isColumnModified(DudiPeer::JABATAN_CP)) $criteria->add(DudiPeer::JABATAN_CP, $this->jabatan_cp);
        if ($this->isColumnModified(DudiPeer::LAST_UPDATE)) $criteria->add(DudiPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(DudiPeer::SOFT_DELETE)) $criteria->add(DudiPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(DudiPeer::LAST_SYNC)) $criteria->add(DudiPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(DudiPeer::UPDATER_ID)) $criteria->add(DudiPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(DudiPeer::DATABASE_NAME);
        $criteria->add(DudiPeer::DUDI_ID, $this->dudi_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getDudiId();
    }

    /**
     * Generic method to set the primary key (dudi_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setDudiId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getDudiId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Dudi (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBidangUsahaId($this->getBidangUsahaId());
        $copyObj->setNamaDudi($this->getNamaDudi());
        $copyObj->setNpwp($this->getNpwp());
        $copyObj->setTelpKantor($this->getTelpKantor());
        $copyObj->setFax($this->getFax());
        $copyObj->setContactPerson($this->getContactPerson());
        $copyObj->setTelpCp($this->getTelpCp());
        $copyObj->setJabatanCp($this->getJabatanCp());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getMousRelatedByDudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMouRelatedByDudiId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMousRelatedByDudiId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMouRelatedByDudiId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setDudiId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Dudi Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return DudiPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new DudiPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a BidangUsaha object.
     *
     * @param             BidangUsaha $v
     * @return Dudi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBidangUsahaRelatedByBidangUsahaId(BidangUsaha $v = null)
    {
        if ($v === null) {
            $this->setBidangUsahaId(NULL);
        } else {
            $this->setBidangUsahaId($v->getBidangUsahaId());
        }

        $this->aBidangUsahaRelatedByBidangUsahaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the BidangUsaha object, it will not be re-added.
        if ($v !== null) {
            $v->addDudiRelatedByBidangUsahaId($this);
        }


        return $this;
    }


    /**
     * Get the associated BidangUsaha object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return BidangUsaha The associated BidangUsaha object.
     * @throws PropelException
     */
    public function getBidangUsahaRelatedByBidangUsahaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aBidangUsahaRelatedByBidangUsahaId === null && (($this->bidang_usaha_id !== "" && $this->bidang_usaha_id !== null)) && $doQuery) {
            $this->aBidangUsahaRelatedByBidangUsahaId = BidangUsahaQuery::create()->findPk($this->bidang_usaha_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBidangUsahaRelatedByBidangUsahaId->addDudisRelatedByBidangUsahaId($this);
             */
        }

        return $this->aBidangUsahaRelatedByBidangUsahaId;
    }

    /**
     * Declares an association between this object and a BidangUsaha object.
     *
     * @param             BidangUsaha $v
     * @return Dudi The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBidangUsahaRelatedByBidangUsahaId(BidangUsaha $v = null)
    {
        if ($v === null) {
            $this->setBidangUsahaId(NULL);
        } else {
            $this->setBidangUsahaId($v->getBidangUsahaId());
        }

        $this->aBidangUsahaRelatedByBidangUsahaId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the BidangUsaha object, it will not be re-added.
        if ($v !== null) {
            $v->addDudiRelatedByBidangUsahaId($this);
        }


        return $this;
    }


    /**
     * Get the associated BidangUsaha object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return BidangUsaha The associated BidangUsaha object.
     * @throws PropelException
     */
    public function getBidangUsahaRelatedByBidangUsahaId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aBidangUsahaRelatedByBidangUsahaId === null && (($this->bidang_usaha_id !== "" && $this->bidang_usaha_id !== null)) && $doQuery) {
            $this->aBidangUsahaRelatedByBidangUsahaId = BidangUsahaQuery::create()->findPk($this->bidang_usaha_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBidangUsahaRelatedByBidangUsahaId->addDudisRelatedByBidangUsahaId($this);
             */
        }

        return $this->aBidangUsahaRelatedByBidangUsahaId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('MouRelatedByDudiId' == $relationName) {
            $this->initMousRelatedByDudiId();
        }
        if ('MouRelatedByDudiId' == $relationName) {
            $this->initMousRelatedByDudiId();
        }
    }

    /**
     * Clears out the collMousRelatedByDudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Dudi The current object (for fluent API support)
     * @see        addMousRelatedByDudiId()
     */
    public function clearMousRelatedByDudiId()
    {
        $this->collMousRelatedByDudiId = null; // important to set this to null since that means it is uninitialized
        $this->collMousRelatedByDudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collMousRelatedByDudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialMousRelatedByDudiId($v = true)
    {
        $this->collMousRelatedByDudiIdPartial = $v;
    }

    /**
     * Initializes the collMousRelatedByDudiId collection.
     *
     * By default this just sets the collMousRelatedByDudiId collection to an empty array (like clearcollMousRelatedByDudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMousRelatedByDudiId($overrideExisting = true)
    {
        if (null !== $this->collMousRelatedByDudiId && !$overrideExisting) {
            return;
        }
        $this->collMousRelatedByDudiId = new PropelObjectCollection();
        $this->collMousRelatedByDudiId->setModel('Mou');
    }

    /**
     * Gets an array of Mou objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Dudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Mou[] List of Mou objects
     * @throws PropelException
     */
    public function getMousRelatedByDudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collMousRelatedByDudiIdPartial && !$this->isNew();
        if (null === $this->collMousRelatedByDudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMousRelatedByDudiId) {
                // return empty collection
                $this->initMousRelatedByDudiId();
            } else {
                $collMousRelatedByDudiId = MouQuery::create(null, $criteria)
                    ->filterByDudiRelatedByDudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collMousRelatedByDudiIdPartial && count($collMousRelatedByDudiId)) {
                      $this->initMousRelatedByDudiId(false);

                      foreach($collMousRelatedByDudiId as $obj) {
                        if (false == $this->collMousRelatedByDudiId->contains($obj)) {
                          $this->collMousRelatedByDudiId->append($obj);
                        }
                      }

                      $this->collMousRelatedByDudiIdPartial = true;
                    }

                    $collMousRelatedByDudiId->getInternalIterator()->rewind();
                    return $collMousRelatedByDudiId;
                }

                if($partial && $this->collMousRelatedByDudiId) {
                    foreach($this->collMousRelatedByDudiId as $obj) {
                        if($obj->isNew()) {
                            $collMousRelatedByDudiId[] = $obj;
                        }
                    }
                }

                $this->collMousRelatedByDudiId = $collMousRelatedByDudiId;
                $this->collMousRelatedByDudiIdPartial = false;
            }
        }

        return $this->collMousRelatedByDudiId;
    }

    /**
     * Sets a collection of MouRelatedByDudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $mousRelatedByDudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Dudi The current object (for fluent API support)
     */
    public function setMousRelatedByDudiId(PropelCollection $mousRelatedByDudiId, PropelPDO $con = null)
    {
        $mousRelatedByDudiIdToDelete = $this->getMousRelatedByDudiId(new Criteria(), $con)->diff($mousRelatedByDudiId);

        $this->mousRelatedByDudiIdScheduledForDeletion = unserialize(serialize($mousRelatedByDudiIdToDelete));

        foreach ($mousRelatedByDudiIdToDelete as $mouRelatedByDudiIdRemoved) {
            $mouRelatedByDudiIdRemoved->setDudiRelatedByDudiId(null);
        }

        $this->collMousRelatedByDudiId = null;
        foreach ($mousRelatedByDudiId as $mouRelatedByDudiId) {
            $this->addMouRelatedByDudiId($mouRelatedByDudiId);
        }

        $this->collMousRelatedByDudiId = $mousRelatedByDudiId;
        $this->collMousRelatedByDudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Mou objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Mou objects.
     * @throws PropelException
     */
    public function countMousRelatedByDudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collMousRelatedByDudiIdPartial && !$this->isNew();
        if (null === $this->collMousRelatedByDudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMousRelatedByDudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getMousRelatedByDudiId());
            }
            $query = MouQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDudiRelatedByDudiId($this)
                ->count($con);
        }

        return count($this->collMousRelatedByDudiId);
    }

    /**
     * Method called to associate a Mou object to this object
     * through the Mou foreign key attribute.
     *
     * @param    Mou $l Mou
     * @return Dudi The current object (for fluent API support)
     */
    public function addMouRelatedByDudiId(Mou $l)
    {
        if ($this->collMousRelatedByDudiId === null) {
            $this->initMousRelatedByDudiId();
            $this->collMousRelatedByDudiIdPartial = true;
        }
        if (!in_array($l, $this->collMousRelatedByDudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddMouRelatedByDudiId($l);
        }

        return $this;
    }

    /**
     * @param	MouRelatedByDudiId $mouRelatedByDudiId The mouRelatedByDudiId object to add.
     */
    protected function doAddMouRelatedByDudiId($mouRelatedByDudiId)
    {
        $this->collMousRelatedByDudiId[]= $mouRelatedByDudiId;
        $mouRelatedByDudiId->setDudiRelatedByDudiId($this);
    }

    /**
     * @param	MouRelatedByDudiId $mouRelatedByDudiId The mouRelatedByDudiId object to remove.
     * @return Dudi The current object (for fluent API support)
     */
    public function removeMouRelatedByDudiId($mouRelatedByDudiId)
    {
        if ($this->getMousRelatedByDudiId()->contains($mouRelatedByDudiId)) {
            $this->collMousRelatedByDudiId->remove($this->collMousRelatedByDudiId->search($mouRelatedByDudiId));
            if (null === $this->mousRelatedByDudiIdScheduledForDeletion) {
                $this->mousRelatedByDudiIdScheduledForDeletion = clone $this->collMousRelatedByDudiId;
                $this->mousRelatedByDudiIdScheduledForDeletion->clear();
            }
            $this->mousRelatedByDudiIdScheduledForDeletion[]= $mouRelatedByDudiId;
            $mouRelatedByDudiId->setDudiRelatedByDudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Dudi is new, it will return
     * an empty collection; or if this Dudi has previously
     * been saved, it will retrieve related MousRelatedByDudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Dudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Mou[] List of Mou objects
     */
    public function getMousRelatedByDudiIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = MouQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getMousRelatedByDudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Dudi is new, it will return
     * an empty collection; or if this Dudi has previously
     * been saved, it will retrieve related MousRelatedByDudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Dudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Mou[] List of Mou objects
     */
    public function getMousRelatedByDudiIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = MouQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getMousRelatedByDudiId($query, $con);
    }

    /**
     * Clears out the collMousRelatedByDudiId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Dudi The current object (for fluent API support)
     * @see        addMousRelatedByDudiId()
     */
    public function clearMousRelatedByDudiId()
    {
        $this->collMousRelatedByDudiId = null; // important to set this to null since that means it is uninitialized
        $this->collMousRelatedByDudiIdPartial = null;

        return $this;
    }

    /**
     * reset is the collMousRelatedByDudiId collection loaded partially
     *
     * @return void
     */
    public function resetPartialMousRelatedByDudiId($v = true)
    {
        $this->collMousRelatedByDudiIdPartial = $v;
    }

    /**
     * Initializes the collMousRelatedByDudiId collection.
     *
     * By default this just sets the collMousRelatedByDudiId collection to an empty array (like clearcollMousRelatedByDudiId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMousRelatedByDudiId($overrideExisting = true)
    {
        if (null !== $this->collMousRelatedByDudiId && !$overrideExisting) {
            return;
        }
        $this->collMousRelatedByDudiId = new PropelObjectCollection();
        $this->collMousRelatedByDudiId->setModel('Mou');
    }

    /**
     * Gets an array of Mou objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Dudi is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Mou[] List of Mou objects
     * @throws PropelException
     */
    public function getMousRelatedByDudiId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collMousRelatedByDudiIdPartial && !$this->isNew();
        if (null === $this->collMousRelatedByDudiId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMousRelatedByDudiId) {
                // return empty collection
                $this->initMousRelatedByDudiId();
            } else {
                $collMousRelatedByDudiId = MouQuery::create(null, $criteria)
                    ->filterByDudiRelatedByDudiId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collMousRelatedByDudiIdPartial && count($collMousRelatedByDudiId)) {
                      $this->initMousRelatedByDudiId(false);

                      foreach($collMousRelatedByDudiId as $obj) {
                        if (false == $this->collMousRelatedByDudiId->contains($obj)) {
                          $this->collMousRelatedByDudiId->append($obj);
                        }
                      }

                      $this->collMousRelatedByDudiIdPartial = true;
                    }

                    $collMousRelatedByDudiId->getInternalIterator()->rewind();
                    return $collMousRelatedByDudiId;
                }

                if($partial && $this->collMousRelatedByDudiId) {
                    foreach($this->collMousRelatedByDudiId as $obj) {
                        if($obj->isNew()) {
                            $collMousRelatedByDudiId[] = $obj;
                        }
                    }
                }

                $this->collMousRelatedByDudiId = $collMousRelatedByDudiId;
                $this->collMousRelatedByDudiIdPartial = false;
            }
        }

        return $this->collMousRelatedByDudiId;
    }

    /**
     * Sets a collection of MouRelatedByDudiId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $mousRelatedByDudiId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Dudi The current object (for fluent API support)
     */
    public function setMousRelatedByDudiId(PropelCollection $mousRelatedByDudiId, PropelPDO $con = null)
    {
        $mousRelatedByDudiIdToDelete = $this->getMousRelatedByDudiId(new Criteria(), $con)->diff($mousRelatedByDudiId);

        $this->mousRelatedByDudiIdScheduledForDeletion = unserialize(serialize($mousRelatedByDudiIdToDelete));

        foreach ($mousRelatedByDudiIdToDelete as $mouRelatedByDudiIdRemoved) {
            $mouRelatedByDudiIdRemoved->setDudiRelatedByDudiId(null);
        }

        $this->collMousRelatedByDudiId = null;
        foreach ($mousRelatedByDudiId as $mouRelatedByDudiId) {
            $this->addMouRelatedByDudiId($mouRelatedByDudiId);
        }

        $this->collMousRelatedByDudiId = $mousRelatedByDudiId;
        $this->collMousRelatedByDudiIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Mou objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Mou objects.
     * @throws PropelException
     */
    public function countMousRelatedByDudiId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collMousRelatedByDudiIdPartial && !$this->isNew();
        if (null === $this->collMousRelatedByDudiId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMousRelatedByDudiId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getMousRelatedByDudiId());
            }
            $query = MouQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDudiRelatedByDudiId($this)
                ->count($con);
        }

        return count($this->collMousRelatedByDudiId);
    }

    /**
     * Method called to associate a Mou object to this object
     * through the Mou foreign key attribute.
     *
     * @param    Mou $l Mou
     * @return Dudi The current object (for fluent API support)
     */
    public function addMouRelatedByDudiId(Mou $l)
    {
        if ($this->collMousRelatedByDudiId === null) {
            $this->initMousRelatedByDudiId();
            $this->collMousRelatedByDudiIdPartial = true;
        }
        if (!in_array($l, $this->collMousRelatedByDudiId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddMouRelatedByDudiId($l);
        }

        return $this;
    }

    /**
     * @param	MouRelatedByDudiId $mouRelatedByDudiId The mouRelatedByDudiId object to add.
     */
    protected function doAddMouRelatedByDudiId($mouRelatedByDudiId)
    {
        $this->collMousRelatedByDudiId[]= $mouRelatedByDudiId;
        $mouRelatedByDudiId->setDudiRelatedByDudiId($this);
    }

    /**
     * @param	MouRelatedByDudiId $mouRelatedByDudiId The mouRelatedByDudiId object to remove.
     * @return Dudi The current object (for fluent API support)
     */
    public function removeMouRelatedByDudiId($mouRelatedByDudiId)
    {
        if ($this->getMousRelatedByDudiId()->contains($mouRelatedByDudiId)) {
            $this->collMousRelatedByDudiId->remove($this->collMousRelatedByDudiId->search($mouRelatedByDudiId));
            if (null === $this->mousRelatedByDudiIdScheduledForDeletion) {
                $this->mousRelatedByDudiIdScheduledForDeletion = clone $this->collMousRelatedByDudiId;
                $this->mousRelatedByDudiIdScheduledForDeletion->clear();
            }
            $this->mousRelatedByDudiIdScheduledForDeletion[]= $mouRelatedByDudiId;
            $mouRelatedByDudiId->setDudiRelatedByDudiId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Dudi is new, it will return
     * an empty collection; or if this Dudi has previously
     * been saved, it will retrieve related MousRelatedByDudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Dudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Mou[] List of Mou objects
     */
    public function getMousRelatedByDudiIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = MouQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getMousRelatedByDudiId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Dudi is new, it will return
     * an empty collection; or if this Dudi has previously
     * been saved, it will retrieve related MousRelatedByDudiId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Dudi.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Mou[] List of Mou objects
     */
    public function getMousRelatedByDudiIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = MouQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getMousRelatedByDudiId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->dudi_id = null;
        $this->bidang_usaha_id = null;
        $this->nama_dudi = null;
        $this->npwp = null;
        $this->telp_kantor = null;
        $this->fax = null;
        $this->contact_person = null;
        $this->telp_cp = null;
        $this->jabatan_cp = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collMousRelatedByDudiId) {
                foreach ($this->collMousRelatedByDudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMousRelatedByDudiId) {
                foreach ($this->collMousRelatedByDudiId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aBidangUsahaRelatedByBidangUsahaId instanceof Persistent) {
              $this->aBidangUsahaRelatedByBidangUsahaId->clearAllReferences($deep);
            }
            if ($this->aBidangUsahaRelatedByBidangUsahaId instanceof Persistent) {
              $this->aBidangUsahaRelatedByBidangUsahaId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collMousRelatedByDudiId instanceof PropelCollection) {
            $this->collMousRelatedByDudiId->clearIterator();
        }
        $this->collMousRelatedByDudiId = null;
        if ($this->collMousRelatedByDudiId instanceof PropelCollection) {
            $this->collMousRelatedByDudiId->clearIterator();
        }
        $this->collMousRelatedByDudiId = null;
        $this->aBidangUsahaRelatedByBidangUsahaId = null;
        $this->aBidangUsahaRelatedByBidangUsahaId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DudiPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
