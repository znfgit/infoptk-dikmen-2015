<?php

namespace angulex\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\AkreditasiProdi;
use angulex\Model\AkreditasiProdiQuery;
use angulex\Model\Jurusan;
use angulex\Model\JurusanKerjasama;
use angulex\Model\JurusanKerjasamaQuery;
use angulex\Model\JurusanQuery;
use angulex\Model\JurusanSp;
use angulex\Model\JurusanSpPeer;
use angulex\Model\JurusanSpQuery;
use angulex\Model\KebutuhanKhusus;
use angulex\Model\KebutuhanKhususQuery;
use angulex\Model\RegistrasiPesertaDidik;
use angulex\Model\RegistrasiPesertaDidikQuery;
use angulex\Model\RombonganBelajar;
use angulex\Model\RombonganBelajarQuery;
use angulex\Model\Sekolah;
use angulex\Model\SekolahQuery;
use angulex\Model\VldJurusanSp;
use angulex\Model\VldJurusanSpQuery;

/**
 * Base class that represents a row from the 'jurusan_sp' table.
 *
 * 
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseJurusanSp extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'angulex\\Model\\JurusanSpPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        JurusanSpPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the jurusan_sp_id field.
     * @var        string
     */
    protected $jurusan_sp_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the kebutuhan_khusus_id field.
     * @var        int
     */
    protected $kebutuhan_khusus_id;

    /**
     * The value for the jurusan_id field.
     * @var        string
     */
    protected $jurusan_id;

    /**
     * The value for the nama_jurusan_sp field.
     * @var        string
     */
    protected $nama_jurusan_sp;

    /**
     * The value for the sk_izin field.
     * @var        string
     */
    protected $sk_izin;

    /**
     * The value for the tanggal_sk_izin field.
     * @var        string
     */
    protected $tanggal_sk_izin;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Sekolah
     */
    protected $aSekolahRelatedBySekolahId;

    /**
     * @var        Jurusan
     */
    protected $aJurusanRelatedByJurusanId;

    /**
     * @var        Jurusan
     */
    protected $aJurusanRelatedByJurusanId;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususId;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhususRelatedByKebutuhanKhususId;

    /**
     * @var        PropelObjectCollection|RegistrasiPesertaDidik[] Collection to store aggregation of RegistrasiPesertaDidik objects.
     */
    protected $collRegistrasiPesertaDidiksRelatedByJurusanSpId;
    protected $collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|RegistrasiPesertaDidik[] Collection to store aggregation of RegistrasiPesertaDidik objects.
     */
    protected $collRegistrasiPesertaDidiksRelatedByJurusanSpId;
    protected $collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|VldJurusanSp[] Collection to store aggregation of VldJurusanSp objects.
     */
    protected $collVldJurusanSpsRelatedByJurusanSpId;
    protected $collVldJurusanSpsRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|VldJurusanSp[] Collection to store aggregation of VldJurusanSp objects.
     */
    protected $collVldJurusanSpsRelatedByJurusanSpId;
    protected $collVldJurusanSpsRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|JurusanKerjasama[] Collection to store aggregation of JurusanKerjasama objects.
     */
    protected $collJurusanKerjasamasRelatedByJurusanSpId;
    protected $collJurusanKerjasamasRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|JurusanKerjasama[] Collection to store aggregation of JurusanKerjasama objects.
     */
    protected $collJurusanKerjasamasRelatedByJurusanSpId;
    protected $collJurusanKerjasamasRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajarsRelatedByJurusanSpId;
    protected $collRombonganBelajarsRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajarsRelatedByJurusanSpId;
    protected $collRombonganBelajarsRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|AkreditasiProdi[] Collection to store aggregation of AkreditasiProdi objects.
     */
    protected $collAkreditasiProdisRelatedByJurusanSpId;
    protected $collAkreditasiProdisRelatedByJurusanSpIdPartial;

    /**
     * @var        PropelObjectCollection|AkreditasiProdi[] Collection to store aggregation of AkreditasiProdi objects.
     */
    protected $collAkreditasiProdisRelatedByJurusanSpId;
    protected $collAkreditasiProdisRelatedByJurusanSpIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion = null;

    /**
     * Get the [jurusan_sp_id] column value.
     * 
     * @return string
     */
    public function getJurusanSpId()
    {
        return $this->jurusan_sp_id;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [kebutuhan_khusus_id] column value.
     * 
     * @return int
     */
    public function getKebutuhanKhususId()
    {
        return $this->kebutuhan_khusus_id;
    }

    /**
     * Get the [jurusan_id] column value.
     * 
     * @return string
     */
    public function getJurusanId()
    {
        return $this->jurusan_id;
    }

    /**
     * Get the [nama_jurusan_sp] column value.
     * 
     * @return string
     */
    public function getNamaJurusanSp()
    {
        return $this->nama_jurusan_sp;
    }

    /**
     * Get the [sk_izin] column value.
     * 
     * @return string
     */
    public function getSkIzin()
    {
        return $this->sk_izin;
    }

    /**
     * Get the [tanggal_sk_izin] column value.
     * 
     * @return string
     */
    public function getTanggalSkIzin()
    {
        return $this->tanggal_sk_izin;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [updater_id] column value.
     * 
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [jurusan_sp_id] column.
     * 
     * @param string $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setJurusanSpId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jurusan_sp_id !== $v) {
            $this->jurusan_sp_id = $v;
            $this->modifiedColumns[] = JurusanSpPeer::JURUSAN_SP_ID;
        }


        return $this;
    } // setJurusanSpId()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = JurusanSpPeer::SEKOLAH_ID;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }

        if ($this->aSekolahRelatedBySekolahId !== null && $this->aSekolahRelatedBySekolahId->getSekolahId() !== $v) {
            $this->aSekolahRelatedBySekolahId = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [kebutuhan_khusus_id] column.
     * 
     * @param int $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setKebutuhanKhususId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kebutuhan_khusus_id !== $v) {
            $this->kebutuhan_khusus_id = $v;
            $this->modifiedColumns[] = JurusanSpPeer::KEBUTUHAN_KHUSUS_ID;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }

        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }


        return $this;
    } // setKebutuhanKhususId()

    /**
     * Set the value of [jurusan_id] column.
     * 
     * @param string $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setJurusanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jurusan_id !== $v) {
            $this->jurusan_id = $v;
            $this->modifiedColumns[] = JurusanSpPeer::JURUSAN_ID;
        }

        if ($this->aJurusanRelatedByJurusanId !== null && $this->aJurusanRelatedByJurusanId->getJurusanId() !== $v) {
            $this->aJurusanRelatedByJurusanId = null;
        }

        if ($this->aJurusanRelatedByJurusanId !== null && $this->aJurusanRelatedByJurusanId->getJurusanId() !== $v) {
            $this->aJurusanRelatedByJurusanId = null;
        }


        return $this;
    } // setJurusanId()

    /**
     * Set the value of [nama_jurusan_sp] column.
     * 
     * @param string $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setNamaJurusanSp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_jurusan_sp !== $v) {
            $this->nama_jurusan_sp = $v;
            $this->modifiedColumns[] = JurusanSpPeer::NAMA_JURUSAN_SP;
        }


        return $this;
    } // setNamaJurusanSp()

    /**
     * Set the value of [sk_izin] column.
     * 
     * @param string $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setSkIzin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sk_izin !== $v) {
            $this->sk_izin = $v;
            $this->modifiedColumns[] = JurusanSpPeer::SK_IZIN;
        }


        return $this;
    } // setSkIzin()

    /**
     * Set the value of [tanggal_sk_izin] column.
     * 
     * @param string $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setTanggalSkIzin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_sk_izin !== $v) {
            $this->tanggal_sk_izin = $v;
            $this->modifiedColumns[] = JurusanSpPeer::TANGGAL_SK_IZIN;
        }


        return $this;
    } // setTanggalSkIzin()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = JurusanSpPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param string $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = JurusanSpPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = JurusanSpPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     * 
     * @param string $v new value
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = JurusanSpPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->jurusan_sp_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->sekolah_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->kebutuhan_khusus_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->jurusan_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->nama_jurusan_sp = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->sk_izin = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->tanggal_sk_izin = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->last_update = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->soft_delete = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->last_sync = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->updater_id = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 11; // 11 = JurusanSpPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating JurusanSp object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aSekolahRelatedBySekolahId !== null && $this->sekolah_id !== $this->aSekolahRelatedBySekolahId->getSekolahId()) {
            $this->aSekolahRelatedBySekolahId = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->kebutuhan_khusus_id !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null && $this->kebutuhan_khusus_id !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getKebutuhanKhususId()) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        }
        if ($this->aJurusanRelatedByJurusanId !== null && $this->jurusan_id !== $this->aJurusanRelatedByJurusanId->getJurusanId()) {
            $this->aJurusanRelatedByJurusanId = null;
        }
        if ($this->aJurusanRelatedByJurusanId !== null && $this->jurusan_id !== $this->aJurusanRelatedByJurusanId->getJurusanId()) {
            $this->aJurusanRelatedByJurusanId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JurusanSpPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = JurusanSpPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSekolahRelatedBySekolahId = null;
            $this->aSekolahRelatedBySekolahId = null;
            $this->aJurusanRelatedByJurusanId = null;
            $this->aJurusanRelatedByJurusanId = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
            $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = null;

            $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = null;

            $this->collVldJurusanSpsRelatedByJurusanSpId = null;

            $this->collVldJurusanSpsRelatedByJurusanSpId = null;

            $this->collJurusanKerjasamasRelatedByJurusanSpId = null;

            $this->collJurusanKerjasamasRelatedByJurusanSpId = null;

            $this->collRombonganBelajarsRelatedByJurusanSpId = null;

            $this->collRombonganBelajarsRelatedByJurusanSpId = null;

            $this->collAkreditasiProdisRelatedByJurusanSpId = null;

            $this->collAkreditasiProdisRelatedByJurusanSpId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JurusanSpPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = JurusanSpQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(JurusanSpPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                JurusanSpPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if ($this->aSekolahRelatedBySekolahId->isModified() || $this->aSekolahRelatedBySekolahId->isNew()) {
                    $affectedRows += $this->aSekolahRelatedBySekolahId->save($con);
                }
                $this->setSekolahRelatedBySekolahId($this->aSekolahRelatedBySekolahId);
            }

            if ($this->aJurusanRelatedByJurusanId !== null) {
                if ($this->aJurusanRelatedByJurusanId->isModified() || $this->aJurusanRelatedByJurusanId->isNew()) {
                    $affectedRows += $this->aJurusanRelatedByJurusanId->save($con);
                }
                $this->setJurusanRelatedByJurusanId($this->aJurusanRelatedByJurusanId);
            }

            if ($this->aJurusanRelatedByJurusanId !== null) {
                if ($this->aJurusanRelatedByJurusanId->isModified() || $this->aJurusanRelatedByJurusanId->isNew()) {
                    $affectedRows += $this->aJurusanRelatedByJurusanId->save($con);
                }
                $this->setJurusanRelatedByJurusanId($this->aJurusanRelatedByJurusanId);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususId->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususId->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususId($this->aKebutuhanKhususRelatedByKebutuhanKhususId);
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId->isModified() || $this->aKebutuhanKhususRelatedByKebutuhanKhususId->isNew()) {
                    $affectedRows += $this->aKebutuhanKhususRelatedByKebutuhanKhususId->save($con);
                }
                $this->setKebutuhanKhususRelatedByKebutuhanKhususId($this->aKebutuhanKhususRelatedByKebutuhanKhususId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion as $registrasiPesertaDidikRelatedByJurusanSpId) {
                        // need to save related object because we set the relation to null
                        $registrasiPesertaDidikRelatedByJurusanSpId->save($con);
                    }
                    $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId !== null) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion as $registrasiPesertaDidikRelatedByJurusanSpId) {
                        // need to save related object because we set the relation to null
                        $registrasiPesertaDidikRelatedByJurusanSpId->save($con);
                    }
                    $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId !== null) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    VldJurusanSpQuery::create()
                        ->filterByPrimaryKeys($this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldJurusanSpsRelatedByJurusanSpId !== null) {
                foreach ($this->collVldJurusanSpsRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    VldJurusanSpQuery::create()
                        ->filterByPrimaryKeys($this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collVldJurusanSpsRelatedByJurusanSpId !== null) {
                foreach ($this->collVldJurusanSpsRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    JurusanKerjasamaQuery::create()
                        ->filterByPrimaryKeys($this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collJurusanKerjasamasRelatedByJurusanSpId !== null) {
                foreach ($this->collJurusanKerjasamasRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    JurusanKerjasamaQuery::create()
                        ->filterByPrimaryKeys($this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collJurusanKerjasamasRelatedByJurusanSpId !== null) {
                foreach ($this->collJurusanKerjasamasRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion as $rombonganBelajarRelatedByJurusanSpId) {
                        // need to save related object because we set the relation to null
                        $rombonganBelajarRelatedByJurusanSpId->save($con);
                    }
                    $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajarsRelatedByJurusanSpId !== null) {
                foreach ($this->collRombonganBelajarsRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion as $rombonganBelajarRelatedByJurusanSpId) {
                        // need to save related object because we set the relation to null
                        $rombonganBelajarRelatedByJurusanSpId->save($con);
                    }
                    $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajarsRelatedByJurusanSpId !== null) {
                foreach ($this->collRombonganBelajarsRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    AkreditasiProdiQuery::create()
                        ->filterByPrimaryKeys($this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collAkreditasiProdisRelatedByJurusanSpId !== null) {
                foreach ($this->collAkreditasiProdisRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion !== null) {
                if (!$this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion->isEmpty()) {
                    AkreditasiProdiQuery::create()
                        ->filterByPrimaryKeys($this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion = null;
                }
            }

            if ($this->collAkreditasiProdisRelatedByJurusanSpId !== null) {
                foreach ($this->collAkreditasiProdisRelatedByJurusanSpId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aSekolahRelatedBySekolahId !== null) {
                if (!$this->aSekolahRelatedBySekolahId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolahRelatedBySekolahId->getValidationFailures());
                }
            }

            if ($this->aJurusanRelatedByJurusanId !== null) {
                if (!$this->aJurusanRelatedByJurusanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJurusanRelatedByJurusanId->getValidationFailures());
                }
            }

            if ($this->aJurusanRelatedByJurusanId !== null) {
                if (!$this->aJurusanRelatedByJurusanId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJurusanRelatedByJurusanId->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId !== null) {
                if (!$this->aKebutuhanKhususRelatedByKebutuhanKhususId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhususRelatedByKebutuhanKhususId->getValidationFailures());
                }
            }


            if (($retval = JurusanSpPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId !== null) {
                    foreach ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId !== null) {
                    foreach ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldJurusanSpsRelatedByJurusanSpId !== null) {
                    foreach ($this->collVldJurusanSpsRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldJurusanSpsRelatedByJurusanSpId !== null) {
                    foreach ($this->collVldJurusanSpsRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collJurusanKerjasamasRelatedByJurusanSpId !== null) {
                    foreach ($this->collJurusanKerjasamasRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collJurusanKerjasamasRelatedByJurusanSpId !== null) {
                    foreach ($this->collJurusanKerjasamasRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajarsRelatedByJurusanSpId !== null) {
                    foreach ($this->collRombonganBelajarsRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajarsRelatedByJurusanSpId !== null) {
                    foreach ($this->collRombonganBelajarsRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAkreditasiProdisRelatedByJurusanSpId !== null) {
                    foreach ($this->collAkreditasiProdisRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAkreditasiProdisRelatedByJurusanSpId !== null) {
                    foreach ($this->collAkreditasiProdisRelatedByJurusanSpId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JurusanSpPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getJurusanSpId();
                break;
            case 1:
                return $this->getSekolahId();
                break;
            case 2:
                return $this->getKebutuhanKhususId();
                break;
            case 3:
                return $this->getJurusanId();
                break;
            case 4:
                return $this->getNamaJurusanSp();
                break;
            case 5:
                return $this->getSkIzin();
                break;
            case 6:
                return $this->getTanggalSkIzin();
                break;
            case 7:
                return $this->getLastUpdate();
                break;
            case 8:
                return $this->getSoftDelete();
                break;
            case 9:
                return $this->getLastSync();
                break;
            case 10:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['JurusanSp'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['JurusanSp'][$this->getPrimaryKey()] = true;
        $keys = JurusanSpPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getJurusanSpId(),
            $keys[1] => $this->getSekolahId(),
            $keys[2] => $this->getKebutuhanKhususId(),
            $keys[3] => $this->getJurusanId(),
            $keys[4] => $this->getNamaJurusanSp(),
            $keys[5] => $this->getSkIzin(),
            $keys[6] => $this->getTanggalSkIzin(),
            $keys[7] => $this->getLastUpdate(),
            $keys[8] => $this->getSoftDelete(),
            $keys[9] => $this->getLastSync(),
            $keys[10] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolahRelatedBySekolahId) {
                $result['SekolahRelatedBySekolahId'] = $this->aSekolahRelatedBySekolahId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJurusanRelatedByJurusanId) {
                $result['JurusanRelatedByJurusanId'] = $this->aJurusanRelatedByJurusanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJurusanRelatedByJurusanId) {
                $result['JurusanRelatedByJurusanId'] = $this->aJurusanRelatedByJurusanId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususId'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhususRelatedByKebutuhanKhususId) {
                $result['KebutuhanKhususRelatedByKebutuhanKhususId'] = $this->aKebutuhanKhususRelatedByKebutuhanKhususId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                $result['RegistrasiPesertaDidiksRelatedByJurusanSpId'] = $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                $result['RegistrasiPesertaDidiksRelatedByJurusanSpId'] = $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldJurusanSpsRelatedByJurusanSpId) {
                $result['VldJurusanSpsRelatedByJurusanSpId'] = $this->collVldJurusanSpsRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldJurusanSpsRelatedByJurusanSpId) {
                $result['VldJurusanSpsRelatedByJurusanSpId'] = $this->collVldJurusanSpsRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collJurusanKerjasamasRelatedByJurusanSpId) {
                $result['JurusanKerjasamasRelatedByJurusanSpId'] = $this->collJurusanKerjasamasRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collJurusanKerjasamasRelatedByJurusanSpId) {
                $result['JurusanKerjasamasRelatedByJurusanSpId'] = $this->collJurusanKerjasamasRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajarsRelatedByJurusanSpId) {
                $result['RombonganBelajarsRelatedByJurusanSpId'] = $this->collRombonganBelajarsRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajarsRelatedByJurusanSpId) {
                $result['RombonganBelajarsRelatedByJurusanSpId'] = $this->collRombonganBelajarsRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAkreditasiProdisRelatedByJurusanSpId) {
                $result['AkreditasiProdisRelatedByJurusanSpId'] = $this->collAkreditasiProdisRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAkreditasiProdisRelatedByJurusanSpId) {
                $result['AkreditasiProdisRelatedByJurusanSpId'] = $this->collAkreditasiProdisRelatedByJurusanSpId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = JurusanSpPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setJurusanSpId($value);
                break;
            case 1:
                $this->setSekolahId($value);
                break;
            case 2:
                $this->setKebutuhanKhususId($value);
                break;
            case 3:
                $this->setJurusanId($value);
                break;
            case 4:
                $this->setNamaJurusanSp($value);
                break;
            case 5:
                $this->setSkIzin($value);
                break;
            case 6:
                $this->setTanggalSkIzin($value);
                break;
            case 7:
                $this->setLastUpdate($value);
                break;
            case 8:
                $this->setSoftDelete($value);
                break;
            case 9:
                $this->setLastSync($value);
                break;
            case 10:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = JurusanSpPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setJurusanSpId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSekolahId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setKebutuhanKhususId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setJurusanId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNamaJurusanSp($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSkIzin($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTanggalSkIzin($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLastUpdate($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setSoftDelete($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setLastSync($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUpdaterId($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(JurusanSpPeer::DATABASE_NAME);

        if ($this->isColumnModified(JurusanSpPeer::JURUSAN_SP_ID)) $criteria->add(JurusanSpPeer::JURUSAN_SP_ID, $this->jurusan_sp_id);
        if ($this->isColumnModified(JurusanSpPeer::SEKOLAH_ID)) $criteria->add(JurusanSpPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID)) $criteria->add(JurusanSpPeer::KEBUTUHAN_KHUSUS_ID, $this->kebutuhan_khusus_id);
        if ($this->isColumnModified(JurusanSpPeer::JURUSAN_ID)) $criteria->add(JurusanSpPeer::JURUSAN_ID, $this->jurusan_id);
        if ($this->isColumnModified(JurusanSpPeer::NAMA_JURUSAN_SP)) $criteria->add(JurusanSpPeer::NAMA_JURUSAN_SP, $this->nama_jurusan_sp);
        if ($this->isColumnModified(JurusanSpPeer::SK_IZIN)) $criteria->add(JurusanSpPeer::SK_IZIN, $this->sk_izin);
        if ($this->isColumnModified(JurusanSpPeer::TANGGAL_SK_IZIN)) $criteria->add(JurusanSpPeer::TANGGAL_SK_IZIN, $this->tanggal_sk_izin);
        if ($this->isColumnModified(JurusanSpPeer::LAST_UPDATE)) $criteria->add(JurusanSpPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(JurusanSpPeer::SOFT_DELETE)) $criteria->add(JurusanSpPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(JurusanSpPeer::LAST_SYNC)) $criteria->add(JurusanSpPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(JurusanSpPeer::UPDATER_ID)) $criteria->add(JurusanSpPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(JurusanSpPeer::DATABASE_NAME);
        $criteria->add(JurusanSpPeer::JURUSAN_SP_ID, $this->jurusan_sp_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getJurusanSpId();
    }

    /**
     * Generic method to set the primary key (jurusan_sp_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setJurusanSpId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getJurusanSpId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of JurusanSp (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setKebutuhanKhususId($this->getKebutuhanKhususId());
        $copyObj->setJurusanId($this->getJurusanId());
        $copyObj->setNamaJurusanSp($this->getNamaJurusanSp());
        $copyObj->setSkIzin($this->getSkIzin());
        $copyObj->setTanggalSkIzin($this->getTanggalSkIzin());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getRegistrasiPesertaDidiksRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRegistrasiPesertaDidikRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRegistrasiPesertaDidiksRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRegistrasiPesertaDidikRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldJurusanSpsRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldJurusanSpRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldJurusanSpsRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldJurusanSpRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getJurusanKerjasamasRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addJurusanKerjasamaRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getJurusanKerjasamasRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addJurusanKerjasamaRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajarsRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajarRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajarsRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajarRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAkreditasiProdisRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAkreditasiProdiRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAkreditasiProdisRelatedByJurusanSpId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAkreditasiProdiRelatedByJurusanSpId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setJurusanSpId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return JurusanSp Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return JurusanSpPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new JurusanSpPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return JurusanSp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addJurusanSpRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addJurusanSpsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return JurusanSp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolahRelatedBySekolahId(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolahRelatedBySekolahId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addJurusanSpRelatedBySekolahId($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolahRelatedBySekolahId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolahRelatedBySekolahId === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolahRelatedBySekolahId = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolahRelatedBySekolahId->addJurusanSpsRelatedBySekolahId($this);
             */
        }

        return $this->aSekolahRelatedBySekolahId;
    }

    /**
     * Declares an association between this object and a Jurusan object.
     *
     * @param             Jurusan $v
     * @return JurusanSp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJurusanRelatedByJurusanId(Jurusan $v = null)
    {
        if ($v === null) {
            $this->setJurusanId(NULL);
        } else {
            $this->setJurusanId($v->getJurusanId());
        }

        $this->aJurusanRelatedByJurusanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Jurusan object, it will not be re-added.
        if ($v !== null) {
            $v->addJurusanSpRelatedByJurusanId($this);
        }


        return $this;
    }


    /**
     * Get the associated Jurusan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Jurusan The associated Jurusan object.
     * @throws PropelException
     */
    public function getJurusanRelatedByJurusanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJurusanRelatedByJurusanId === null && (($this->jurusan_id !== "" && $this->jurusan_id !== null)) && $doQuery) {
            $this->aJurusanRelatedByJurusanId = JurusanQuery::create()->findPk($this->jurusan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJurusanRelatedByJurusanId->addJurusanSpsRelatedByJurusanId($this);
             */
        }

        return $this->aJurusanRelatedByJurusanId;
    }

    /**
     * Declares an association between this object and a Jurusan object.
     *
     * @param             Jurusan $v
     * @return JurusanSp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJurusanRelatedByJurusanId(Jurusan $v = null)
    {
        if ($v === null) {
            $this->setJurusanId(NULL);
        } else {
            $this->setJurusanId($v->getJurusanId());
        }

        $this->aJurusanRelatedByJurusanId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Jurusan object, it will not be re-added.
        if ($v !== null) {
            $v->addJurusanSpRelatedByJurusanId($this);
        }


        return $this;
    }


    /**
     * Get the associated Jurusan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Jurusan The associated Jurusan object.
     * @throws PropelException
     */
    public function getJurusanRelatedByJurusanId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJurusanRelatedByJurusanId === null && (($this->jurusan_id !== "" && $this->jurusan_id !== null)) && $doQuery) {
            $this->aJurusanRelatedByJurusanId = JurusanQuery::create()->findPk($this->jurusan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJurusanRelatedByJurusanId->addJurusanSpsRelatedByJurusanId($this);
             */
        }

        return $this->aJurusanRelatedByJurusanId;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return JurusanSp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususId(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususId(NULL);
        } else {
            $this->setKebutuhanKhususId($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addJurusanSpRelatedByKebutuhanKhususId($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId === null && ($this->kebutuhan_khusus_id !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususId->addJurusanSpsRelatedByKebutuhanKhususId($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususId;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return JurusanSp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhususRelatedByKebutuhanKhususId(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususId(NULL);
        } else {
            $this->setKebutuhanKhususId($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addJurusanSpRelatedByKebutuhanKhususId($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhususRelatedByKebutuhanKhususId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId === null && ($this->kebutuhan_khusus_id !== null) && $doQuery) {
            $this->aKebutuhanKhususRelatedByKebutuhanKhususId = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhususRelatedByKebutuhanKhususId->addJurusanSpsRelatedByKebutuhanKhususId($this);
             */
        }

        return $this->aKebutuhanKhususRelatedByKebutuhanKhususId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RegistrasiPesertaDidikRelatedByJurusanSpId' == $relationName) {
            $this->initRegistrasiPesertaDidiksRelatedByJurusanSpId();
        }
        if ('RegistrasiPesertaDidikRelatedByJurusanSpId' == $relationName) {
            $this->initRegistrasiPesertaDidiksRelatedByJurusanSpId();
        }
        if ('VldJurusanSpRelatedByJurusanSpId' == $relationName) {
            $this->initVldJurusanSpsRelatedByJurusanSpId();
        }
        if ('VldJurusanSpRelatedByJurusanSpId' == $relationName) {
            $this->initVldJurusanSpsRelatedByJurusanSpId();
        }
        if ('JurusanKerjasamaRelatedByJurusanSpId' == $relationName) {
            $this->initJurusanKerjasamasRelatedByJurusanSpId();
        }
        if ('JurusanKerjasamaRelatedByJurusanSpId' == $relationName) {
            $this->initJurusanKerjasamasRelatedByJurusanSpId();
        }
        if ('RombonganBelajarRelatedByJurusanSpId' == $relationName) {
            $this->initRombonganBelajarsRelatedByJurusanSpId();
        }
        if ('RombonganBelajarRelatedByJurusanSpId' == $relationName) {
            $this->initRombonganBelajarsRelatedByJurusanSpId();
        }
        if ('AkreditasiProdiRelatedByJurusanSpId' == $relationName) {
            $this->initAkreditasiProdisRelatedByJurusanSpId();
        }
        if ('AkreditasiProdiRelatedByJurusanSpId' == $relationName) {
            $this->initAkreditasiProdisRelatedByJurusanSpId();
        }
    }

    /**
     * Clears out the collRegistrasiPesertaDidiksRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addRegistrasiPesertaDidiksRelatedByJurusanSpId()
     */
    public function clearRegistrasiPesertaDidiksRelatedByJurusanSpId()
    {
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRegistrasiPesertaDidiksRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRegistrasiPesertaDidiksRelatedByJurusanSpId($v = true)
    {
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collRegistrasiPesertaDidiksRelatedByJurusanSpId collection.
     *
     * By default this just sets the collRegistrasiPesertaDidiksRelatedByJurusanSpId collection to an empty array (like clearcollRegistrasiPesertaDidiksRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRegistrasiPesertaDidiksRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->setModel('RegistrasiPesertaDidik');
    }

    /**
     * Gets an array of RegistrasiPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     * @throws PropelException
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                // return empty collection
                $this->initRegistrasiPesertaDidiksRelatedByJurusanSpId();
            } else {
                $collRegistrasiPesertaDidiksRelatedByJurusanSpId = RegistrasiPesertaDidikQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial && count($collRegistrasiPesertaDidiksRelatedByJurusanSpId)) {
                      $this->initRegistrasiPesertaDidiksRelatedByJurusanSpId(false);

                      foreach($collRegistrasiPesertaDidiksRelatedByJurusanSpId as $obj) {
                        if (false == $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->contains($obj)) {
                          $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = true;
                    }

                    $collRegistrasiPesertaDidiksRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collRegistrasiPesertaDidiksRelatedByJurusanSpId;
                }

                if($partial && $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                    foreach($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collRegistrasiPesertaDidiksRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = $collRegistrasiPesertaDidiksRelatedByJurusanSpId;
                $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of RegistrasiPesertaDidikRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $registrasiPesertaDidiksRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setRegistrasiPesertaDidiksRelatedByJurusanSpId(PropelCollection $registrasiPesertaDidiksRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $registrasiPesertaDidiksRelatedByJurusanSpIdToDelete = $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId(new Criteria(), $con)->diff($registrasiPesertaDidiksRelatedByJurusanSpId);

        $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($registrasiPesertaDidiksRelatedByJurusanSpIdToDelete));

        foreach ($registrasiPesertaDidiksRelatedByJurusanSpIdToDelete as $registrasiPesertaDidikRelatedByJurusanSpIdRemoved) {
            $registrasiPesertaDidikRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = null;
        foreach ($registrasiPesertaDidiksRelatedByJurusanSpId as $registrasiPesertaDidikRelatedByJurusanSpId) {
            $this->addRegistrasiPesertaDidikRelatedByJurusanSpId($registrasiPesertaDidikRelatedByJurusanSpId);
        }

        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = $registrasiPesertaDidiksRelatedByJurusanSpId;
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RegistrasiPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RegistrasiPesertaDidik objects.
     * @throws PropelException
     */
    public function countRegistrasiPesertaDidiksRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRegistrasiPesertaDidiksRelatedByJurusanSpId());
            }
            $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a RegistrasiPesertaDidik object to this object
     * through the RegistrasiPesertaDidik foreign key attribute.
     *
     * @param    RegistrasiPesertaDidik $l RegistrasiPesertaDidik
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addRegistrasiPesertaDidikRelatedByJurusanSpId(RegistrasiPesertaDidik $l)
    {
        if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId === null) {
            $this->initRegistrasiPesertaDidiksRelatedByJurusanSpId();
            $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRegistrasiPesertaDidikRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByJurusanSpId $registrasiPesertaDidikRelatedByJurusanSpId The registrasiPesertaDidikRelatedByJurusanSpId object to add.
     */
    protected function doAddRegistrasiPesertaDidikRelatedByJurusanSpId($registrasiPesertaDidikRelatedByJurusanSpId)
    {
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId[]= $registrasiPesertaDidikRelatedByJurusanSpId;
        $registrasiPesertaDidikRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByJurusanSpId $registrasiPesertaDidikRelatedByJurusanSpId The registrasiPesertaDidikRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeRegistrasiPesertaDidikRelatedByJurusanSpId($registrasiPesertaDidikRelatedByJurusanSpId)
    {
        if ($this->getRegistrasiPesertaDidiksRelatedByJurusanSpId()->contains($registrasiPesertaDidikRelatedByJurusanSpId)) {
            $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->remove($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->search($registrasiPesertaDidikRelatedByJurusanSpId));
            if (null === $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion) {
                $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion = clone $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId;
                $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion[]= $registrasiPesertaDidikRelatedByJurusanSpId;
            $registrasiPesertaDidikRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collRegistrasiPesertaDidiksRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addRegistrasiPesertaDidiksRelatedByJurusanSpId()
     */
    public function clearRegistrasiPesertaDidiksRelatedByJurusanSpId()
    {
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRegistrasiPesertaDidiksRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRegistrasiPesertaDidiksRelatedByJurusanSpId($v = true)
    {
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collRegistrasiPesertaDidiksRelatedByJurusanSpId collection.
     *
     * By default this just sets the collRegistrasiPesertaDidiksRelatedByJurusanSpId collection to an empty array (like clearcollRegistrasiPesertaDidiksRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRegistrasiPesertaDidiksRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->setModel('RegistrasiPesertaDidik');
    }

    /**
     * Gets an array of RegistrasiPesertaDidik objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     * @throws PropelException
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                // return empty collection
                $this->initRegistrasiPesertaDidiksRelatedByJurusanSpId();
            } else {
                $collRegistrasiPesertaDidiksRelatedByJurusanSpId = RegistrasiPesertaDidikQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial && count($collRegistrasiPesertaDidiksRelatedByJurusanSpId)) {
                      $this->initRegistrasiPesertaDidiksRelatedByJurusanSpId(false);

                      foreach($collRegistrasiPesertaDidiksRelatedByJurusanSpId as $obj) {
                        if (false == $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->contains($obj)) {
                          $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = true;
                    }

                    $collRegistrasiPesertaDidiksRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collRegistrasiPesertaDidiksRelatedByJurusanSpId;
                }

                if($partial && $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                    foreach($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collRegistrasiPesertaDidiksRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = $collRegistrasiPesertaDidiksRelatedByJurusanSpId;
                $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of RegistrasiPesertaDidikRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $registrasiPesertaDidiksRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setRegistrasiPesertaDidiksRelatedByJurusanSpId(PropelCollection $registrasiPesertaDidiksRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $registrasiPesertaDidiksRelatedByJurusanSpIdToDelete = $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId(new Criteria(), $con)->diff($registrasiPesertaDidiksRelatedByJurusanSpId);

        $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($registrasiPesertaDidiksRelatedByJurusanSpIdToDelete));

        foreach ($registrasiPesertaDidiksRelatedByJurusanSpIdToDelete as $registrasiPesertaDidikRelatedByJurusanSpIdRemoved) {
            $registrasiPesertaDidikRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = null;
        foreach ($registrasiPesertaDidiksRelatedByJurusanSpId as $registrasiPesertaDidikRelatedByJurusanSpId) {
            $this->addRegistrasiPesertaDidikRelatedByJurusanSpId($registrasiPesertaDidikRelatedByJurusanSpId);
        }

        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = $registrasiPesertaDidiksRelatedByJurusanSpId;
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RegistrasiPesertaDidik objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RegistrasiPesertaDidik objects.
     * @throws PropelException
     */
    public function countRegistrasiPesertaDidiksRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRegistrasiPesertaDidiksRelatedByJurusanSpId());
            }
            $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a RegistrasiPesertaDidik object to this object
     * through the RegistrasiPesertaDidik foreign key attribute.
     *
     * @param    RegistrasiPesertaDidik $l RegistrasiPesertaDidik
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addRegistrasiPesertaDidikRelatedByJurusanSpId(RegistrasiPesertaDidik $l)
    {
        if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId === null) {
            $this->initRegistrasiPesertaDidiksRelatedByJurusanSpId();
            $this->collRegistrasiPesertaDidiksRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRegistrasiPesertaDidikRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByJurusanSpId $registrasiPesertaDidikRelatedByJurusanSpId The registrasiPesertaDidikRelatedByJurusanSpId object to add.
     */
    protected function doAddRegistrasiPesertaDidikRelatedByJurusanSpId($registrasiPesertaDidikRelatedByJurusanSpId)
    {
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId[]= $registrasiPesertaDidikRelatedByJurusanSpId;
        $registrasiPesertaDidikRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	RegistrasiPesertaDidikRelatedByJurusanSpId $registrasiPesertaDidikRelatedByJurusanSpId The registrasiPesertaDidikRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeRegistrasiPesertaDidikRelatedByJurusanSpId($registrasiPesertaDidikRelatedByJurusanSpId)
    {
        if ($this->getRegistrasiPesertaDidiksRelatedByJurusanSpId()->contains($registrasiPesertaDidikRelatedByJurusanSpId)) {
            $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->remove($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->search($registrasiPesertaDidikRelatedByJurusanSpId));
            if (null === $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion) {
                $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion = clone $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId;
                $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->registrasiPesertaDidiksRelatedByJurusanSpIdScheduledForDeletion[]= $registrasiPesertaDidikRelatedByJurusanSpId;
            $registrasiPesertaDidikRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinPesertaDidikRelatedByPesertaDidikId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('PesertaDidikRelatedByPesertaDidikId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinJenisKeluarRelatedByJenisKeluarId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisKeluarRelatedByJenisKeluarId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RegistrasiPesertaDidiksRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RegistrasiPesertaDidik[] List of RegistrasiPesertaDidik objects
     */
    public function getRegistrasiPesertaDidiksRelatedByJurusanSpIdJoinJenisPendaftaranRelatedByJenisPendaftaranId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RegistrasiPesertaDidikQuery::create(null, $criteria);
        $query->joinWith('JenisPendaftaranRelatedByJenisPendaftaranId', $join_behavior);

        return $this->getRegistrasiPesertaDidiksRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collVldJurusanSpsRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addVldJurusanSpsRelatedByJurusanSpId()
     */
    public function clearVldJurusanSpsRelatedByJurusanSpId()
    {
        $this->collVldJurusanSpsRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldJurusanSpsRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldJurusanSpsRelatedByJurusanSpId($v = true)
    {
        $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collVldJurusanSpsRelatedByJurusanSpId collection.
     *
     * By default this just sets the collVldJurusanSpsRelatedByJurusanSpId collection to an empty array (like clearcollVldJurusanSpsRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldJurusanSpsRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collVldJurusanSpsRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collVldJurusanSpsRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collVldJurusanSpsRelatedByJurusanSpId->setModel('VldJurusanSp');
    }

    /**
     * Gets an array of VldJurusanSp objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     * @throws PropelException
     */
    public function getVldJurusanSpsRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldJurusanSpsRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collVldJurusanSpsRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldJurusanSpsRelatedByJurusanSpId) {
                // return empty collection
                $this->initVldJurusanSpsRelatedByJurusanSpId();
            } else {
                $collVldJurusanSpsRelatedByJurusanSpId = VldJurusanSpQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldJurusanSpsRelatedByJurusanSpIdPartial && count($collVldJurusanSpsRelatedByJurusanSpId)) {
                      $this->initVldJurusanSpsRelatedByJurusanSpId(false);

                      foreach($collVldJurusanSpsRelatedByJurusanSpId as $obj) {
                        if (false == $this->collVldJurusanSpsRelatedByJurusanSpId->contains($obj)) {
                          $this->collVldJurusanSpsRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = true;
                    }

                    $collVldJurusanSpsRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collVldJurusanSpsRelatedByJurusanSpId;
                }

                if($partial && $this->collVldJurusanSpsRelatedByJurusanSpId) {
                    foreach($this->collVldJurusanSpsRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collVldJurusanSpsRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collVldJurusanSpsRelatedByJurusanSpId = $collVldJurusanSpsRelatedByJurusanSpId;
                $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collVldJurusanSpsRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of VldJurusanSpRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldJurusanSpsRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setVldJurusanSpsRelatedByJurusanSpId(PropelCollection $vldJurusanSpsRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $vldJurusanSpsRelatedByJurusanSpIdToDelete = $this->getVldJurusanSpsRelatedByJurusanSpId(new Criteria(), $con)->diff($vldJurusanSpsRelatedByJurusanSpId);

        $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($vldJurusanSpsRelatedByJurusanSpIdToDelete));

        foreach ($vldJurusanSpsRelatedByJurusanSpIdToDelete as $vldJurusanSpRelatedByJurusanSpIdRemoved) {
            $vldJurusanSpRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collVldJurusanSpsRelatedByJurusanSpId = null;
        foreach ($vldJurusanSpsRelatedByJurusanSpId as $vldJurusanSpRelatedByJurusanSpId) {
            $this->addVldJurusanSpRelatedByJurusanSpId($vldJurusanSpRelatedByJurusanSpId);
        }

        $this->collVldJurusanSpsRelatedByJurusanSpId = $vldJurusanSpsRelatedByJurusanSpId;
        $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldJurusanSp objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldJurusanSp objects.
     * @throws PropelException
     */
    public function countVldJurusanSpsRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldJurusanSpsRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collVldJurusanSpsRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldJurusanSpsRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldJurusanSpsRelatedByJurusanSpId());
            }
            $query = VldJurusanSpQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collVldJurusanSpsRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a VldJurusanSp object to this object
     * through the VldJurusanSp foreign key attribute.
     *
     * @param    VldJurusanSp $l VldJurusanSp
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addVldJurusanSpRelatedByJurusanSpId(VldJurusanSp $l)
    {
        if ($this->collVldJurusanSpsRelatedByJurusanSpId === null) {
            $this->initVldJurusanSpsRelatedByJurusanSpId();
            $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collVldJurusanSpsRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldJurusanSpRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	VldJurusanSpRelatedByJurusanSpId $vldJurusanSpRelatedByJurusanSpId The vldJurusanSpRelatedByJurusanSpId object to add.
     */
    protected function doAddVldJurusanSpRelatedByJurusanSpId($vldJurusanSpRelatedByJurusanSpId)
    {
        $this->collVldJurusanSpsRelatedByJurusanSpId[]= $vldJurusanSpRelatedByJurusanSpId;
        $vldJurusanSpRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	VldJurusanSpRelatedByJurusanSpId $vldJurusanSpRelatedByJurusanSpId The vldJurusanSpRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeVldJurusanSpRelatedByJurusanSpId($vldJurusanSpRelatedByJurusanSpId)
    {
        if ($this->getVldJurusanSpsRelatedByJurusanSpId()->contains($vldJurusanSpRelatedByJurusanSpId)) {
            $this->collVldJurusanSpsRelatedByJurusanSpId->remove($this->collVldJurusanSpsRelatedByJurusanSpId->search($vldJurusanSpRelatedByJurusanSpId));
            if (null === $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion) {
                $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion = clone $this->collVldJurusanSpsRelatedByJurusanSpId;
                $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion[]= clone $vldJurusanSpRelatedByJurusanSpId;
            $vldJurusanSpRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related VldJurusanSpsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     */
    public function getVldJurusanSpsRelatedByJurusanSpIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldJurusanSpQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldJurusanSpsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related VldJurusanSpsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     */
    public function getVldJurusanSpsRelatedByJurusanSpIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldJurusanSpQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldJurusanSpsRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collVldJurusanSpsRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addVldJurusanSpsRelatedByJurusanSpId()
     */
    public function clearVldJurusanSpsRelatedByJurusanSpId()
    {
        $this->collVldJurusanSpsRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collVldJurusanSpsRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldJurusanSpsRelatedByJurusanSpId($v = true)
    {
        $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collVldJurusanSpsRelatedByJurusanSpId collection.
     *
     * By default this just sets the collVldJurusanSpsRelatedByJurusanSpId collection to an empty array (like clearcollVldJurusanSpsRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldJurusanSpsRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collVldJurusanSpsRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collVldJurusanSpsRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collVldJurusanSpsRelatedByJurusanSpId->setModel('VldJurusanSp');
    }

    /**
     * Gets an array of VldJurusanSp objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     * @throws PropelException
     */
    public function getVldJurusanSpsRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldJurusanSpsRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collVldJurusanSpsRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldJurusanSpsRelatedByJurusanSpId) {
                // return empty collection
                $this->initVldJurusanSpsRelatedByJurusanSpId();
            } else {
                $collVldJurusanSpsRelatedByJurusanSpId = VldJurusanSpQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldJurusanSpsRelatedByJurusanSpIdPartial && count($collVldJurusanSpsRelatedByJurusanSpId)) {
                      $this->initVldJurusanSpsRelatedByJurusanSpId(false);

                      foreach($collVldJurusanSpsRelatedByJurusanSpId as $obj) {
                        if (false == $this->collVldJurusanSpsRelatedByJurusanSpId->contains($obj)) {
                          $this->collVldJurusanSpsRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = true;
                    }

                    $collVldJurusanSpsRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collVldJurusanSpsRelatedByJurusanSpId;
                }

                if($partial && $this->collVldJurusanSpsRelatedByJurusanSpId) {
                    foreach($this->collVldJurusanSpsRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collVldJurusanSpsRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collVldJurusanSpsRelatedByJurusanSpId = $collVldJurusanSpsRelatedByJurusanSpId;
                $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collVldJurusanSpsRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of VldJurusanSpRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldJurusanSpsRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setVldJurusanSpsRelatedByJurusanSpId(PropelCollection $vldJurusanSpsRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $vldJurusanSpsRelatedByJurusanSpIdToDelete = $this->getVldJurusanSpsRelatedByJurusanSpId(new Criteria(), $con)->diff($vldJurusanSpsRelatedByJurusanSpId);

        $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($vldJurusanSpsRelatedByJurusanSpIdToDelete));

        foreach ($vldJurusanSpsRelatedByJurusanSpIdToDelete as $vldJurusanSpRelatedByJurusanSpIdRemoved) {
            $vldJurusanSpRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collVldJurusanSpsRelatedByJurusanSpId = null;
        foreach ($vldJurusanSpsRelatedByJurusanSpId as $vldJurusanSpRelatedByJurusanSpId) {
            $this->addVldJurusanSpRelatedByJurusanSpId($vldJurusanSpRelatedByJurusanSpId);
        }

        $this->collVldJurusanSpsRelatedByJurusanSpId = $vldJurusanSpsRelatedByJurusanSpId;
        $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldJurusanSp objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldJurusanSp objects.
     * @throws PropelException
     */
    public function countVldJurusanSpsRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldJurusanSpsRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collVldJurusanSpsRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldJurusanSpsRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldJurusanSpsRelatedByJurusanSpId());
            }
            $query = VldJurusanSpQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collVldJurusanSpsRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a VldJurusanSp object to this object
     * through the VldJurusanSp foreign key attribute.
     *
     * @param    VldJurusanSp $l VldJurusanSp
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addVldJurusanSpRelatedByJurusanSpId(VldJurusanSp $l)
    {
        if ($this->collVldJurusanSpsRelatedByJurusanSpId === null) {
            $this->initVldJurusanSpsRelatedByJurusanSpId();
            $this->collVldJurusanSpsRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collVldJurusanSpsRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldJurusanSpRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	VldJurusanSpRelatedByJurusanSpId $vldJurusanSpRelatedByJurusanSpId The vldJurusanSpRelatedByJurusanSpId object to add.
     */
    protected function doAddVldJurusanSpRelatedByJurusanSpId($vldJurusanSpRelatedByJurusanSpId)
    {
        $this->collVldJurusanSpsRelatedByJurusanSpId[]= $vldJurusanSpRelatedByJurusanSpId;
        $vldJurusanSpRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	VldJurusanSpRelatedByJurusanSpId $vldJurusanSpRelatedByJurusanSpId The vldJurusanSpRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeVldJurusanSpRelatedByJurusanSpId($vldJurusanSpRelatedByJurusanSpId)
    {
        if ($this->getVldJurusanSpsRelatedByJurusanSpId()->contains($vldJurusanSpRelatedByJurusanSpId)) {
            $this->collVldJurusanSpsRelatedByJurusanSpId->remove($this->collVldJurusanSpsRelatedByJurusanSpId->search($vldJurusanSpRelatedByJurusanSpId));
            if (null === $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion) {
                $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion = clone $this->collVldJurusanSpsRelatedByJurusanSpId;
                $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->vldJurusanSpsRelatedByJurusanSpIdScheduledForDeletion[]= clone $vldJurusanSpRelatedByJurusanSpId;
            $vldJurusanSpRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related VldJurusanSpsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     */
    public function getVldJurusanSpsRelatedByJurusanSpIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldJurusanSpQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldJurusanSpsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related VldJurusanSpsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldJurusanSp[] List of VldJurusanSp objects
     */
    public function getVldJurusanSpsRelatedByJurusanSpIdJoinErrortypeRelatedByIdtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldJurusanSpQuery::create(null, $criteria);
        $query->joinWith('ErrortypeRelatedByIdtype', $join_behavior);

        return $this->getVldJurusanSpsRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collJurusanKerjasamasRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addJurusanKerjasamasRelatedByJurusanSpId()
     */
    public function clearJurusanKerjasamasRelatedByJurusanSpId()
    {
        $this->collJurusanKerjasamasRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collJurusanKerjasamasRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialJurusanKerjasamasRelatedByJurusanSpId($v = true)
    {
        $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collJurusanKerjasamasRelatedByJurusanSpId collection.
     *
     * By default this just sets the collJurusanKerjasamasRelatedByJurusanSpId collection to an empty array (like clearcollJurusanKerjasamasRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initJurusanKerjasamasRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collJurusanKerjasamasRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collJurusanKerjasamasRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collJurusanKerjasamasRelatedByJurusanSpId->setModel('JurusanKerjasama');
    }

    /**
     * Gets an array of JurusanKerjasama objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     * @throws PropelException
     */
    public function getJurusanKerjasamasRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collJurusanKerjasamasRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collJurusanKerjasamasRelatedByJurusanSpId) {
                // return empty collection
                $this->initJurusanKerjasamasRelatedByJurusanSpId();
            } else {
                $collJurusanKerjasamasRelatedByJurusanSpId = JurusanKerjasamaQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial && count($collJurusanKerjasamasRelatedByJurusanSpId)) {
                      $this->initJurusanKerjasamasRelatedByJurusanSpId(false);

                      foreach($collJurusanKerjasamasRelatedByJurusanSpId as $obj) {
                        if (false == $this->collJurusanKerjasamasRelatedByJurusanSpId->contains($obj)) {
                          $this->collJurusanKerjasamasRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = true;
                    }

                    $collJurusanKerjasamasRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collJurusanKerjasamasRelatedByJurusanSpId;
                }

                if($partial && $this->collJurusanKerjasamasRelatedByJurusanSpId) {
                    foreach($this->collJurusanKerjasamasRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collJurusanKerjasamasRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collJurusanKerjasamasRelatedByJurusanSpId = $collJurusanKerjasamasRelatedByJurusanSpId;
                $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collJurusanKerjasamasRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of JurusanKerjasamaRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $jurusanKerjasamasRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setJurusanKerjasamasRelatedByJurusanSpId(PropelCollection $jurusanKerjasamasRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $jurusanKerjasamasRelatedByJurusanSpIdToDelete = $this->getJurusanKerjasamasRelatedByJurusanSpId(new Criteria(), $con)->diff($jurusanKerjasamasRelatedByJurusanSpId);

        $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($jurusanKerjasamasRelatedByJurusanSpIdToDelete));

        foreach ($jurusanKerjasamasRelatedByJurusanSpIdToDelete as $jurusanKerjasamaRelatedByJurusanSpIdRemoved) {
            $jurusanKerjasamaRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collJurusanKerjasamasRelatedByJurusanSpId = null;
        foreach ($jurusanKerjasamasRelatedByJurusanSpId as $jurusanKerjasamaRelatedByJurusanSpId) {
            $this->addJurusanKerjasamaRelatedByJurusanSpId($jurusanKerjasamaRelatedByJurusanSpId);
        }

        $this->collJurusanKerjasamasRelatedByJurusanSpId = $jurusanKerjasamasRelatedByJurusanSpId;
        $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related JurusanKerjasama objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related JurusanKerjasama objects.
     * @throws PropelException
     */
    public function countJurusanKerjasamasRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collJurusanKerjasamasRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collJurusanKerjasamasRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getJurusanKerjasamasRelatedByJurusanSpId());
            }
            $query = JurusanKerjasamaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collJurusanKerjasamasRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a JurusanKerjasama object to this object
     * through the JurusanKerjasama foreign key attribute.
     *
     * @param    JurusanKerjasama $l JurusanKerjasama
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addJurusanKerjasamaRelatedByJurusanSpId(JurusanKerjasama $l)
    {
        if ($this->collJurusanKerjasamasRelatedByJurusanSpId === null) {
            $this->initJurusanKerjasamasRelatedByJurusanSpId();
            $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collJurusanKerjasamasRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddJurusanKerjasamaRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	JurusanKerjasamaRelatedByJurusanSpId $jurusanKerjasamaRelatedByJurusanSpId The jurusanKerjasamaRelatedByJurusanSpId object to add.
     */
    protected function doAddJurusanKerjasamaRelatedByJurusanSpId($jurusanKerjasamaRelatedByJurusanSpId)
    {
        $this->collJurusanKerjasamasRelatedByJurusanSpId[]= $jurusanKerjasamaRelatedByJurusanSpId;
        $jurusanKerjasamaRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	JurusanKerjasamaRelatedByJurusanSpId $jurusanKerjasamaRelatedByJurusanSpId The jurusanKerjasamaRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeJurusanKerjasamaRelatedByJurusanSpId($jurusanKerjasamaRelatedByJurusanSpId)
    {
        if ($this->getJurusanKerjasamasRelatedByJurusanSpId()->contains($jurusanKerjasamaRelatedByJurusanSpId)) {
            $this->collJurusanKerjasamasRelatedByJurusanSpId->remove($this->collJurusanKerjasamasRelatedByJurusanSpId->search($jurusanKerjasamaRelatedByJurusanSpId));
            if (null === $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion) {
                $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion = clone $this->collJurusanKerjasamasRelatedByJurusanSpId;
                $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion[]= clone $jurusanKerjasamaRelatedByJurusanSpId;
            $jurusanKerjasamaRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related JurusanKerjasamasRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     */
    public function getJurusanKerjasamasRelatedByJurusanSpIdJoinMouRelatedByMouId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JurusanKerjasamaQuery::create(null, $criteria);
        $query->joinWith('MouRelatedByMouId', $join_behavior);

        return $this->getJurusanKerjasamasRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related JurusanKerjasamasRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     */
    public function getJurusanKerjasamasRelatedByJurusanSpIdJoinMouRelatedByMouId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JurusanKerjasamaQuery::create(null, $criteria);
        $query->joinWith('MouRelatedByMouId', $join_behavior);

        return $this->getJurusanKerjasamasRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collJurusanKerjasamasRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addJurusanKerjasamasRelatedByJurusanSpId()
     */
    public function clearJurusanKerjasamasRelatedByJurusanSpId()
    {
        $this->collJurusanKerjasamasRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collJurusanKerjasamasRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialJurusanKerjasamasRelatedByJurusanSpId($v = true)
    {
        $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collJurusanKerjasamasRelatedByJurusanSpId collection.
     *
     * By default this just sets the collJurusanKerjasamasRelatedByJurusanSpId collection to an empty array (like clearcollJurusanKerjasamasRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initJurusanKerjasamasRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collJurusanKerjasamasRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collJurusanKerjasamasRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collJurusanKerjasamasRelatedByJurusanSpId->setModel('JurusanKerjasama');
    }

    /**
     * Gets an array of JurusanKerjasama objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     * @throws PropelException
     */
    public function getJurusanKerjasamasRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collJurusanKerjasamasRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collJurusanKerjasamasRelatedByJurusanSpId) {
                // return empty collection
                $this->initJurusanKerjasamasRelatedByJurusanSpId();
            } else {
                $collJurusanKerjasamasRelatedByJurusanSpId = JurusanKerjasamaQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial && count($collJurusanKerjasamasRelatedByJurusanSpId)) {
                      $this->initJurusanKerjasamasRelatedByJurusanSpId(false);

                      foreach($collJurusanKerjasamasRelatedByJurusanSpId as $obj) {
                        if (false == $this->collJurusanKerjasamasRelatedByJurusanSpId->contains($obj)) {
                          $this->collJurusanKerjasamasRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = true;
                    }

                    $collJurusanKerjasamasRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collJurusanKerjasamasRelatedByJurusanSpId;
                }

                if($partial && $this->collJurusanKerjasamasRelatedByJurusanSpId) {
                    foreach($this->collJurusanKerjasamasRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collJurusanKerjasamasRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collJurusanKerjasamasRelatedByJurusanSpId = $collJurusanKerjasamasRelatedByJurusanSpId;
                $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collJurusanKerjasamasRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of JurusanKerjasamaRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $jurusanKerjasamasRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setJurusanKerjasamasRelatedByJurusanSpId(PropelCollection $jurusanKerjasamasRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $jurusanKerjasamasRelatedByJurusanSpIdToDelete = $this->getJurusanKerjasamasRelatedByJurusanSpId(new Criteria(), $con)->diff($jurusanKerjasamasRelatedByJurusanSpId);

        $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($jurusanKerjasamasRelatedByJurusanSpIdToDelete));

        foreach ($jurusanKerjasamasRelatedByJurusanSpIdToDelete as $jurusanKerjasamaRelatedByJurusanSpIdRemoved) {
            $jurusanKerjasamaRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collJurusanKerjasamasRelatedByJurusanSpId = null;
        foreach ($jurusanKerjasamasRelatedByJurusanSpId as $jurusanKerjasamaRelatedByJurusanSpId) {
            $this->addJurusanKerjasamaRelatedByJurusanSpId($jurusanKerjasamaRelatedByJurusanSpId);
        }

        $this->collJurusanKerjasamasRelatedByJurusanSpId = $jurusanKerjasamasRelatedByJurusanSpId;
        $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related JurusanKerjasama objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related JurusanKerjasama objects.
     * @throws PropelException
     */
    public function countJurusanKerjasamasRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collJurusanKerjasamasRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collJurusanKerjasamasRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getJurusanKerjasamasRelatedByJurusanSpId());
            }
            $query = JurusanKerjasamaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collJurusanKerjasamasRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a JurusanKerjasama object to this object
     * through the JurusanKerjasama foreign key attribute.
     *
     * @param    JurusanKerjasama $l JurusanKerjasama
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addJurusanKerjasamaRelatedByJurusanSpId(JurusanKerjasama $l)
    {
        if ($this->collJurusanKerjasamasRelatedByJurusanSpId === null) {
            $this->initJurusanKerjasamasRelatedByJurusanSpId();
            $this->collJurusanKerjasamasRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collJurusanKerjasamasRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddJurusanKerjasamaRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	JurusanKerjasamaRelatedByJurusanSpId $jurusanKerjasamaRelatedByJurusanSpId The jurusanKerjasamaRelatedByJurusanSpId object to add.
     */
    protected function doAddJurusanKerjasamaRelatedByJurusanSpId($jurusanKerjasamaRelatedByJurusanSpId)
    {
        $this->collJurusanKerjasamasRelatedByJurusanSpId[]= $jurusanKerjasamaRelatedByJurusanSpId;
        $jurusanKerjasamaRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	JurusanKerjasamaRelatedByJurusanSpId $jurusanKerjasamaRelatedByJurusanSpId The jurusanKerjasamaRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeJurusanKerjasamaRelatedByJurusanSpId($jurusanKerjasamaRelatedByJurusanSpId)
    {
        if ($this->getJurusanKerjasamasRelatedByJurusanSpId()->contains($jurusanKerjasamaRelatedByJurusanSpId)) {
            $this->collJurusanKerjasamasRelatedByJurusanSpId->remove($this->collJurusanKerjasamasRelatedByJurusanSpId->search($jurusanKerjasamaRelatedByJurusanSpId));
            if (null === $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion) {
                $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion = clone $this->collJurusanKerjasamasRelatedByJurusanSpId;
                $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->jurusanKerjasamasRelatedByJurusanSpIdScheduledForDeletion[]= clone $jurusanKerjasamaRelatedByJurusanSpId;
            $jurusanKerjasamaRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related JurusanKerjasamasRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     */
    public function getJurusanKerjasamasRelatedByJurusanSpIdJoinMouRelatedByMouId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JurusanKerjasamaQuery::create(null, $criteria);
        $query->joinWith('MouRelatedByMouId', $join_behavior);

        return $this->getJurusanKerjasamasRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related JurusanKerjasamasRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|JurusanKerjasama[] List of JurusanKerjasama objects
     */
    public function getJurusanKerjasamasRelatedByJurusanSpIdJoinMouRelatedByMouId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JurusanKerjasamaQuery::create(null, $criteria);
        $query->joinWith('MouRelatedByMouId', $join_behavior);

        return $this->getJurusanKerjasamasRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collRombonganBelajarsRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addRombonganBelajarsRelatedByJurusanSpId()
     */
    public function clearRombonganBelajarsRelatedByJurusanSpId()
    {
        $this->collRombonganBelajarsRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajarsRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajarsRelatedByJurusanSpId($v = true)
    {
        $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajarsRelatedByJurusanSpId collection.
     *
     * By default this just sets the collRombonganBelajarsRelatedByJurusanSpId collection to an empty array (like clearcollRombonganBelajarsRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajarsRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajarsRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajarsRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collRombonganBelajarsRelatedByJurusanSpId->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajarsRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByJurusanSpId) {
                // return empty collection
                $this->initRombonganBelajarsRelatedByJurusanSpId();
            } else {
                $collRombonganBelajarsRelatedByJurusanSpId = RombonganBelajarQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsRelatedByJurusanSpIdPartial && count($collRombonganBelajarsRelatedByJurusanSpId)) {
                      $this->initRombonganBelajarsRelatedByJurusanSpId(false);

                      foreach($collRombonganBelajarsRelatedByJurusanSpId as $obj) {
                        if (false == $this->collRombonganBelajarsRelatedByJurusanSpId->contains($obj)) {
                          $this->collRombonganBelajarsRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = true;
                    }

                    $collRombonganBelajarsRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collRombonganBelajarsRelatedByJurusanSpId;
                }

                if($partial && $this->collRombonganBelajarsRelatedByJurusanSpId) {
                    foreach($this->collRombonganBelajarsRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajarsRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajarsRelatedByJurusanSpId = $collRombonganBelajarsRelatedByJurusanSpId;
                $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collRombonganBelajarsRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of RombonganBelajarRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajarsRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setRombonganBelajarsRelatedByJurusanSpId(PropelCollection $rombonganBelajarsRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $rombonganBelajarsRelatedByJurusanSpIdToDelete = $this->getRombonganBelajarsRelatedByJurusanSpId(new Criteria(), $con)->diff($rombonganBelajarsRelatedByJurusanSpId);

        $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($rombonganBelajarsRelatedByJurusanSpIdToDelete));

        foreach ($rombonganBelajarsRelatedByJurusanSpIdToDelete as $rombonganBelajarRelatedByJurusanSpIdRemoved) {
            $rombonganBelajarRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collRombonganBelajarsRelatedByJurusanSpId = null;
        foreach ($rombonganBelajarsRelatedByJurusanSpId as $rombonganBelajarRelatedByJurusanSpId) {
            $this->addRombonganBelajarRelatedByJurusanSpId($rombonganBelajarRelatedByJurusanSpId);
        }

        $this->collRombonganBelajarsRelatedByJurusanSpId = $rombonganBelajarsRelatedByJurusanSpId;
        $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajarsRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajarsRelatedByJurusanSpId());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collRombonganBelajarsRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addRombonganBelajarRelatedByJurusanSpId(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajarsRelatedByJurusanSpId === null) {
            $this->initRombonganBelajarsRelatedByJurusanSpId();
            $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajarsRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajarRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajarRelatedByJurusanSpId $rombonganBelajarRelatedByJurusanSpId The rombonganBelajarRelatedByJurusanSpId object to add.
     */
    protected function doAddRombonganBelajarRelatedByJurusanSpId($rombonganBelajarRelatedByJurusanSpId)
    {
        $this->collRombonganBelajarsRelatedByJurusanSpId[]= $rombonganBelajarRelatedByJurusanSpId;
        $rombonganBelajarRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	RombonganBelajarRelatedByJurusanSpId $rombonganBelajarRelatedByJurusanSpId The rombonganBelajarRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeRombonganBelajarRelatedByJurusanSpId($rombonganBelajarRelatedByJurusanSpId)
    {
        if ($this->getRombonganBelajarsRelatedByJurusanSpId()->contains($rombonganBelajarRelatedByJurusanSpId)) {
            $this->collRombonganBelajarsRelatedByJurusanSpId->remove($this->collRombonganBelajarsRelatedByJurusanSpId->search($rombonganBelajarRelatedByJurusanSpId));
            if (null === $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion) {
                $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion = clone $this->collRombonganBelajarsRelatedByJurusanSpId;
                $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion[]= $rombonganBelajarRelatedByJurusanSpId;
            $rombonganBelajarRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collRombonganBelajarsRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addRombonganBelajarsRelatedByJurusanSpId()
     */
    public function clearRombonganBelajarsRelatedByJurusanSpId()
    {
        $this->collRombonganBelajarsRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajarsRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajarsRelatedByJurusanSpId($v = true)
    {
        $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajarsRelatedByJurusanSpId collection.
     *
     * By default this just sets the collRombonganBelajarsRelatedByJurusanSpId collection to an empty array (like clearcollRombonganBelajarsRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajarsRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajarsRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajarsRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collRombonganBelajarsRelatedByJurusanSpId->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajarsRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByJurusanSpId) {
                // return empty collection
                $this->initRombonganBelajarsRelatedByJurusanSpId();
            } else {
                $collRombonganBelajarsRelatedByJurusanSpId = RombonganBelajarQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsRelatedByJurusanSpIdPartial && count($collRombonganBelajarsRelatedByJurusanSpId)) {
                      $this->initRombonganBelajarsRelatedByJurusanSpId(false);

                      foreach($collRombonganBelajarsRelatedByJurusanSpId as $obj) {
                        if (false == $this->collRombonganBelajarsRelatedByJurusanSpId->contains($obj)) {
                          $this->collRombonganBelajarsRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = true;
                    }

                    $collRombonganBelajarsRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collRombonganBelajarsRelatedByJurusanSpId;
                }

                if($partial && $this->collRombonganBelajarsRelatedByJurusanSpId) {
                    foreach($this->collRombonganBelajarsRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajarsRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajarsRelatedByJurusanSpId = $collRombonganBelajarsRelatedByJurusanSpId;
                $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collRombonganBelajarsRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of RombonganBelajarRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajarsRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setRombonganBelajarsRelatedByJurusanSpId(PropelCollection $rombonganBelajarsRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $rombonganBelajarsRelatedByJurusanSpIdToDelete = $this->getRombonganBelajarsRelatedByJurusanSpId(new Criteria(), $con)->diff($rombonganBelajarsRelatedByJurusanSpId);

        $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($rombonganBelajarsRelatedByJurusanSpIdToDelete));

        foreach ($rombonganBelajarsRelatedByJurusanSpIdToDelete as $rombonganBelajarRelatedByJurusanSpIdRemoved) {
            $rombonganBelajarRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collRombonganBelajarsRelatedByJurusanSpId = null;
        foreach ($rombonganBelajarsRelatedByJurusanSpId as $rombonganBelajarRelatedByJurusanSpId) {
            $this->addRombonganBelajarRelatedByJurusanSpId($rombonganBelajarRelatedByJurusanSpId);
        }

        $this->collRombonganBelajarsRelatedByJurusanSpId = $rombonganBelajarsRelatedByJurusanSpId;
        $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajarsRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collRombonganBelajarsRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajarsRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajarsRelatedByJurusanSpId());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collRombonganBelajarsRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addRombonganBelajarRelatedByJurusanSpId(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajarsRelatedByJurusanSpId === null) {
            $this->initRombonganBelajarsRelatedByJurusanSpId();
            $this->collRombonganBelajarsRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajarsRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajarRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajarRelatedByJurusanSpId $rombonganBelajarRelatedByJurusanSpId The rombonganBelajarRelatedByJurusanSpId object to add.
     */
    protected function doAddRombonganBelajarRelatedByJurusanSpId($rombonganBelajarRelatedByJurusanSpId)
    {
        $this->collRombonganBelajarsRelatedByJurusanSpId[]= $rombonganBelajarRelatedByJurusanSpId;
        $rombonganBelajarRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	RombonganBelajarRelatedByJurusanSpId $rombonganBelajarRelatedByJurusanSpId The rombonganBelajarRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeRombonganBelajarRelatedByJurusanSpId($rombonganBelajarRelatedByJurusanSpId)
    {
        if ($this->getRombonganBelajarsRelatedByJurusanSpId()->contains($rombonganBelajarRelatedByJurusanSpId)) {
            $this->collRombonganBelajarsRelatedByJurusanSpId->remove($this->collRombonganBelajarsRelatedByJurusanSpId->search($rombonganBelajarRelatedByJurusanSpId));
            if (null === $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion) {
                $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion = clone $this->collRombonganBelajarsRelatedByJurusanSpId;
                $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsRelatedByJurusanSpIdScheduledForDeletion[]= $rombonganBelajarRelatedByJurusanSpId;
            $rombonganBelajarRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinPrasaranaRelatedByPrasaranaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PrasaranaRelatedByPrasaranaId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinPtkRelatedByPtkId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('PtkRelatedByPtkId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinSekolahRelatedBySekolahId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SekolahRelatedBySekolahId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinKebutuhanKhususRelatedByKebutuhanKhususId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhususRelatedByKebutuhanKhususId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinKurikulumRelatedByKurikulumId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KurikulumRelatedByKurikulumId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinSemesterRelatedBySemesterId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('SemesterRelatedBySemesterId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related RombonganBelajarsRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsRelatedByJurusanSpIdJoinTingkatPendidikanRelatedByTingkatPendidikanId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikanRelatedByTingkatPendidikanId', $join_behavior);

        return $this->getRombonganBelajarsRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collAkreditasiProdisRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addAkreditasiProdisRelatedByJurusanSpId()
     */
    public function clearAkreditasiProdisRelatedByJurusanSpId()
    {
        $this->collAkreditasiProdisRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAkreditasiProdisRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialAkreditasiProdisRelatedByJurusanSpId($v = true)
    {
        $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collAkreditasiProdisRelatedByJurusanSpId collection.
     *
     * By default this just sets the collAkreditasiProdisRelatedByJurusanSpId collection to an empty array (like clearcollAkreditasiProdisRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAkreditasiProdisRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collAkreditasiProdisRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collAkreditasiProdisRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collAkreditasiProdisRelatedByJurusanSpId->setModel('AkreditasiProdi');
    }

    /**
     * Gets an array of AkreditasiProdi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     * @throws PropelException
     */
    public function getAkreditasiProdisRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAkreditasiProdisRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collAkreditasiProdisRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAkreditasiProdisRelatedByJurusanSpId) {
                // return empty collection
                $this->initAkreditasiProdisRelatedByJurusanSpId();
            } else {
                $collAkreditasiProdisRelatedByJurusanSpId = AkreditasiProdiQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAkreditasiProdisRelatedByJurusanSpIdPartial && count($collAkreditasiProdisRelatedByJurusanSpId)) {
                      $this->initAkreditasiProdisRelatedByJurusanSpId(false);

                      foreach($collAkreditasiProdisRelatedByJurusanSpId as $obj) {
                        if (false == $this->collAkreditasiProdisRelatedByJurusanSpId->contains($obj)) {
                          $this->collAkreditasiProdisRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = true;
                    }

                    $collAkreditasiProdisRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collAkreditasiProdisRelatedByJurusanSpId;
                }

                if($partial && $this->collAkreditasiProdisRelatedByJurusanSpId) {
                    foreach($this->collAkreditasiProdisRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collAkreditasiProdisRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collAkreditasiProdisRelatedByJurusanSpId = $collAkreditasiProdisRelatedByJurusanSpId;
                $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collAkreditasiProdisRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of AkreditasiProdiRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $akreditasiProdisRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setAkreditasiProdisRelatedByJurusanSpId(PropelCollection $akreditasiProdisRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $akreditasiProdisRelatedByJurusanSpIdToDelete = $this->getAkreditasiProdisRelatedByJurusanSpId(new Criteria(), $con)->diff($akreditasiProdisRelatedByJurusanSpId);

        $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($akreditasiProdisRelatedByJurusanSpIdToDelete));

        foreach ($akreditasiProdisRelatedByJurusanSpIdToDelete as $akreditasiProdiRelatedByJurusanSpIdRemoved) {
            $akreditasiProdiRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collAkreditasiProdisRelatedByJurusanSpId = null;
        foreach ($akreditasiProdisRelatedByJurusanSpId as $akreditasiProdiRelatedByJurusanSpId) {
            $this->addAkreditasiProdiRelatedByJurusanSpId($akreditasiProdiRelatedByJurusanSpId);
        }

        $this->collAkreditasiProdisRelatedByJurusanSpId = $akreditasiProdisRelatedByJurusanSpId;
        $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AkreditasiProdi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related AkreditasiProdi objects.
     * @throws PropelException
     */
    public function countAkreditasiProdisRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAkreditasiProdisRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collAkreditasiProdisRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAkreditasiProdisRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getAkreditasiProdisRelatedByJurusanSpId());
            }
            $query = AkreditasiProdiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collAkreditasiProdisRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a AkreditasiProdi object to this object
     * through the AkreditasiProdi foreign key attribute.
     *
     * @param    AkreditasiProdi $l AkreditasiProdi
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addAkreditasiProdiRelatedByJurusanSpId(AkreditasiProdi $l)
    {
        if ($this->collAkreditasiProdisRelatedByJurusanSpId === null) {
            $this->initAkreditasiProdisRelatedByJurusanSpId();
            $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collAkreditasiProdisRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAkreditasiProdiRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	AkreditasiProdiRelatedByJurusanSpId $akreditasiProdiRelatedByJurusanSpId The akreditasiProdiRelatedByJurusanSpId object to add.
     */
    protected function doAddAkreditasiProdiRelatedByJurusanSpId($akreditasiProdiRelatedByJurusanSpId)
    {
        $this->collAkreditasiProdisRelatedByJurusanSpId[]= $akreditasiProdiRelatedByJurusanSpId;
        $akreditasiProdiRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	AkreditasiProdiRelatedByJurusanSpId $akreditasiProdiRelatedByJurusanSpId The akreditasiProdiRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeAkreditasiProdiRelatedByJurusanSpId($akreditasiProdiRelatedByJurusanSpId)
    {
        if ($this->getAkreditasiProdisRelatedByJurusanSpId()->contains($akreditasiProdiRelatedByJurusanSpId)) {
            $this->collAkreditasiProdisRelatedByJurusanSpId->remove($this->collAkreditasiProdisRelatedByJurusanSpId->search($akreditasiProdiRelatedByJurusanSpId));
            if (null === $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion) {
                $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion = clone $this->collAkreditasiProdisRelatedByJurusanSpId;
                $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion[]= clone $akreditasiProdiRelatedByJurusanSpId;
            $akreditasiProdiRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related AkreditasiProdisRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     */
    public function getAkreditasiProdisRelatedByJurusanSpIdJoinAkreditasiRelatedByAkreditasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AkreditasiProdiQuery::create(null, $criteria);
        $query->joinWith('AkreditasiRelatedByAkreditasiId', $join_behavior);

        return $this->getAkreditasiProdisRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related AkreditasiProdisRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     */
    public function getAkreditasiProdisRelatedByJurusanSpIdJoinAkreditasiRelatedByAkreditasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AkreditasiProdiQuery::create(null, $criteria);
        $query->joinWith('AkreditasiRelatedByAkreditasiId', $join_behavior);

        return $this->getAkreditasiProdisRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related AkreditasiProdisRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     */
    public function getAkreditasiProdisRelatedByJurusanSpIdJoinLembagaAkreditasiRelatedByLaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AkreditasiProdiQuery::create(null, $criteria);
        $query->joinWith('LembagaAkreditasiRelatedByLaId', $join_behavior);

        return $this->getAkreditasiProdisRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related AkreditasiProdisRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     */
    public function getAkreditasiProdisRelatedByJurusanSpIdJoinLembagaAkreditasiRelatedByLaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AkreditasiProdiQuery::create(null, $criteria);
        $query->joinWith('LembagaAkreditasiRelatedByLaId', $join_behavior);

        return $this->getAkreditasiProdisRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears out the collAkreditasiProdisRelatedByJurusanSpId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return JurusanSp The current object (for fluent API support)
     * @see        addAkreditasiProdisRelatedByJurusanSpId()
     */
    public function clearAkreditasiProdisRelatedByJurusanSpId()
    {
        $this->collAkreditasiProdisRelatedByJurusanSpId = null; // important to set this to null since that means it is uninitialized
        $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = null;

        return $this;
    }

    /**
     * reset is the collAkreditasiProdisRelatedByJurusanSpId collection loaded partially
     *
     * @return void
     */
    public function resetPartialAkreditasiProdisRelatedByJurusanSpId($v = true)
    {
        $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = $v;
    }

    /**
     * Initializes the collAkreditasiProdisRelatedByJurusanSpId collection.
     *
     * By default this just sets the collAkreditasiProdisRelatedByJurusanSpId collection to an empty array (like clearcollAkreditasiProdisRelatedByJurusanSpId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAkreditasiProdisRelatedByJurusanSpId($overrideExisting = true)
    {
        if (null !== $this->collAkreditasiProdisRelatedByJurusanSpId && !$overrideExisting) {
            return;
        }
        $this->collAkreditasiProdisRelatedByJurusanSpId = new PropelObjectCollection();
        $this->collAkreditasiProdisRelatedByJurusanSpId->setModel('AkreditasiProdi');
    }

    /**
     * Gets an array of AkreditasiProdi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this JurusanSp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     * @throws PropelException
     */
    public function getAkreditasiProdisRelatedByJurusanSpId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAkreditasiProdisRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collAkreditasiProdisRelatedByJurusanSpId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAkreditasiProdisRelatedByJurusanSpId) {
                // return empty collection
                $this->initAkreditasiProdisRelatedByJurusanSpId();
            } else {
                $collAkreditasiProdisRelatedByJurusanSpId = AkreditasiProdiQuery::create(null, $criteria)
                    ->filterByJurusanSpRelatedByJurusanSpId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAkreditasiProdisRelatedByJurusanSpIdPartial && count($collAkreditasiProdisRelatedByJurusanSpId)) {
                      $this->initAkreditasiProdisRelatedByJurusanSpId(false);

                      foreach($collAkreditasiProdisRelatedByJurusanSpId as $obj) {
                        if (false == $this->collAkreditasiProdisRelatedByJurusanSpId->contains($obj)) {
                          $this->collAkreditasiProdisRelatedByJurusanSpId->append($obj);
                        }
                      }

                      $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = true;
                    }

                    $collAkreditasiProdisRelatedByJurusanSpId->getInternalIterator()->rewind();
                    return $collAkreditasiProdisRelatedByJurusanSpId;
                }

                if($partial && $this->collAkreditasiProdisRelatedByJurusanSpId) {
                    foreach($this->collAkreditasiProdisRelatedByJurusanSpId as $obj) {
                        if($obj->isNew()) {
                            $collAkreditasiProdisRelatedByJurusanSpId[] = $obj;
                        }
                    }
                }

                $this->collAkreditasiProdisRelatedByJurusanSpId = $collAkreditasiProdisRelatedByJurusanSpId;
                $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = false;
            }
        }

        return $this->collAkreditasiProdisRelatedByJurusanSpId;
    }

    /**
     * Sets a collection of AkreditasiProdiRelatedByJurusanSpId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $akreditasiProdisRelatedByJurusanSpId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return JurusanSp The current object (for fluent API support)
     */
    public function setAkreditasiProdisRelatedByJurusanSpId(PropelCollection $akreditasiProdisRelatedByJurusanSpId, PropelPDO $con = null)
    {
        $akreditasiProdisRelatedByJurusanSpIdToDelete = $this->getAkreditasiProdisRelatedByJurusanSpId(new Criteria(), $con)->diff($akreditasiProdisRelatedByJurusanSpId);

        $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion = unserialize(serialize($akreditasiProdisRelatedByJurusanSpIdToDelete));

        foreach ($akreditasiProdisRelatedByJurusanSpIdToDelete as $akreditasiProdiRelatedByJurusanSpIdRemoved) {
            $akreditasiProdiRelatedByJurusanSpIdRemoved->setJurusanSpRelatedByJurusanSpId(null);
        }

        $this->collAkreditasiProdisRelatedByJurusanSpId = null;
        foreach ($akreditasiProdisRelatedByJurusanSpId as $akreditasiProdiRelatedByJurusanSpId) {
            $this->addAkreditasiProdiRelatedByJurusanSpId($akreditasiProdiRelatedByJurusanSpId);
        }

        $this->collAkreditasiProdisRelatedByJurusanSpId = $akreditasiProdisRelatedByJurusanSpId;
        $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AkreditasiProdi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related AkreditasiProdi objects.
     * @throws PropelException
     */
    public function countAkreditasiProdisRelatedByJurusanSpId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAkreditasiProdisRelatedByJurusanSpIdPartial && !$this->isNew();
        if (null === $this->collAkreditasiProdisRelatedByJurusanSpId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAkreditasiProdisRelatedByJurusanSpId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getAkreditasiProdisRelatedByJurusanSpId());
            }
            $query = AkreditasiProdiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByJurusanSpRelatedByJurusanSpId($this)
                ->count($con);
        }

        return count($this->collAkreditasiProdisRelatedByJurusanSpId);
    }

    /**
     * Method called to associate a AkreditasiProdi object to this object
     * through the AkreditasiProdi foreign key attribute.
     *
     * @param    AkreditasiProdi $l AkreditasiProdi
     * @return JurusanSp The current object (for fluent API support)
     */
    public function addAkreditasiProdiRelatedByJurusanSpId(AkreditasiProdi $l)
    {
        if ($this->collAkreditasiProdisRelatedByJurusanSpId === null) {
            $this->initAkreditasiProdisRelatedByJurusanSpId();
            $this->collAkreditasiProdisRelatedByJurusanSpIdPartial = true;
        }
        if (!in_array($l, $this->collAkreditasiProdisRelatedByJurusanSpId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAkreditasiProdiRelatedByJurusanSpId($l);
        }

        return $this;
    }

    /**
     * @param	AkreditasiProdiRelatedByJurusanSpId $akreditasiProdiRelatedByJurusanSpId The akreditasiProdiRelatedByJurusanSpId object to add.
     */
    protected function doAddAkreditasiProdiRelatedByJurusanSpId($akreditasiProdiRelatedByJurusanSpId)
    {
        $this->collAkreditasiProdisRelatedByJurusanSpId[]= $akreditasiProdiRelatedByJurusanSpId;
        $akreditasiProdiRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId($this);
    }

    /**
     * @param	AkreditasiProdiRelatedByJurusanSpId $akreditasiProdiRelatedByJurusanSpId The akreditasiProdiRelatedByJurusanSpId object to remove.
     * @return JurusanSp The current object (for fluent API support)
     */
    public function removeAkreditasiProdiRelatedByJurusanSpId($akreditasiProdiRelatedByJurusanSpId)
    {
        if ($this->getAkreditasiProdisRelatedByJurusanSpId()->contains($akreditasiProdiRelatedByJurusanSpId)) {
            $this->collAkreditasiProdisRelatedByJurusanSpId->remove($this->collAkreditasiProdisRelatedByJurusanSpId->search($akreditasiProdiRelatedByJurusanSpId));
            if (null === $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion) {
                $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion = clone $this->collAkreditasiProdisRelatedByJurusanSpId;
                $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion->clear();
            }
            $this->akreditasiProdisRelatedByJurusanSpIdScheduledForDeletion[]= clone $akreditasiProdiRelatedByJurusanSpId;
            $akreditasiProdiRelatedByJurusanSpId->setJurusanSpRelatedByJurusanSpId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related AkreditasiProdisRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     */
    public function getAkreditasiProdisRelatedByJurusanSpIdJoinAkreditasiRelatedByAkreditasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AkreditasiProdiQuery::create(null, $criteria);
        $query->joinWith('AkreditasiRelatedByAkreditasiId', $join_behavior);

        return $this->getAkreditasiProdisRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related AkreditasiProdisRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     */
    public function getAkreditasiProdisRelatedByJurusanSpIdJoinAkreditasiRelatedByAkreditasiId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AkreditasiProdiQuery::create(null, $criteria);
        $query->joinWith('AkreditasiRelatedByAkreditasiId', $join_behavior);

        return $this->getAkreditasiProdisRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related AkreditasiProdisRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     */
    public function getAkreditasiProdisRelatedByJurusanSpIdJoinLembagaAkreditasiRelatedByLaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AkreditasiProdiQuery::create(null, $criteria);
        $query->joinWith('LembagaAkreditasiRelatedByLaId', $join_behavior);

        return $this->getAkreditasiProdisRelatedByJurusanSpId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this JurusanSp is new, it will return
     * an empty collection; or if this JurusanSp has previously
     * been saved, it will retrieve related AkreditasiProdisRelatedByJurusanSpId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in JurusanSp.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|AkreditasiProdi[] List of AkreditasiProdi objects
     */
    public function getAkreditasiProdisRelatedByJurusanSpIdJoinLembagaAkreditasiRelatedByLaId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AkreditasiProdiQuery::create(null, $criteria);
        $query->joinWith('LembagaAkreditasiRelatedByLaId', $join_behavior);

        return $this->getAkreditasiProdisRelatedByJurusanSpId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->jurusan_sp_id = null;
        $this->sekolah_id = null;
        $this->kebutuhan_khusus_id = null;
        $this->jurusan_id = null;
        $this->nama_jurusan_sp = null;
        $this->sk_izin = null;
        $this->tanggal_sk_izin = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId) {
                foreach ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldJurusanSpsRelatedByJurusanSpId) {
                foreach ($this->collVldJurusanSpsRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldJurusanSpsRelatedByJurusanSpId) {
                foreach ($this->collVldJurusanSpsRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collJurusanKerjasamasRelatedByJurusanSpId) {
                foreach ($this->collJurusanKerjasamasRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collJurusanKerjasamasRelatedByJurusanSpId) {
                foreach ($this->collJurusanKerjasamasRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajarsRelatedByJurusanSpId) {
                foreach ($this->collRombonganBelajarsRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajarsRelatedByJurusanSpId) {
                foreach ($this->collRombonganBelajarsRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAkreditasiProdisRelatedByJurusanSpId) {
                foreach ($this->collAkreditasiProdisRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAkreditasiProdisRelatedByJurusanSpId) {
                foreach ($this->collAkreditasiProdisRelatedByJurusanSpId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aSekolahRelatedBySekolahId instanceof Persistent) {
              $this->aSekolahRelatedBySekolahId->clearAllReferences($deep);
            }
            if ($this->aJurusanRelatedByJurusanId instanceof Persistent) {
              $this->aJurusanRelatedByJurusanId->clearAllReferences($deep);
            }
            if ($this->aJurusanRelatedByJurusanId instanceof Persistent) {
              $this->aJurusanRelatedByJurusanId->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususId->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhususRelatedByKebutuhanKhususId instanceof Persistent) {
              $this->aKebutuhanKhususRelatedByKebutuhanKhususId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->clearIterator();
        }
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = null;
        if ($this->collRegistrasiPesertaDidiksRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId->clearIterator();
        }
        $this->collRegistrasiPesertaDidiksRelatedByJurusanSpId = null;
        if ($this->collVldJurusanSpsRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collVldJurusanSpsRelatedByJurusanSpId->clearIterator();
        }
        $this->collVldJurusanSpsRelatedByJurusanSpId = null;
        if ($this->collVldJurusanSpsRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collVldJurusanSpsRelatedByJurusanSpId->clearIterator();
        }
        $this->collVldJurusanSpsRelatedByJurusanSpId = null;
        if ($this->collJurusanKerjasamasRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collJurusanKerjasamasRelatedByJurusanSpId->clearIterator();
        }
        $this->collJurusanKerjasamasRelatedByJurusanSpId = null;
        if ($this->collJurusanKerjasamasRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collJurusanKerjasamasRelatedByJurusanSpId->clearIterator();
        }
        $this->collJurusanKerjasamasRelatedByJurusanSpId = null;
        if ($this->collRombonganBelajarsRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collRombonganBelajarsRelatedByJurusanSpId->clearIterator();
        }
        $this->collRombonganBelajarsRelatedByJurusanSpId = null;
        if ($this->collRombonganBelajarsRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collRombonganBelajarsRelatedByJurusanSpId->clearIterator();
        }
        $this->collRombonganBelajarsRelatedByJurusanSpId = null;
        if ($this->collAkreditasiProdisRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collAkreditasiProdisRelatedByJurusanSpId->clearIterator();
        }
        $this->collAkreditasiProdisRelatedByJurusanSpId = null;
        if ($this->collAkreditasiProdisRelatedByJurusanSpId instanceof PropelCollection) {
            $this->collAkreditasiProdisRelatedByJurusanSpId->clearIterator();
        }
        $this->collAkreditasiProdisRelatedByJurusanSpId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aSekolahRelatedBySekolahId = null;
        $this->aJurusanRelatedByJurusanId = null;
        $this->aJurusanRelatedByJurusanId = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
        $this->aKebutuhanKhususRelatedByKebutuhanKhususId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(JurusanSpPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
