<?php

namespace angulex\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use angulex\Model\AkreditasiSp;
use angulex\Model\AnggotaGugus;
use angulex\Model\BentukPendidikan;
use angulex\Model\Blockgrant;
use angulex\Model\BukuAlat;
use angulex\Model\GugusSekolah;
use angulex\Model\JurusanSp;
use angulex\Model\KebutuhanKhusus;
use angulex\Model\Mou;
use angulex\Model\MstWilayah;
use angulex\Model\Pengguna;
use angulex\Model\PesertaDidik;
use angulex\Model\PesertaDidikBaru;
use angulex\Model\Prasarana;
use angulex\Model\ProgramInklusi;
use angulex\Model\Ptk;
use angulex\Model\PtkBaru;
use angulex\Model\PtkTerdaftar;
use angulex\Model\RegistrasiPesertaDidik;
use angulex\Model\RombonganBelajar;
use angulex\Model\Sanitasi;
use angulex\Model\Sarana;
use angulex\Model\SasaranPengawasan;
use angulex\Model\SasaranSurvey;
use angulex\Model\Sekolah;
use angulex\Model\SekolahLongitudinal;
use angulex\Model\SekolahPeer;
use angulex\Model\SekolahQuery;
use angulex\Model\StatusKepemilikan;
use angulex\Model\SyncLog;
use angulex\Model\SyncSession;
use angulex\Model\TableSyncLog;
use angulex\Model\TugasTambahan;
use angulex\Model\UnitUsaha;
use angulex\Model\VldSekolah;
use angulex\Model\WPendingJob;
use angulex\Model\WdstSyncLog;
use angulex\Model\WsrcSyncLog;
use angulex\Model\WtDstSyncLog;
use angulex\Model\WtSrcSyncLog;

/**
 * Base class that represents a query for the 'sekolah' table.
 *
 * 
 *
 * @method SekolahQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method SekolahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method SekolahQuery orderByNamaNomenklatur($order = Criteria::ASC) Order by the nama_nomenklatur column
 * @method SekolahQuery orderByNss($order = Criteria::ASC) Order by the nss column
 * @method SekolahQuery orderByNpsn($order = Criteria::ASC) Order by the npsn column
 * @method SekolahQuery orderByBentukPendidikanId($order = Criteria::ASC) Order by the bentuk_pendidikan_id column
 * @method SekolahQuery orderByAlamatJalan($order = Criteria::ASC) Order by the alamat_jalan column
 * @method SekolahQuery orderByRt($order = Criteria::ASC) Order by the rt column
 * @method SekolahQuery orderByRw($order = Criteria::ASC) Order by the rw column
 * @method SekolahQuery orderByNamaDusun($order = Criteria::ASC) Order by the nama_dusun column
 * @method SekolahQuery orderByDesaKelurahan($order = Criteria::ASC) Order by the desa_kelurahan column
 * @method SekolahQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method SekolahQuery orderByKodePos($order = Criteria::ASC) Order by the kode_pos column
 * @method SekolahQuery orderByLintang($order = Criteria::ASC) Order by the lintang column
 * @method SekolahQuery orderByBujur($order = Criteria::ASC) Order by the bujur column
 * @method SekolahQuery orderByNomorTelepon($order = Criteria::ASC) Order by the nomor_telepon column
 * @method SekolahQuery orderByNomorFax($order = Criteria::ASC) Order by the nomor_fax column
 * @method SekolahQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method SekolahQuery orderByWebsite($order = Criteria::ASC) Order by the website column
 * @method SekolahQuery orderByKebutuhanKhususId($order = Criteria::ASC) Order by the kebutuhan_khusus_id column
 * @method SekolahQuery orderByStatusSekolah($order = Criteria::ASC) Order by the status_sekolah column
 * @method SekolahQuery orderBySkPendirianSekolah($order = Criteria::ASC) Order by the sk_pendirian_sekolah column
 * @method SekolahQuery orderByTanggalSkPendirian($order = Criteria::ASC) Order by the tanggal_sk_pendirian column
 * @method SekolahQuery orderByStatusKepemilikanId($order = Criteria::ASC) Order by the status_kepemilikan_id column
 * @method SekolahQuery orderByYayasanId($order = Criteria::ASC) Order by the yayasan_id column
 * @method SekolahQuery orderBySkIzinOperasional($order = Criteria::ASC) Order by the sk_izin_operasional column
 * @method SekolahQuery orderByTanggalSkIzinOperasional($order = Criteria::ASC) Order by the tanggal_sk_izin_operasional column
 * @method SekolahQuery orderByNoRekening($order = Criteria::ASC) Order by the no_rekening column
 * @method SekolahQuery orderByNamaBank($order = Criteria::ASC) Order by the nama_bank column
 * @method SekolahQuery orderByCabangKcpUnit($order = Criteria::ASC) Order by the cabang_kcp_unit column
 * @method SekolahQuery orderByRekeningAtasNama($order = Criteria::ASC) Order by the rekening_atas_nama column
 * @method SekolahQuery orderByMbs($order = Criteria::ASC) Order by the mbs column
 * @method SekolahQuery orderByLuasTanahMilik($order = Criteria::ASC) Order by the luas_tanah_milik column
 * @method SekolahQuery orderByLuasTanahBukanMilik($order = Criteria::ASC) Order by the luas_tanah_bukan_milik column
 * @method SekolahQuery orderByKodeRegistrasi($order = Criteria::ASC) Order by the kode_registrasi column
 * @method SekolahQuery orderByFlag($order = Criteria::ASC) Order by the flag column
 * @method SekolahQuery orderByPicId($order = Criteria::ASC) Order by the pic_id column
 * @method SekolahQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method SekolahQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method SekolahQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method SekolahQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method SekolahQuery groupBySekolahId() Group by the sekolah_id column
 * @method SekolahQuery groupByNama() Group by the nama column
 * @method SekolahQuery groupByNamaNomenklatur() Group by the nama_nomenklatur column
 * @method SekolahQuery groupByNss() Group by the nss column
 * @method SekolahQuery groupByNpsn() Group by the npsn column
 * @method SekolahQuery groupByBentukPendidikanId() Group by the bentuk_pendidikan_id column
 * @method SekolahQuery groupByAlamatJalan() Group by the alamat_jalan column
 * @method SekolahQuery groupByRt() Group by the rt column
 * @method SekolahQuery groupByRw() Group by the rw column
 * @method SekolahQuery groupByNamaDusun() Group by the nama_dusun column
 * @method SekolahQuery groupByDesaKelurahan() Group by the desa_kelurahan column
 * @method SekolahQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method SekolahQuery groupByKodePos() Group by the kode_pos column
 * @method SekolahQuery groupByLintang() Group by the lintang column
 * @method SekolahQuery groupByBujur() Group by the bujur column
 * @method SekolahQuery groupByNomorTelepon() Group by the nomor_telepon column
 * @method SekolahQuery groupByNomorFax() Group by the nomor_fax column
 * @method SekolahQuery groupByEmail() Group by the email column
 * @method SekolahQuery groupByWebsite() Group by the website column
 * @method SekolahQuery groupByKebutuhanKhususId() Group by the kebutuhan_khusus_id column
 * @method SekolahQuery groupByStatusSekolah() Group by the status_sekolah column
 * @method SekolahQuery groupBySkPendirianSekolah() Group by the sk_pendirian_sekolah column
 * @method SekolahQuery groupByTanggalSkPendirian() Group by the tanggal_sk_pendirian column
 * @method SekolahQuery groupByStatusKepemilikanId() Group by the status_kepemilikan_id column
 * @method SekolahQuery groupByYayasanId() Group by the yayasan_id column
 * @method SekolahQuery groupBySkIzinOperasional() Group by the sk_izin_operasional column
 * @method SekolahQuery groupByTanggalSkIzinOperasional() Group by the tanggal_sk_izin_operasional column
 * @method SekolahQuery groupByNoRekening() Group by the no_rekening column
 * @method SekolahQuery groupByNamaBank() Group by the nama_bank column
 * @method SekolahQuery groupByCabangKcpUnit() Group by the cabang_kcp_unit column
 * @method SekolahQuery groupByRekeningAtasNama() Group by the rekening_atas_nama column
 * @method SekolahQuery groupByMbs() Group by the mbs column
 * @method SekolahQuery groupByLuasTanahMilik() Group by the luas_tanah_milik column
 * @method SekolahQuery groupByLuasTanahBukanMilik() Group by the luas_tanah_bukan_milik column
 * @method SekolahQuery groupByKodeRegistrasi() Group by the kode_registrasi column
 * @method SekolahQuery groupByFlag() Group by the flag column
 * @method SekolahQuery groupByPicId() Group by the pic_id column
 * @method SekolahQuery groupByLastUpdate() Group by the Last_update column
 * @method SekolahQuery groupBySoftDelete() Group by the Soft_delete column
 * @method SekolahQuery groupByLastSync() Group by the last_sync column
 * @method SekolahQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method SekolahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SekolahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SekolahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SekolahQuery leftJoinBentukPendidikanRelatedByBentukPendidikanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BentukPendidikanRelatedByBentukPendidikanId relation
 * @method SekolahQuery rightJoinBentukPendidikanRelatedByBentukPendidikanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BentukPendidikanRelatedByBentukPendidikanId relation
 * @method SekolahQuery innerJoinBentukPendidikanRelatedByBentukPendidikanId($relationAlias = null) Adds a INNER JOIN clause to the query using the BentukPendidikanRelatedByBentukPendidikanId relation
 *
 * @method SekolahQuery leftJoinBentukPendidikanRelatedByBentukPendidikanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BentukPendidikanRelatedByBentukPendidikanId relation
 * @method SekolahQuery rightJoinBentukPendidikanRelatedByBentukPendidikanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BentukPendidikanRelatedByBentukPendidikanId relation
 * @method SekolahQuery innerJoinBentukPendidikanRelatedByBentukPendidikanId($relationAlias = null) Adds a INNER JOIN clause to the query using the BentukPendidikanRelatedByBentukPendidikanId relation
 *
 * @method SekolahQuery leftJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a LEFT JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 * @method SekolahQuery rightJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 * @method SekolahQuery innerJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a INNER JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 *
 * @method SekolahQuery leftJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a LEFT JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 * @method SekolahQuery rightJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 * @method SekolahQuery innerJoinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null) Adds a INNER JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
 *
 * @method SekolahQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method SekolahQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method SekolahQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method SekolahQuery leftJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a LEFT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method SekolahQuery rightJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 * @method SekolahQuery innerJoinMstWilayahRelatedByKodeWilayah($relationAlias = null) Adds a INNER JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
 *
 * @method SekolahQuery leftJoinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusKepemilikanRelatedByStatusKepemilikanId relation
 * @method SekolahQuery rightJoinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusKepemilikanRelatedByStatusKepemilikanId relation
 * @method SekolahQuery innerJoinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusKepemilikanRelatedByStatusKepemilikanId relation
 *
 * @method SekolahQuery leftJoinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusKepemilikanRelatedByStatusKepemilikanId relation
 * @method SekolahQuery rightJoinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusKepemilikanRelatedByStatusKepemilikanId relation
 * @method SekolahQuery innerJoinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusKepemilikanRelatedByStatusKepemilikanId relation
 *
 * @method SekolahQuery leftJoinBukuAlatRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatRelatedBySekolahId relation
 * @method SekolahQuery rightJoinBukuAlatRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatRelatedBySekolahId relation
 * @method SekolahQuery innerJoinBukuAlatRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinBukuAlatRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlatRelatedBySekolahId relation
 * @method SekolahQuery rightJoinBukuAlatRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlatRelatedBySekolahId relation
 * @method SekolahQuery innerJoinBukuAlatRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlatRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinJurusanSpRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JurusanSpRelatedBySekolahId relation
 * @method SekolahQuery rightJoinJurusanSpRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JurusanSpRelatedBySekolahId relation
 * @method SekolahQuery innerJoinJurusanSpRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the JurusanSpRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinJurusanSpRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the JurusanSpRelatedBySekolahId relation
 * @method SekolahQuery rightJoinJurusanSpRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JurusanSpRelatedBySekolahId relation
 * @method SekolahQuery innerJoinJurusanSpRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the JurusanSpRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSekolahLongitudinalRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahLongitudinalRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSekolahLongitudinalRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahLongitudinalRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSekolahLongitudinalRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahLongitudinalRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSekolahLongitudinalRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SekolahLongitudinalRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSekolahLongitudinalRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SekolahLongitudinalRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSekolahLongitudinalRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SekolahLongitudinalRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSanitasiRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SanitasiRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSanitasiRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SanitasiRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSanitasiRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SanitasiRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSanitasiRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SanitasiRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSanitasiRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SanitasiRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSanitasiRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SanitasiRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RegistrasiPesertaDidikRelatedBySekolahId relation
 * @method SekolahQuery rightJoinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RegistrasiPesertaDidikRelatedBySekolahId relation
 * @method SekolahQuery innerJoinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the RegistrasiPesertaDidikRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RegistrasiPesertaDidikRelatedBySekolahId relation
 * @method SekolahQuery rightJoinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RegistrasiPesertaDidikRelatedBySekolahId relation
 * @method SekolahQuery innerJoinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the RegistrasiPesertaDidikRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPenggunaRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPenggunaRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPenggunaRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPenggunaRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPenggunaRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPenggunaRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPesertaDidikBaruRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikBaruRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPesertaDidikBaruRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikBaruRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPesertaDidikBaruRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikBaruRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPesertaDidikBaruRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PesertaDidikBaruRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPesertaDidikBaruRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PesertaDidikBaruRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPesertaDidikBaruRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PesertaDidikBaruRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSaranaRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSaranaRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSaranaRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSaranaRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaranaRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSaranaRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaranaRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSaranaRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SaranaRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinAkreditasiSpRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AkreditasiSpRelatedBySekolahId relation
 * @method SekolahQuery rightJoinAkreditasiSpRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AkreditasiSpRelatedBySekolahId relation
 * @method SekolahQuery innerJoinAkreditasiSpRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the AkreditasiSpRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinAkreditasiSpRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AkreditasiSpRelatedBySekolahId relation
 * @method SekolahQuery rightJoinAkreditasiSpRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AkreditasiSpRelatedBySekolahId relation
 * @method SekolahQuery innerJoinAkreditasiSpRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the AkreditasiSpRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinGugusSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the GugusSekolahRelatedBySekolahId relation
 * @method SekolahQuery rightJoinGugusSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GugusSekolahRelatedBySekolahId relation
 * @method SekolahQuery innerJoinGugusSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the GugusSekolahRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinGugusSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the GugusSekolahRelatedBySekolahId relation
 * @method SekolahQuery rightJoinGugusSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GugusSekolahRelatedBySekolahId relation
 * @method SekolahQuery innerJoinGugusSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the GugusSekolahRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPtkTerdaftarRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkTerdaftarRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPtkTerdaftarRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkTerdaftarRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPtkTerdaftarRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkTerdaftarRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPtkTerdaftarRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkTerdaftarRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPtkTerdaftarRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkTerdaftarRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPtkTerdaftarRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkTerdaftarRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinBlockgrantRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BlockgrantRelatedBySekolahId relation
 * @method SekolahQuery rightJoinBlockgrantRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BlockgrantRelatedBySekolahId relation
 * @method SekolahQuery innerJoinBlockgrantRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the BlockgrantRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinBlockgrantRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the BlockgrantRelatedBySekolahId relation
 * @method SekolahQuery rightJoinBlockgrantRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BlockgrantRelatedBySekolahId relation
 * @method SekolahQuery innerJoinBlockgrantRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the BlockgrantRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinMouRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MouRelatedBySekolahId relation
 * @method SekolahQuery rightJoinMouRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MouRelatedBySekolahId relation
 * @method SekolahQuery innerJoinMouRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the MouRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinMouRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MouRelatedBySekolahId relation
 * @method SekolahQuery rightJoinMouRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MouRelatedBySekolahId relation
 * @method SekolahQuery innerJoinMouRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the MouRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinUnitUsahaRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnitUsahaRelatedBySekolahId relation
 * @method SekolahQuery rightJoinUnitUsahaRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnitUsahaRelatedBySekolahId relation
 * @method SekolahQuery innerJoinUnitUsahaRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the UnitUsahaRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinUnitUsahaRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnitUsahaRelatedBySekolahId relation
 * @method SekolahQuery rightJoinUnitUsahaRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnitUsahaRelatedBySekolahId relation
 * @method SekolahQuery innerJoinUnitUsahaRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the UnitUsahaRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSyncSessionRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SyncSessionRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSyncSessionRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SyncSessionRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSyncSessionRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SyncSessionRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSyncSessionRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SyncSessionRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSyncSessionRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SyncSessionRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSyncSessionRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SyncSessionRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPtkRelatedByEntrySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByEntrySekolahId relation
 * @method SekolahQuery rightJoinPtkRelatedByEntrySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByEntrySekolahId relation
 * @method SekolahQuery innerJoinPtkRelatedByEntrySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByEntrySekolahId relation
 *
 * @method SekolahQuery leftJoinPtkRelatedByEntrySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkRelatedByEntrySekolahId relation
 * @method SekolahQuery rightJoinPtkRelatedByEntrySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkRelatedByEntrySekolahId relation
 * @method SekolahQuery innerJoinPtkRelatedByEntrySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkRelatedByEntrySekolahId relation
 *
 * @method SekolahQuery leftJoinWtSrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtSrcSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWtSrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtSrcSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWtSrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WtSrcSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWtSrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtSrcSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWtSrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtSrcSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWtSrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WtSrcSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinTableSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the TableSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinTableSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TableSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinTableSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the TableSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinTableSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the TableSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinTableSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TableSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinTableSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the TableSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWtDstSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtDstSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWtDstSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtDstSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWtDstSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WtDstSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWtDstSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WtDstSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWtDstSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WtDstSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWtDstSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WtDstSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPrasaranaRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPrasaranaRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPrasaranaRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPrasaranaRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPrasaranaRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPrasaranaRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinRombonganBelajarRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedBySekolahId relation
 * @method SekolahQuery rightJoinRombonganBelajarRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedBySekolahId relation
 * @method SekolahQuery innerJoinRombonganBelajarRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinRombonganBelajarRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajarRelatedBySekolahId relation
 * @method SekolahQuery rightJoinRombonganBelajarRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajarRelatedBySekolahId relation
 * @method SekolahQuery innerJoinRombonganBelajarRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajarRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSasaranSurveyRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SasaranSurveyRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSasaranSurveyRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SasaranSurveyRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSasaranSurveyRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SasaranSurveyRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSasaranSurveyRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SasaranSurveyRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSasaranSurveyRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SasaranSurveyRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSasaranSurveyRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SasaranSurveyRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWsrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WsrcSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWsrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WsrcSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWsrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WsrcSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWsrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WsrcSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWsrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WsrcSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWsrcSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WsrcSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSasaranPengawasanRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SasaranPengawasanRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSasaranPengawasanRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SasaranPengawasanRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSasaranPengawasanRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SasaranPengawasanRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSasaranPengawasanRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SasaranPengawasanRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSasaranPengawasanRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SasaranPengawasanRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSasaranPengawasanRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SasaranPengawasanRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWdstSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WdstSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWdstSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WdstSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWdstSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WdstSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWdstSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WdstSyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWdstSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WdstSyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWdstSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WdstSyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinSyncLogRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SyncLogRelatedBySekolahId relation
 * @method SekolahQuery rightJoinSyncLogRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SyncLogRelatedBySekolahId relation
 * @method SekolahQuery innerJoinSyncLogRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the SyncLogRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWPendingJobRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WPendingJobRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWPendingJobRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WPendingJobRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWPendingJobRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WPendingJobRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinWPendingJobRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the WPendingJobRelatedBySekolahId relation
 * @method SekolahQuery rightJoinWPendingJobRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WPendingJobRelatedBySekolahId relation
 * @method SekolahQuery innerJoinWPendingJobRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the WPendingJobRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinProgramInklusiRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProgramInklusiRelatedBySekolahId relation
 * @method SekolahQuery rightJoinProgramInklusiRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProgramInklusiRelatedBySekolahId relation
 * @method SekolahQuery innerJoinProgramInklusiRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the ProgramInklusiRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinProgramInklusiRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProgramInklusiRelatedBySekolahId relation
 * @method SekolahQuery rightJoinProgramInklusiRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProgramInklusiRelatedBySekolahId relation
 * @method SekolahQuery innerJoinProgramInklusiRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the ProgramInklusiRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPtkBaruRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkBaruRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPtkBaruRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkBaruRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPtkBaruRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkBaruRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinPtkBaruRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PtkBaruRelatedBySekolahId relation
 * @method SekolahQuery rightJoinPtkBaruRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PtkBaruRelatedBySekolahId relation
 * @method SekolahQuery innerJoinPtkBaruRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the PtkBaruRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinVldSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldSekolahRelatedBySekolahId relation
 * @method SekolahQuery rightJoinVldSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldSekolahRelatedBySekolahId relation
 * @method SekolahQuery innerJoinVldSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldSekolahRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinVldSekolahRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldSekolahRelatedBySekolahId relation
 * @method SekolahQuery rightJoinVldSekolahRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldSekolahRelatedBySekolahId relation
 * @method SekolahQuery innerJoinVldSekolahRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the VldSekolahRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinAnggotaGugusRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AnggotaGugusRelatedBySekolahId relation
 * @method SekolahQuery rightJoinAnggotaGugusRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AnggotaGugusRelatedBySekolahId relation
 * @method SekolahQuery innerJoinAnggotaGugusRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the AnggotaGugusRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinAnggotaGugusRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the AnggotaGugusRelatedBySekolahId relation
 * @method SekolahQuery rightJoinAnggotaGugusRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AnggotaGugusRelatedBySekolahId relation
 * @method SekolahQuery innerJoinAnggotaGugusRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the AnggotaGugusRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinTugasTambahanRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the TugasTambahanRelatedBySekolahId relation
 * @method SekolahQuery rightJoinTugasTambahanRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TugasTambahanRelatedBySekolahId relation
 * @method SekolahQuery innerJoinTugasTambahanRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the TugasTambahanRelatedBySekolahId relation
 *
 * @method SekolahQuery leftJoinTugasTambahanRelatedBySekolahId($relationAlias = null) Adds a LEFT JOIN clause to the query using the TugasTambahanRelatedBySekolahId relation
 * @method SekolahQuery rightJoinTugasTambahanRelatedBySekolahId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TugasTambahanRelatedBySekolahId relation
 * @method SekolahQuery innerJoinTugasTambahanRelatedBySekolahId($relationAlias = null) Adds a INNER JOIN clause to the query using the TugasTambahanRelatedBySekolahId relation
 *
 * @method Sekolah findOne(PropelPDO $con = null) Return the first Sekolah matching the query
 * @method Sekolah findOneOrCreate(PropelPDO $con = null) Return the first Sekolah matching the query, or a new Sekolah object populated from the query conditions when no match is found
 *
 * @method Sekolah findOneByNama(string $nama) Return the first Sekolah filtered by the nama column
 * @method Sekolah findOneByNamaNomenklatur(string $nama_nomenklatur) Return the first Sekolah filtered by the nama_nomenklatur column
 * @method Sekolah findOneByNss(string $nss) Return the first Sekolah filtered by the nss column
 * @method Sekolah findOneByNpsn(string $npsn) Return the first Sekolah filtered by the npsn column
 * @method Sekolah findOneByBentukPendidikanId(int $bentuk_pendidikan_id) Return the first Sekolah filtered by the bentuk_pendidikan_id column
 * @method Sekolah findOneByAlamatJalan(string $alamat_jalan) Return the first Sekolah filtered by the alamat_jalan column
 * @method Sekolah findOneByRt(string $rt) Return the first Sekolah filtered by the rt column
 * @method Sekolah findOneByRw(string $rw) Return the first Sekolah filtered by the rw column
 * @method Sekolah findOneByNamaDusun(string $nama_dusun) Return the first Sekolah filtered by the nama_dusun column
 * @method Sekolah findOneByDesaKelurahan(string $desa_kelurahan) Return the first Sekolah filtered by the desa_kelurahan column
 * @method Sekolah findOneByKodeWilayah(string $kode_wilayah) Return the first Sekolah filtered by the kode_wilayah column
 * @method Sekolah findOneByKodePos(string $kode_pos) Return the first Sekolah filtered by the kode_pos column
 * @method Sekolah findOneByLintang(string $lintang) Return the first Sekolah filtered by the lintang column
 * @method Sekolah findOneByBujur(string $bujur) Return the first Sekolah filtered by the bujur column
 * @method Sekolah findOneByNomorTelepon(string $nomor_telepon) Return the first Sekolah filtered by the nomor_telepon column
 * @method Sekolah findOneByNomorFax(string $nomor_fax) Return the first Sekolah filtered by the nomor_fax column
 * @method Sekolah findOneByEmail(string $email) Return the first Sekolah filtered by the email column
 * @method Sekolah findOneByWebsite(string $website) Return the first Sekolah filtered by the website column
 * @method Sekolah findOneByKebutuhanKhususId(int $kebutuhan_khusus_id) Return the first Sekolah filtered by the kebutuhan_khusus_id column
 * @method Sekolah findOneByStatusSekolah(string $status_sekolah) Return the first Sekolah filtered by the status_sekolah column
 * @method Sekolah findOneBySkPendirianSekolah(string $sk_pendirian_sekolah) Return the first Sekolah filtered by the sk_pendirian_sekolah column
 * @method Sekolah findOneByTanggalSkPendirian(string $tanggal_sk_pendirian) Return the first Sekolah filtered by the tanggal_sk_pendirian column
 * @method Sekolah findOneByStatusKepemilikanId(string $status_kepemilikan_id) Return the first Sekolah filtered by the status_kepemilikan_id column
 * @method Sekolah findOneByYayasanId(string $yayasan_id) Return the first Sekolah filtered by the yayasan_id column
 * @method Sekolah findOneBySkIzinOperasional(string $sk_izin_operasional) Return the first Sekolah filtered by the sk_izin_operasional column
 * @method Sekolah findOneByTanggalSkIzinOperasional(string $tanggal_sk_izin_operasional) Return the first Sekolah filtered by the tanggal_sk_izin_operasional column
 * @method Sekolah findOneByNoRekening(string $no_rekening) Return the first Sekolah filtered by the no_rekening column
 * @method Sekolah findOneByNamaBank(string $nama_bank) Return the first Sekolah filtered by the nama_bank column
 * @method Sekolah findOneByCabangKcpUnit(string $cabang_kcp_unit) Return the first Sekolah filtered by the cabang_kcp_unit column
 * @method Sekolah findOneByRekeningAtasNama(string $rekening_atas_nama) Return the first Sekolah filtered by the rekening_atas_nama column
 * @method Sekolah findOneByMbs(string $mbs) Return the first Sekolah filtered by the mbs column
 * @method Sekolah findOneByLuasTanahMilik(string $luas_tanah_milik) Return the first Sekolah filtered by the luas_tanah_milik column
 * @method Sekolah findOneByLuasTanahBukanMilik(string $luas_tanah_bukan_milik) Return the first Sekolah filtered by the luas_tanah_bukan_milik column
 * @method Sekolah findOneByKodeRegistrasi(string $kode_registrasi) Return the first Sekolah filtered by the kode_registrasi column
 * @method Sekolah findOneByFlag(string $flag) Return the first Sekolah filtered by the flag column
 * @method Sekolah findOneByPicId(string $pic_id) Return the first Sekolah filtered by the pic_id column
 * @method Sekolah findOneByLastUpdate(string $Last_update) Return the first Sekolah filtered by the Last_update column
 * @method Sekolah findOneBySoftDelete(string $Soft_delete) Return the first Sekolah filtered by the Soft_delete column
 * @method Sekolah findOneByLastSync(string $last_sync) Return the first Sekolah filtered by the last_sync column
 * @method Sekolah findOneByUpdaterId(string $Updater_ID) Return the first Sekolah filtered by the Updater_ID column
 *
 * @method array findBySekolahId(string $sekolah_id) Return Sekolah objects filtered by the sekolah_id column
 * @method array findByNama(string $nama) Return Sekolah objects filtered by the nama column
 * @method array findByNamaNomenklatur(string $nama_nomenklatur) Return Sekolah objects filtered by the nama_nomenklatur column
 * @method array findByNss(string $nss) Return Sekolah objects filtered by the nss column
 * @method array findByNpsn(string $npsn) Return Sekolah objects filtered by the npsn column
 * @method array findByBentukPendidikanId(int $bentuk_pendidikan_id) Return Sekolah objects filtered by the bentuk_pendidikan_id column
 * @method array findByAlamatJalan(string $alamat_jalan) Return Sekolah objects filtered by the alamat_jalan column
 * @method array findByRt(string $rt) Return Sekolah objects filtered by the rt column
 * @method array findByRw(string $rw) Return Sekolah objects filtered by the rw column
 * @method array findByNamaDusun(string $nama_dusun) Return Sekolah objects filtered by the nama_dusun column
 * @method array findByDesaKelurahan(string $desa_kelurahan) Return Sekolah objects filtered by the desa_kelurahan column
 * @method array findByKodeWilayah(string $kode_wilayah) Return Sekolah objects filtered by the kode_wilayah column
 * @method array findByKodePos(string $kode_pos) Return Sekolah objects filtered by the kode_pos column
 * @method array findByLintang(string $lintang) Return Sekolah objects filtered by the lintang column
 * @method array findByBujur(string $bujur) Return Sekolah objects filtered by the bujur column
 * @method array findByNomorTelepon(string $nomor_telepon) Return Sekolah objects filtered by the nomor_telepon column
 * @method array findByNomorFax(string $nomor_fax) Return Sekolah objects filtered by the nomor_fax column
 * @method array findByEmail(string $email) Return Sekolah objects filtered by the email column
 * @method array findByWebsite(string $website) Return Sekolah objects filtered by the website column
 * @method array findByKebutuhanKhususId(int $kebutuhan_khusus_id) Return Sekolah objects filtered by the kebutuhan_khusus_id column
 * @method array findByStatusSekolah(string $status_sekolah) Return Sekolah objects filtered by the status_sekolah column
 * @method array findBySkPendirianSekolah(string $sk_pendirian_sekolah) Return Sekolah objects filtered by the sk_pendirian_sekolah column
 * @method array findByTanggalSkPendirian(string $tanggal_sk_pendirian) Return Sekolah objects filtered by the tanggal_sk_pendirian column
 * @method array findByStatusKepemilikanId(string $status_kepemilikan_id) Return Sekolah objects filtered by the status_kepemilikan_id column
 * @method array findByYayasanId(string $yayasan_id) Return Sekolah objects filtered by the yayasan_id column
 * @method array findBySkIzinOperasional(string $sk_izin_operasional) Return Sekolah objects filtered by the sk_izin_operasional column
 * @method array findByTanggalSkIzinOperasional(string $tanggal_sk_izin_operasional) Return Sekolah objects filtered by the tanggal_sk_izin_operasional column
 * @method array findByNoRekening(string $no_rekening) Return Sekolah objects filtered by the no_rekening column
 * @method array findByNamaBank(string $nama_bank) Return Sekolah objects filtered by the nama_bank column
 * @method array findByCabangKcpUnit(string $cabang_kcp_unit) Return Sekolah objects filtered by the cabang_kcp_unit column
 * @method array findByRekeningAtasNama(string $rekening_atas_nama) Return Sekolah objects filtered by the rekening_atas_nama column
 * @method array findByMbs(string $mbs) Return Sekolah objects filtered by the mbs column
 * @method array findByLuasTanahMilik(string $luas_tanah_milik) Return Sekolah objects filtered by the luas_tanah_milik column
 * @method array findByLuasTanahBukanMilik(string $luas_tanah_bukan_milik) Return Sekolah objects filtered by the luas_tanah_bukan_milik column
 * @method array findByKodeRegistrasi(string $kode_registrasi) Return Sekolah objects filtered by the kode_registrasi column
 * @method array findByFlag(string $flag) Return Sekolah objects filtered by the flag column
 * @method array findByPicId(string $pic_id) Return Sekolah objects filtered by the pic_id column
 * @method array findByLastUpdate(string $Last_update) Return Sekolah objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Sekolah objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Sekolah objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Sekolah objects filtered by the Updater_ID column
 *
 * @package    propel.generator.angulex.Model.om
 */
abstract class BaseSekolahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSekolahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodikmen', $modelName = 'angulex\\Model\\Sekolah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SekolahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SekolahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SekolahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SekolahQuery) {
            return $criteria;
        }
        $query = new SekolahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Sekolah|Sekolah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SekolahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sekolah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneBySekolahId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Sekolah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [sekolah_id], [nama], [nama_nomenklatur], [nss], [npsn], [bentuk_pendidikan_id], [alamat_jalan], [rt], [rw], [nama_dusun], [desa_kelurahan], [kode_wilayah], [kode_pos], [lintang], [bujur], [nomor_telepon], [nomor_fax], [email], [website], [kebutuhan_khusus_id], [status_sekolah], [sk_pendirian_sekolah], [tanggal_sk_pendirian], [status_kepemilikan_id], [yayasan_id], [sk_izin_operasional], [tanggal_sk_izin_operasional], [no_rekening], [nama_bank], [cabang_kcp_unit], [rekening_atas_nama], [mbs], [luas_tanah_milik], [luas_tanah_bukan_milik], [kode_registrasi], [flag], [pic_id], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [sekolah] WHERE [sekolah_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Sekolah();
            $obj->hydrate($row);
            SekolahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Sekolah|Sekolah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Sekolah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SekolahPeer::SEKOLAH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SekolahPeer::SEKOLAH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the nama_nomenklatur column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaNomenklatur('fooValue');   // WHERE nama_nomenklatur = 'fooValue'
     * $query->filterByNamaNomenklatur('%fooValue%'); // WHERE nama_nomenklatur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaNomenklatur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNamaNomenklatur($namaNomenklatur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaNomenklatur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaNomenklatur)) {
                $namaNomenklatur = str_replace('*', '%', $namaNomenklatur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NAMA_NOMENKLATUR, $namaNomenklatur, $comparison);
    }

    /**
     * Filter the query on the nss column
     *
     * Example usage:
     * <code>
     * $query->filterByNss('fooValue');   // WHERE nss = 'fooValue'
     * $query->filterByNss('%fooValue%'); // WHERE nss LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nss The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNss($nss = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nss)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nss)) {
                $nss = str_replace('*', '%', $nss);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NSS, $nss, $comparison);
    }

    /**
     * Filter the query on the npsn column
     *
     * Example usage:
     * <code>
     * $query->filterByNpsn('fooValue');   // WHERE npsn = 'fooValue'
     * $query->filterByNpsn('%fooValue%'); // WHERE npsn LIKE '%fooValue%'
     * </code>
     *
     * @param     string $npsn The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNpsn($npsn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($npsn)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $npsn)) {
                $npsn = str_replace('*', '%', $npsn);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NPSN, $npsn, $comparison);
    }

    /**
     * Filter the query on the bentuk_pendidikan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBentukPendidikanId(1234); // WHERE bentuk_pendidikan_id = 1234
     * $query->filterByBentukPendidikanId(array(12, 34)); // WHERE bentuk_pendidikan_id IN (12, 34)
     * $query->filterByBentukPendidikanId(array('min' => 12)); // WHERE bentuk_pendidikan_id >= 12
     * $query->filterByBentukPendidikanId(array('max' => 12)); // WHERE bentuk_pendidikan_id <= 12
     * </code>
     *
     * @see       filterByBentukPendidikanRelatedByBentukPendidikanId()
     *
     * @see       filterByBentukPendidikanRelatedByBentukPendidikanId()
     *
     * @param     mixed $bentukPendidikanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByBentukPendidikanId($bentukPendidikanId = null, $comparison = null)
    {
        if (is_array($bentukPendidikanId)) {
            $useMinMax = false;
            if (isset($bentukPendidikanId['min'])) {
                $this->addUsingAlias(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bentukPendidikanId['max'])) {
                $this->addUsingAlias(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikanId, $comparison);
    }

    /**
     * Filter the query on the alamat_jalan column
     *
     * Example usage:
     * <code>
     * $query->filterByAlamatJalan('fooValue');   // WHERE alamat_jalan = 'fooValue'
     * $query->filterByAlamatJalan('%fooValue%'); // WHERE alamat_jalan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alamatJalan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByAlamatJalan($alamatJalan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alamatJalan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alamatJalan)) {
                $alamatJalan = str_replace('*', '%', $alamatJalan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::ALAMAT_JALAN, $alamatJalan, $comparison);
    }

    /**
     * Filter the query on the rt column
     *
     * Example usage:
     * <code>
     * $query->filterByRt(1234); // WHERE rt = 1234
     * $query->filterByRt(array(12, 34)); // WHERE rt IN (12, 34)
     * $query->filterByRt(array('min' => 12)); // WHERE rt >= 12
     * $query->filterByRt(array('max' => 12)); // WHERE rt <= 12
     * </code>
     *
     * @param     mixed $rt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByRt($rt = null, $comparison = null)
    {
        if (is_array($rt)) {
            $useMinMax = false;
            if (isset($rt['min'])) {
                $this->addUsingAlias(SekolahPeer::RT, $rt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rt['max'])) {
                $this->addUsingAlias(SekolahPeer::RT, $rt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::RT, $rt, $comparison);
    }

    /**
     * Filter the query on the rw column
     *
     * Example usage:
     * <code>
     * $query->filterByRw(1234); // WHERE rw = 1234
     * $query->filterByRw(array(12, 34)); // WHERE rw IN (12, 34)
     * $query->filterByRw(array('min' => 12)); // WHERE rw >= 12
     * $query->filterByRw(array('max' => 12)); // WHERE rw <= 12
     * </code>
     *
     * @param     mixed $rw The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByRw($rw = null, $comparison = null)
    {
        if (is_array($rw)) {
            $useMinMax = false;
            if (isset($rw['min'])) {
                $this->addUsingAlias(SekolahPeer::RW, $rw['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rw['max'])) {
                $this->addUsingAlias(SekolahPeer::RW, $rw['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::RW, $rw, $comparison);
    }

    /**
     * Filter the query on the nama_dusun column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaDusun('fooValue');   // WHERE nama_dusun = 'fooValue'
     * $query->filterByNamaDusun('%fooValue%'); // WHERE nama_dusun LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaDusun The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNamaDusun($namaDusun = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaDusun)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaDusun)) {
                $namaDusun = str_replace('*', '%', $namaDusun);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NAMA_DUSUN, $namaDusun, $comparison);
    }

    /**
     * Filter the query on the desa_kelurahan column
     *
     * Example usage:
     * <code>
     * $query->filterByDesaKelurahan('fooValue');   // WHERE desa_kelurahan = 'fooValue'
     * $query->filterByDesaKelurahan('%fooValue%'); // WHERE desa_kelurahan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desaKelurahan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByDesaKelurahan($desaKelurahan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desaKelurahan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desaKelurahan)) {
                $desaKelurahan = str_replace('*', '%', $desaKelurahan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::DESA_KELURAHAN, $desaKelurahan, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the kode_pos column
     *
     * Example usage:
     * <code>
     * $query->filterByKodePos('fooValue');   // WHERE kode_pos = 'fooValue'
     * $query->filterByKodePos('%fooValue%'); // WHERE kode_pos LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodePos The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByKodePos($kodePos = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodePos)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodePos)) {
                $kodePos = str_replace('*', '%', $kodePos);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::KODE_POS, $kodePos, $comparison);
    }

    /**
     * Filter the query on the lintang column
     *
     * Example usage:
     * <code>
     * $query->filterByLintang(1234); // WHERE lintang = 1234
     * $query->filterByLintang(array(12, 34)); // WHERE lintang IN (12, 34)
     * $query->filterByLintang(array('min' => 12)); // WHERE lintang >= 12
     * $query->filterByLintang(array('max' => 12)); // WHERE lintang <= 12
     * </code>
     *
     * @param     mixed $lintang The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByLintang($lintang = null, $comparison = null)
    {
        if (is_array($lintang)) {
            $useMinMax = false;
            if (isset($lintang['min'])) {
                $this->addUsingAlias(SekolahPeer::LINTANG, $lintang['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lintang['max'])) {
                $this->addUsingAlias(SekolahPeer::LINTANG, $lintang['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::LINTANG, $lintang, $comparison);
    }

    /**
     * Filter the query on the bujur column
     *
     * Example usage:
     * <code>
     * $query->filterByBujur(1234); // WHERE bujur = 1234
     * $query->filterByBujur(array(12, 34)); // WHERE bujur IN (12, 34)
     * $query->filterByBujur(array('min' => 12)); // WHERE bujur >= 12
     * $query->filterByBujur(array('max' => 12)); // WHERE bujur <= 12
     * </code>
     *
     * @param     mixed $bujur The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByBujur($bujur = null, $comparison = null)
    {
        if (is_array($bujur)) {
            $useMinMax = false;
            if (isset($bujur['min'])) {
                $this->addUsingAlias(SekolahPeer::BUJUR, $bujur['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bujur['max'])) {
                $this->addUsingAlias(SekolahPeer::BUJUR, $bujur['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::BUJUR, $bujur, $comparison);
    }

    /**
     * Filter the query on the nomor_telepon column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorTelepon('fooValue');   // WHERE nomor_telepon = 'fooValue'
     * $query->filterByNomorTelepon('%fooValue%'); // WHERE nomor_telepon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorTelepon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNomorTelepon($nomorTelepon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorTelepon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorTelepon)) {
                $nomorTelepon = str_replace('*', '%', $nomorTelepon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NOMOR_TELEPON, $nomorTelepon, $comparison);
    }

    /**
     * Filter the query on the nomor_fax column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorFax('fooValue');   // WHERE nomor_fax = 'fooValue'
     * $query->filterByNomorFax('%fooValue%'); // WHERE nomor_fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorFax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNomorFax($nomorFax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorFax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorFax)) {
                $nomorFax = str_replace('*', '%', $nomorFax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NOMOR_FAX, $nomorFax, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsite('fooValue');   // WHERE website = 'fooValue'
     * $query->filterByWebsite('%fooValue%'); // WHERE website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByWebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $website)) {
                $website = str_replace('*', '%', $website);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query on the kebutuhan_khusus_id column
     *
     * Example usage:
     * <code>
     * $query->filterByKebutuhanKhususId(1234); // WHERE kebutuhan_khusus_id = 1234
     * $query->filterByKebutuhanKhususId(array(12, 34)); // WHERE kebutuhan_khusus_id IN (12, 34)
     * $query->filterByKebutuhanKhususId(array('min' => 12)); // WHERE kebutuhan_khusus_id >= 12
     * $query->filterByKebutuhanKhususId(array('max' => 12)); // WHERE kebutuhan_khusus_id <= 12
     * </code>
     *
     * @see       filterByKebutuhanKhususRelatedByKebutuhanKhususId()
     *
     * @see       filterByKebutuhanKhususRelatedByKebutuhanKhususId()
     *
     * @param     mixed $kebutuhanKhususId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByKebutuhanKhususId($kebutuhanKhususId = null, $comparison = null)
    {
        if (is_array($kebutuhanKhususId)) {
            $useMinMax = false;
            if (isset($kebutuhanKhususId['min'])) {
                $this->addUsingAlias(SekolahPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhususId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kebutuhanKhususId['max'])) {
                $this->addUsingAlias(SekolahPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhususId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhususId, $comparison);
    }

    /**
     * Filter the query on the status_sekolah column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusSekolah(1234); // WHERE status_sekolah = 1234
     * $query->filterByStatusSekolah(array(12, 34)); // WHERE status_sekolah IN (12, 34)
     * $query->filterByStatusSekolah(array('min' => 12)); // WHERE status_sekolah >= 12
     * $query->filterByStatusSekolah(array('max' => 12)); // WHERE status_sekolah <= 12
     * </code>
     *
     * @param     mixed $statusSekolah The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByStatusSekolah($statusSekolah = null, $comparison = null)
    {
        if (is_array($statusSekolah)) {
            $useMinMax = false;
            if (isset($statusSekolah['min'])) {
                $this->addUsingAlias(SekolahPeer::STATUS_SEKOLAH, $statusSekolah['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusSekolah['max'])) {
                $this->addUsingAlias(SekolahPeer::STATUS_SEKOLAH, $statusSekolah['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::STATUS_SEKOLAH, $statusSekolah, $comparison);
    }

    /**
     * Filter the query on the sk_pendirian_sekolah column
     *
     * Example usage:
     * <code>
     * $query->filterBySkPendirianSekolah('fooValue');   // WHERE sk_pendirian_sekolah = 'fooValue'
     * $query->filterBySkPendirianSekolah('%fooValue%'); // WHERE sk_pendirian_sekolah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skPendirianSekolah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterBySkPendirianSekolah($skPendirianSekolah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skPendirianSekolah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $skPendirianSekolah)) {
                $skPendirianSekolah = str_replace('*', '%', $skPendirianSekolah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::SK_PENDIRIAN_SEKOLAH, $skPendirianSekolah, $comparison);
    }

    /**
     * Filter the query on the tanggal_sk_pendirian column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalSkPendirian('fooValue');   // WHERE tanggal_sk_pendirian = 'fooValue'
     * $query->filterByTanggalSkPendirian('%fooValue%'); // WHERE tanggal_sk_pendirian LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalSkPendirian The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByTanggalSkPendirian($tanggalSkPendirian = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalSkPendirian)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalSkPendirian)) {
                $tanggalSkPendirian = str_replace('*', '%', $tanggalSkPendirian);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::TANGGAL_SK_PENDIRIAN, $tanggalSkPendirian, $comparison);
    }

    /**
     * Filter the query on the status_kepemilikan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusKepemilikanId(1234); // WHERE status_kepemilikan_id = 1234
     * $query->filterByStatusKepemilikanId(array(12, 34)); // WHERE status_kepemilikan_id IN (12, 34)
     * $query->filterByStatusKepemilikanId(array('min' => 12)); // WHERE status_kepemilikan_id >= 12
     * $query->filterByStatusKepemilikanId(array('max' => 12)); // WHERE status_kepemilikan_id <= 12
     * </code>
     *
     * @see       filterByStatusKepemilikanRelatedByStatusKepemilikanId()
     *
     * @see       filterByStatusKepemilikanRelatedByStatusKepemilikanId()
     *
     * @param     mixed $statusKepemilikanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByStatusKepemilikanId($statusKepemilikanId = null, $comparison = null)
    {
        if (is_array($statusKepemilikanId)) {
            $useMinMax = false;
            if (isset($statusKepemilikanId['min'])) {
                $this->addUsingAlias(SekolahPeer::STATUS_KEPEMILIKAN_ID, $statusKepemilikanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusKepemilikanId['max'])) {
                $this->addUsingAlias(SekolahPeer::STATUS_KEPEMILIKAN_ID, $statusKepemilikanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::STATUS_KEPEMILIKAN_ID, $statusKepemilikanId, $comparison);
    }

    /**
     * Filter the query on the yayasan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByYayasanId('fooValue');   // WHERE yayasan_id = 'fooValue'
     * $query->filterByYayasanId('%fooValue%'); // WHERE yayasan_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $yayasanId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByYayasanId($yayasanId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($yayasanId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $yayasanId)) {
                $yayasanId = str_replace('*', '%', $yayasanId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::YAYASAN_ID, $yayasanId, $comparison);
    }

    /**
     * Filter the query on the sk_izin_operasional column
     *
     * Example usage:
     * <code>
     * $query->filterBySkIzinOperasional('fooValue');   // WHERE sk_izin_operasional = 'fooValue'
     * $query->filterBySkIzinOperasional('%fooValue%'); // WHERE sk_izin_operasional LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skIzinOperasional The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterBySkIzinOperasional($skIzinOperasional = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skIzinOperasional)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $skIzinOperasional)) {
                $skIzinOperasional = str_replace('*', '%', $skIzinOperasional);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::SK_IZIN_OPERASIONAL, $skIzinOperasional, $comparison);
    }

    /**
     * Filter the query on the tanggal_sk_izin_operasional column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalSkIzinOperasional('fooValue');   // WHERE tanggal_sk_izin_operasional = 'fooValue'
     * $query->filterByTanggalSkIzinOperasional('%fooValue%'); // WHERE tanggal_sk_izin_operasional LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalSkIzinOperasional The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByTanggalSkIzinOperasional($tanggalSkIzinOperasional = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalSkIzinOperasional)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalSkIzinOperasional)) {
                $tanggalSkIzinOperasional = str_replace('*', '%', $tanggalSkIzinOperasional);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::TANGGAL_SK_IZIN_OPERASIONAL, $tanggalSkIzinOperasional, $comparison);
    }

    /**
     * Filter the query on the no_rekening column
     *
     * Example usage:
     * <code>
     * $query->filterByNoRekening('fooValue');   // WHERE no_rekening = 'fooValue'
     * $query->filterByNoRekening('%fooValue%'); // WHERE no_rekening LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noRekening The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNoRekening($noRekening = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noRekening)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $noRekening)) {
                $noRekening = str_replace('*', '%', $noRekening);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NO_REKENING, $noRekening, $comparison);
    }

    /**
     * Filter the query on the nama_bank column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaBank('fooValue');   // WHERE nama_bank = 'fooValue'
     * $query->filterByNamaBank('%fooValue%'); // WHERE nama_bank LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaBank The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByNamaBank($namaBank = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaBank)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaBank)) {
                $namaBank = str_replace('*', '%', $namaBank);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::NAMA_BANK, $namaBank, $comparison);
    }

    /**
     * Filter the query on the cabang_kcp_unit column
     *
     * Example usage:
     * <code>
     * $query->filterByCabangKcpUnit('fooValue');   // WHERE cabang_kcp_unit = 'fooValue'
     * $query->filterByCabangKcpUnit('%fooValue%'); // WHERE cabang_kcp_unit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cabangKcpUnit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByCabangKcpUnit($cabangKcpUnit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cabangKcpUnit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cabangKcpUnit)) {
                $cabangKcpUnit = str_replace('*', '%', $cabangKcpUnit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::CABANG_KCP_UNIT, $cabangKcpUnit, $comparison);
    }

    /**
     * Filter the query on the rekening_atas_nama column
     *
     * Example usage:
     * <code>
     * $query->filterByRekeningAtasNama('fooValue');   // WHERE rekening_atas_nama = 'fooValue'
     * $query->filterByRekeningAtasNama('%fooValue%'); // WHERE rekening_atas_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rekeningAtasNama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByRekeningAtasNama($rekeningAtasNama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rekeningAtasNama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $rekeningAtasNama)) {
                $rekeningAtasNama = str_replace('*', '%', $rekeningAtasNama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::REKENING_ATAS_NAMA, $rekeningAtasNama, $comparison);
    }

    /**
     * Filter the query on the mbs column
     *
     * Example usage:
     * <code>
     * $query->filterByMbs(1234); // WHERE mbs = 1234
     * $query->filterByMbs(array(12, 34)); // WHERE mbs IN (12, 34)
     * $query->filterByMbs(array('min' => 12)); // WHERE mbs >= 12
     * $query->filterByMbs(array('max' => 12)); // WHERE mbs <= 12
     * </code>
     *
     * @param     mixed $mbs The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByMbs($mbs = null, $comparison = null)
    {
        if (is_array($mbs)) {
            $useMinMax = false;
            if (isset($mbs['min'])) {
                $this->addUsingAlias(SekolahPeer::MBS, $mbs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mbs['max'])) {
                $this->addUsingAlias(SekolahPeer::MBS, $mbs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::MBS, $mbs, $comparison);
    }

    /**
     * Filter the query on the luas_tanah_milik column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasTanahMilik(1234); // WHERE luas_tanah_milik = 1234
     * $query->filterByLuasTanahMilik(array(12, 34)); // WHERE luas_tanah_milik IN (12, 34)
     * $query->filterByLuasTanahMilik(array('min' => 12)); // WHERE luas_tanah_milik >= 12
     * $query->filterByLuasTanahMilik(array('max' => 12)); // WHERE luas_tanah_milik <= 12
     * </code>
     *
     * @param     mixed $luasTanahMilik The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByLuasTanahMilik($luasTanahMilik = null, $comparison = null)
    {
        if (is_array($luasTanahMilik)) {
            $useMinMax = false;
            if (isset($luasTanahMilik['min'])) {
                $this->addUsingAlias(SekolahPeer::LUAS_TANAH_MILIK, $luasTanahMilik['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasTanahMilik['max'])) {
                $this->addUsingAlias(SekolahPeer::LUAS_TANAH_MILIK, $luasTanahMilik['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::LUAS_TANAH_MILIK, $luasTanahMilik, $comparison);
    }

    /**
     * Filter the query on the luas_tanah_bukan_milik column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasTanahBukanMilik(1234); // WHERE luas_tanah_bukan_milik = 1234
     * $query->filterByLuasTanahBukanMilik(array(12, 34)); // WHERE luas_tanah_bukan_milik IN (12, 34)
     * $query->filterByLuasTanahBukanMilik(array('min' => 12)); // WHERE luas_tanah_bukan_milik >= 12
     * $query->filterByLuasTanahBukanMilik(array('max' => 12)); // WHERE luas_tanah_bukan_milik <= 12
     * </code>
     *
     * @param     mixed $luasTanahBukanMilik The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByLuasTanahBukanMilik($luasTanahBukanMilik = null, $comparison = null)
    {
        if (is_array($luasTanahBukanMilik)) {
            $useMinMax = false;
            if (isset($luasTanahBukanMilik['min'])) {
                $this->addUsingAlias(SekolahPeer::LUAS_TANAH_BUKAN_MILIK, $luasTanahBukanMilik['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasTanahBukanMilik['max'])) {
                $this->addUsingAlias(SekolahPeer::LUAS_TANAH_BUKAN_MILIK, $luasTanahBukanMilik['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::LUAS_TANAH_BUKAN_MILIK, $luasTanahBukanMilik, $comparison);
    }

    /**
     * Filter the query on the kode_registrasi column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeRegistrasi(1234); // WHERE kode_registrasi = 1234
     * $query->filterByKodeRegistrasi(array(12, 34)); // WHERE kode_registrasi IN (12, 34)
     * $query->filterByKodeRegistrasi(array('min' => 12)); // WHERE kode_registrasi >= 12
     * $query->filterByKodeRegistrasi(array('max' => 12)); // WHERE kode_registrasi <= 12
     * </code>
     *
     * @param     mixed $kodeRegistrasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByKodeRegistrasi($kodeRegistrasi = null, $comparison = null)
    {
        if (is_array($kodeRegistrasi)) {
            $useMinMax = false;
            if (isset($kodeRegistrasi['min'])) {
                $this->addUsingAlias(SekolahPeer::KODE_REGISTRASI, $kodeRegistrasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kodeRegistrasi['max'])) {
                $this->addUsingAlias(SekolahPeer::KODE_REGISTRASI, $kodeRegistrasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::KODE_REGISTRASI, $kodeRegistrasi, $comparison);
    }

    /**
     * Filter the query on the flag column
     *
     * Example usage:
     * <code>
     * $query->filterByFlag('fooValue');   // WHERE flag = 'fooValue'
     * $query->filterByFlag('%fooValue%'); // WHERE flag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $flag The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByFlag($flag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($flag)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $flag)) {
                $flag = str_replace('*', '%', $flag);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::FLAG, $flag, $comparison);
    }

    /**
     * Filter the query on the pic_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPicId('fooValue');   // WHERE pic_id = 'fooValue'
     * $query->filterByPicId('%fooValue%'); // WHERE pic_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $picId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByPicId($picId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($picId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $picId)) {
                $picId = str_replace('*', '%', $picId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::PIC_ID, $picId, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(SekolahPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(SekolahPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(SekolahPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(SekolahPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(SekolahPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(SekolahPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SekolahPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SekolahPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related BentukPendidikan object
     *
     * @param   BentukPendidikan|PropelObjectCollection $bentukPendidikan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBentukPendidikanRelatedByBentukPendidikanId($bentukPendidikan, $comparison = null)
    {
        if ($bentukPendidikan instanceof BentukPendidikan) {
            return $this
                ->addUsingAlias(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikan->getBentukPendidikanId(), $comparison);
        } elseif ($bentukPendidikan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikan->toKeyValue('PrimaryKey', 'BentukPendidikanId'), $comparison);
        } else {
            throw new PropelException('filterByBentukPendidikanRelatedByBentukPendidikanId() only accepts arguments of type BentukPendidikan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BentukPendidikanRelatedByBentukPendidikanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinBentukPendidikanRelatedByBentukPendidikanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BentukPendidikanRelatedByBentukPendidikanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BentukPendidikanRelatedByBentukPendidikanId');
        }

        return $this;
    }

    /**
     * Use the BentukPendidikanRelatedByBentukPendidikanId relation BentukPendidikan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BentukPendidikanQuery A secondary query class using the current class as primary query
     */
    public function useBentukPendidikanRelatedByBentukPendidikanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBentukPendidikanRelatedByBentukPendidikanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BentukPendidikanRelatedByBentukPendidikanId', '\angulex\Model\BentukPendidikanQuery');
    }

    /**
     * Filter the query by a related BentukPendidikan object
     *
     * @param   BentukPendidikan|PropelObjectCollection $bentukPendidikan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBentukPendidikanRelatedByBentukPendidikanId($bentukPendidikan, $comparison = null)
    {
        if ($bentukPendidikan instanceof BentukPendidikan) {
            return $this
                ->addUsingAlias(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikan->getBentukPendidikanId(), $comparison);
        } elseif ($bentukPendidikan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikan->toKeyValue('PrimaryKey', 'BentukPendidikanId'), $comparison);
        } else {
            throw new PropelException('filterByBentukPendidikanRelatedByBentukPendidikanId() only accepts arguments of type BentukPendidikan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BentukPendidikanRelatedByBentukPendidikanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinBentukPendidikanRelatedByBentukPendidikanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BentukPendidikanRelatedByBentukPendidikanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BentukPendidikanRelatedByBentukPendidikanId');
        }

        return $this;
    }

    /**
     * Use the BentukPendidikanRelatedByBentukPendidikanId relation BentukPendidikan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BentukPendidikanQuery A secondary query class using the current class as primary query
     */
    public function useBentukPendidikanRelatedByBentukPendidikanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBentukPendidikanRelatedByBentukPendidikanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BentukPendidikanRelatedByBentukPendidikanId', '\angulex\Model\BentukPendidikanQuery');
    }

    /**
     * Filter the query by a related KebutuhanKhusus object
     *
     * @param   KebutuhanKhusus|PropelObjectCollection $kebutuhanKhusus The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByKebutuhanKhususRelatedByKebutuhanKhususId($kebutuhanKhusus, $comparison = null)
    {
        if ($kebutuhanKhusus instanceof KebutuhanKhusus) {
            return $this
                ->addUsingAlias(SekolahPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhusus->getKebutuhanKhususId(), $comparison);
        } elseif ($kebutuhanKhusus instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhusus->toKeyValue('PrimaryKey', 'KebutuhanKhususId'), $comparison);
        } else {
            throw new PropelException('filterByKebutuhanKhususRelatedByKebutuhanKhususId() only accepts arguments of type KebutuhanKhusus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('KebutuhanKhususRelatedByKebutuhanKhususId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'KebutuhanKhususRelatedByKebutuhanKhususId');
        }

        return $this;
    }

    /**
     * Use the KebutuhanKhususRelatedByKebutuhanKhususId relation KebutuhanKhusus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\KebutuhanKhususQuery A secondary query class using the current class as primary query
     */
    public function useKebutuhanKhususRelatedByKebutuhanKhususIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'KebutuhanKhususRelatedByKebutuhanKhususId', '\angulex\Model\KebutuhanKhususQuery');
    }

    /**
     * Filter the query by a related KebutuhanKhusus object
     *
     * @param   KebutuhanKhusus|PropelObjectCollection $kebutuhanKhusus The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByKebutuhanKhususRelatedByKebutuhanKhususId($kebutuhanKhusus, $comparison = null)
    {
        if ($kebutuhanKhusus instanceof KebutuhanKhusus) {
            return $this
                ->addUsingAlias(SekolahPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhusus->getKebutuhanKhususId(), $comparison);
        } elseif ($kebutuhanKhusus instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahPeer::KEBUTUHAN_KHUSUS_ID, $kebutuhanKhusus->toKeyValue('PrimaryKey', 'KebutuhanKhususId'), $comparison);
        } else {
            throw new PropelException('filterByKebutuhanKhususRelatedByKebutuhanKhususId() only accepts arguments of type KebutuhanKhusus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the KebutuhanKhususRelatedByKebutuhanKhususId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('KebutuhanKhususRelatedByKebutuhanKhususId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'KebutuhanKhususRelatedByKebutuhanKhususId');
        }

        return $this;
    }

    /**
     * Use the KebutuhanKhususRelatedByKebutuhanKhususId relation KebutuhanKhusus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\KebutuhanKhususQuery A secondary query class using the current class as primary query
     */
    public function useKebutuhanKhususRelatedByKebutuhanKhususIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinKebutuhanKhususRelatedByKebutuhanKhususId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'KebutuhanKhususRelatedByKebutuhanKhususId', '\angulex\Model\KebutuhanKhususQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(SekolahPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahPeer::KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related MstWilayah object
     *
     * @param   MstWilayah|PropelObjectCollection $mstWilayah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMstWilayahRelatedByKodeWilayah($mstWilayah, $comparison = null)
    {
        if ($mstWilayah instanceof MstWilayah) {
            return $this
                ->addUsingAlias(SekolahPeer::KODE_WILAYAH, $mstWilayah->getKodeWilayah(), $comparison);
        } elseif ($mstWilayah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahPeer::KODE_WILAYAH, $mstWilayah->toKeyValue('PrimaryKey', 'KodeWilayah'), $comparison);
        } else {
            throw new PropelException('filterByMstWilayahRelatedByKodeWilayah() only accepts arguments of type MstWilayah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MstWilayahRelatedByKodeWilayah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinMstWilayahRelatedByKodeWilayah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MstWilayahRelatedByKodeWilayah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MstWilayahRelatedByKodeWilayah');
        }

        return $this;
    }

    /**
     * Use the MstWilayahRelatedByKodeWilayah relation MstWilayah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MstWilayahQuery A secondary query class using the current class as primary query
     */
    public function useMstWilayahRelatedByKodeWilayahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMstWilayahRelatedByKodeWilayah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MstWilayahRelatedByKodeWilayah', '\angulex\Model\MstWilayahQuery');
    }

    /**
     * Filter the query by a related StatusKepemilikan object
     *
     * @param   StatusKepemilikan|PropelObjectCollection $statusKepemilikan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByStatusKepemilikanRelatedByStatusKepemilikanId($statusKepemilikan, $comparison = null)
    {
        if ($statusKepemilikan instanceof StatusKepemilikan) {
            return $this
                ->addUsingAlias(SekolahPeer::STATUS_KEPEMILIKAN_ID, $statusKepemilikan->getStatusKepemilikanId(), $comparison);
        } elseif ($statusKepemilikan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahPeer::STATUS_KEPEMILIKAN_ID, $statusKepemilikan->toKeyValue('PrimaryKey', 'StatusKepemilikanId'), $comparison);
        } else {
            throw new PropelException('filterByStatusKepemilikanRelatedByStatusKepemilikanId() only accepts arguments of type StatusKepemilikan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusKepemilikanRelatedByStatusKepemilikanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusKepemilikanRelatedByStatusKepemilikanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusKepemilikanRelatedByStatusKepemilikanId');
        }

        return $this;
    }

    /**
     * Use the StatusKepemilikanRelatedByStatusKepemilikanId relation StatusKepemilikan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\StatusKepemilikanQuery A secondary query class using the current class as primary query
     */
    public function useStatusKepemilikanRelatedByStatusKepemilikanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusKepemilikanRelatedByStatusKepemilikanId', '\angulex\Model\StatusKepemilikanQuery');
    }

    /**
     * Filter the query by a related StatusKepemilikan object
     *
     * @param   StatusKepemilikan|PropelObjectCollection $statusKepemilikan The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByStatusKepemilikanRelatedByStatusKepemilikanId($statusKepemilikan, $comparison = null)
    {
        if ($statusKepemilikan instanceof StatusKepemilikan) {
            return $this
                ->addUsingAlias(SekolahPeer::STATUS_KEPEMILIKAN_ID, $statusKepemilikan->getStatusKepemilikanId(), $comparison);
        } elseif ($statusKepemilikan instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SekolahPeer::STATUS_KEPEMILIKAN_ID, $statusKepemilikan->toKeyValue('PrimaryKey', 'StatusKepemilikanId'), $comparison);
        } else {
            throw new PropelException('filterByStatusKepemilikanRelatedByStatusKepemilikanId() only accepts arguments of type StatusKepemilikan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusKepemilikanRelatedByStatusKepemilikanId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusKepemilikanRelatedByStatusKepemilikanId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusKepemilikanRelatedByStatusKepemilikanId');
        }

        return $this;
    }

    /**
     * Use the StatusKepemilikanRelatedByStatusKepemilikanId relation StatusKepemilikan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\StatusKepemilikanQuery A secondary query class using the current class as primary query
     */
    public function useStatusKepemilikanRelatedByStatusKepemilikanIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusKepemilikanRelatedByStatusKepemilikanId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusKepemilikanRelatedByStatusKepemilikanId', '\angulex\Model\StatusKepemilikanQuery');
    }

    /**
     * Filter the query by a related BukuAlat object
     *
     * @param   BukuAlat|PropelObjectCollection $bukuAlat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatRelatedBySekolahId($bukuAlat, $comparison = null)
    {
        if ($bukuAlat instanceof BukuAlat) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $bukuAlat->getSekolahId(), $comparison);
        } elseif ($bukuAlat instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($bukuAlat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlatRelatedBySekolahId() only accepts arguments of type BukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinBukuAlatRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatRelatedBySekolahId relation BukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBukuAlatRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatRelatedBySekolahId', '\angulex\Model\BukuAlatQuery');
    }

    /**
     * Filter the query by a related BukuAlat object
     *
     * @param   BukuAlat|PropelObjectCollection $bukuAlat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlatRelatedBySekolahId($bukuAlat, $comparison = null)
    {
        if ($bukuAlat instanceof BukuAlat) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $bukuAlat->getSekolahId(), $comparison);
        } elseif ($bukuAlat instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($bukuAlat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlatRelatedBySekolahId() only accepts arguments of type BukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlatRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinBukuAlatRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlatRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlatRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the BukuAlatRelatedBySekolahId relation BukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBukuAlatRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlatRelatedBySekolahId', '\angulex\Model\BukuAlatQuery');
    }

    /**
     * Filter the query by a related JurusanSp object
     *
     * @param   JurusanSp|PropelObjectCollection $jurusanSp  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJurusanSpRelatedBySekolahId($jurusanSp, $comparison = null)
    {
        if ($jurusanSp instanceof JurusanSp) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $jurusanSp->getSekolahId(), $comparison);
        } elseif ($jurusanSp instanceof PropelObjectCollection) {
            return $this
                ->useJurusanSpRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($jurusanSp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJurusanSpRelatedBySekolahId() only accepts arguments of type JurusanSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JurusanSpRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinJurusanSpRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JurusanSpRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JurusanSpRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the JurusanSpRelatedBySekolahId relation JurusanSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JurusanSpQuery A secondary query class using the current class as primary query
     */
    public function useJurusanSpRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJurusanSpRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JurusanSpRelatedBySekolahId', '\angulex\Model\JurusanSpQuery');
    }

    /**
     * Filter the query by a related JurusanSp object
     *
     * @param   JurusanSp|PropelObjectCollection $jurusanSp  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJurusanSpRelatedBySekolahId($jurusanSp, $comparison = null)
    {
        if ($jurusanSp instanceof JurusanSp) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $jurusanSp->getSekolahId(), $comparison);
        } elseif ($jurusanSp instanceof PropelObjectCollection) {
            return $this
                ->useJurusanSpRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($jurusanSp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJurusanSpRelatedBySekolahId() only accepts arguments of type JurusanSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JurusanSpRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinJurusanSpRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JurusanSpRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JurusanSpRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the JurusanSpRelatedBySekolahId relation JurusanSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\JurusanSpQuery A secondary query class using the current class as primary query
     */
    public function useJurusanSpRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJurusanSpRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JurusanSpRelatedBySekolahId', '\angulex\Model\JurusanSpQuery');
    }

    /**
     * Filter the query by a related SekolahLongitudinal object
     *
     * @param   SekolahLongitudinal|PropelObjectCollection $sekolahLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahLongitudinalRelatedBySekolahId($sekolahLongitudinal, $comparison = null)
    {
        if ($sekolahLongitudinal instanceof SekolahLongitudinal) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sekolahLongitudinal->getSekolahId(), $comparison);
        } elseif ($sekolahLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useSekolahLongitudinalRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sekolahLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySekolahLongitudinalRelatedBySekolahId() only accepts arguments of type SekolahLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahLongitudinalRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSekolahLongitudinalRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahLongitudinalRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahLongitudinalRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahLongitudinalRelatedBySekolahId relation SekolahLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useSekolahLongitudinalRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahLongitudinalRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahLongitudinalRelatedBySekolahId', '\angulex\Model\SekolahLongitudinalQuery');
    }

    /**
     * Filter the query by a related SekolahLongitudinal object
     *
     * @param   SekolahLongitudinal|PropelObjectCollection $sekolahLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolahLongitudinalRelatedBySekolahId($sekolahLongitudinal, $comparison = null)
    {
        if ($sekolahLongitudinal instanceof SekolahLongitudinal) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sekolahLongitudinal->getSekolahId(), $comparison);
        } elseif ($sekolahLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->useSekolahLongitudinalRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sekolahLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySekolahLongitudinalRelatedBySekolahId() only accepts arguments of type SekolahLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SekolahLongitudinalRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSekolahLongitudinalRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SekolahLongitudinalRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SekolahLongitudinalRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SekolahLongitudinalRelatedBySekolahId relation SekolahLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SekolahLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function useSekolahLongitudinalRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolahLongitudinalRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SekolahLongitudinalRelatedBySekolahId', '\angulex\Model\SekolahLongitudinalQuery');
    }

    /**
     * Filter the query by a related PesertaDidik object
     *
     * @param   PesertaDidik|PropelObjectCollection $pesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikRelatedBySekolahId($pesertaDidik, $comparison = null)
    {
        if ($pesertaDidik instanceof PesertaDidik) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $pesertaDidik->getSekolahId(), $comparison);
        } elseif ($pesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->usePesertaDidikRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($pesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPesertaDidikRelatedBySekolahId() only accepts arguments of type PesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPesertaDidikRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikRelatedBySekolahId relation PesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikRelatedBySekolahId', '\angulex\Model\PesertaDidikQuery');
    }

    /**
     * Filter the query by a related PesertaDidik object
     *
     * @param   PesertaDidik|PropelObjectCollection $pesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikRelatedBySekolahId($pesertaDidik, $comparison = null)
    {
        if ($pesertaDidik instanceof PesertaDidik) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $pesertaDidik->getSekolahId(), $comparison);
        } elseif ($pesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->usePesertaDidikRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($pesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPesertaDidikRelatedBySekolahId() only accepts arguments of type PesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPesertaDidikRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikRelatedBySekolahId relation PesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikRelatedBySekolahId', '\angulex\Model\PesertaDidikQuery');
    }

    /**
     * Filter the query by a related Sanitasi object
     *
     * @param   Sanitasi|PropelObjectCollection $sanitasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySanitasiRelatedBySekolahId($sanitasi, $comparison = null)
    {
        if ($sanitasi instanceof Sanitasi) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sanitasi->getSekolahId(), $comparison);
        } elseif ($sanitasi instanceof PropelObjectCollection) {
            return $this
                ->useSanitasiRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sanitasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySanitasiRelatedBySekolahId() only accepts arguments of type Sanitasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SanitasiRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSanitasiRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SanitasiRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SanitasiRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SanitasiRelatedBySekolahId relation Sanitasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SanitasiQuery A secondary query class using the current class as primary query
     */
    public function useSanitasiRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSanitasiRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SanitasiRelatedBySekolahId', '\angulex\Model\SanitasiQuery');
    }

    /**
     * Filter the query by a related Sanitasi object
     *
     * @param   Sanitasi|PropelObjectCollection $sanitasi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySanitasiRelatedBySekolahId($sanitasi, $comparison = null)
    {
        if ($sanitasi instanceof Sanitasi) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sanitasi->getSekolahId(), $comparison);
        } elseif ($sanitasi instanceof PropelObjectCollection) {
            return $this
                ->useSanitasiRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sanitasi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySanitasiRelatedBySekolahId() only accepts arguments of type Sanitasi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SanitasiRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSanitasiRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SanitasiRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SanitasiRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SanitasiRelatedBySekolahId relation Sanitasi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SanitasiQuery A secondary query class using the current class as primary query
     */
    public function useSanitasiRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSanitasiRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SanitasiRelatedBySekolahId', '\angulex\Model\SanitasiQuery');
    }

    /**
     * Filter the query by a related RegistrasiPesertaDidik object
     *
     * @param   RegistrasiPesertaDidik|PropelObjectCollection $registrasiPesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRegistrasiPesertaDidikRelatedBySekolahId($registrasiPesertaDidik, $comparison = null)
    {
        if ($registrasiPesertaDidik instanceof RegistrasiPesertaDidik) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $registrasiPesertaDidik->getSekolahId(), $comparison);
        } elseif ($registrasiPesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->useRegistrasiPesertaDidikRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($registrasiPesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRegistrasiPesertaDidikRelatedBySekolahId() only accepts arguments of type RegistrasiPesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RegistrasiPesertaDidikRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RegistrasiPesertaDidikRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RegistrasiPesertaDidikRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the RegistrasiPesertaDidikRelatedBySekolahId relation RegistrasiPesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RegistrasiPesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function useRegistrasiPesertaDidikRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RegistrasiPesertaDidikRelatedBySekolahId', '\angulex\Model\RegistrasiPesertaDidikQuery');
    }

    /**
     * Filter the query by a related RegistrasiPesertaDidik object
     *
     * @param   RegistrasiPesertaDidik|PropelObjectCollection $registrasiPesertaDidik  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRegistrasiPesertaDidikRelatedBySekolahId($registrasiPesertaDidik, $comparison = null)
    {
        if ($registrasiPesertaDidik instanceof RegistrasiPesertaDidik) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $registrasiPesertaDidik->getSekolahId(), $comparison);
        } elseif ($registrasiPesertaDidik instanceof PropelObjectCollection) {
            return $this
                ->useRegistrasiPesertaDidikRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($registrasiPesertaDidik->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRegistrasiPesertaDidikRelatedBySekolahId() only accepts arguments of type RegistrasiPesertaDidik or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RegistrasiPesertaDidikRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RegistrasiPesertaDidikRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RegistrasiPesertaDidikRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the RegistrasiPesertaDidikRelatedBySekolahId relation RegistrasiPesertaDidik object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RegistrasiPesertaDidikQuery A secondary query class using the current class as primary query
     */
    public function useRegistrasiPesertaDidikRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRegistrasiPesertaDidikRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RegistrasiPesertaDidikRelatedBySekolahId', '\angulex\Model\RegistrasiPesertaDidikQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedBySekolahId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $pengguna->getSekolahId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            return $this
                ->usePenggunaRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($pengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPenggunaRelatedBySekolahId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedBySekolahId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedBySekolahId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPenggunaRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedBySekolahId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related Pengguna object
     *
     * @param   Pengguna|PropelObjectCollection $pengguna  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPenggunaRelatedBySekolahId($pengguna, $comparison = null)
    {
        if ($pengguna instanceof Pengguna) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $pengguna->getSekolahId(), $comparison);
        } elseif ($pengguna instanceof PropelObjectCollection) {
            return $this
                ->usePenggunaRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($pengguna->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPenggunaRelatedBySekolahId() only accepts arguments of type Pengguna or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPenggunaRelatedBySekolahId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PenggunaRelatedBySekolahId relation Pengguna object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PenggunaQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPenggunaRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaRelatedBySekolahId', '\angulex\Model\PenggunaQuery');
    }

    /**
     * Filter the query by a related PesertaDidikBaru object
     *
     * @param   PesertaDidikBaru|PropelObjectCollection $pesertaDidikBaru  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikBaruRelatedBySekolahId($pesertaDidikBaru, $comparison = null)
    {
        if ($pesertaDidikBaru instanceof PesertaDidikBaru) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $pesertaDidikBaru->getSekolahId(), $comparison);
        } elseif ($pesertaDidikBaru instanceof PropelObjectCollection) {
            return $this
                ->usePesertaDidikBaruRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($pesertaDidikBaru->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPesertaDidikBaruRelatedBySekolahId() only accepts arguments of type PesertaDidikBaru or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikBaruRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPesertaDidikBaruRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikBaruRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikBaruRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikBaruRelatedBySekolahId relation PesertaDidikBaru object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikBaruQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikBaruRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikBaruRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikBaruRelatedBySekolahId', '\angulex\Model\PesertaDidikBaruQuery');
    }

    /**
     * Filter the query by a related PesertaDidikBaru object
     *
     * @param   PesertaDidikBaru|PropelObjectCollection $pesertaDidikBaru  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPesertaDidikBaruRelatedBySekolahId($pesertaDidikBaru, $comparison = null)
    {
        if ($pesertaDidikBaru instanceof PesertaDidikBaru) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $pesertaDidikBaru->getSekolahId(), $comparison);
        } elseif ($pesertaDidikBaru instanceof PropelObjectCollection) {
            return $this
                ->usePesertaDidikBaruRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($pesertaDidikBaru->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPesertaDidikBaruRelatedBySekolahId() only accepts arguments of type PesertaDidikBaru or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PesertaDidikBaruRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPesertaDidikBaruRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PesertaDidikBaruRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PesertaDidikBaruRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PesertaDidikBaruRelatedBySekolahId relation PesertaDidikBaru object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PesertaDidikBaruQuery A secondary query class using the current class as primary query
     */
    public function usePesertaDidikBaruRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPesertaDidikBaruRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PesertaDidikBaruRelatedBySekolahId', '\angulex\Model\PesertaDidikBaruQuery');
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaRelatedBySekolahId($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sarana->getSekolahId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaRelatedBySekolahId() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSaranaRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SaranaRelatedBySekolahId relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaRelatedBySekolahId', '\angulex\Model\SaranaQuery');
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySaranaRelatedBySekolahId($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sarana->getSekolahId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaranaRelatedBySekolahId() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaranaRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSaranaRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaranaRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaranaRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SaranaRelatedBySekolahId relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaranaRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaranaRelatedBySekolahId', '\angulex\Model\SaranaQuery');
    }

    /**
     * Filter the query by a related AkreditasiSp object
     *
     * @param   AkreditasiSp|PropelObjectCollection $akreditasiSp  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAkreditasiSpRelatedBySekolahId($akreditasiSp, $comparison = null)
    {
        if ($akreditasiSp instanceof AkreditasiSp) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $akreditasiSp->getSekolahId(), $comparison);
        } elseif ($akreditasiSp instanceof PropelObjectCollection) {
            return $this
                ->useAkreditasiSpRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($akreditasiSp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAkreditasiSpRelatedBySekolahId() only accepts arguments of type AkreditasiSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AkreditasiSpRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinAkreditasiSpRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AkreditasiSpRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AkreditasiSpRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the AkreditasiSpRelatedBySekolahId relation AkreditasiSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AkreditasiSpQuery A secondary query class using the current class as primary query
     */
    public function useAkreditasiSpRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAkreditasiSpRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AkreditasiSpRelatedBySekolahId', '\angulex\Model\AkreditasiSpQuery');
    }

    /**
     * Filter the query by a related AkreditasiSp object
     *
     * @param   AkreditasiSp|PropelObjectCollection $akreditasiSp  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAkreditasiSpRelatedBySekolahId($akreditasiSp, $comparison = null)
    {
        if ($akreditasiSp instanceof AkreditasiSp) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $akreditasiSp->getSekolahId(), $comparison);
        } elseif ($akreditasiSp instanceof PropelObjectCollection) {
            return $this
                ->useAkreditasiSpRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($akreditasiSp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAkreditasiSpRelatedBySekolahId() only accepts arguments of type AkreditasiSp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AkreditasiSpRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinAkreditasiSpRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AkreditasiSpRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AkreditasiSpRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the AkreditasiSpRelatedBySekolahId relation AkreditasiSp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AkreditasiSpQuery A secondary query class using the current class as primary query
     */
    public function useAkreditasiSpRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAkreditasiSpRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AkreditasiSpRelatedBySekolahId', '\angulex\Model\AkreditasiSpQuery');
    }

    /**
     * Filter the query by a related GugusSekolah object
     *
     * @param   GugusSekolah|PropelObjectCollection $gugusSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByGugusSekolahRelatedBySekolahId($gugusSekolah, $comparison = null)
    {
        if ($gugusSekolah instanceof GugusSekolah) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $gugusSekolah->getSekolahId(), $comparison);
        } elseif ($gugusSekolah instanceof PropelObjectCollection) {
            return $this
                ->useGugusSekolahRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($gugusSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGugusSekolahRelatedBySekolahId() only accepts arguments of type GugusSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GugusSekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinGugusSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GugusSekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GugusSekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the GugusSekolahRelatedBySekolahId relation GugusSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\GugusSekolahQuery A secondary query class using the current class as primary query
     */
    public function useGugusSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGugusSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GugusSekolahRelatedBySekolahId', '\angulex\Model\GugusSekolahQuery');
    }

    /**
     * Filter the query by a related GugusSekolah object
     *
     * @param   GugusSekolah|PropelObjectCollection $gugusSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByGugusSekolahRelatedBySekolahId($gugusSekolah, $comparison = null)
    {
        if ($gugusSekolah instanceof GugusSekolah) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $gugusSekolah->getSekolahId(), $comparison);
        } elseif ($gugusSekolah instanceof PropelObjectCollection) {
            return $this
                ->useGugusSekolahRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($gugusSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGugusSekolahRelatedBySekolahId() only accepts arguments of type GugusSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GugusSekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinGugusSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GugusSekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GugusSekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the GugusSekolahRelatedBySekolahId relation GugusSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\GugusSekolahQuery A secondary query class using the current class as primary query
     */
    public function useGugusSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGugusSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GugusSekolahRelatedBySekolahId', '\angulex\Model\GugusSekolahQuery');
    }

    /**
     * Filter the query by a related PtkTerdaftar object
     *
     * @param   PtkTerdaftar|PropelObjectCollection $ptkTerdaftar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkTerdaftarRelatedBySekolahId($ptkTerdaftar, $comparison = null)
    {
        if ($ptkTerdaftar instanceof PtkTerdaftar) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $ptkTerdaftar->getSekolahId(), $comparison);
        } elseif ($ptkTerdaftar instanceof PropelObjectCollection) {
            return $this
                ->usePtkTerdaftarRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($ptkTerdaftar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPtkTerdaftarRelatedBySekolahId() only accepts arguments of type PtkTerdaftar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkTerdaftarRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPtkTerdaftarRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkTerdaftarRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkTerdaftarRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PtkTerdaftarRelatedBySekolahId relation PtkTerdaftar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkTerdaftarQuery A secondary query class using the current class as primary query
     */
    public function usePtkTerdaftarRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkTerdaftarRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkTerdaftarRelatedBySekolahId', '\angulex\Model\PtkTerdaftarQuery');
    }

    /**
     * Filter the query by a related PtkTerdaftar object
     *
     * @param   PtkTerdaftar|PropelObjectCollection $ptkTerdaftar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkTerdaftarRelatedBySekolahId($ptkTerdaftar, $comparison = null)
    {
        if ($ptkTerdaftar instanceof PtkTerdaftar) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $ptkTerdaftar->getSekolahId(), $comparison);
        } elseif ($ptkTerdaftar instanceof PropelObjectCollection) {
            return $this
                ->usePtkTerdaftarRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($ptkTerdaftar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPtkTerdaftarRelatedBySekolahId() only accepts arguments of type PtkTerdaftar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkTerdaftarRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPtkTerdaftarRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkTerdaftarRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkTerdaftarRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PtkTerdaftarRelatedBySekolahId relation PtkTerdaftar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkTerdaftarQuery A secondary query class using the current class as primary query
     */
    public function usePtkTerdaftarRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkTerdaftarRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkTerdaftarRelatedBySekolahId', '\angulex\Model\PtkTerdaftarQuery');
    }

    /**
     * Filter the query by a related Blockgrant object
     *
     * @param   Blockgrant|PropelObjectCollection $blockgrant  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBlockgrantRelatedBySekolahId($blockgrant, $comparison = null)
    {
        if ($blockgrant instanceof Blockgrant) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $blockgrant->getSekolahId(), $comparison);
        } elseif ($blockgrant instanceof PropelObjectCollection) {
            return $this
                ->useBlockgrantRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($blockgrant->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBlockgrantRelatedBySekolahId() only accepts arguments of type Blockgrant or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BlockgrantRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinBlockgrantRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BlockgrantRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BlockgrantRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the BlockgrantRelatedBySekolahId relation Blockgrant object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BlockgrantQuery A secondary query class using the current class as primary query
     */
    public function useBlockgrantRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBlockgrantRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BlockgrantRelatedBySekolahId', '\angulex\Model\BlockgrantQuery');
    }

    /**
     * Filter the query by a related Blockgrant object
     *
     * @param   Blockgrant|PropelObjectCollection $blockgrant  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBlockgrantRelatedBySekolahId($blockgrant, $comparison = null)
    {
        if ($blockgrant instanceof Blockgrant) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $blockgrant->getSekolahId(), $comparison);
        } elseif ($blockgrant instanceof PropelObjectCollection) {
            return $this
                ->useBlockgrantRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($blockgrant->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBlockgrantRelatedBySekolahId() only accepts arguments of type Blockgrant or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BlockgrantRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinBlockgrantRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BlockgrantRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BlockgrantRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the BlockgrantRelatedBySekolahId relation Blockgrant object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\BlockgrantQuery A secondary query class using the current class as primary query
     */
    public function useBlockgrantRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBlockgrantRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BlockgrantRelatedBySekolahId', '\angulex\Model\BlockgrantQuery');
    }

    /**
     * Filter the query by a related Mou object
     *
     * @param   Mou|PropelObjectCollection $mou  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMouRelatedBySekolahId($mou, $comparison = null)
    {
        if ($mou instanceof Mou) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $mou->getSekolahId(), $comparison);
        } elseif ($mou instanceof PropelObjectCollection) {
            return $this
                ->useMouRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($mou->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMouRelatedBySekolahId() only accepts arguments of type Mou or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MouRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinMouRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MouRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MouRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the MouRelatedBySekolahId relation Mou object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MouQuery A secondary query class using the current class as primary query
     */
    public function useMouRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMouRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MouRelatedBySekolahId', '\angulex\Model\MouQuery');
    }

    /**
     * Filter the query by a related Mou object
     *
     * @param   Mou|PropelObjectCollection $mou  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMouRelatedBySekolahId($mou, $comparison = null)
    {
        if ($mou instanceof Mou) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $mou->getSekolahId(), $comparison);
        } elseif ($mou instanceof PropelObjectCollection) {
            return $this
                ->useMouRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($mou->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMouRelatedBySekolahId() only accepts arguments of type Mou or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MouRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinMouRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MouRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MouRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the MouRelatedBySekolahId relation Mou object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\MouQuery A secondary query class using the current class as primary query
     */
    public function useMouRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMouRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MouRelatedBySekolahId', '\angulex\Model\MouQuery');
    }

    /**
     * Filter the query by a related UnitUsaha object
     *
     * @param   UnitUsaha|PropelObjectCollection $unitUsaha  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUnitUsahaRelatedBySekolahId($unitUsaha, $comparison = null)
    {
        if ($unitUsaha instanceof UnitUsaha) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $unitUsaha->getSekolahId(), $comparison);
        } elseif ($unitUsaha instanceof PropelObjectCollection) {
            return $this
                ->useUnitUsahaRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($unitUsaha->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUnitUsahaRelatedBySekolahId() only accepts arguments of type UnitUsaha or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnitUsahaRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinUnitUsahaRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnitUsahaRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnitUsahaRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the UnitUsahaRelatedBySekolahId relation UnitUsaha object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\UnitUsahaQuery A secondary query class using the current class as primary query
     */
    public function useUnitUsahaRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUnitUsahaRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnitUsahaRelatedBySekolahId', '\angulex\Model\UnitUsahaQuery');
    }

    /**
     * Filter the query by a related UnitUsaha object
     *
     * @param   UnitUsaha|PropelObjectCollection $unitUsaha  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUnitUsahaRelatedBySekolahId($unitUsaha, $comparison = null)
    {
        if ($unitUsaha instanceof UnitUsaha) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $unitUsaha->getSekolahId(), $comparison);
        } elseif ($unitUsaha instanceof PropelObjectCollection) {
            return $this
                ->useUnitUsahaRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($unitUsaha->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUnitUsahaRelatedBySekolahId() only accepts arguments of type UnitUsaha or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnitUsahaRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinUnitUsahaRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnitUsahaRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnitUsahaRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the UnitUsahaRelatedBySekolahId relation UnitUsaha object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\UnitUsahaQuery A secondary query class using the current class as primary query
     */
    public function useUnitUsahaRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUnitUsahaRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnitUsahaRelatedBySekolahId', '\angulex\Model\UnitUsahaQuery');
    }

    /**
     * Filter the query by a related SyncSession object
     *
     * @param   SyncSession|PropelObjectCollection $syncSession  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySyncSessionRelatedBySekolahId($syncSession, $comparison = null)
    {
        if ($syncSession instanceof SyncSession) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $syncSession->getSekolahId(), $comparison);
        } elseif ($syncSession instanceof PropelObjectCollection) {
            return $this
                ->useSyncSessionRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($syncSession->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySyncSessionRelatedBySekolahId() only accepts arguments of type SyncSession or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SyncSessionRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSyncSessionRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SyncSessionRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SyncSessionRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SyncSessionRelatedBySekolahId relation SyncSession object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SyncSessionQuery A secondary query class using the current class as primary query
     */
    public function useSyncSessionRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSyncSessionRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SyncSessionRelatedBySekolahId', '\angulex\Model\SyncSessionQuery');
    }

    /**
     * Filter the query by a related SyncSession object
     *
     * @param   SyncSession|PropelObjectCollection $syncSession  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySyncSessionRelatedBySekolahId($syncSession, $comparison = null)
    {
        if ($syncSession instanceof SyncSession) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $syncSession->getSekolahId(), $comparison);
        } elseif ($syncSession instanceof PropelObjectCollection) {
            return $this
                ->useSyncSessionRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($syncSession->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySyncSessionRelatedBySekolahId() only accepts arguments of type SyncSession or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SyncSessionRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSyncSessionRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SyncSessionRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SyncSessionRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SyncSessionRelatedBySekolahId relation SyncSession object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SyncSessionQuery A secondary query class using the current class as primary query
     */
    public function useSyncSessionRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSyncSessionRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SyncSessionRelatedBySekolahId', '\angulex\Model\SyncSessionQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByEntrySekolahId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $ptk->getEntrySekolahId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            return $this
                ->usePtkRelatedByEntrySekolahIdQuery()
                ->filterByPrimaryKeys($ptk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPtkRelatedByEntrySekolahId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByEntrySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByEntrySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByEntrySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByEntrySekolahId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByEntrySekolahId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByEntrySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByEntrySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByEntrySekolahId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related Ptk object
     *
     * @param   Ptk|PropelObjectCollection $ptk  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkRelatedByEntrySekolahId($ptk, $comparison = null)
    {
        if ($ptk instanceof Ptk) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $ptk->getEntrySekolahId(), $comparison);
        } elseif ($ptk instanceof PropelObjectCollection) {
            return $this
                ->usePtkRelatedByEntrySekolahIdQuery()
                ->filterByPrimaryKeys($ptk->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPtkRelatedByEntrySekolahId() only accepts arguments of type Ptk or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkRelatedByEntrySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPtkRelatedByEntrySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkRelatedByEntrySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkRelatedByEntrySekolahId');
        }

        return $this;
    }

    /**
     * Use the PtkRelatedByEntrySekolahId relation Ptk object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkQuery A secondary query class using the current class as primary query
     */
    public function usePtkRelatedByEntrySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkRelatedByEntrySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkRelatedByEntrySekolahId', '\angulex\Model\PtkQuery');
    }

    /**
     * Filter the query by a related WtSrcSyncLog object
     *
     * @param   WtSrcSyncLog|PropelObjectCollection $wtSrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtSrcSyncLogRelatedBySekolahId($wtSrcSyncLog, $comparison = null)
    {
        if ($wtSrcSyncLog instanceof WtSrcSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wtSrcSyncLog->getSekolahId(), $comparison);
        } elseif ($wtSrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtSrcSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wtSrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtSrcSyncLogRelatedBySekolahId() only accepts arguments of type WtSrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtSrcSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWtSrcSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtSrcSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtSrcSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WtSrcSyncLogRelatedBySekolahId relation WtSrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtSrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtSrcSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtSrcSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtSrcSyncLogRelatedBySekolahId', '\angulex\Model\WtSrcSyncLogQuery');
    }

    /**
     * Filter the query by a related WtSrcSyncLog object
     *
     * @param   WtSrcSyncLog|PropelObjectCollection $wtSrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtSrcSyncLogRelatedBySekolahId($wtSrcSyncLog, $comparison = null)
    {
        if ($wtSrcSyncLog instanceof WtSrcSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wtSrcSyncLog->getSekolahId(), $comparison);
        } elseif ($wtSrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtSrcSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wtSrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtSrcSyncLogRelatedBySekolahId() only accepts arguments of type WtSrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtSrcSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWtSrcSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtSrcSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtSrcSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WtSrcSyncLogRelatedBySekolahId relation WtSrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtSrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtSrcSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtSrcSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtSrcSyncLogRelatedBySekolahId', '\angulex\Model\WtSrcSyncLogQuery');
    }

    /**
     * Filter the query by a related TableSyncLog object
     *
     * @param   TableSyncLog|PropelObjectCollection $tableSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTableSyncLogRelatedBySekolahId($tableSyncLog, $comparison = null)
    {
        if ($tableSyncLog instanceof TableSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $tableSyncLog->getSekolahId(), $comparison);
        } elseif ($tableSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useTableSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($tableSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTableSyncLogRelatedBySekolahId() only accepts arguments of type TableSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TableSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinTableSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TableSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TableSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the TableSyncLogRelatedBySekolahId relation TableSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TableSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useTableSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTableSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TableSyncLogRelatedBySekolahId', '\angulex\Model\TableSyncLogQuery');
    }

    /**
     * Filter the query by a related TableSyncLog object
     *
     * @param   TableSyncLog|PropelObjectCollection $tableSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTableSyncLogRelatedBySekolahId($tableSyncLog, $comparison = null)
    {
        if ($tableSyncLog instanceof TableSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $tableSyncLog->getSekolahId(), $comparison);
        } elseif ($tableSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useTableSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($tableSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTableSyncLogRelatedBySekolahId() only accepts arguments of type TableSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TableSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinTableSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TableSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TableSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the TableSyncLogRelatedBySekolahId relation TableSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TableSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useTableSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTableSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TableSyncLogRelatedBySekolahId', '\angulex\Model\TableSyncLogQuery');
    }

    /**
     * Filter the query by a related WtDstSyncLog object
     *
     * @param   WtDstSyncLog|PropelObjectCollection $wtDstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtDstSyncLogRelatedBySekolahId($wtDstSyncLog, $comparison = null)
    {
        if ($wtDstSyncLog instanceof WtDstSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wtDstSyncLog->getSekolahId(), $comparison);
        } elseif ($wtDstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtDstSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wtDstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtDstSyncLogRelatedBySekolahId() only accepts arguments of type WtDstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtDstSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWtDstSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtDstSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtDstSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WtDstSyncLogRelatedBySekolahId relation WtDstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtDstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtDstSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtDstSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtDstSyncLogRelatedBySekolahId', '\angulex\Model\WtDstSyncLogQuery');
    }

    /**
     * Filter the query by a related WtDstSyncLog object
     *
     * @param   WtDstSyncLog|PropelObjectCollection $wtDstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWtDstSyncLogRelatedBySekolahId($wtDstSyncLog, $comparison = null)
    {
        if ($wtDstSyncLog instanceof WtDstSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wtDstSyncLog->getSekolahId(), $comparison);
        } elseif ($wtDstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWtDstSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wtDstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWtDstSyncLogRelatedBySekolahId() only accepts arguments of type WtDstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WtDstSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWtDstSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WtDstSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WtDstSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WtDstSyncLogRelatedBySekolahId relation WtDstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WtDstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWtDstSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWtDstSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WtDstSyncLogRelatedBySekolahId', '\angulex\Model\WtDstSyncLogQuery');
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedBySekolahId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $prasarana->getSekolahId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($prasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaRelatedBySekolahId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedBySekolahId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedBySekolahId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedBySekolahId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $prasarana->getSekolahId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($prasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaRelatedBySekolahId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedBySekolahId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedBySekolahId', '\angulex\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedBySekolahId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $rombonganBelajar->getSekolahId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedBySekolahId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedBySekolahId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedBySekolahId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajarRelatedBySekolahId($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $rombonganBelajar->getSekolahId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajarRelatedBySekolahId() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajarRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinRombonganBelajarRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajarRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajarRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajarRelatedBySekolahId relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajarRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajarRelatedBySekolahId', '\angulex\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related SasaranSurvey object
     *
     * @param   SasaranSurvey|PropelObjectCollection $sasaranSurvey  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySasaranSurveyRelatedBySekolahId($sasaranSurvey, $comparison = null)
    {
        if ($sasaranSurvey instanceof SasaranSurvey) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sasaranSurvey->getSekolahId(), $comparison);
        } elseif ($sasaranSurvey instanceof PropelObjectCollection) {
            return $this
                ->useSasaranSurveyRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sasaranSurvey->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySasaranSurveyRelatedBySekolahId() only accepts arguments of type SasaranSurvey or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SasaranSurveyRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSasaranSurveyRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SasaranSurveyRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SasaranSurveyRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SasaranSurveyRelatedBySekolahId relation SasaranSurvey object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SasaranSurveyQuery A secondary query class using the current class as primary query
     */
    public function useSasaranSurveyRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSasaranSurveyRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SasaranSurveyRelatedBySekolahId', '\angulex\Model\SasaranSurveyQuery');
    }

    /**
     * Filter the query by a related SasaranSurvey object
     *
     * @param   SasaranSurvey|PropelObjectCollection $sasaranSurvey  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySasaranSurveyRelatedBySekolahId($sasaranSurvey, $comparison = null)
    {
        if ($sasaranSurvey instanceof SasaranSurvey) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sasaranSurvey->getSekolahId(), $comparison);
        } elseif ($sasaranSurvey instanceof PropelObjectCollection) {
            return $this
                ->useSasaranSurveyRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sasaranSurvey->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySasaranSurveyRelatedBySekolahId() only accepts arguments of type SasaranSurvey or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SasaranSurveyRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSasaranSurveyRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SasaranSurveyRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SasaranSurveyRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SasaranSurveyRelatedBySekolahId relation SasaranSurvey object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SasaranSurveyQuery A secondary query class using the current class as primary query
     */
    public function useSasaranSurveyRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSasaranSurveyRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SasaranSurveyRelatedBySekolahId', '\angulex\Model\SasaranSurveyQuery');
    }

    /**
     * Filter the query by a related WsrcSyncLog object
     *
     * @param   WsrcSyncLog|PropelObjectCollection $wsrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWsrcSyncLogRelatedBySekolahId($wsrcSyncLog, $comparison = null)
    {
        if ($wsrcSyncLog instanceof WsrcSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wsrcSyncLog->getSekolahId(), $comparison);
        } elseif ($wsrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWsrcSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wsrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWsrcSyncLogRelatedBySekolahId() only accepts arguments of type WsrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WsrcSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWsrcSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WsrcSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WsrcSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WsrcSyncLogRelatedBySekolahId relation WsrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WsrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWsrcSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWsrcSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WsrcSyncLogRelatedBySekolahId', '\angulex\Model\WsrcSyncLogQuery');
    }

    /**
     * Filter the query by a related WsrcSyncLog object
     *
     * @param   WsrcSyncLog|PropelObjectCollection $wsrcSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWsrcSyncLogRelatedBySekolahId($wsrcSyncLog, $comparison = null)
    {
        if ($wsrcSyncLog instanceof WsrcSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wsrcSyncLog->getSekolahId(), $comparison);
        } elseif ($wsrcSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWsrcSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wsrcSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWsrcSyncLogRelatedBySekolahId() only accepts arguments of type WsrcSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WsrcSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWsrcSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WsrcSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WsrcSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WsrcSyncLogRelatedBySekolahId relation WsrcSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WsrcSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWsrcSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWsrcSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WsrcSyncLogRelatedBySekolahId', '\angulex\Model\WsrcSyncLogQuery');
    }

    /**
     * Filter the query by a related SasaranPengawasan object
     *
     * @param   SasaranPengawasan|PropelObjectCollection $sasaranPengawasan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySasaranPengawasanRelatedBySekolahId($sasaranPengawasan, $comparison = null)
    {
        if ($sasaranPengawasan instanceof SasaranPengawasan) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sasaranPengawasan->getSekolahId(), $comparison);
        } elseif ($sasaranPengawasan instanceof PropelObjectCollection) {
            return $this
                ->useSasaranPengawasanRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sasaranPengawasan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySasaranPengawasanRelatedBySekolahId() only accepts arguments of type SasaranPengawasan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SasaranPengawasanRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSasaranPengawasanRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SasaranPengawasanRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SasaranPengawasanRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SasaranPengawasanRelatedBySekolahId relation SasaranPengawasan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SasaranPengawasanQuery A secondary query class using the current class as primary query
     */
    public function useSasaranPengawasanRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSasaranPengawasanRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SasaranPengawasanRelatedBySekolahId', '\angulex\Model\SasaranPengawasanQuery');
    }

    /**
     * Filter the query by a related SasaranPengawasan object
     *
     * @param   SasaranPengawasan|PropelObjectCollection $sasaranPengawasan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySasaranPengawasanRelatedBySekolahId($sasaranPengawasan, $comparison = null)
    {
        if ($sasaranPengawasan instanceof SasaranPengawasan) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sasaranPengawasan->getSekolahId(), $comparison);
        } elseif ($sasaranPengawasan instanceof PropelObjectCollection) {
            return $this
                ->useSasaranPengawasanRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($sasaranPengawasan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySasaranPengawasanRelatedBySekolahId() only accepts arguments of type SasaranPengawasan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SasaranPengawasanRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSasaranPengawasanRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SasaranPengawasanRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SasaranPengawasanRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SasaranPengawasanRelatedBySekolahId relation SasaranPengawasan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SasaranPengawasanQuery A secondary query class using the current class as primary query
     */
    public function useSasaranPengawasanRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSasaranPengawasanRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SasaranPengawasanRelatedBySekolahId', '\angulex\Model\SasaranPengawasanQuery');
    }

    /**
     * Filter the query by a related WdstSyncLog object
     *
     * @param   WdstSyncLog|PropelObjectCollection $wdstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWdstSyncLogRelatedBySekolahId($wdstSyncLog, $comparison = null)
    {
        if ($wdstSyncLog instanceof WdstSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wdstSyncLog->getSekolahId(), $comparison);
        } elseif ($wdstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWdstSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wdstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWdstSyncLogRelatedBySekolahId() only accepts arguments of type WdstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WdstSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWdstSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WdstSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WdstSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WdstSyncLogRelatedBySekolahId relation WdstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WdstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWdstSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWdstSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WdstSyncLogRelatedBySekolahId', '\angulex\Model\WdstSyncLogQuery');
    }

    /**
     * Filter the query by a related WdstSyncLog object
     *
     * @param   WdstSyncLog|PropelObjectCollection $wdstSyncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWdstSyncLogRelatedBySekolahId($wdstSyncLog, $comparison = null)
    {
        if ($wdstSyncLog instanceof WdstSyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wdstSyncLog->getSekolahId(), $comparison);
        } elseif ($wdstSyncLog instanceof PropelObjectCollection) {
            return $this
                ->useWdstSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wdstSyncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWdstSyncLogRelatedBySekolahId() only accepts arguments of type WdstSyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WdstSyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWdstSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WdstSyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WdstSyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WdstSyncLogRelatedBySekolahId relation WdstSyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WdstSyncLogQuery A secondary query class using the current class as primary query
     */
    public function useWdstSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWdstSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WdstSyncLogRelatedBySekolahId', '\angulex\Model\WdstSyncLogQuery');
    }

    /**
     * Filter the query by a related SyncLog object
     *
     * @param   SyncLog|PropelObjectCollection $syncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySyncLogRelatedBySekolahId($syncLog, $comparison = null)
    {
        if ($syncLog instanceof SyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $syncLog->getSekolahId(), $comparison);
        } elseif ($syncLog instanceof PropelObjectCollection) {
            return $this
                ->useSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($syncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySyncLogRelatedBySekolahId() only accepts arguments of type SyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SyncLogRelatedBySekolahId relation SyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SyncLogQuery A secondary query class using the current class as primary query
     */
    public function useSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SyncLogRelatedBySekolahId', '\angulex\Model\SyncLogQuery');
    }

    /**
     * Filter the query by a related SyncLog object
     *
     * @param   SyncLog|PropelObjectCollection $syncLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySyncLogRelatedBySekolahId($syncLog, $comparison = null)
    {
        if ($syncLog instanceof SyncLog) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $syncLog->getSekolahId(), $comparison);
        } elseif ($syncLog instanceof PropelObjectCollection) {
            return $this
                ->useSyncLogRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($syncLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySyncLogRelatedBySekolahId() only accepts arguments of type SyncLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SyncLogRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinSyncLogRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SyncLogRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SyncLogRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the SyncLogRelatedBySekolahId relation SyncLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\SyncLogQuery A secondary query class using the current class as primary query
     */
    public function useSyncLogRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSyncLogRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SyncLogRelatedBySekolahId', '\angulex\Model\SyncLogQuery');
    }

    /**
     * Filter the query by a related WPendingJob object
     *
     * @param   WPendingJob|PropelObjectCollection $wPendingJob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWPendingJobRelatedBySekolahId($wPendingJob, $comparison = null)
    {
        if ($wPendingJob instanceof WPendingJob) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wPendingJob->getSekolahId(), $comparison);
        } elseif ($wPendingJob instanceof PropelObjectCollection) {
            return $this
                ->useWPendingJobRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wPendingJob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWPendingJobRelatedBySekolahId() only accepts arguments of type WPendingJob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WPendingJobRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWPendingJobRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WPendingJobRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WPendingJobRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WPendingJobRelatedBySekolahId relation WPendingJob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WPendingJobQuery A secondary query class using the current class as primary query
     */
    public function useWPendingJobRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWPendingJobRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WPendingJobRelatedBySekolahId', '\angulex\Model\WPendingJobQuery');
    }

    /**
     * Filter the query by a related WPendingJob object
     *
     * @param   WPendingJob|PropelObjectCollection $wPendingJob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByWPendingJobRelatedBySekolahId($wPendingJob, $comparison = null)
    {
        if ($wPendingJob instanceof WPendingJob) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $wPendingJob->getSekolahId(), $comparison);
        } elseif ($wPendingJob instanceof PropelObjectCollection) {
            return $this
                ->useWPendingJobRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($wPendingJob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWPendingJobRelatedBySekolahId() only accepts arguments of type WPendingJob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WPendingJobRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinWPendingJobRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WPendingJobRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WPendingJobRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the WPendingJobRelatedBySekolahId relation WPendingJob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\WPendingJobQuery A secondary query class using the current class as primary query
     */
    public function useWPendingJobRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWPendingJobRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WPendingJobRelatedBySekolahId', '\angulex\Model\WPendingJobQuery');
    }

    /**
     * Filter the query by a related ProgramInklusi object
     *
     * @param   ProgramInklusi|PropelObjectCollection $programInklusi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByProgramInklusiRelatedBySekolahId($programInklusi, $comparison = null)
    {
        if ($programInklusi instanceof ProgramInklusi) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $programInklusi->getSekolahId(), $comparison);
        } elseif ($programInklusi instanceof PropelObjectCollection) {
            return $this
                ->useProgramInklusiRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($programInklusi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProgramInklusiRelatedBySekolahId() only accepts arguments of type ProgramInklusi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProgramInklusiRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinProgramInklusiRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProgramInklusiRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProgramInklusiRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the ProgramInklusiRelatedBySekolahId relation ProgramInklusi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\ProgramInklusiQuery A secondary query class using the current class as primary query
     */
    public function useProgramInklusiRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProgramInklusiRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProgramInklusiRelatedBySekolahId', '\angulex\Model\ProgramInklusiQuery');
    }

    /**
     * Filter the query by a related ProgramInklusi object
     *
     * @param   ProgramInklusi|PropelObjectCollection $programInklusi  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByProgramInklusiRelatedBySekolahId($programInklusi, $comparison = null)
    {
        if ($programInklusi instanceof ProgramInklusi) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $programInklusi->getSekolahId(), $comparison);
        } elseif ($programInklusi instanceof PropelObjectCollection) {
            return $this
                ->useProgramInklusiRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($programInklusi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProgramInklusiRelatedBySekolahId() only accepts arguments of type ProgramInklusi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProgramInklusiRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinProgramInklusiRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProgramInklusiRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProgramInklusiRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the ProgramInklusiRelatedBySekolahId relation ProgramInklusi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\ProgramInklusiQuery A secondary query class using the current class as primary query
     */
    public function useProgramInklusiRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProgramInklusiRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProgramInklusiRelatedBySekolahId', '\angulex\Model\ProgramInklusiQuery');
    }

    /**
     * Filter the query by a related PtkBaru object
     *
     * @param   PtkBaru|PropelObjectCollection $ptkBaru  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkBaruRelatedBySekolahId($ptkBaru, $comparison = null)
    {
        if ($ptkBaru instanceof PtkBaru) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $ptkBaru->getSekolahId(), $comparison);
        } elseif ($ptkBaru instanceof PropelObjectCollection) {
            return $this
                ->usePtkBaruRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($ptkBaru->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPtkBaruRelatedBySekolahId() only accepts arguments of type PtkBaru or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkBaruRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPtkBaruRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkBaruRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkBaruRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PtkBaruRelatedBySekolahId relation PtkBaru object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkBaruQuery A secondary query class using the current class as primary query
     */
    public function usePtkBaruRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkBaruRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkBaruRelatedBySekolahId', '\angulex\Model\PtkBaruQuery');
    }

    /**
     * Filter the query by a related PtkBaru object
     *
     * @param   PtkBaru|PropelObjectCollection $ptkBaru  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPtkBaruRelatedBySekolahId($ptkBaru, $comparison = null)
    {
        if ($ptkBaru instanceof PtkBaru) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $ptkBaru->getSekolahId(), $comparison);
        } elseif ($ptkBaru instanceof PropelObjectCollection) {
            return $this
                ->usePtkBaruRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($ptkBaru->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPtkBaruRelatedBySekolahId() only accepts arguments of type PtkBaru or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PtkBaruRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinPtkBaruRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PtkBaruRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PtkBaruRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the PtkBaruRelatedBySekolahId relation PtkBaru object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\PtkBaruQuery A secondary query class using the current class as primary query
     */
    public function usePtkBaruRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPtkBaruRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PtkBaruRelatedBySekolahId', '\angulex\Model\PtkBaruQuery');
    }

    /**
     * Filter the query by a related VldSekolah object
     *
     * @param   VldSekolah|PropelObjectCollection $vldSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldSekolahRelatedBySekolahId($vldSekolah, $comparison = null)
    {
        if ($vldSekolah instanceof VldSekolah) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $vldSekolah->getSekolahId(), $comparison);
        } elseif ($vldSekolah instanceof PropelObjectCollection) {
            return $this
                ->useVldSekolahRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($vldSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldSekolahRelatedBySekolahId() only accepts arguments of type VldSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldSekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinVldSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldSekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldSekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the VldSekolahRelatedBySekolahId relation VldSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldSekolahQuery A secondary query class using the current class as primary query
     */
    public function useVldSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldSekolahRelatedBySekolahId', '\angulex\Model\VldSekolahQuery');
    }

    /**
     * Filter the query by a related VldSekolah object
     *
     * @param   VldSekolah|PropelObjectCollection $vldSekolah  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldSekolahRelatedBySekolahId($vldSekolah, $comparison = null)
    {
        if ($vldSekolah instanceof VldSekolah) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $vldSekolah->getSekolahId(), $comparison);
        } elseif ($vldSekolah instanceof PropelObjectCollection) {
            return $this
                ->useVldSekolahRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($vldSekolah->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldSekolahRelatedBySekolahId() only accepts arguments of type VldSekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldSekolahRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinVldSekolahRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldSekolahRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldSekolahRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the VldSekolahRelatedBySekolahId relation VldSekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\VldSekolahQuery A secondary query class using the current class as primary query
     */
    public function useVldSekolahRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldSekolahRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldSekolahRelatedBySekolahId', '\angulex\Model\VldSekolahQuery');
    }

    /**
     * Filter the query by a related AnggotaGugus object
     *
     * @param   AnggotaGugus|PropelObjectCollection $anggotaGugus  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAnggotaGugusRelatedBySekolahId($anggotaGugus, $comparison = null)
    {
        if ($anggotaGugus instanceof AnggotaGugus) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $anggotaGugus->getSekolahId(), $comparison);
        } elseif ($anggotaGugus instanceof PropelObjectCollection) {
            return $this
                ->useAnggotaGugusRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($anggotaGugus->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAnggotaGugusRelatedBySekolahId() only accepts arguments of type AnggotaGugus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AnggotaGugusRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinAnggotaGugusRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AnggotaGugusRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AnggotaGugusRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the AnggotaGugusRelatedBySekolahId relation AnggotaGugus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AnggotaGugusQuery A secondary query class using the current class as primary query
     */
    public function useAnggotaGugusRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAnggotaGugusRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AnggotaGugusRelatedBySekolahId', '\angulex\Model\AnggotaGugusQuery');
    }

    /**
     * Filter the query by a related AnggotaGugus object
     *
     * @param   AnggotaGugus|PropelObjectCollection $anggotaGugus  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAnggotaGugusRelatedBySekolahId($anggotaGugus, $comparison = null)
    {
        if ($anggotaGugus instanceof AnggotaGugus) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $anggotaGugus->getSekolahId(), $comparison);
        } elseif ($anggotaGugus instanceof PropelObjectCollection) {
            return $this
                ->useAnggotaGugusRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($anggotaGugus->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAnggotaGugusRelatedBySekolahId() only accepts arguments of type AnggotaGugus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AnggotaGugusRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinAnggotaGugusRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AnggotaGugusRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AnggotaGugusRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the AnggotaGugusRelatedBySekolahId relation AnggotaGugus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\AnggotaGugusQuery A secondary query class using the current class as primary query
     */
    public function useAnggotaGugusRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAnggotaGugusRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AnggotaGugusRelatedBySekolahId', '\angulex\Model\AnggotaGugusQuery');
    }

    /**
     * Filter the query by a related TugasTambahan object
     *
     * @param   TugasTambahan|PropelObjectCollection $tugasTambahan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTugasTambahanRelatedBySekolahId($tugasTambahan, $comparison = null)
    {
        if ($tugasTambahan instanceof TugasTambahan) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $tugasTambahan->getSekolahId(), $comparison);
        } elseif ($tugasTambahan instanceof PropelObjectCollection) {
            return $this
                ->useTugasTambahanRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($tugasTambahan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTugasTambahanRelatedBySekolahId() only accepts arguments of type TugasTambahan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TugasTambahanRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinTugasTambahanRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TugasTambahanRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TugasTambahanRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the TugasTambahanRelatedBySekolahId relation TugasTambahan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TugasTambahanQuery A secondary query class using the current class as primary query
     */
    public function useTugasTambahanRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTugasTambahanRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TugasTambahanRelatedBySekolahId', '\angulex\Model\TugasTambahanQuery');
    }

    /**
     * Filter the query by a related TugasTambahan object
     *
     * @param   TugasTambahan|PropelObjectCollection $tugasTambahan  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SekolahQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTugasTambahanRelatedBySekolahId($tugasTambahan, $comparison = null)
    {
        if ($tugasTambahan instanceof TugasTambahan) {
            return $this
                ->addUsingAlias(SekolahPeer::SEKOLAH_ID, $tugasTambahan->getSekolahId(), $comparison);
        } elseif ($tugasTambahan instanceof PropelObjectCollection) {
            return $this
                ->useTugasTambahanRelatedBySekolahIdQuery()
                ->filterByPrimaryKeys($tugasTambahan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTugasTambahanRelatedBySekolahId() only accepts arguments of type TugasTambahan or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TugasTambahanRelatedBySekolahId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function joinTugasTambahanRelatedBySekolahId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TugasTambahanRelatedBySekolahId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TugasTambahanRelatedBySekolahId');
        }

        return $this;
    }

    /**
     * Use the TugasTambahanRelatedBySekolahId relation TugasTambahan object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \angulex\Model\TugasTambahanQuery A secondary query class using the current class as primary query
     */
    public function useTugasTambahanRelatedBySekolahIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTugasTambahanRelatedBySekolahId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TugasTambahanRelatedBySekolahId', '\angulex\Model\TugasTambahanQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Sekolah $sekolah Object to remove from the list of results
     *
     * @return SekolahQuery The current query, for fluid interface
     */
    public function prune($sekolah = null)
    {
        if ($sekolah) {
            $this->addUsingAlias(SekolahPeer::SEKOLAH_ID, $sekolah->getSekolahId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
