<?php

namespace angulex\Model;

use angulex\Model\om\BaseGelarAkademikQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'ref.gelar_akademik' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.angulex.Model
 */
class GelarAkademikQuery extends BaseGelarAkademikQuery
{
}
