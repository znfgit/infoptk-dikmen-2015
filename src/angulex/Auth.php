<?php
namespace angulex;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Auth
{
	function login(Request $request, Application $app){
		
		$fetchArr = array();
		$arr = array();
		
		$username = $request->get('username');
		$password = $request->get('password');

		// return $username." ".$password;die;
		$sql = "select 
					lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '{$password}'), 2) ) as iPassword,
					* 
				from 
					pengguna 
				where 
					username = '{$username}' 
				AND Soft_delete = 0
				AND aktif = 1";

		// return $sql;die;

		$fetch = getDataBySql($sql);

		foreach ($fetch as $f) {
			foreach ($f as $fc => $v) {
				$arr[$fc] = $v;

			}

			array_push($fetchArr, $arr);
		}

		// return json_encode($fetchArr);

		if(sizeof($fetchArr) > 0){
			//case 1: user ketemu menggunakan nuptk

			if($arr['iPassword'] == $arr['password']){
				// case 1.1: passwordnya oke

				$app['session']->set('username', $username);
				$app['session']->set('peran_id', $fetch[0]['peran_id']);
				$app['session']->set('ptk_id', $fetch[0]['pengguna_id']);
				$app['session']->set('kode_wilayah', $fetch[0]['kode_wilayah']);
				$app['session']->set('sekolah_id', $fetch[0]['sekolah_id']);
				$app['session']->set('nama', $fetch[0]['nama']);

				// level wilayah stuff

				$sql_c1 = "select top 1 * from ref.mst_wilayah where kode_wilayah = ".$fetch[0]['kode_wilayah'];
				$fetch_c1 = getDataBySql($sql_c1);

				$app['session']->set('id_level_wilayah', $fetch_c1[0]['id_level_wilayah']);
				
				// end of level wilayah stuff


				$return['success'] = true;
				$return['message'] = "Login berhasil";

			}else{
				// case 1.2: password nggak oke

				$return['success'] = false;
				$return['message'] = "<b>Password</b> yg anda masukan salah";

			}
		
		}else{
			// case 2: user nggak ketemu mengunakan nuptk. coba cari menggunakan nik

			$sql = "select 
						lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '{$password}'), 2) ) as iPassword,
						* 
					from 
						pengguna 
					where 
						nama = '{$username}' 
					AND Soft_delete = 0
					AND aktif = 1
					AND nama != '-'";

			$fetch = getDataBySql($sql);

			foreach ($fetch as $f) {
				foreach ($f as $fc => $v) {
					$arr[$fc] = $v;

				}

				array_push($fetchArr, $arr);
			}

			if(sizeof($fetchArr) > 0){
				// case 2.1: user ketemu menggunakan nik

				if($arr['iPassword'] == $arr['password']){
					// case 2.1.1: passwordnya oke
					
					$app['session']->set('username', $username);
					$app['session']->set('peran_id', $fetch[0]['peran_id']);
					$app['session']->set('ptk_id', $fetch[0]['pengguna_id']);
					$app['session']->set('kode_wilayah', $fetch[0]['kode_wilayah']);
					$app['session']->set('sekolah_id', $fetch[0]['sekolah_id']);
					$app['session']->set('nama', $fetch[0]['nama']);

					// level wilayah stuff

					$sql_c1 = "select top 1 * from ref.mst_wilayah where kode_wilayah = ".$fetch[0]['kode_wilayah'];
					$fetch_c1 = getDataBySql($sql_c1);

					$app['session']->set('id_level_wilayah', $fetch_c1[0]['id_level_wilayah']);
					
					// end of level wilayah stuff


					$return['success'] = true;
					$return['message'] = "Login berhasil";					

				}else{
					// case 2.1.2: password nggak oke

					$return['success'] = false;
					$return['message'] = "<b>Password</b> yg anda masukan salah";

				}


			}else{
				// case 2.2: user nggak ketemu menggunakan nik. lempar keluar

				$return['success'] = false;
				$return['message'] = "<b>Username</b> yg anda masukan tidak ditemukan di dalam sistem";

			}

		}

		return json_encode($return);	

	}

	function session(Request $request, Application $app){
		
		if(WORK_OFFLINE == true){
			return '{"session":true}';
		}else{
			if($app['session']->get('username')){
				$array = array();

				$array['session'] = true;
				$array['username'] = $app['session']->get('username');
				$array['nama'] = $app['session']->get('nama');
				$array['peran_id'] = $app['session']->get('peran_id');
				$array['ptk_id'] = $app['session']->get('ptk_id');
				$array['id_level_wilayah'] = $app['session']->get('id_level_wilayah');
				$array['kode_wilayah'] = $app['session']->get('kode_wilayah');
				$array['sekolah_id'] = $app['session']->get('sekolah_id');

				$sql = "select * from ref.peran where peran_id = ".$array['peran_id'];

				$fetch = getDataBySql($sql);
								
				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
				}
				
				$array['peran_id_str'] = $arr['nama'];

				$sql = "select * from ref.level_wilayah where id_level_wilayah = ".$array['id_level_wilayah'];

				$fetch = getDataBySql($sql);
								
				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
				}

				$array['id_level_wilayah_str'] = $arr['level_wilayah'];

				$sql = "select * from ref.mst_wilayah where kode_wilayah = ".$array['kode_wilayah'];

				$fetch = getDataBySql($sql);
								
				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
				}

				$array['kode_wilayah_str'] = $arr['nama'];
				$array['mst_kode_wilayah'] = $arr['mst_kode_wilayah'];

				$sql = "select * from ref.semester where semester_id = ".SEMESTER_BERJALAN;

				$fetch = getDataBySql($sql);
								
				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
				}

				$array['semester_id_str'] = $arr['nama'];
				$array['semester_id'] = SEMESTER_BERJALAN;
				$array['tahun_ajaran_id'] = TA_BERJALAN;

				return json_encode($array);

			}else{
				$array = array();

				$array['session'] = false;

				return json_encode($array);
			}
		}
	}

	function logout(Request $request, Application $app){
		$app['session']->clear();
		return $app->redirect('.');
		// return var_dump($app['session']);
	}
}
?>